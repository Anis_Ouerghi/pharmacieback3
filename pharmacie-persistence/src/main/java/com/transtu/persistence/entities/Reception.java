package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Reception implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6095557810339205613L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	private Date dateReception;
	@ManyToOne
	@JoinColumn(name = "day_id")
	private Day day;
	private String numReception;
	private Long typeId; //Additif=2 or Reception Ordinaire=1 
	private String numExternalDelivery;
	private Date dateLiv;
	private String numInvoice;
	private Date dateInvoice;

	@ManyToOne
	@JoinColumn(name = "utilistaure_id")
	private Utilisateur utilisateur;
	
	@ManyToOne
	@JoinColumn(name = "emplacement_id")
	private Location location; 
	
	@OneToMany(mappedBy="reception")
	@JsonManagedReference
	List<ReceptionLine> receptionlines;
	
	@ManyToOne
	@JoinColumn(name = "state_id")
	private State state;
	
	@ManyToOne
	@JoinColumn(name = "externalOrder_id")
	@JsonBackReference
	private ExternalOrder externalOrder;
	
	
	
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Day getDay() {
		return day;
	}
	public void setDay(Day day) {
		this.day = day;
	}
	public String getNumReception() {
		return numReception;
	}
	public void setNumReception(String numReception) {
		this.numReception = numReception;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public List<ReceptionLine> getReceptionlines() {
		return receptionlines;
	}
	public void setReceptionlines(List<ReceptionLine> receptionlines) {
		this.receptionlines = receptionlines;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	
	public ExternalOrder getExternalOrder() {
		return externalOrder;
	}
	public void setExternalOrder(ExternalOrder externalOrder) {
		this.externalOrder = externalOrder;
	}
	public Date getDateReception() {
		return dateReception;
	}
	public void setDateReception(Date dateReception) {
		this.dateReception = dateReception;
	}
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	public String getNumExternalDelivery() {
		return numExternalDelivery;
	}
	public void setNumExternalDelivery(String numExternalDelivery) {
		this.numExternalDelivery = numExternalDelivery;
	}
	public Date getDateLiv() {
		return dateLiv;
	}
	public void setDateLiv(Date dateLiv) {
		this.dateLiv = dateLiv;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public String getNumInvoice() {
		return numInvoice;
	}
	public void setNumInvoice(String numInvoice) {
		this.numInvoice = numInvoice;
	}
	public Date getDateInvoice() {
		return dateInvoice;
	}
	public void setDateInvoice(Date dateInvoice) {
		this.dateInvoice = dateInvoice;
	}
	
	

	
//	@ManyToOne
//	@JoinColumn(name = "externalOrder_id")
//	@JsonBackReference
//	private ExternalOrder externalOrder;

	
}
