package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class SequenceDelivery implements Serializable{

	private static final long serialVersionUID = 4112515174502404408L;

	@Id
	@GeneratedValue
	private Integer id;

	private Integer year;
	private Integer num;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public SequenceDelivery() {
		super();
	}
	public SequenceDelivery(Integer year, Integer num) {
		super();
		this.year = year;
		this.num = num;
	}
	
	
	
	
}
