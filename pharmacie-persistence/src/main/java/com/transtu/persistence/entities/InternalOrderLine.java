package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class InternalOrderLine implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6549335273400119645L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "internalOrder_id")
	@JsonBackReference
	private InternalOrder internalOrder;
	
	private double orderQte = 0;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public InternalOrder getInternalOrder() {
		return internalOrder;
	}

	public void setInternalOrder(InternalOrder internalOrder) {
		this.internalOrder = internalOrder;
	}

	public double getOrderQte() {
		return orderQte;
	}

	public void setOrderQte(double orderQte) {
		this.orderQte = orderQte;
	}

	public InternalOrderLine() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InternalOrderLine(Long id, Product product, InternalOrder internalOrder, double orderQte) {
		super();
		this.id = id;
		this.product = product;
		this.internalOrder = internalOrder;
		this.orderQte = orderQte;
	}

	public InternalOrderLine(Product product, InternalOrder internalOrder, double orderQte) {
		super();
		this.product = product;
		this.internalOrder = internalOrder;
		this.orderQte = orderQte;
	}
	
	
	
}
