package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
public class InternalOrder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2242138314723505987L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	private Date dateOrder;
	private String numOrder;
	@ManyToOne
	@JoinColumn(name = "emplacement_id")
	private Location location; 
	@ManyToOne
	@JoinColumn(name = "day_id")
	private Day day;
	
	@ManyToOne
	@JoinColumn(name = "type_id")
	private Type type;
	
	
	
	
	@OneToMany(mappedBy="internalOrder")
	@JsonManagedReference
	List<InternalOrderLine> internalOrderLine;
	@ManyToOne
	@JoinColumn(name = "state_id")
	private State state; 
	
	public InternalOrder() {
		super();
		// TODO Auto-generated constructor stub
	}
	public InternalOrder(Long id, Date dateOrder, String numOrder, Location location,
			List<InternalOrderLine> internalOrderLine, State state) {
		super();
		Id = id;
		this.dateOrder = dateOrder;
		this.numOrder = numOrder;
		this.location = location;
		this.internalOrderLine = internalOrderLine;
		this.state = state;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Date getDateOrder() {
		return dateOrder;
	}
	public void setDateOrder(Date dateOrder) {
		this.dateOrder = dateOrder;
	}
	public String getNumOrder() {
		return numOrder;
	}
	public void setNumOrder(String numOrder) {
		this.numOrder = numOrder;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public List<InternalOrderLine> getInternalOrderLine() {
		return internalOrderLine;
	}
	public void setInternalOrderLine(List<InternalOrderLine> internalOrderLine) {
		this.internalOrderLine = internalOrderLine;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public Day getDay() {
		return day;
	}
	public void setDay(Day day) {
		this.day = day;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public InternalOrder(Long id, Date dateOrder, String numOrder, Location location, Day day, Type type,
			List<InternalOrderLine> internalOrderLine, State state) {
		super();
		Id = id;
		this.dateOrder = dateOrder;
		this.numOrder = numOrder;
		this.location = location;
		this.day = day;
		this.type = type;
		this.internalOrderLine = internalOrderLine;
		this.state = state;
	}
	
		
	
}
