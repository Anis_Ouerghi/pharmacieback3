package com.transtu.persistence.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class SequenceOrder {

	
	@Id
	@GeneratedValue
	private Integer id;

	private Integer year;
	private Integer num;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public SequenceOrder() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SequenceOrder(Integer id, Integer year, Integer num) {
		super();
		this.id = id;
		this.year = year;
		this.num = num;
	}
	public SequenceOrder(Integer year, Integer num) {
		super();
		this.year = year;
		this.num = num;
	}
	
	
	
	
	
}
