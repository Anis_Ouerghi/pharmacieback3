package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import javax.persistence.Table;
@Entity @Table(name = "Day")
public class Day implements Serializable {


	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
//	@Id 
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQUENCE1") 
//	@SequenceGenerator(name="SEQUENCE1", sequenceName="SEQUENCE1", allocationSize=1) 
	private Long id ; 
	@NotNull
	private boolean state; 
	@NotNull
    @Column(name = "DATEDAY") 
    //@Column(name = "Date")
	private Date date ;
	
	@ManyToOne
	@JoinColumn(name = "type_id")
	private DayType dayType;
	
	@OneToMany(mappedBy="day")
	//@JsonManagedReference
	List<UserDay> userDays;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public DayType getDayType() {
		return dayType;
	}

	public void setDayType(DayType dayType) {
		this.dayType = dayType;
	}
	


}
