package com.transtu.persistence.entities;



import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class Role implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long Id;
	private String role;
	private String discription;
	@JsonIgnore
	@ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
	private Set<Utilisateur> user;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public Role(Long id, String role, String discription) {
		super();
		Id = id;
		this.role = role;
		this.discription = discription;
	}
	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Set<Utilisateur> getUser() {
		return user;
	}
	public void setUser(Set<Utilisateur> user) {
		this.user = user;
	}
	
	
	
	
	

	
}
