package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;



@Entity
public class UserDay implements Serializable{
	
	private static final long serialVersionUID = 4483314117670673415L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "utilisateur_id")
	@JsonBackReference
	private Utilisateur utilisateur;
	
	@ManyToOne
	@JoinColumn(name = "day_id")
	//@JsonBackReference
	private Day day;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public UserDay(long id, Utilisateur utilisateur, Day day) {
		super();
		this.id = id;
		this.utilisateur = utilisateur;
		this.day = day;
	}

	public UserDay() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	

}
