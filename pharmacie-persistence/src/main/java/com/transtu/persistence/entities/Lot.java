package com.transtu.persistence.entities;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;



@Entity
@Table(name = "Lot")
public class Lot  implements Serializable {
	
		private static final long serialVersionUID = 1L;
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Long id ; 
		private Date dateRefusal ;
		@NotNull
		private String label;
		@ManyToOne
		@JsonIgnore
		@JoinColumn(name = "produit_id")
		private Product product;
		private boolean state;
		@OneToMany(mappedBy="lot")
		@JsonManagedReference
		//@JsonBackReference
		//@JsonIgnore
		private List<Stocklot> Stocklots;
	

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Date getDateRefusal() {
			return dateRefusal;
		}

		public void setDateRefusal(Date dateRefusal) {
			this.dateRefusal = dateRefusal;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public Product getProduct() {
			return product;
		}

		public void setProduct(Product product) {
			this.product = product;
		}

		public List<Stocklot> getStocklots() {
			return Stocklots;
		}

		public void setStocklots(List<Stocklot> stocklots) {
			Stocklots = stocklots;
		}

		public boolean isState() {
			return state;
		}

		public void setState(boolean state) {
			this.state = state;
		}

		
		
		
		
		
		

	

}
