package com.transtu.persistence.entities;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class InternalDeliveryLine implements Serializable {

	private static final long serialVersionUID = 1L;
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 
	private double qte ; 
	private double missingqte;
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product ;
	
	@ManyToOne
	@JoinColumn(name = "lot_id")
	private Lot lot ;
	
	@ManyToOne
	@JoinColumn(name = "internal_delivery_id")
	@JsonBackReference
	private InternalDelivery internalDelivery ;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getQte() {
		return qte;
	}

	public void setQte(double qte) {
		this.qte = qte;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public InternalDeliveryLine() {
		super();
	}

	public InternalDelivery getInternalDelivery() {
		return internalDelivery;
	}

	public void setInternalDelivery(InternalDelivery internalDelivery) {
		this.internalDelivery = internalDelivery;
	}

	public Lot getLot() {
		return lot;
	}

	public void setLot(Lot lot) {
		this.lot = lot;
	}

	public double getMissingqte() {
		return missingqte;
	}

	public void setMissingqte(double missingqte) {
		this.missingqte = missingqte;
	}

	
	
	
	
	
	
}
