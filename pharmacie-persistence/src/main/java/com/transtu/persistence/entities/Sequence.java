package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreType;


@Entity
@JsonIgnoreType
public class Sequence  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
//	@Id 
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ1") 
//	@SequenceGenerator(name="SEQ1", sequenceName="SEQ1", allocationSize=1) 
	private Integer id;
	private Integer year;
	private Integer num;
	private Date lastDate;
	
	
	
	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Sequence(Integer year, Integer num) {
		super();
		this.year = year;
		this.num = num;
	}

	public Sequence() {
		super();
	}

	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

}
