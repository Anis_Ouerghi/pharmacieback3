package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Depot implements Serializable{

	private static final long serialVersionUID = -5521344669416009848L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id ;
	
	@NotNull
	private String label;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Depot(String label) {
		super();
		this.label = label;
	}

	public Depot(Long id, String label) {
		super();
		this.id = id;
		this.label = label;
	}

	public Depot() {
		super();
	}
	
	
	
	
}
