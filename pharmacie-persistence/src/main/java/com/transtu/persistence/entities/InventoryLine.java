package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;





@Entity
public class InventoryLine implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	private double  price;
	private double vatRate;
	private double inventQte = 0;
	private double stockQte;
	private double stockQteLot;
	private double discard;
	private String note;
	
	@ManyToOne
	@JoinColumn(name = "lot_id")
	private Lot lot ;
	
	@ManyToOne
	//@JsonBackReference
	@JsonIgnore
	@JoinColumn(name = "inventory_id")
	private Inventory inventory;
	
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public double getInventQte() {
		return inventQte;
	}
	public void setInventQte(double inventQte) {
		this.inventQte = inventQte;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	public Inventory getInventory() {
		return inventory;
	}
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	public InventoryLine() {
		super();
		// TODO Auto-generated constructor stub
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getVatRate() {
		return vatRate;
	}
	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}
	public double getStockQte() {
		return stockQte;
	}
	public void setStockQte(double stockQte) {
		this.stockQte = stockQte;
	}
	public double getDiscard() {
		return discard;
	}
	public void setDiscard(double discard) {
		this.discard = discard;
	}
	public Lot getLot() {
		return lot;
	}
	public void setLot(Lot lot) {
		this.lot = lot;
	}
	public double getStockQteLot() {
		return stockQteLot;
	}
	public void setStockQteLot(double stockQteLot) {
		this.stockQteLot = stockQteLot;
	}
	
	
	

}
