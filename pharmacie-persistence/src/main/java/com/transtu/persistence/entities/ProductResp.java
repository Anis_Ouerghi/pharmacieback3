
package com.transtu.persistence.entities;

import java.util.List;

public class ProductResp {

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	private static final long serialVersionUID = 1L;
	private Long id;
	private String lib;
	private String codePctProd;
	private double vatRate;
	private double  price;
	private double  colisage;
	private Depot depot;
	private Presentation presentation;
	//private List<Stock> Stocks;
	private double totalQt ;
	public ProductResp(Long id, String lib, String codePctProd, double vatRate, double price, double colisage,
			Depot depot, Presentation presentation, double totalQt) {
		super();
		this.id = id;
		this.lib = lib;
		this.codePctProd = codePctProd;
		this.vatRate = vatRate;
		this.price = price;
		this.colisage = colisage;
		this.depot = depot;
		this.presentation = presentation;
		this.totalQt = totalQt;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLib() {
		return lib;
	}
	public void setLib(String lib) {
		this.lib = lib;
	}
	public String getCodePctProd() {
		return codePctProd;
	}
	public void setCodePctProd(String codePctProd) {
		this.codePctProd = codePctProd;
	}
	public double getVatRate() {
		return vatRate;
	}
	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getColisage() {
		return colisage;
	}
	public void setColisage(double colisage) {
		this.colisage = colisage;
	}
	public Depot getDepot() {
		return depot;
	}
	public void setDepot(Depot depot) {
		this.depot = depot;
	}
	public Presentation getPresentation() {
		return presentation;
	}
	public void setPresentation(Presentation presentation) {
		this.presentation = presentation;
	}
	public double getTotalQt() {
		return totalQt;
	}
	public void setTotalQt(double totalQt) {
		this.totalQt = totalQt;
	}
	
	
	
	
	
	
	
	
	
	


	

	
}
