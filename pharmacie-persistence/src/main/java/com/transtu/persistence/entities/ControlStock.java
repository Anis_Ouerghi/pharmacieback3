package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ControlStock  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7051466001559324260L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	@ManyToOne
	@JoinColumn(name = "stock_id")
	private Stock stock;
	private double safetyStock;
	private double minStock;
	private double warningStock;
	public ControlStock() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ControlStock(Long id, Stock stock, double safetyStock, double minStock, double warningStock) {
		super();
		Id = id;
		this.stock = stock;
		this.safetyStock = safetyStock;
		this.minStock = minStock;
		this.warningStock = warningStock;
		
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Stock getStock() {
		return stock;
	}
	public void setStock(Stock stock) {
		this.stock = stock;
	}
	public double getSafetyStock() {
		return safetyStock;
	}
	public void setSafetyStock(double safetyStock) {
		this.safetyStock = safetyStock;
	}
	public double getMinStock() {
		return minStock;
	}
	public void setMinStock(double minStock) {
		this.minStock = minStock;
	}
	public double getWarningStock() {
		return warningStock;
	}
	public void setWarningStock(double warningStock) {
		this.warningStock = warningStock;
	}
	
	
}
