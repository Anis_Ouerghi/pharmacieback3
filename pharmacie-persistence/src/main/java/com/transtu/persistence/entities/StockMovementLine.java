package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
public class StockMovementLine implements Serializable {
	
	private static final long serialVersionUID = 2154590007133945501L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "motif_id")
	private Motif motif;
	
	private double movmentQte = 0;
	
	@ManyToOne
	@JoinColumn(name = "stockMovement_id")
	@JsonBackReference
	private StockMovement stockMovement;
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "lot_id")
	private Lot lot ;
	


	public Motif getMotif() {
		return motif;
	}

	public void setMotif(Motif motif) {
		this.motif = motif;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public double getMovmentQte() {
		return movmentQte;
	}

	public void setMovmentQte(double movmentQte) {
		this.movmentQte = movmentQte;
	}

	public StockMovement getStockMovement() {
		return stockMovement;
	}

	public void setStockMovement(StockMovement stockMovement) {
		this.stockMovement = stockMovement;
	}

	public Lot getLot() {
		return lot;
	}

	public void setLot(Lot lot) {
		this.lot = lot;
	}

	
	
	
	
	}
	
	
	


