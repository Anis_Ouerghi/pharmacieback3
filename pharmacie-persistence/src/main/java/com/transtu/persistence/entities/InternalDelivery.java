package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class InternalDelivery  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long Id;
	private Date dateDelivery;
	private String numDelivery ;
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
	@ManyToOne
	@JoinColumn(name = "day_id")
	private Day day;
	@OneToOne
	@JoinColumn(name = "InternalOrder_id")
	private InternalOrder internalOrder; 
	@ManyToOne
	@JoinColumn(name = "utilistaure_id")
	private Utilisateur utilisateur;
	@OneToMany(mappedBy="internalDelivery")
	@JsonManagedReference
	private List<InternalDeliveryLine> internalDeliveryLines;
	
	@ManyToOne
	@JoinColumn(name = "state_id")
	private State state ;
	
	@ManyToOne
	@JoinColumn(name = "location_id")
	private Location location; 
	
	
	
	

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public InternalDelivery() {
		super();
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Date getDateDelivery() {
		return dateDelivery;
	}

	public void setDateDelivery(Date dateDelivery) {
		this.dateDelivery = dateDelivery;
	}

	public InternalOrder getInternalOrder() {
		return internalOrder;
	}

	public void setInternalOrder(InternalOrder internalOrder) {
		this.internalOrder = internalOrder;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getNumDelivery() {
		return numDelivery;
	}

	public void setNumDelivery(String numDelivery) {
		this.numDelivery = numDelivery;
	}

	public List<InternalDeliveryLine> getInternalDeliveryLines() {
		return internalDeliveryLines;
	}

	public void setInternalDeliveryLines(List<InternalDeliveryLine> internalDeliveryLines) {
		this.internalDeliveryLines = internalDeliveryLines;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public InternalDelivery(Long id, Date dateDelivery, String numDelivery, Customer customer,
			InternalOrder internalOrder, List<InternalDeliveryLine> internalDeliveryLines, State state,
			Location location) {
		super();
		Id = id;
		this.dateDelivery = dateDelivery;
		this.numDelivery = numDelivery;
		this.customer = customer;
		this.internalOrder = internalOrder;
		this.internalDeliveryLines = internalDeliveryLines;
		this.state = state;
		this.location = location;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	} 
	
	
	
	
	
	
}
