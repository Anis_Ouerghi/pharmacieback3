package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;



@Entity
public class DistributionLine implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 
	public Date getNextDistributionDate() {
		return nextDistributionDate;
	}

	public void setNextDistributionDate(Date nextDistributionDate) {
		this.nextDistributionDate = nextDistributionDate;
	}

	private Date nextDistributionDate;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "distribution_id")
	@JsonBackReference
	private Distribution distribution;
	private double deliveredQt = 0;
	private double missingQt = 0;
    private  double toDistribute;
    private  String posologie;
	 
	 
	public DistributionLine() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Distribution getDistribution() {
		return distribution;
	}

	public void setDistribution(Distribution distribution) {
		this.distribution = distribution;
	}

	public double getDeliveredQt() {
		return deliveredQt;
	}

	public void setDeliveredQt(double deliveredQt) {
		this.deliveredQt = deliveredQt;
	}

	public double getMissingQt() {
		return missingQt;
	}

	public void setMissingQt(double missingQt) {
		this.missingQt = missingQt;
	}

	public double getToDistribute() {
		return toDistribute;
	}

	public void setToDistribute(double toDistribute) {
		this.toDistribute = toDistribute;
	}

	public String getPosologie() {
		return posologie;
	}

	public void setPosologie(String posologie) {
		this.posologie = posologie;
	}

	public DistributionLine(Long id, Date nextDistributionDate, Product product, Distribution distribution,
			double deliveredQt, double missingQt, double toDistribute) {
		super();
		this.id = id;
		this.nextDistributionDate = nextDistributionDate;
		this.product = product;
		this.distribution = distribution;
		this.deliveredQt = deliveredQt;
		this.missingQt = missingQt;
		this.toDistribute = toDistribute;
	}

	public DistributionLine(Long id, Product product, Distribution distribution, double deliveredQt, double missingQt,
			double toDistribute) {
		super();
		this.id = id;
		this.product = product;
		this.distribution = distribution;
		this.deliveredQt = deliveredQt;
		this.missingQt = missingQt;
		this.toDistribute = toDistribute;
	}

	
}