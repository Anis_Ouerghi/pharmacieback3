package com.transtu.persistence.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class DistributionState {

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	//@JsonBackReference
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "state_id")
	private State state;
	
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "distribution_id")
	private Distribution distribution;
	
	private Date dateState;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public Distribution getDistribution() {
		return distribution;
	}
	public void setDistribution(Distribution distribution) {
		this.distribution = distribution;
	}
	public Date getDateState() {
		return dateState;
	}
	public void setDateState(Date dateState) {
		this.dateState = dateState;
	}
	public DistributionState() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DistributionState(Long id, State state, Distribution distribution, Date dateState) {
		super();
		this.id = id;
		this.state = state;
		this.distribution = distribution;
		this.dateState = dateState;
	}
	
	
	
	
	
}
