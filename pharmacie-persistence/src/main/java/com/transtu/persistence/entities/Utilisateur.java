package com.transtu.persistence.entities;



import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
@Entity
public class Utilisateur implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	//@Column ( name = "use" )
	private String username;
	private String firstname;
	private String lastname;
	private String function;
	// @Column ( name = "pass" )
	private String password;
	// @Column ( name = "use2" )
	private boolean actived;
	private String email;
	
	@ManyToMany
	@JoinTable(name = "Utilisateur_Role")
	private Collection<Role> roles;
	
//	@OneToMany(mappedBy="utilisateur")
//	@JsonManagedReference
//	List<UserDay> userDays;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActived() {
		return actived;
	}

	public void setActived(boolean actived) {
		this.actived = actived;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

//	public List<UserDay> getUserDays() {
//		return userDays;
//	}
//
//	public void setUserDays(List<UserDay> userDays) {
//		this.userDays = userDays;
//	}

	public Utilisateur(Long id, String username, String password, boolean actived, String email, Collection<Role> roles,
			List<UserDay> userDays) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.actived = actived;
		this.email = email;
		this.roles = roles;
	//	this.userDays = userDays;
	}

	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Utilisateur(Long id, String username, String firstname, String lastname, String password, boolean actived,
			String email, Collection<Role> roles, List<UserDay> userDays) {
		super();
		this.id = id;
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.password = password;
		this.actived = actived;
		this.email = email;
		this.roles = roles;
	//	this.userDays = userDays;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public Utilisateur(Long id, String username, String firstname, String lastname, String function, String password,
			boolean actived, String email, Collection<Role> roles, List<UserDay> userDays) {
		super();
		this.id = id;
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.function = function;
		this.password = password;
		this.actived = actived;
		this.email = email;
		this.roles = roles;
//		this.userDays = userDays;
	}
	
	
	
	
	
}
