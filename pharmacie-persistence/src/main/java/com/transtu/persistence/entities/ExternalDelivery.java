//package com.transtu.persistence.entities;
//
//import java.io.Serializable;
//import java.util.Date;
//import java.util.List;
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToMany;
//import javax.persistence.OneToOne;
//
//import com.fasterxml.jackson.annotation.JsonBackReference;
//import com.fasterxml.jackson.annotation.JsonManagedReference;
//
//@Entity
//public class ExternalDelivery implements Serializable {
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 7359690331797878118L;
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	private Long Id;
//	private Date dateExternalDelivery;
//	@ManyToOne
//	@JoinColumn(name = "day_id")
//	private Day day;
//	private String numExternalDelivery;
//	@ManyToOne
//	@JoinColumn(name = "state_id")
//	private State state;
//	
//	@OneToOne
//	@JoinColumn(name = "externalOrder_id")
//	@JsonBackReference
//	private ExternalOrder externalOrder;
//
////	@ManyToOne
////	@JoinColumn(name = "externalOrder_id")
////	@JsonBackReference
////	private ExternalOrder externalOrder;
//	
//	@OneToMany(mappedBy="externalDelivery")
//	@JsonManagedReference
//	List<Reception> receptions;
//	
//	public ExternalOrder getExternalOrder() {
//		return externalOrder;
//	}
//
//	public void setExternalOrder(ExternalOrder externalOrder) {
//		this.externalOrder = externalOrder;
//	}
//
//	public List<Reception> getReceptions() {
//		return receptions;
//	}
//
//	public void setReceptions(List<Reception> receptions) {
//		this.receptions = receptions;
//	}
//
//	public Long getId() {
//		return Id;
//	}
//
//	public void setId(Long id) {
//		Id = id;
//	}
//
//	public Day getDay() {
//		return day;
//	}
//
//	public void setDay(Day day) {
//		this.day = day;
//	}
//
//	public String getNumExternalDelivery() {
//		return numExternalDelivery;
//	}
//
//	public void setNumExternalDelivery(String numExternalDelivery) {
//		this.numExternalDelivery = numExternalDelivery;
//	}
//
//	public State getState() {
//		return state;
//	}
//
//	public void setState(State state) {
//		this.state = state;
//	}
//
//	public ExternalDelivery() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//	public ExternalDelivery(Long id, Day day, String numExternalDelivery, State state, ExternalOrder externalOrder,
//			List<Reception> receptions) {
//		super();
//		Id = id;
//		this.day = day;
//		this.numExternalDelivery = numExternalDelivery;
//		this.state = state;
//		this.externalOrder = externalOrder;
//		this.receptions = receptions;
//	}
//
//	public Date getDateExternalDelivery() {
//		return dateExternalDelivery;
//	}
//
//	public void setDateExternalDelivery(Date dateExternalDelivery) {
//		this.dateExternalDelivery = dateExternalDelivery;
//	}
//	
//	
//	
//}
