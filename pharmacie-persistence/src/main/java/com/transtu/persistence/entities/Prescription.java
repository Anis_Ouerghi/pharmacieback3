package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Prescription implements Serializable {

	private static final long serialVersionUID = 5696028901605256384L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String prescriptionNumber;
	private Boolean distributed;
	private String agentId;
	//new field
	//@Column(name="agent_situation")
	private String agentSituation;
	private String type ;
	private Date prescriptionDate;
	@OneToMany(mappedBy="prescription")
    @JsonManagedReference
	private List<PrescriptionLine> prescriptionLine = new ArrayList<>();
	@OneToMany(mappedBy="prescription")
	@JsonManagedReference
	private List<Distribution> distribution = new ArrayList<>();
	@ManyToOne
	@JoinColumn(name = "doctor_id")
	private Doctor doctor;
	
	
	
	
	
	
	public Prescription(Long id, String prescriptionNumber, Boolean distributed, String agentId, String type,
			Date prescriptionDate, List<PrescriptionLine> prescriptionLine, List<Distribution> distribution) {
		super();
		this.id = id;
		this.prescriptionNumber = prescriptionNumber;
		this.distributed = distributed;
		this.agentId = agentId;
		this.type = type;
		this.prescriptionDate = prescriptionDate;
		this.prescriptionLine = prescriptionLine;
		this.distribution = distribution;
	}


	public Boolean getDistributed() {
		return distributed;
	}


	public void setDistributed(Boolean distributed) {
		this.distributed = distributed;
	}


	
	public Prescription() {
		super();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getPrescriptionNumber() {
		return prescriptionNumber;
	}


	public void setPrescriptionNumber(String prescriptionNumber) {
		this.prescriptionNumber = prescriptionNumber;
	}


	


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Date getPrescriptionDate() {
		return prescriptionDate;
	}


	public void setPrescriptionDate(Date prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}


	public List<PrescriptionLine> getPrescriptionLine() {
		return prescriptionLine;
	}


	public void setPrescriptionLine(List<PrescriptionLine> prescriptionLine) {
		this.prescriptionLine = prescriptionLine;
	}


	public List<Distribution> getDistribution() {
		return distribution;
	}


	public void setDistribution(List<Distribution> distribution) {
		this.distribution = distribution;
	}


	public String getAgentId() {
		return agentId;
	}


	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}


	public Prescription(Long id, String prescriptionNumber, String agentId, String type, Date prescriptionDate,
			List<PrescriptionLine> prescriptionLine, List<Distribution> distribution) {
		super();
		this.id = id;
		this.prescriptionNumber = prescriptionNumber;
		this.agentId = agentId;
		this.type = type;
		this.prescriptionDate = prescriptionDate;
		this.prescriptionLine = prescriptionLine;
		this.distribution = distribution;
	}


	public Doctor getDoctor() {
		return doctor;
	}


	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}


	public String getAgentSituation() {
		return agentSituation;
	}


	public void setAgentSituation(String agentSituation) {
		this.agentSituation = agentSituation;
	}


	

	
	
	
	


}