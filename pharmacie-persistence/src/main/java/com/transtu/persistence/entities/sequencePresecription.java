package com.transtu.persistence.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
@Entity
public class sequencePresecription {
	@Id
	@GeneratedValue
//	@Id
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ")
//	@SequenceGenerator(name="SEQ", sequenceName="SEQ", allocationSize=1)
	private Integer id;

	private Integer year;
	private Integer num;
	private Date lasteDate ;
	
	public sequencePresecription(Integer year, Integer num) {
		super();
		this.year = year;
		this.num = num;
	}
	public sequencePresecription(Integer id, Integer year, Integer num) {
		super();
		this.id = id;
		this.year = year;
		this.num = num;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Date getLasteDate() {
		return lasteDate;
	}
	public void setLasteDate(Date lasteDate) {
		this.lasteDate = lasteDate;
	}
	public sequencePresecription() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
