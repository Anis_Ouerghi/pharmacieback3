package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
public class PrescriptionLine implements Serializable {

	private static final long serialVersionUID = 4483314117670673415L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private double totalQt ;
	private Boolean isPeriodic;
	private String periodicity;
	private Integer days;
	private Integer distNumber;
	private Boolean distributed;
	private  String posologie;
	
	public String getPosologie() {
		return posologie;
	}



	public void setPosologie(String posologie) {
		this.posologie = posologie;
	}


	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	public PrescriptionLine(long id, double totalQt, Boolean isPeriodic, String periodicity, Integer days,
			Integer distNumber, Date nextDistributionDate, Product product, Prescription prescription) {
		super();
		this.id = id;
		this.totalQt = totalQt;
		this.isPeriodic = isPeriodic;
		this.periodicity = periodicity;
		this.days = days;
		this.distNumber = distNumber;
		//this.nextDistributionDate = nextDistributionDate;
		this.product = product;
		this.prescription = prescription;
	}


	@ManyToOne
	@JoinColumn(name = "prescription_id")
	@JsonBackReference
	private Prescription prescription;


	public PrescriptionLine() {
		super();
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}





	public double getTotalQt() {
		return totalQt;
	}



	public void setTotalQt(double qteTotal) {
		totalQt = qteTotal;
	}

	

	public Product getProduct() {
		return product;
	}



	public void setProduct(Product product) {
		this.product = product;
	}



	public Prescription getPrescription() {
		return prescription;
	}

	public void setPrescription(Prescription prescription) {
		this.prescription = prescription;
	}




	public Boolean getIsPeriodic() {
		return isPeriodic;
	}



	public void setIsPeriodic(Boolean isPeriodic) {
		this.isPeriodic = isPeriodic;
	}



	public PrescriptionLine(long id, double totalQt, Boolean isPeriodic, String periodicity, Integer days,
			Integer distNumber, Boolean distributed, Product product, Prescription prescription) {
		super();
		this.id = id;
		this.totalQt = totalQt;
		this.isPeriodic = isPeriodic;
		this.periodicity = periodicity;
		this.days = days;
		this.distNumber = distNumber;
		this.distributed = distributed;
		this.product = product;
		this.prescription = prescription;
	}



	public Boolean getDistributed() {
		return distributed;
	}



	public void setDistributed(Boolean distributed) {
		this.distributed = distributed;
	}



	public String getPeriodicity() {
		return periodicity;
	}



	public void setPeriodicity(String periodicity) {
		this.periodicity = periodicity;
	}



	public Integer getDays() {
		return days;
	}



	public void setDays(Integer days) {
		this.days = days;
	}



	public Integer getDistNumber() {
		return distNumber;
	}



	public void setDistNumber(Integer distNumber) {
		this.distNumber = distNumber;
	}



	
	

	

	



	

}