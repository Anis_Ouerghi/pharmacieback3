package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

@Entity
public class District  implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
//	@Id 
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ1") 
//	@SequenceGenerator(name="SEQ1", sequenceName="SEQ1", allocationSize=1) 
	private Long id;
	private String lib;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLib() {
		return lib;
	}

	public void setLib(String libelle) {
		this.lib = libelle;
	}

	public District(Long id, String libelle) {
		super();
		this.id = id;
		this.lib = libelle;
	}

	

	public District() {
		super();
	}

	public District(String libelle) {
		super();
		this.lib = libelle;
	}

}