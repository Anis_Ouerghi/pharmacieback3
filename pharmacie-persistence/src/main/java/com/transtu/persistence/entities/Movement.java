	/**
 * 
 */
package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
//@JsonIgnoreType
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@DiscriminatorColumn(
//	    name="discriminator",
//	    discriminatorType=DiscriminatorType.STRING
//	    )

public class Movement  implements Serializable {

	private static final long serialVersionUID = -7993531876597538656L;

	@Id
	@GeneratedValue
	private Long id;
	private String type; // -1: Sortie 1:Entré
	private String description;
	@Column(name = "DATEMVT") 
	private Date date;
	private double quantity;

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "emplacement_id")
	//@JsonBackReference(value = "mouvements")
	@JsonIgnore
	private Location location;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "produit_id")
	//@JsonBackReference(value = "mouvements")
	@JsonIgnore
	private Product product;
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "distribution_id")
	//@JsonManagedReference(value = "mouvements")
	//@JsonIgnore
	private Distribution distribution ;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "reception_id")
	@JsonIgnore
	//@JsonBackReference(value = "ExceptionalMovement")
	private Reception reception;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "internalDelivery_id")
	//@JsonBackReference(value = "ExceptionalMovement")
	@JsonIgnore
	private InternalDelivery internalDelivery;

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "referenceType_id")
   // @JsonBackReference(value = "ExceptionalMovement")
	@JsonIgnore
	private ReferenceType referenceType;

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	

	public Distribution getDistribution() {
		return distribution;
	}

	public void setDistribution(Distribution distribution) {
		this.distribution = distribution;
	}

	public Movement() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Movement(Long id, String type, String description, Date date, double quantity, Location location,
			Product product) {
		super();
		this.id = id;
		this.type = type;
		this.description = description;
		this.date = date;
		this.quantity = quantity;
		this.location = location;
		this.product = product;
	}

	public Movement(Long id, String type, String description, Date date, double quantity) {
		super();
		this.id = id;
		this.type = type;
		this.description = description;
		this.date = date;
		this.quantity = quantity;
	}

	
	
}
