package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
public class ExternalOrder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6522228110857613349L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	private Date dateExterOrder;
	@ManyToOne
	@JoinColumn(name = "day_id")
	private Day day;
	private String numExternalOrder;
	@ManyToOne
	@JoinColumn(name = "emplacement_id")
	private Location location; 
	
	@OneToMany(mappedBy="externalOrder")
	@JsonManagedReference
	List<ExternalOrderLine> externalOrderLines;
	
//	@OneToOne(mappedBy="externalOrder")
//	@JsonManagedReference
//	ExternalDelivery externalDelivery;
	
	@OneToMany(mappedBy="externalOrder")
	@JsonManagedReference
	List<Reception> receptions;
	
	@ManyToOne
	@JoinColumn(name = "state_id")
	private State state;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public String getNumExternalOrder() {
		return numExternalOrder;
	}

	public void setNumExternalOrder(String numExternalOrder) {
		this.numExternalOrder = numExternalOrder;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<ExternalOrderLine> getExternalOrderLines() {
		return externalOrderLines;
	}

	public void setExternalOrderLines(List<ExternalOrderLine> externalOrderLines) {
		this.externalOrderLines = externalOrderLines;
	}

//	public ExternalDelivery getExternalDelivery() {
//		return externalDelivery;
//	}
//
//	public void setExternalDelivery(ExternalDelivery externalDelivery) {
//		this.externalDelivery = externalDelivery;
//	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Date getDateExterOrder() {
		return dateExterOrder;
	}

	public void setDateExterOrder(Date dateExterOrder) {
		this.dateExterOrder = dateExterOrder;
	}

	public List<Reception> getReceptions() {
		return receptions;
	}

	public void setReceptions(List<Reception> receptions) {
		this.receptions = receptions;
	}
	

//	public List<ExternalDelivery> getExternalDelivery() {
//		return externalDelivery;
//	}
//
//	public void setExternalDelivery(List<ExternalDelivery> externalDelivery) {
//		this.externalDelivery = externalDelivery;
//	}

//	public ExternalDelivery getExternalDelivery() {
//		return externalDelivery;
//	}
//
//	public void setExternalDelivery(ExternalDelivery externalDelivery) {
//		this.externalDelivery = externalDelivery;
//	}
	
	

	
	
	
}
