package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity

public class Distribution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3327240259140302531L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	private String distributionNumber ; 
	private Date dateDist ;

	@ManyToOne
	@JoinColumn(name = "utilistaure_id")
	private Utilisateur utilisateur;

	@ManyToOne
	@JoinColumn(name = "emplacement_id")
	private Location location;
	
	@ManyToOne
	@JoinColumn(name = "day_id")
	private Day day ; 
	
	@ManyToOne
	@JoinColumn(name = "ordonnance_id")
	@JsonBackReference
	private Prescription prescription ;

	@ManyToOne
	@JoinColumn(name = "district_id")
	private District district;
	
	
//	@ManyToOne
//	@JoinColumn(name = "doctor_id")
//	private Doctor doctor;
	
	
	@OneToMany(mappedBy="distribution")
	@JsonManagedReference
	private List<DistributionLine> distributionLines;
	@OneToMany(mappedBy="distribution")
	@JsonManagedReference
	private List<DistributionState> distributionStates;
	
//	@OneToMany(mappedBy="distribution")
//	@JsonManagedReference
//	private List<StockMovement> stockMovements;
	
	public List<DistributionState> getDistributionStates() {
		return distributionStates;
	}
	public void setDistributionStates(List<DistributionState> distributionStates) {
		this.distributionStates = distributionStates;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location emplacement) {
		this.location = emplacement;
	}
	public Prescription getPrescription() {
		return prescription;
	}
	public void setPrescription(Prescription ordonnance) {
		this.prescription = ordonnance;
	}
	public District getDistrict() {
		return district;
	}
	public void setDistrict(District district) {
		this.district = district;
	}
	public String getDistributionNumber() {
		return distributionNumber;
	}
	public void setDistributionNumber(String distributionNumber) {
		this.distributionNumber = distributionNumber;
	}
	public Date getDateDist() {
		return dateDist;
	}
	public void setDateDist(Date dateDist) {
		this.dateDist = dateDist;
	}
	public List<DistributionLine> getDistributionLines() {
		return distributionLines;
	}
	public void setDistributionLines(List<DistributionLine> distributionLines) {
		this.distributionLines = distributionLines;
	}
	public Day getDay() {
		return day;
	}
	public void setDay(Day day) {
		this.day = day;
	}
	public Distribution(Long id, String distributionNumber, Date dateDist, Location location, Day day,
			Prescription prescription, District district, List<DistributionLine> distributionLines,
			List<DistributionState> distributionStates) {
		super();
		Id = id;
		this.distributionNumber = distributionNumber;
		this.dateDist = dateDist;
		this.location = location;
		this.day = day;
		this.prescription = prescription;
		this.district = district;
		this.distributionLines = distributionLines;
		this.distributionStates = distributionStates;
	}
	public Distribution() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
//	public Doctor getDoctor() {
//		return doctor;
//	}
//	public void setDoctor(Doctor doctor) {
//		this.doctor = doctor;
//	}
	

	
	

}
