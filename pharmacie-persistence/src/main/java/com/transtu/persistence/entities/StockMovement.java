package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
public class StockMovement implements Serializable {

	
	private static final long serialVersionUID = -6095557810339205613L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	private Date dateMovement;
	@ManyToOne
	@JoinColumn(name = "day_id")
	private Day day;
	
	@ManyToOne
	@JoinColumn(name = "location_id")
	private Location location;
	
	@ManyToOne
	@JoinColumn(name = "utilistaure_id")
	private Utilisateur utilisateur;
	
	private String numMovement;
	private String typeMvt;
	private String descriminator;
	
	@OneToMany(mappedBy="stockMovement")
	@JsonManagedReference
	List<StockMovementLine> stockMovementLine;
	
	@ManyToOne
	@JoinColumn(name = "referenceType_id")
	private ReferenceType referenceType;
	
	
	@ManyToOne
	@JoinColumn(name = "distribution_id")
	private Distribution distribution;
	
	@ManyToOne
	@JoinColumn(name = "internalDelivery_id")
	private InternalDelivery internalDelivery;
	
	@ManyToOne
	@JoinColumn(name = "reception_id")
	private Reception reception;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Date getDateMovement() {
		return dateMovement;
	}

	public void setDateMovement(Date dateMovement) {
		this.dateMovement = dateMovement;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	

	public String getNumMovement() {
		return numMovement;
	}

	public void setNumMovement(String numMovement) {
		this.numMovement = numMovement;
	}

	public String getTypeMvt() {
		return typeMvt;
	}

	public void setTypeMvt(String typeMvt) {
		this.typeMvt = typeMvt;
	}

	public String getDescriminator() {
		return descriminator;
	}

	public void setDescriminator(String descriminator) {
		this.descriminator = descriminator;
	}

	public List<StockMovementLine> getStockMovementLine() {
		return stockMovementLine;
	}

	public void setStockMovementLine(List<StockMovementLine> stockMovementLine) {
		this.stockMovementLine = stockMovementLine;
	}

	public StockMovement() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReferenceType getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(ReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	public Distribution getDistribution() {
		return distribution;
	}

	public void setDistribution(Distribution distribution) {
		this.distribution = distribution;
	}

	public InternalDelivery getInternalDelivery() {
		return internalDelivery;
	}

	public void setInternalDelivery(InternalDelivery internalDelivery) {
		this.internalDelivery = internalDelivery;
	}

	public Reception getReception() {
		return reception;
	}

	public void setReception(Reception reception) {
		this.reception = reception;
	}

	public StockMovement(Long id, Date dateMovement, Day day, Location location, Utilisateur utilisateur,
			String numMovement, String typeMvt, String descriminator, List<StockMovementLine> stockMovementLine,
			ReferenceType referenceType, Distribution distribution, InternalDelivery internalDelivery,
			Reception reception) {
		super();
		Id = id;
		this.dateMovement = dateMovement;
		this.day = day;
		this.location = location;
		this.utilisateur = utilisateur;
		this.numMovement = numMovement;
		this.typeMvt = typeMvt;
		this.descriminator = descriminator;
		this.stockMovementLine = stockMovementLine;
		this.referenceType = referenceType;
		this.distribution = distribution;
		this.internalDelivery = internalDelivery;
		this.reception = reception;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	
	
	
	
}
