package com.transtu.persistence.entities;

import java.util.Date;

public class PresecriptionRepJasper {
	
	
	
	    private String prescriptionNumber ;
		private Date prescriptionDate ;
		private double montantOrd; 
		private String agentId ;
		private String agentSituation ;
		
		
		
		
		
		
		
		public PresecriptionRepJasper(String prescriptionNumber, Date prescriptionDate, double montantOrd,
				String agentId, String agentSituation) {
			super();
			this.prescriptionNumber = prescriptionNumber;
			this.prescriptionDate = prescriptionDate;
			this.montantOrd = montantOrd;
			this.agentId = agentId;
			this.agentSituation = agentSituation;
		}
		
		
		public String getPrescriptionNumber() {
			return prescriptionNumber;
		}
		public void setPrescriptionNumber(String prescriptionNumber) {
			this.prescriptionNumber = prescriptionNumber;
		}
		public Date getPrescriptionDate() {
			return prescriptionDate;
		}
		public void setPrescriptionDate(Date prescriptionDate) {
			this.prescriptionDate = prescriptionDate;
		}
		public double getMontantOrd() {
			return montantOrd;
		}
		public void setMontantOrd(double montantOrd) {
			this.montantOrd = montantOrd;
		}
		public String getAgentId() {
			return agentId;
		}
		public void setAgentId(String agentId) {
			this.agentId = agentId;
		}
		public String getAgentSituation() {
			return agentSituation;
		}
		public void setAgentSituation(String agentSituation) {
			this.agentSituation = agentSituation;
		}
		
		
		
		
		
		
		
		
		
		
		

}
