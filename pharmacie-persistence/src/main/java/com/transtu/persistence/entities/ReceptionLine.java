package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
public class ReceptionLine implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2154590007133945501L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "reception_id")
	@JsonBackReference
	private Reception reception;
	private double invoiceQte = 0;
	private double receptionQte = 0;
	private double price = 0;
	private double vat;
	private String lotNumber;
	private Date expirationDate;
	
	
	public double getVat() {
		return vat;
	}

	public void setVat(double vat) {
		this.vat = vat;
	}

	public ReceptionLine(Long id, Product product, Reception reception, double invoiceQte, double receptionQte) {
		super();
		this.id = id;
		this.product = product;
		this.reception = reception;
		this.invoiceQte = invoiceQte;
		this.receptionQte = receptionQte;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Reception getReception() {
		return reception;
	}
	public void setReception(Reception reception) {
		this.reception = reception;
	}
	public double getReceptionQte() {
		return receptionQte;
	}
	public void setReceptionQte(double receptionQte) {
		this.receptionQte = receptionQte;
	}
	public ReceptionLine(Long id, Product product, Reception reception, double receptionQte) {
		super();
		this.id = id;
		this.product = product;
		this.reception = reception;
		this.receptionQte = receptionQte;
	}
	public ReceptionLine() {
		super();
		// TODO Auto-generated constructor stub
	}
	public double getInvoiceQte() {
		return invoiceQte;
	}
	public void setInvoiceQte(double invoiceQte) {
		this.invoiceQte = invoiceQte;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public ReceptionLine(Long id, Product product, Reception reception, double invoiceQte, double receptionQte,
			double price, double vat) {
		super();
		this.id = id;
		this.product = product;
		this.reception = reception;
		this.invoiceQte = invoiceQte;
		this.receptionQte = receptionQte;
		this.price = price;
		this.vat = vat;
	}

	public String getLotNumber() {
		return lotNumber;
	}

	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}




	
	
	
}
