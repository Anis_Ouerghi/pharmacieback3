package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;



@Entity
@JsonIgnoreProperties
public class Inventory implements Serializable { 
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Date dateInv;
	@ManyToOne
	@JoinColumn(name = "state_id")
	private State state;
	private String note;
	@ManyToOne
	@JoinColumn(name = "location_id")
	private Location location;
	@ManyToOne
	@JoinColumn(name = "utilistaure_id")
	private Utilisateur utilisateur;
	
	@JsonIgnore
	@OneToMany(mappedBy="inventory")
	@JsonManagedReference
	List<InventoryLine> inventoryLine;
	
	
	
	
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDateInv() {
		return dateInv;
	}
	public void setDateInv(Date dateInv) {
		this.dateInv = dateInv;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public List<InventoryLine> getInventoryLine() {
		return inventoryLine;
	}
	public void setInventoryLine(List<InventoryLine> inventoryLine) {
		this.inventoryLine = inventoryLine;
	}
	
	
	
//	public List<InventoryLine> getInventoryLine() {
//		return inventoryLine;
//	}
//	public void setInventoryLine(List<InventoryLine> inventoryLine) {
//		this.inventoryLine = inventoryLine;
//	}
//	public Inventory() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
	
	

	
	
	

}
