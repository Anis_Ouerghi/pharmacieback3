package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Stock  implements Serializable {

	private static final long serialVersionUID = 4233917538888204140L;

	@Id
	@GeneratedValue
	private Long id;
	private String locker;
	private double quantity;
	@ManyToOne
	//@JsonBackReference
	@JsonIgnore
	@JoinColumn(name = "produit_id")
	private Product product;
	@ManyToOne
	//@JsonManagedReference
	@JoinColumn(name = "emplacement_id")
	private Location location;

	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getLocker() {
		return locker;
	}


	public void setLocker(String locker) {
		this.locker = locker;
	}


	public double getQuantity() {
		return quantity;
	}


	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}


	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}


	public Location getLocation() {
		return location;
	}


	public void setLocation(Location location) {
		this.location = location;
	}


	public Stock() {
		super();
		// TODO Auto-generated constructor stub
	}

	

}
