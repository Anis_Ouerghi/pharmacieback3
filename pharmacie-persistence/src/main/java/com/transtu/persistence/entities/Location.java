package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

//import com.transtu.domain.base.DomainBase;

@Entity
public class Location  implements Serializable {

	private static final long serialVersionUID = 1250461945246642698L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String lib;
	@OneToMany(mappedBy="location")
	@JsonBackReference
	private List<Stock> Stocks;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLib() {
		return lib;
	}

	public void setLib(String lib) {
		this.lib = lib;
	}





	public Location() {
		super();
		// TODO Auto-generated constructor stub
	}



}
