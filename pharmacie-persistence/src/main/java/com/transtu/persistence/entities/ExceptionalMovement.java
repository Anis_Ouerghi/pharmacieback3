//package com.transtu.persistence.entities;
//
//import java.util.Date;
//
//import javax.persistence.DiscriminatorValue;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import com.fasterxml.jackson.annotation.JsonBackReference;
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//@Entity
//@DiscriminatorValue(value="EXP")
//public class ExceptionalMovement extends Movement {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	
//	
//
//
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "reception_id")
//	@JsonIgnore
//	//@JsonBackReference(value = "ExceptionalMovement")
//	private Reception reception;
//	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "internalDelivery_id")
//	//@JsonBackReference(value = "ExceptionalMovement")
//	@JsonIgnore
//	private InternalDelivery internalDelivery;
//
//	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "referenceType_id")
//   // @JsonBackReference(value = "ExceptionalMovement")
//	@JsonIgnore
//	private ReferenceType referenceType;
//
//
//
//
//	public ExceptionalMovement(Long id, String type, String description, Date date, double quantity, Location location,
//			Product product, Reception reception, InternalDelivery internalDelivery, ReferenceType referenceType) {
//		super(id, type, description, date, quantity, location, product);
//		this.reception = reception;
//		this.internalDelivery = internalDelivery;
//		this.referenceType = referenceType;
//	}
//
//
//	public Reception getReception() {
//		return reception;
//	}
//
//
//	public void setReception(Reception reception) {
//		this.reception = reception;
//	}
//
//
//	public InternalDelivery getInternalDelivery() {
//		return internalDelivery;
//	}
//
//
//	public void setInternalDelivery(InternalDelivery internalDelivery) {
//		this.internalDelivery = internalDelivery;
//	}
//
//
//	public ReferenceType getReferenceType() {
//		return referenceType;
//	}
//
//
//	public void setReferenceType(ReferenceType referenceType) {
//		this.referenceType = referenceType;
//	}
//
//
//
//
//	public ExceptionalMovement() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//
//	public ExceptionalMovement(Long id, String type, String description, Date date, double quantity, Location location,
//			Product product) {
//		super(id, type, description, date, quantity, location, product);
//		// TODO Auto-generated constructor stub
//	}
//
//
//	public ExceptionalMovement(Long id, String type, String description, Date date, double quantity) {
//		super(id, type, description, date, quantity);
//		// TODO Auto-generated constructor stub
//	}
//
//	
//	
//
//	
//
//
//
//}
