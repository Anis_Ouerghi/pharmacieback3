package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
@Entity
public class ExternalOrderLine implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -438004944105728916L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	@ManyToOne
	@JoinColumn(name = "externalOrder_id")
	@JsonBackReference
	private ExternalOrder externalOrder;
	//@NotNull
	@Column(name = "vatRate")
	private double vatRate = 0;
	//@NotNull
	@Column(name = "price")
	private double  price = 0;
	private double orderQte = 0;
	
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ExternalOrder getExternalOrder() {
		return externalOrder;
	}

	public void setExternalOrder(ExternalOrder externalOrder) {
		this.externalOrder = externalOrder;
	}

	public double getOrderQte() {
		return orderQte;
	}

	public void setOrderQte(double orderQte) {
		this.orderQte = orderQte;
	}

	public ExternalOrderLine() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExternalOrderLine(Long id, Product product, ExternalOrder externalOrder, double orderQte) {
		super();
		this.id = id;
		this.product = product;
		this.externalOrder = externalOrder;
		this.orderQte = orderQte;
	}

	public double getVatRate() {
		return vatRate;
	}

	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	
}
