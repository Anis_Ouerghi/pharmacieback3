package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Presentation implements Serializable{

	
	private static final long serialVersionUID = 5696028901605256384L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id ;
	private String label ;

	
	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getLabel() {
		return label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public Presentation() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Presentation(Long id, String label) {
		super();
		this.id = id;
		this.label = label;
		
	}


	public Presentation(String label) {
		super();
		this.label = label;
	}
	
	
	
	
	
}
