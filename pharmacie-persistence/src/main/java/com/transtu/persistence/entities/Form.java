package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Form implements Serializable{

	private static final long serialVersionUID = -2578972536753303270L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id ;
	private String label ;
	
	public Form(String label) {
		super();
		this.label = label;
	}

	public Form() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
