package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreType;


@Entity
@JsonIgnoreType
public class SequenceMvtExepEntre implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;

	private Integer year;
	private Integer num;
	private Date lastDate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Date getLastDate() {
		return lastDate;
	}
	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}
	public SequenceMvtExepEntre(Integer year, Integer num) {
		super();
		this.year = year;
		this.num = num;
	}
	public SequenceMvtExepEntre(Integer id, Integer year, Integer num, Date lastDate) {
		super();
		this.id = id;
		this.year = year;
		this.num = num;
		this.lastDate = lastDate;
	}
	public SequenceMvtExepEntre() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
