
package com.transtu.persistence.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(indexes = { @Index(name = "index_1", columnList = "barCode , lib, codePctProd") ,
					@Index(name = "index_2", columnList = "actived")})
public class Product  implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String barCode;
	private String codePctProd;
	private String lib;
	
	private boolean actived = false;
	public List<Movement> getMovements() {
		return movements;
	}

	public void setMovements(List<Movement> movements) {
		this.movements = movements;
	}

	private double  price;
	private double vatRate;
	private double wauCost;
	private double safetyStock;
	private double minStock;
	private double warningStock;
	private double  colisage;
	
	@ManyToOne
	@JoinColumn(name = "dossage_id")
	//@JoinColumn(name = "dosage")
	@Value("${maxuploadfilesize:'100'}")
	private Dosage dossage;
	
	@ManyToOne
	@JoinColumn(name = "type_id")
	private Type type;
	
	@ManyToOne
	@JoinColumn(name = "dci_id")
	
	private Dci dci;
	
	@ManyToOne
	@JoinColumn(name = "distribution_unit_id")
	
	private DistributionUnit distributionUnit;
	
	@ManyToOne
	@JoinColumn(name = "form_id")
	
	private Form form;
	
	@ManyToOne
	@JoinColumn(name = "pharma_class_id")
	
	private PharmaClass pharmaClass;
	
	@ManyToOne
	@JoinColumn(name = "depot_id")
	
	private Depot depot;
	
	@ManyToOne
	@JoinColumn(name = "presentation_id")

	private Presentation presentation;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "famille_id")
	
	private Family family;


	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "PRODUITS_GENERIQUE", joinColumns = { @JoinColumn(name = "PRODUIT_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "PRODUITGENERIQUE_ID") })
	@JsonIgnore
	private Set<Product> produitsGeneric = new HashSet<Product>();

	@ManyToMany(mappedBy = "produitsGeneric")
	@JsonIgnore
	private Set<Product> produitParent = new HashSet<Product>();
	
	@OneToMany(mappedBy="product")
	//@JsonManagedReference
	//@JsonBackReference
	@JsonIgnore
	private List<Stock> Stocks;

	@OneToMany(mappedBy="product")
	@JsonIgnore
	private List<Lot> Lots;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getCodePctProd() {
		return codePctProd;
	}

	public void setCodePctProd(String codePctProd) {
		this.codePctProd = codePctProd;
	}

	public String getLib() {
		return lib;
	}

	public void setLib(String lib) {
		this.lib = lib;
	}

	public double getVatRate() {
		return vatRate;
	}

	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}

	public double getWauCost() {
		return wauCost;
	}

	public void setWauCost(double wauCost) {
		this.wauCost = wauCost;
	}

	public double getSafetyStock() {
		return safetyStock;
	}

	public void setSafetyStock(double safetyStock) {
		this.safetyStock = safetyStock;
	}

	public double getMinStock() {
		return minStock;
	}

	public void setMinStock(double minStock) {
		this.minStock = minStock;
	}

	public double getWarningStock() {
		return warningStock;
	}

	public void setWarningStock(double warningStock) {
		this.warningStock = warningStock;
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	public Set<Product> getProduitsGeneric() {
		return produitsGeneric;
	}

	public void setProduitsGeneric(Set<Product> produitsGeneric) {
		this.produitsGeneric = produitsGeneric;
	}

	public Set<Product> getProduitParent() {
		return produitParent;
	}

	public void setProduitParent(Set<Product> produitParent) {
		this.produitParent = produitParent;
	}

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Product(Long id, String barCode, String codePctProd, String lib, double vatRate, double wauCost,
			double safetyStock, double minStock, double warningStock,
			Family family, Set<Product> produitsGeneric, Set<Product> produitParent) {
		super();
		this.id = id;
		this.barCode = barCode;
		this.codePctProd = codePctProd;
		this.lib = lib;
		this.vatRate = vatRate;
		this.wauCost = wauCost;
		this.safetyStock = safetyStock;
		this.minStock = minStock;
		this.warningStock = warningStock;
		this.family = family;
		this.produitsGeneric = produitsGeneric;
		this.produitParent = produitParent;
	}

	public Product(Long id, String barCode, String codePctProd, String lib) {
		super();
		this.id = id;
		this.barCode = barCode;
		this.codePctProd = codePctProd;
		this.lib = lib;
	}

	public Dci getDci() {
		return dci;
	}

	public void setDci(Dci dci) {
		this.dci = dci;
	}

	public DistributionUnit getDistributionUnit() {
		return distributionUnit;
	}

	public void setDistributionUnit(DistributionUnit distributionUnit) {
		this.distributionUnit = distributionUnit;
	}
	
	public Depot getDepot() {
		return depot;
	}

	public void setDepot(Depot depot) {
		this.depot = depot;
	}

	public Presentation getPresentation() {
		return presentation;
	}

	public void setPresentation(Presentation presentation) {
		this.presentation = presentation;
	}

	public Form getForm() {
		return form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	public PharmaClass getPharmaClass() {
		return pharmaClass;
	}

	public void setPharmaClass(PharmaClass pharmaClass) {
		this.pharmaClass = pharmaClass;
	}

	public List<Stock> getStocks() {
		return Stocks;
	}

	public void setStocks(List<Stock> stocks) {
		Stocks = stocks;
	}

	public Dosage getDossage() {
		return dossage;
	}

	public void setDossage(Dosage dossage) {
		this.dossage = dossage;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Product(Long id, String barCode, String codePctProd, String lib, double vatRate, double wauCost,
			double safetyStock, double minStock, double warningStock, Dosage dossage, Type type, Dci dci,
			DistributionUnit distributionUnit, Form form, PharmaClass pharmaClass, Depot depot,
			Presentation presentation, Family family, Set<Product> produitsGeneric, Set<Product> produitParent,
			List<Stock> stocks) {
		super();
		this.id = id;
		this.barCode = barCode;
		this.codePctProd = codePctProd;
		this.lib = lib;
		this.vatRate = vatRate;
		this.wauCost = wauCost;
		this.safetyStock = safetyStock;
		this.minStock = minStock;
		this.warningStock = warningStock;
		this.dossage = dossage;
		this.type = type;
		this.dci = dci;
		this.distributionUnit = distributionUnit;
		this.form = form;
		this.pharmaClass = pharmaClass;
		this.depot = depot;
		this.presentation = presentation;
		this.family = family;
		this.produitsGeneric = produitsGeneric;
		this.produitParent = produitParent;
		Stocks = stocks;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Product(Long id, String barCode, String codePctProd, String lib, double vatRate, double wauCost,
			double safetyStock, double minStock, double warningStock, double price, Dosage dossage, Type type, Dci dci,
			DistributionUnit distributionUnit, Form form, PharmaClass pharmaClass, Depot depot,
			Presentation presentation, Family family, Set<Product> produitsGeneric, Set<Product> produitParent,
			List<Stock> stocks) {
		super();
		this.id = id;
		this.barCode = barCode;
		this.codePctProd = codePctProd;
		this.lib = lib;
		this.vatRate = vatRate;
		this.wauCost = wauCost;
		this.safetyStock = safetyStock;
		this.minStock = minStock;
		this.warningStock = warningStock;
		this.price = price;
		this.dossage = dossage;
		this.type = type;
		this.dci = dci;
		this.distributionUnit = distributionUnit;
		this.form = form;
		this.pharmaClass = pharmaClass;
		this.depot = depot;
		this.presentation = presentation;
		this.family = family;
		this.produitsGeneric = produitsGeneric;
		this.produitParent = produitParent;
		Stocks = stocks;
	}

	public boolean isActived() {
		return actived;
	}

	public void setActived(boolean actived) {
		this.actived = actived;
	}

	public Product(Long id, String barCode, String codePctProd, String lib, boolean actived, double vatRate,
			double wauCost, double safetyStock, double minStock, double warningStock, double price, Dosage dossage,
			Type type, Dci dci, DistributionUnit distributionUnit, Form form, PharmaClass pharmaClass, Depot depot,
			Presentation presentation, Family family, Set<Product> produitsGeneric, Set<Product> produitParent,
			List<Stock> stocks) {
		super();
		this.id = id;
		this.barCode = barCode;
		this.codePctProd = codePctProd;
		this.lib = lib;
		this.actived = actived;
		this.vatRate = vatRate;
		this.wauCost = wauCost;
		this.safetyStock = safetyStock;
		this.minStock = minStock;
		this.warningStock = warningStock;
		this.price = price;
		this.dossage = dossage;
		this.type = type;
		this.dci = dci;
		this.distributionUnit = distributionUnit;
		this.form = form;
		this.pharmaClass = pharmaClass;
		this.depot = depot;
		this.presentation = presentation;
		this.family = family;
		this.produitsGeneric = produitsGeneric;
		this.produitParent = produitParent;
		Stocks = stocks;
	}
	
	
	public double getColisage() {
		return colisage;
	}

	public void setColisage(double colisage) {
		this.colisage = colisage;
	}


	@OneToMany(mappedBy="product")
	@JsonBackReference
	private List<Movement> movements ;
	public List<Lot> getLots() {
		return Lots;
	}

	public void setLots(List<Lot> lots) {
		Lots = lots;
	}


	

//
//	public List<PrescriptionLine> getPrescriptionLines() {
//		return prescriptionLines;
//	}
//
//
//
//
//	public void setPrescriptionLines(List<PrescriptionLine> prescriptionLines) {
//		this.prescriptionLines = prescriptionLines;
//	}
//	
	

	

	
}
