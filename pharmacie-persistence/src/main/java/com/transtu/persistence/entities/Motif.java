package com.transtu.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Motif  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2612897487598351416L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String label;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Motif() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Motif(Long id, String label) {
		super();
		this.id = id;
		this.label = label;
	}
	
	

}
