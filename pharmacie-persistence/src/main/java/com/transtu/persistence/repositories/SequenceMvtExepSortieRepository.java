package com.transtu.persistence.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.transtu.persistence.entities.SequenceMvtExepSortie;

public interface SequenceMvtExepSortieRepository extends JpaRepository<SequenceMvtExepSortie, Integer> {


	public SequenceMvtExepSortie findByYear(Integer currentYear);
	public SequenceMvtExepSortie findByLastDate(Date lastDate);
	
	@Query("SELECT max(s.id) FROM SequenceMvtExepSortie s")
    public Integer findTopById();

}
