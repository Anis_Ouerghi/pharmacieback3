package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Role;
import com.transtu.persistence.entities.Utilisateur;

@RepositoryRestResource
public interface RoleRepository extends JpaRepository<Role, Long> { 
	
	public Role findByRole (String role);
	public Role findByUser (Utilisateur utilisateur);

}
