package com.transtu.persistence.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.Role;
import com.transtu.persistence.entities.StockMovement;


@RepositoryRestResource
public interface StockMovementRepository extends JpaRepository<StockMovement, Long>, QueryDslPredicateExecutor{

	public List<StockMovement> findByTypeMvt(String typemvt);
	public List<StockMovement> findByDescriminator(String descriminator);
	public StockMovement findByDistribution(Distribution distribution);
	public List<StockMovement> findBydateMovementBetween(Date start, Date end);
	
	@Query(value = "SELECT Max(s.id) from StockMovement s")
	public Long getTopId ( );
}
