package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	public Customer findByLabel(String label);
	
	

}
