package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.State;

@RepositoryRestResource
public interface StateRepository extends JpaRepository<State, Long>{
	public State findByType(String type);
	
}
