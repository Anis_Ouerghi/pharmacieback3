package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.transtu.persistence.entities.ReferenceType;



public interface ReferenceTypeRepository extends JpaRepository<ReferenceType, Long>, QueryDslPredicateExecutor {

}
