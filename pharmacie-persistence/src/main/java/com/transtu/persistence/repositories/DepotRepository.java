package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.Depot;

public interface DepotRepository extends JpaRepository<Depot, Long> {
	
	Depot findByLabel(String label);
}
