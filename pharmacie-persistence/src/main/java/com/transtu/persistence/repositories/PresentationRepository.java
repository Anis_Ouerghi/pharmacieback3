package com.transtu.persistence.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import com.transtu.persistence.entities.Presentation;

public interface PresentationRepository  extends JpaRepository< Presentation, Long> {
	public Presentation findByLabel(String label);
}
