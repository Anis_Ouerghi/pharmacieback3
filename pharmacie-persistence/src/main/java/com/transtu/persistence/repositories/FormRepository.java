package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.Form;

public interface FormRepository extends JpaRepository<Form, Long> {
	public Form findByLabel(String label);
}
