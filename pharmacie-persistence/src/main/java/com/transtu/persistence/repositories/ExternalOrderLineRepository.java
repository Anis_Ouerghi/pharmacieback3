package com.transtu.persistence.repositories;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.transtu.persistence.entities.ExternalOrder;
import com.transtu.persistence.entities.ExternalOrderLine;
import com.transtu.persistence.entities.Product;

public interface ExternalOrderLineRepository extends JpaRepository<ExternalOrderLine, Long>,QueryDslPredicateExecutor {

	
	public ExternalOrderLine findByProductAndExternalOrder(Product product , ExternalOrder externalOrder);
	public List<ExternalOrderLine> findByExternalOrder(ExternalOrder externalOrder);
	
}
