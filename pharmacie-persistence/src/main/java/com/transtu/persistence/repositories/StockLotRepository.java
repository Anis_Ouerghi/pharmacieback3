package com.transtu.persistence.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Lot;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.State;
import com.transtu.persistence.entities.Stocklot;




@RepositoryRestResource
@Transactional
public interface StockLotRepository  extends JpaRepository<Stocklot, Long> {
	
	public Stocklot findByLot(Lot lot);
	public Stocklot findByLotAndLocation(Lot lot, Location location);
	public Stocklot findByLotAndLocationAndLotProduct(Lot lot, Location location, Product prod);
	
		

}
