package com.transtu.persistence.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.transtu.persistence.entities.SequenceMvtOrdEntre;

public interface SequenceMvtOrdEntreRepository extends JpaRepository<SequenceMvtOrdEntre, Integer> {


	public SequenceMvtOrdEntre findByYear(Integer currentYear);
	public SequenceMvtOrdEntre findByLastDate(Date lastDate);
	
//	@Query("SELECT max(s.id) FROM SequenceMvtOrdEntre s")
//    public Integer findTopById();

}