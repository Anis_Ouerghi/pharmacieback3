
package com.transtu.persistence.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Prescription;
import com.transtu.persistence.entities.PrescriptionLine;
import com.transtu.persistence.entities.Product;

@RepositoryRestResource
public interface PrescriptionLineRepository extends JpaRepository<PrescriptionLine, Long>,QueryDslPredicateExecutor {
	public List<PrescriptionLine> findByPrescription(Prescription prescription);
	public PrescriptionLine findByPrescriptionAndProduct (Prescription prescription, Product product );
	public  List<PrescriptionLine> findByProduct(Product product);
	
	

	
	@Query("SELECT SUM(l.totalQt*p.price) from PrescriptionLine l, Product p"
         	+ " where p.id=l.product.id "
         	+ "and l.prescription.id =:idpresc ")
    public double totalpricePre(@Param ("idpresc") Long idpresc);
	
	
	@Query("select count(*) from Prescription ")
	public double  countPrescriptionLine();
	
}
