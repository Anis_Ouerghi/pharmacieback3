package com.transtu.persistence.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Day;
import com.transtu.persistence.entities.Role;
import com.transtu.persistence.entities.Utilisateur;

@RepositoryRestResource
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

	public List<Utilisateur> findByUsername(String username);
	public List<Utilisateur> findByUsernameContainingIgnoreCase(String username);

	public List<Utilisateur> findByEmail(String email);
	//public Utilisateur findByEmail(String email);
	public Utilisateur findById(Long userId);
	public Boolean findByRolesRole(Role role);

//	public List<Utilisateur> findByUserDaysDayDayTypeIdAndUserDaysDayDate( Date dateDay,Long id);
//	
//	public List<Utilisateur> findByUserDaysDay( Day day);
	
	//public List<Utilisateur> findByUserDaysDayDayTypeIdAndUserDaysDayDate( @DateTimeFormat (iso = ISO.NONE)@Param("dateDay") Date dateDay, @Param ("id")Long id);

	//public User findById(Long id);
}
