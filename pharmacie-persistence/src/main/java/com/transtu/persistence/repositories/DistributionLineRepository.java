package com.transtu.persistence.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Depot;
import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.DistributionLine;
import com.transtu.persistence.entities.Prescription;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.ProductResp;


@RepositoryRestResource
public interface DistributionLineRepository extends JpaRepository<DistributionLine, Long>, QueryDslPredicateExecutor {

	public List<DistributionLine> findByDistribution(Distribution distribution);
	public List<DistributionLine> findByDistributionAndProduct(Distribution distribution,Product product);
	public List<DistributionLine> findBydistribution_Prescription(Prescription prescription);
	public List<DistributionLine> findByProduct(Product product);
	public List<DistributionLine> findByDistributionDateDistBetween(Date start, Date end);
	public List<DistributionLine> findByDistribution_PrescriptionAndProduct(Prescription prescription,Product product);
	public List<DistributionLine> findByDistributionPrescriptionAndProduct (Prescription prescription,Product product);

	
	@Query("SELECT SUM(l.deliveredQt+l.missingQt) from DistributionLine l, Distribution p"
         	+ " where p.id=l.distribution.id "
         	+ "and p.dateDist >= :start "
         	+ "and p.dateDist <= :end "
         	+ "and  l.product.id =:idprod ")
    public double smeqte1(@Param ("idprod") Long depotid,@Param("start") Date dateDist1 ,@Param("end") Date dateDist2 );
	
	@Query("SELECT SUM(l.totalQt) from PrescriptionLine l, Prescription p"
         	+ " where p.id=l.prescription.id "
         	+ "and p.prescriptionDate >= :start "
         	+ "and p.prescriptionDate <= :end "
         	+ "and  l.product.id =:idprod ")
    public double smeqte(@Param ("idprod") Long depotid,@Param("start") Date dateDist1 ,@Param("end") Date dateDist2 );
	
	
	
	@Query("SELECT  new com.transtu.persistence.entities.ProductResp( "
			+ "l.product.id, l.product.lib, l.product.codePctProd,"
			+ "l.product.vatRate, l.product.price, l.product.colisage,"
			+ " l.product.depot, l.product.presentation,"
			+ " SUM(l.totalQt) as totalQt)  from PrescriptionLine l, Prescription p"
         	+ " where p.id=l.prescription.id "
         	+ "and p.prescriptionDate >= :start "
         	+ "and p.prescriptionDate <= :end "
         	+ " group by l.product.id, l.product.lib,l.product.codePctProd, l.product.vatRate, l.product.depot,"
         	+ " l.product.price, l.product.colisage,"
         	+ " l.product.presentation")
    public List<ProductResp> prodbyDate(@Param("start") Date dateDist1 ,@Param("end") Date dateDist2 );
	
	
	@Query("SELECT  new com.transtu.persistence.entities.ProductResp("
			+ "l.product.id, l.product.lib, l.product.codePctProd,"
			+ "l.product.vatRate, l.product.price, l.product.colisage,"
			+ " l.product.depot, l.product.presentation,"
			+ " SUM(l.totalQt) as totalQt)  from PrescriptionLine l, Prescription p"
         	+ " where p.id=l.prescription.id and l.product.depot.id = :iddep and p.prescriptionDate >= :start "
         	+ "and p.prescriptionDate <= :end "
         	
         	+ " group by l.product.id, l.product.lib,l.product.codePctProd,  l.product.vatRate, l.product.depot,"
         	+ " l.product.price, l.product.colisage,"
         	+ " l.product.presentation order by l.product.lib asc")
    public List<ProductResp> prodbyDepAndDate(@Param("iddep") Long iddep ,@Param("start") Date dateDist1 ,@Param("end") Date dateDist2 );
	
	
	
	
	@Query("select count(*) from Distribution ")
	public double  countDistributionLine();
}
