package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;



import com.transtu.persistence.entities.Type;

public interface TypeRepository extends JpaRepository<Type, Long>  {
	
	public Type findByLabel(String Label);
	

}
