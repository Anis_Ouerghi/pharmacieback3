package com.transtu.persistence.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.InternalOrder;
import com.transtu.persistence.entities.InternalOrderLine;
import com.transtu.persistence.entities.Product;

@RepositoryRestResource
public interface InternalOrderLineRepository extends JpaRepository<InternalOrderLine, Long>,QueryDslPredicateExecutor {
	public List<InternalOrderLine> findByInternalOrder(InternalOrder internalOrder);
	public InternalOrderLine findByInternalOrderAndProduct(InternalOrder internalOrder, Product product);
}
