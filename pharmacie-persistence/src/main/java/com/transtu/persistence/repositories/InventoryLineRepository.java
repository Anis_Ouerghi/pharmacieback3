package com.transtu.persistence.repositories;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import com.transtu.persistence.entities.Inventory;
import com.transtu.persistence.entities.InventoryLine;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Lot;
import com.transtu.persistence.entities.Product;

public interface InventoryLineRepository  extends JpaRepository<InventoryLine, Long>, QueryDslPredicateExecutor { 
	
	
	 public LinkedHashSet<InventoryLine> findByInventory(Inventory inventory ,Sort sort);
	 public List<InventoryLine> findByInventory(Inventory inventory);
	 @Query("select SUM(i.inventQte) from InventoryLine i  where i.product.id =:id")
	 public double  getSumInventoryprod(@Param("id") Long id);
	 public InventoryLine findByProductIdAndInventory( Long productId, Inventory inventory);
	 public InventoryLine findByProductIdAndInventoryAndId( Long productId, Inventory inventory, Long id);
	 public InventoryLine findByProductIdAndInventoryDateInvEqualsAndInventoryLocationId( Long productId, Date dateinventory, Location location);
	 public InventoryLine findByProductIdAndInventoryDateInvAndInventoryLocationId( Long productId, Date dateinventory,Long LocationId);
	 public List<InventoryLine> findByProductId( Long productId);
	 public List<InventoryLine> findByProduct( Product product);
	 public List<InventoryLine>  findByInventoryAndProduct(Inventory inventory,Product product);
	 @Query("select SUM(i.inventQte) from InventoryLine i  where i.product.id =:id and i.inventory.id=:inventoryId")
	 public double  getSumInventoryprodDep(@Param("id") Long id,@Param("inventoryId") Long inventoryId);
	 public InventoryLine  findByInventoryDateInv(Date dateinventory);
	
	 public InventoryLine findByProductIdAndLotIdAndInventoryId( Long prodId, Long lotId, Long inventoryId );
	 public InventoryLine findByLotIdAndInventory(Long lot, Inventory inventory );

}
