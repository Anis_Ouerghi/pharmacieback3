package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.SequenceReception;

public interface SequenceReceptionRepository extends JpaRepository<SequenceReception, Integer> {
	public SequenceReception findByYear(Integer currentYear);
}
