package com.transtu.persistence.repositories;

import java.awt.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.Dci;

public interface DciRepository extends JpaRepository<Dci, Long> {
	public Dci findByLabel(String label);
	
}
