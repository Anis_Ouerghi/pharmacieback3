package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.transtu.persistence.entities.Motif;

public interface MotifRepository extends JpaRepository<Motif, Long>, QueryDslPredicateExecutor {
	
	public Motif findByLabel(String label);

}
