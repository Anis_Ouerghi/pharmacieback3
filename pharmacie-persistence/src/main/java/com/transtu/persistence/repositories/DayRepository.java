package com.transtu.persistence.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.Day;
import com.transtu.persistence.entities.DayType;

public interface DayRepository extends JpaRepository<Day, Long> {
	public List<Day> findByStateAndDayType(boolean state, DayType dayType);
	public Day findByDateEquals(Date date);
	public List<Day> findByDateAfter(Date date) ;
	public List<Day> findByDayType( DayType dayType);
	public Day findByDayTypeAndDate( DayType dayType, Date date);
	//public Day findByMaxDate();
}
