package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.District;
import com.transtu.persistence.entities.Doctor;


@RepositoryRestResource
public interface DoctorRepository extends JpaRepository<Doctor, Long> { 
	
	public Doctor findByName(String name);

}
