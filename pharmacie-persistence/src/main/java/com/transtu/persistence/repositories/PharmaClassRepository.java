package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;


import com.transtu.persistence.entities.PharmaClass;


public interface PharmaClassRepository extends JpaRepository< PharmaClass, Long> {
	public PharmaClass findByLabel(String label);
}
