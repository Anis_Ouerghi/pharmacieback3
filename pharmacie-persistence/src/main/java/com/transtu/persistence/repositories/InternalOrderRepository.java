package com.transtu.persistence.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.InternalOrder;
import com.transtu.persistence.entities.State;


@RepositoryRestResource
public interface InternalOrderRepository extends JpaRepository<InternalOrder, Long>, QueryDslPredicateExecutor{

	

	@Query("select max(i.dateOrder)	as max_date from InternalOrder i")
	public Date findOneByMaxDateOrder();
	
	@Query("select max(i.Id)as max_Id from InternalOrder i")
	public Integer findOneByMaxIdOrder();
	
	public InternalOrder findByState(State state);
	
	
	

}
