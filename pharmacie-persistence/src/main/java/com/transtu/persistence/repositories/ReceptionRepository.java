package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Reception;
import com.transtu.persistence.entities.State;



@RepositoryRestResource
public interface ReceptionRepository extends JpaRepository<Reception, Long>, QueryDslPredicateExecutor{
public Reception findByNumReception(String numReception);
public Reception findByState(State typesate);
//public Reception findByExternalDelivery(ExternalDelivery externalDelivery);

}
