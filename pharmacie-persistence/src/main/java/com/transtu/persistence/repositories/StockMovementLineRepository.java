package com.transtu.persistence.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.StockMovement;
import com.transtu.persistence.entities.StockMovementLine;

public interface StockMovementLineRepository extends JpaRepository<StockMovementLine, Long>, QueryDslPredicateExecutor {
	
	public List<StockMovementLine> findByProduct(Product product);
	
	public List<StockMovementLine> findByStockMovement(StockMovement stockMovement);
	public List<StockMovementLine> findByStockMovementTypeMvtAndProduct(String TypeMvt , Product product);
	public List<StockMovementLine> findByStockMovementTypeMvtAndProductAndStockMovementLocation(String TypeMvt , Product product, Location location);
	
}
