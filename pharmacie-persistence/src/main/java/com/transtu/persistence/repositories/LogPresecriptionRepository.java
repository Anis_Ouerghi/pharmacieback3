package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.LogPresecription;

public interface LogPresecriptionRepository extends JpaRepository<LogPresecription, Long>{ 

}
