
package com.transtu.persistence.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.Stock;


@RepositoryRestResource
@Transactional
public interface StockRepository extends JpaRepository<Stock, Long> {
	
	
	public List<Stock> findByLocation(Location location);
	public List<Stock> findByProductId(Long prodId);
	public List<Stock> findByProduct(Product product);
	public Stock findByProductAndLocation(Product product, Location location);
	public Stock findByProductIdAndLocationId( Long prodId, Long locationId);
	@Query("select s from Stock s ,ControlStock c, Product p"
			+ " where c.stock.id=s.id and s.location.id=1 and "
			+ "s.product.id=p.id and s.quantity <= c.minStock")
	public List<Stock> getListStockBreak();
	@Query("select s from Stock s ,ControlStock c, Product p"
			+ " where c.stock.id=s.id and "
			+ "s.product.id=p.id ")
	public List<Stock> getListTotalStockBreak();

//	@Query(value = "SELECT sum(s.quantity) from Stock s,ControlStock c, Product p"
//			+ " WHERE c.stock.id=s.id and s.product.id=p.id and s.product.id = :produitId "
//			+ " group by s.product.id ")
//	public double getSumQteStock (@Param ("produitId") Long produitId );
	
	@Query(value = "SELECT s.quantity from Stock s"
			+ " WHERE s.product.id = :produitId "
			+ "and s.location.id = :locationId")
	public double getSumQteStockDep (@Param ("produitId") Long produitId, @Param ("locationId") Long locationId );

	@Query(value = "SELECT sum(s.quantity) from Stock s"
			+ " WHERE s.product.id = :produitId "
			+ " group by s.product.id ")
	public double getSumQteStock (@Param ("produitId") Long produitId );


}
