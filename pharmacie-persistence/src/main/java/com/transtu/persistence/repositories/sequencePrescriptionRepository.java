package com.transtu.persistence.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.sequencePresecription;

public interface sequencePrescriptionRepository extends 
JpaRepository<sequencePresecription, Integer> {
	
	public sequencePresecription findByLasteDate (Date lasteDate);
	public sequencePresecription findByYear(Integer currentYear);

//	@Query("SELECT max(s.id) FROM sequencePresecription s")
//    public Integer findTopById();

}


