package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.Dosage;

public interface DossageRepository extends JpaRepository<Dosage, Long> {
	public Dosage findByLabel(String label);
	
}
