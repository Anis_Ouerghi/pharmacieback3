package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.Day;
import com.transtu.persistence.entities.UserDay;
import com.transtu.persistence.entities.Utilisateur;



public interface UserDayRepository extends JpaRepository<UserDay, Long> {
	
	UserDay findByDayAndUtilisateur(Day day, Utilisateur utilisateur);

}
