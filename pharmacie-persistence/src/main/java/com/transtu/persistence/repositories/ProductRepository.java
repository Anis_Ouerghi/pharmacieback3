
package com.transtu.persistence.repositories;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Dci;
import com.transtu.persistence.entities.Depot;
import com.transtu.persistence.entities.DistributionUnit;
import com.transtu.persistence.entities.Dosage;
import com.transtu.persistence.entities.Family;
import com.transtu.persistence.entities.Form;
import com.transtu.persistence.entities.PharmaClass;
import com.transtu.persistence.entities.Presentation;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.Type;
//import com.transtu.core.product.LightProduct;

@RepositoryRestResource
@Transactional
public interface ProductRepository extends JpaRepository<Product, Long> {
	
	
	public List<Product>  findByBarCode(String BarCode);
	public List<Product>  findBycodePctProd(String BarCode);
	public List<Product> findByLibContainingIgnoreCase(String lib);
	public Product findByLib(String lib);
	public List<Product> findByLibStartingWithIgnoreCase(String name); 
	public List<Product> findByLibStartingWithIgnoreCaseAndActived(String name, Boolean activated);
	public List<Product> findByLibStartingWithIgnoreCaseAndDepot(String name, Depot depot);
	public List<Product> findByLibStartingWithIgnoreCaseAndType(String name, Type type);
	//@Query("select p.id, p.barCode, p.lib, p.codePctProd, p.actived from Product p where UPPER(p.lib) LIKE UPPER( '%' + :beggingLabel) AND p.actived = :activated ORDER BY p.lib ASC")
	//public List<LightProduct> getProductsStartWith(@Param("beggingLabel")String beggingLabel, @Param("activated") Boolean activated);
	public List<Product> findDistinctByActived(Boolean Actived,Sort sort);
//	@Query("SELECT p FROM Product p WHERE p.lib  ORDER BY p.lib ASC")
//	public List<Product> findByLibStartingWithIgnoreCase(String name, Boolean activated );
	public List<Product> findByIdIn(List<Long> listIds);
	@Query("select p from Product p, Stock s"
			+ " where p.id=s.product.id  group by p")
	public List<Product> getListProductEventful();
	public List<Product> findByDci(Dci dci);
	public List<Product> findByDepotAndActived(Depot depot, Boolean boolean1);
	public List<Product> findByDepot(Depot depot);
	public List<Product> findByDistributionUnit(DistributionUnit distributionUnit);
	public List<Product> findByDossage(Dosage dosage);
	public List<Product> findByFamily(Family family);
	public List<Product> findByForm(Form form);
	public List<Product> findByPharmaClass(PharmaClass pharmaClass);
	public List<Product> findByPresentation(Presentation presentation);
	public Product findByPresentationAndId(Presentation presentation, Long id);
	public List<Product> findByType(Type type);
	@Query("select p from Product p, Stock s"			
			+ " where p.barCode = :barCode")	
    public List <Product> findByCodBare(@Param ("barCode") String barCode);			
	@Query("SELECT DISTINCT (p) from Product p, Stock s"
			+ " where p.id=s.product.id and s.quantity>0 order by p.lib")
	public List<Product> getListProductSupp();
	@Query("SELECT DISTINCT (p) from Product p, DistributionLine l, Distribution d"
         	+ " where p.id=l.product.id and p.depot.id = :depotid and d.id=l.distribution.id and d.dateDist >= :start and d.dateDist <= :end"
         	+ " order by p.lib")
    public List<Product> getusedProductDep1(@Param ("depotid") Long depotid,@Param("start") Date dateDist1 ,@Param("end") Date dateDist2);

     @Query("SELECT DISTINCT (p) from Product p, DistributionLine l ,Distribution d"
	+ " where p.id=l.product.id and d.id=l.distribution.id and d.dateDist >= :start and d.dateDist <= :end")
     public List<Product> getusedProduct1(@Param("start") Date dateDist1 ,@Param("end") Date dateDist2);
	
     
 	@Query("SELECT DISTINCT (p) from Product p, PrescriptionLine l, Prescription d"
          	+ " where p.id=l.product.id and p.depot.id = :depotid and d.id=l.prescription.id and d.prescriptionDate >= :start and d.prescriptionDate <= :end"
          	+ " order by p.lib")
     public List<Product> getusedProductDep(@Param ("depotid") Long depotid,@Param("start") Date dateDist1 ,@Param("end") Date dateDist2);

      @Query("SELECT DISTINCT (p) from Product p, PrescriptionLine l ,Prescription d"
 	+ " where p.id=l.product.id and d.id=l.prescription.id and d.prescriptionDate >= :start and d.prescriptionDate <= :end")
      public List<Product> getusedProduct(@Param("start") Date dateDist1 ,@Param("end") Date dateDist2);
      
      
      
      @Query("SELECT COUNT (DISTINCT p)  from Product p, PrescriptionLine l ,Prescription d"
    		 	+ " where p.id=l.product.id and d.id=l.prescription.id and d.prescriptionDate >= :start and d.prescriptionDate <= :end")
      public int gettotalUsedProd(@Param("start") Date dateDist1 ,@Param("end") Date dateDist2);
     
   

     //********************************list des produits Mvt**************************************************//
     
      
      @Query("SELECT DISTINCT (p) from Product p, ReceptionLine l ,Reception d"
    		 	+ " where p.id=l.product.id and d.id=l.reception.id ORDER BY p.lib ASC ")
    		      public List<Product> x();
     
      
//      @Query("SELECT DISTINCT (p) from Product p, StockMovementLine stkml"
//    		 	+ " where  p.id=stkml.product.id ORDER BY p.lib ASC ")
//    		      public List<Product> x();
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
//     @Query("SELECT SUM(l.deliveredQt+l.missingQt) from DistributionLine l, Distribution p"
//          	+ " where p.id=l.distribution.id "
//          	+ "and p.dateDist >= :start "
//          	+ "and p.dateDist <= :end "
//          	+ "and  l.product.id =:idprod ")
//     public double getSumWantedQt(@Param ("idprod") Long depotid,@Param("start") Date dateDist1 ,@Param("end") Date dateDist2 );
	
//	@Query(value = "SELECT DISTINCT (rl.product)"
//			+ " from ReceptionLine rl, "
//			+ "Reception r, ExternalOrder e"
//			+ " WHERE r.externalOrder.id= e.id "
//			+ "and r.id = rl.reception.id "
//			+ "and e.id= :id")
//	public List <Product> findByExternOrder(@Param ("id") Long id);
	
//	@Query(value = "SELECT sum(rl.receptionQte)"
//			+ " from ReceptionLine rl, "
//			+ "Reception r, ExternalOrder e"
//			+ " WHERE r.externalOrder.id= e.id "
//			+ "and r.id = rl.reception.id "
//			+ "and e.id= :idorder and rl.product.id= :idprod")
//	public Long findSumQteProductByExternOrder(@Param ("idorder") Long idorder,
//			@Param ("idprod") Long idprod );
	
	
}