//package com.transtu.persistence.repositories;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.querydsl.QueryDslPredicateExecutor;
//
//import com.transtu.persistence.entities.ExternalDelivery;
//import com.transtu.persistence.entities.State;
//
//public interface ExternalDeliveryRepository extends JpaRepository<ExternalDelivery, Long>,QueryDslPredicateExecutor{
//	public ExternalDelivery findByNumExternalDelivery(String numExternalDelivery);
//	public ExternalDelivery findByStateAndNumExternalDelivery(State state,String numExternalDelivery);
//}

