package com.transtu.persistence.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.Prescription;
import com.transtu.persistence.entities.Utilisateur;

@RepositoryRestResource
public interface DistributionRepository extends JpaRepository<Distribution, Long> , QueryDslPredicateExecutor{

	
	public List<Distribution> findByPrescription(Prescription prescription);
	public List<Distribution>findByPrescription_AgentId(String AgentId);
	public List<Distribution> findByDateDistBetween(Date start , Date end);
	public Distribution findByDistributionLinesId(Long id);
	public List<Distribution> findByDateDistAndPrescriptionAgentId(Date start , Date end, String agentId);
	public List<Distribution> findByPrescriptionAgentIdAndDateDistBetween( String agentId,Date start , Date end);
	public Distribution findByDistributionNumber(String distNum);
	public List<Distribution> findByDateDistBetweenAndUtilisateur(Date start , Date end, Utilisateur utilisateur);
	
}
