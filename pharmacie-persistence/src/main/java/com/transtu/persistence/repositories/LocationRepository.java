package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Location;

@RepositoryRestResource
public interface LocationRepository extends JpaRepository<Location, Long> {

	public Location findByLib(String libelle);

}
