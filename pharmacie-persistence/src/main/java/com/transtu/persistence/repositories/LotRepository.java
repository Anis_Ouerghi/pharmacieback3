package com.transtu.persistence.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Lot;
import com.transtu.persistence.entities.Product;



	
	
	
	@RepositoryRestResource
	@Transactional
	public interface LotRepository  extends JpaRepository<Lot, Long> {
		
		public List<Lot> findByState(Boolean state);
		public Lot findByLabel(String label);
		public Lot findByProduct(Product product);
		public Lot findByProductAndLabel(Product product,String label);
		


}

	

