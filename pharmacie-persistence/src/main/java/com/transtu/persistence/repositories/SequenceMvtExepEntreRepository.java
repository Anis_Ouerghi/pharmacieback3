package com.transtu.persistence.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.transtu.persistence.entities.SequenceMvtExepEntre;
import com.transtu.persistence.entities.SequenceMvtExepSortie;
public interface SequenceMvtExepEntreRepository extends JpaRepository<SequenceMvtExepEntre, Integer> {


	public SequenceMvtExepEntre findByYear(Integer currentYear);
	public SequenceMvtExepEntre findByLastDate(Date lastDate);
	
	@Query("SELECT max(s.id) FROM SequenceMvtExepEntre s")
    public Integer findTopById();
}