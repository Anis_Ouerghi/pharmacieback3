package com.transtu.persistence.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.SequenceOrder;

public interface SequenceOrderRepository extends JpaRepository<SequenceOrder, Integer>{
	public SequenceOrder findByYear(Integer currentYear);

}
