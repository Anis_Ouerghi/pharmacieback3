package com.transtu.persistence.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.DistributionState;

@RepositoryRestResource
public interface DistributionStateRepository extends JpaRepository<DistributionState, Long> {
	
	List<DistributionState> findByDistribution(Distribution distribution);
}
