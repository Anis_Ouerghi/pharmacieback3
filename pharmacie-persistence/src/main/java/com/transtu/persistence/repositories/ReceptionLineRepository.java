package com.transtu.persistence.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import com.transtu.persistence.entities.ExternalOrder;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.Reception;
import com.transtu.persistence.entities.ReceptionLine;

public interface ReceptionLineRepository  extends JpaRepository<ReceptionLine, Long>, QueryDslPredicateExecutor{

public  ReceptionLine findByProductAndReception(Product product, Reception reception );
//public  ReceptionLine findByReceptionId(long id );
	
public  List<ReceptionLine> findByReceptionExternalOrder(ExternalOrder externalOrder );

	
@Query("SELECT rl from ReceptionLine rl,  Reception r , Product p"
     	+ " where r.id=rl.reception.id "
     	+ "and r.externalOrder.id =:idpresc "
     	+ "and rl.product.id=p.id and r.state.id=2 ORDER BY rl.product.lib ASC ")
public List<ReceptionLine>  findByReceptionExternalOrder1(@Param ("idpresc") Long idpresc);
	
	
@Query("SELECT sum(rl.receptionQte) from ReceptionLine rl,  Reception r "
     	+ "where r.id=rl.reception.id "
     	+ "and r.externalOrder.id =:idpresc "
     	+ "and rl.product.id=:idprod  GROUP BY rl.product.lib")
public double sumrecepQTE(@Param ("idpresc") Long idpresc,@Param ("idprod") Long idprod);
 
	
}
	
	
	
	
	
 
	

