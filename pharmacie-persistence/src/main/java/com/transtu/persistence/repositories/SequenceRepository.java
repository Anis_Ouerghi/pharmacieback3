package com.transtu.persistence.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Sequence;

@RepositoryRestResource
public interface SequenceRepository extends JpaRepository<Sequence, Integer> {

	public Sequence findByYear(Integer currentYear);
	public Sequence findByLastDate(Date lastDate);
	
//	@Query("SELECT max(s.id) FROM Sequence s")
//    public Integer findTopById();

}