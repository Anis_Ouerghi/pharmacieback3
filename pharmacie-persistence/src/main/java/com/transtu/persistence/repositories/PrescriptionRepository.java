package com.transtu.persistence.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.Prescription;
import com.transtu.persistence.entities.PrescriptionLine;
import com.transtu.persistence.entities.PresecriptionRepJasper;

@RepositoryRestResource
public interface PrescriptionRepository extends JpaRepository<Prescription , Long>, QueryDslPredicateExecutor {

	public List<Prescription> findByAgentIdAndId(String agentId, Long id);

	public Prescription findByPrescriptionLineIn(PrescriptionLine prescriptionLine);
	
	public Prescription findByDistributionIn(Distribution distribution);
	
	public List<Prescription> findByDoctorId(Long id);
	
	
	
	
	


	
	
	
	@Query("SELECT new com.transtu.persistence.entities.PresecriptionRepJasper(l.prescription.prescriptionNumber,l.prescription.prescriptionDate,"
			+ "SUM(l.totalQt*p.price) as montantOrd, l.prescription.agentId, l.prescription.doctor.specialty)"
			+ " from PrescriptionLine l, Product p"
         	+ " where p.id=l.product.id "
         	+ "and l.prescription.prescriptionDate <=:end "
         	+ "and l.prescription.prescriptionDate >=:start "
         	+ " group by l.prescription.prescriptionNumber,l.prescription.prescriptionDate,l.prescription.agentId, l.prescription.doctor.specialty order by l.prescription.prescriptionDate")
	
	public List<PresecriptionRepJasper> findByPreMaldiDateBetween(@Param ("start") Date start, @Param ("end") Date end);
	
	
	
	
	
	@Query("SELECT new com.transtu.persistence.entities.PresecriptionRepJasper(l.prescription.prescriptionNumber,l.prescription.prescriptionDate,"
			+ "SUM(l.totalQt*p.price) as montantOrd, l.prescription.agentId, l.prescription.doctor.specialty)"
			+ " from PrescriptionLine l, Product p"
         	+ " where p.id=l.product.id "
         	+ "and l.prescription.prescriptionDate <=:end "
         	+ "and l.prescription.prescriptionDate >=:start "
         	+ "and l.prescription.doctor.id =:id  "
         	+ "group by l.prescription.prescriptionNumber,l.prescription.prescriptionDate,l.prescription.agentId, "
         	+ "l.prescription.doctor.specialty ,l.prescription.agentSituation order by l.prescription.prescriptionDate")
	
	
	public List<PresecriptionRepJasper> findByPresDateBetweenAndDoctorId(@Param ("id") Long id  ,@Param ("start") Date start, @Param ("end") Date end );
	
	
	
	
	
	@Query("SELECT new com.transtu.persistence.entities.PresecriptionRepJasper(l.prescription.prescriptionNumber,l.prescription.prescriptionDate,"
			+ "SUM(l.totalQt*p.price) as montantOrd, l.prescription.agentId, l.prescription.agentSituation)"
			+ " from PrescriptionLine l, Product p"
         	+ " where p.id=l.product.id "
         	+ "and l.prescription.prescriptionDate <=:end "
         	+ "and l.prescription.prescriptionDate >=:start "
         	+ " group by l.prescription.prescriptionNumber,l.prescription.prescriptionDate,l.prescription.agentId, l.prescription.agentSituation order by l.prescription.prescriptionDate")
	
	public List<PresecriptionRepJasper> findByPrescrStDateBetween(@Param ("start") Date start, @Param ("end") Date end);
	
	
	
	@Query("SELECT new com.transtu.persistence.entities.PresecriptionRepJasper(l.prescription.prescriptionNumber,l.prescription.prescriptionDate,"
			+ "SUM(l.totalQt*p.price) as montantOrd, l.prescription.agentId, l.prescription.agentSituation)"
			+ " from PrescriptionLine l, Product p"
         	+ " where p.id=l.product.id "
         	+ "and l.prescription.prescriptionDate <=:end "
         	+ "and l.prescription.prescriptionDate >=:start "
         	+ "and  l.prescription.agentSituation =:situation  "
         	+ "group by l.prescription.prescriptionNumber,l.prescription.prescriptionDate,l.prescription.agentId, l.prescription.agentSituation order by l.prescription.prescriptionDate")
	
	public List<PresecriptionRepJasper>findByPresDateBetweenAndAgentSituation(@Param ("start") Date start, @Param ("end") Date end ,
			@Param ("situation") String situation);
	

}
