
package com.transtu.persistence.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.transtu.persistence.entities.Movement;

@RepositoryRestResource
public interface MovementRepository extends JpaRepository<Movement, Long> {


	
	public List<Movement> findByTypeContainingIgnoreCase(String type);
	public List<Movement> findByDate(Date Date);
	public List<Movement> findByDateBetween(Date start , Date end);
	
}
