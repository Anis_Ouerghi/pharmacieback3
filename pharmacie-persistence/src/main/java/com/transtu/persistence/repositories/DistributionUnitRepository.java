package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.DistributionUnit;

public interface DistributionUnitRepository extends JpaRepository<DistributionUnit, Long>{

	DistributionUnit findByLabelContainingIgnoreCase(String label);
}
