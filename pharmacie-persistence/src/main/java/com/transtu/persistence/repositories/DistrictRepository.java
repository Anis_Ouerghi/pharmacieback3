package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.District;

@RepositoryRestResource(collectionResourceRel = "district", path = "district")
public interface DistrictRepository extends JpaRepository<District, Long> {

	public District findByLib(String libelle);


}
