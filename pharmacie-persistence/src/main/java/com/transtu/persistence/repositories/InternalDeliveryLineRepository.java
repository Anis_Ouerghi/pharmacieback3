package com.transtu.persistence.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.transtu.persistence.entities.InternalDelivery;
import com.transtu.persistence.entities.InternalDeliveryLine;
import com.transtu.persistence.entities.Lot;
import com.transtu.persistence.entities.Product;

public interface InternalDeliveryLineRepository extends JpaRepository<InternalDeliveryLine, Long>, QueryDslPredicateExecutor{

	public InternalDeliveryLine findByProductAndInternalDelivery(Product product , InternalDelivery internalDelivery);
	public List<InternalDeliveryLine> findByProductAndInternalDeliveryAndLot(Product product , InternalDelivery internalDelivery, Lot lot);
	public List<InternalDeliveryLine> findByInternalDelivery(InternalDelivery internalDelivery);
	public List<InternalDeliveryLine> findByProductAndLot(Product product,Lot lot);

}
