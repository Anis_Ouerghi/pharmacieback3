package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.Family;

@RepositoryRestResource(path = "family")
public interface FamilyRepository extends JpaRepository<Family, Long> {
	public Family findByLibelle (String libelle);

}
