package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.transtu.persistence.entities.InternalDelivery;
import com.transtu.persistence.entities.InternalOrder;
import com.transtu.persistence.entities.Reception;
import com.transtu.persistence.entities.State;

public interface InternalDeliveryRepository extends JpaRepository<InternalDelivery, Long> , QueryDslPredicateExecutor{
	
	
	public InternalDelivery findByNumDelivery(String Deliverynum);
	public InternalOrder findByState (State state);
	
}
