package com.transtu.persistence.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.transtu.persistence.entities.SequenceMvtOrdSorti;

public interface SequenceMvtOrdSortRepository extends JpaRepository<SequenceMvtOrdSorti, Integer> {


	public SequenceMvtOrdSorti findByYear(Integer currentYear);
	public SequenceMvtOrdSorti findByLastDate(Date lastDate);
	
	@Query("SELECT max(s.id) FROM SequenceMvtOrdSorti s")
    public Integer findTopById();

}