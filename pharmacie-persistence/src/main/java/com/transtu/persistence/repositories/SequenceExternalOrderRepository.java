package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.SequenceExternalOrder;

public interface SequenceExternalOrderRepository extends JpaRepository<SequenceExternalOrder, Integer> {
	public SequenceExternalOrder findByYear(Integer currentYear);
}
