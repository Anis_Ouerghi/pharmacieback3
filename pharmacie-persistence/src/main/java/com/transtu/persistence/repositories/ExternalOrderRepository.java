package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.transtu.persistence.entities.ExternalOrder;

public interface ExternalOrderRepository extends JpaRepository<ExternalOrder, Long>,QueryDslPredicateExecutor{

	//ExternalOrder findByExternalDelivery (ExternalDelivery externalDelivery);
	
}
