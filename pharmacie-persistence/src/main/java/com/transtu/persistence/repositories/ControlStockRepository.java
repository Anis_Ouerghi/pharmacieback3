package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.ControlStock;

public interface ControlStockRepository extends JpaRepository<ControlStock, Long> {

}
