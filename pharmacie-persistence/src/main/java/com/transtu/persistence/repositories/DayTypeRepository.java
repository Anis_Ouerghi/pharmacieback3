

package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.transtu.persistence.entities.DayType;

@RepositoryRestResource
public interface DayTypeRepository extends JpaRepository<DayType, Long>{
	public DayType findByLibelle(String libelle);
	
}
