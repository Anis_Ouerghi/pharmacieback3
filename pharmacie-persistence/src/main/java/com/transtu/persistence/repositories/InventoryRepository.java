package com.transtu.persistence.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.Inventory;


public interface InventoryRepository extends JpaRepository<Inventory, Long> { 
	
	 public Inventory findByDateInvAndLocationId(Date dateinventory,Long productId);

}
