package com.transtu.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transtu.persistence.entities.SequenceDelivery;

public interface SequenceDeliveryRepository extends JpaRepository<SequenceDelivery, Integer>{
	public SequenceDelivery findByYear(Integer currentYear);

}
