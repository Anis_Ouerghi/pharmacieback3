package com.transtu.service;

import org.springframework.mail.MailException;

public interface MailService {

	void sendMail(String covere) throws MailException;

}
