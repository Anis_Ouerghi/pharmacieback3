package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.AddExceptionalMovement;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.MovementResponse;
import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.InternalDelivery;
//import com.transtu.persistence.entities.ExceptionalMovement;
import com.transtu.persistence.entities.Movement;
import com.transtu.persistence.entities.Reception;

public interface ExceptionalMovementService {
	
	public MessageResponse addExceptionalMovement(AddExceptionalMovement addExceptionalMovement);
	public List<MovementResponse> getListExceptionalMovement();
	public MessageResponse updateExceptionalMovement(Long id, String label); 
	public MessageResponse deleteExceptionalMovement(Long id);
	
}
