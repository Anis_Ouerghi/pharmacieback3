package com.transtu.service;

import java.util.List;

import com.transtu.persistence.entities.Location;

public interface LocationService {
	public List<Location> findAllLocations();
}
