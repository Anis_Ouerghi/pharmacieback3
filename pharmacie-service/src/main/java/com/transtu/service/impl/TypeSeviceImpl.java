package com.transtu.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Presentation;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.Type;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.TypeRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.TypeService;

@Service
public class TypeSeviceImpl  implements TypeService{

	
	private final static Logger logger = LoggerFactory.getLogger(TypeSeviceImpl.class);
	@Autowired(required = true)
	TypeRepository typeRepository;
	@Autowired(required = true)
	ProductRepository productRepository;
	
	@Override
	public MessageResponse addtype(String label) {
		logger.info("___________Add Type_______________");
		Type type = typeRepository.findByLabel(label);
		if(type != null ){
			RestExceptionCode code = RestExceptionCode.TYPE_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Type types = new Type() ;
		types.setLabel(label);
		this.typeRepository.save(types);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "le type est ajouté avec succés",types.getLabel());
	}

	@Override
	public List<Type> getListType() {
		logger.info("___________get list type_______________");
		return this.typeRepository.findAll(new Sort(Direction.ASC, "label"));
	}

	@Override
	public MessageResponse updateType(Long id, String label) {
		Type type = typeRepository.findByLabel(label);
		if(type != null ){
			RestExceptionCode code = RestExceptionCode.TYPE_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Type types = this.typeRepository.findOne(id);
		types.setLabel(label);
		this.typeRepository.save(types);
		return new MessageResponse(HttpStatus.OK.toString(), null, "le type est modifié avec succés",types.getLabel());
	
	}

	@Override
	public MessageResponse deleteType(Long id) {
		Type type = typeRepository.findOne(id);
		if(type == null ){
			RestExceptionCode code = RestExceptionCode.TYPE_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<Product> products =productRepository.findByType(type);
		if(products.size()>0 ){
			RestExceptionCode code = RestExceptionCode.TYPE_ALREADY_USED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		this.typeRepository.delete(type);
		return new MessageResponse(HttpStatus.OK.toString(), null, "le type est supprimé avec succés",type.getLabel());
	}

}
