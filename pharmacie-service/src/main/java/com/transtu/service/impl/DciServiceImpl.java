package com.transtu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Dci;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.repositories.DciRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.DciService;




@Service
public class DciServiceImpl implements DciService {
	
	@Autowired(required = true)
	DciRepository dciRepository;
	@Autowired(required = true)
	ProductRepository productRepository;
	
	
	@Override
	public MessageResponse addDci(String label) {
		
		Dci dci = this.dciRepository.findByLabel(label);
		if(dci!=null){
			RestExceptionCode code = RestExceptionCode.DCI_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		this.dciRepository.save(new Dci(label));
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Dci ajouté avec succes",label);	
	}

	@Override
	public MessageResponse updateDci (Long id, String label) {
		
		Dci dci = dciRepository.findByLabel(label);
		System.out.println("--------------");
		if(dci != null ){
			RestExceptionCode code = RestExceptionCode.DCI_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		Dci dcis = this.dciRepository.findOne(id);
		if(dcis == null ){
			RestExceptionCode code = RestExceptionCode.DCI_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		dcis.setLabel(label);
		this.dciRepository.save(dcis);
	
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Dci produit est modifié avec succés",label);
			
	}

	@Override
	public MessageResponse deleteDci(Long id) {
		Dci dci =dciRepository.findOne(id);
		if(dci==null ){
			RestExceptionCode code = RestExceptionCode.DCI_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<Product> product =productRepository.findByDci(dci);
		if(product.size() >0 ){
			RestExceptionCode code = RestExceptionCode.DCI_ALREADY_USED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		this.dciRepository.delete(this.findById(id));
		return new MessageResponse(HttpStatus.OK.toString(), null, "Dci supprimé avec succes",dci.getLabel());	
	}

	@Override
	public Dci findById(Long id) {
		Dci dci = this.dciRepository.findOne(id) ;
		if(dci==null){
			RestExceptionCode code = RestExceptionCode.DCI_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		return dci ;
	}

	@Override
	public Dci findByLabel(String label) {
		Dci dci = this.dciRepository.findByLabel(label);
		if(dci == null ){
			RestExceptionCode code = RestExceptionCode.DCI_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		return dci;
	}

	@Override
	public List<Dci> findAllDci() {
	
		return this.dciRepository.findAll(new Sort(Direction.ASC, "label"));
	}
	
	
}
