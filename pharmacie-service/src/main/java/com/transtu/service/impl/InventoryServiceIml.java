package com.transtu.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.StandardSocketOptions;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.transtu.core.dto.InventoryLineDto;
import com.transtu.core.jasper.InventoryProductJasper;
import com.transtu.core.jasper.StockJasper;
import com.transtu.core.jasper.StockJasperEcart;
import com.transtu.core.json.request.AddInventoryRequest;
import com.transtu.core.json.request.FilterInventoryResponse;
import com.transtu.core.json.request.InventoryLineRequest;
import com.transtu.core.json.request.InventoryPagination;
import com.transtu.core.json.request.InventoryPdfRequest;
import com.transtu.core.json.request.InventoryRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateInventoryRequest;
import com.transtu.persistence.entities.Inventory;
import com.transtu.persistence.entities.InventoryLine;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Lot;
import com.transtu.persistence.entities.Movement;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.QInventory;
import com.transtu.persistence.entities.QInventoryLine;
import com.transtu.persistence.entities.Stock;
import com.transtu.persistence.entities.StockMovement;
import com.transtu.persistence.entities.Stocklot;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.persistence.repositories.InventoryLineRepository;
import com.transtu.persistence.repositories.InventoryRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.MovementRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.StateRepository;
import com.transtu.persistence.repositories.StockLotRepository;
import com.transtu.persistence.repositories.StockMovementLineRepository;
import com.transtu.persistence.repositories.StockMovementRepository;
import com.transtu.persistence.repositories.StockRepository;
import com.transtu.persistence.repositories.UtilisateurRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.InventoryService;
import com.transtu.service.JasperService;
import com.transtu.service.UtilisateurService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


@Service
public class InventoryServiceIml  implements InventoryService{
	
	@PersistenceContext
	EntityManager em ;
	
	@Autowired(required = true)
	InventoryRepository inventoryRepository ; 
	@Autowired(required = true)
	InventoryLineRepository InventoryLineRepository ; 
	@Autowired(required = true)
	LocationRepository locationRepository ; 
	@Autowired(required = true)
	ProductRepository productRepository ; 
	@Autowired(required = true)
	StateRepository stateRepository ; 
	@Autowired(required = true)
	UtilisateurRepository utilisateurRepository ;
	@Autowired(required = true)
	UtilisateurService utilisateurService ;
	@Autowired(required = true)
	JasperService  jasperService;
	@Autowired(required = true)
	StockRepository stockRepository;
	@Autowired(required = true)
	InventoryLineRepository  inventoryLineRepository;
	@Autowired(required = true)
	StockMovementRepository stockMovementRepository;
	@Autowired(required = true)
	StockMovementLineRepository stockMovementLineRepository;
	
	@Autowired(required = true)
	MovementRepository movementRepository;
	@Autowired(required = true)
	StockLotRepository stockLotRepository;
	
	double qtedep=0;
	
	@Transactional
	@Override
	public MessageResponse addInventory(AddInventoryRequest addInventoryRequest) {
		
		List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
		if(user==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		if (addInventoryRequest.getLocationId()==2) {
		Location location = locationRepository.findOne(addInventoryRequest.getLocationId());
		Inventory inventory = new Inventory();
		inventory.setDateInv(addInventoryRequest.getInventoryDate());
		inventory.setState(stateRepository.findOne(1l));//etat brouillon
		inventory.setLocation(location);
		inventory.setUtilisateur(user.get(0));
		inventoryRepository.save(inventory);
		
		
		List<Product> products = new ArrayList<>();
		List<InventoryLine> inventoryLines = new ArrayList<>() ;
		
		//products = this.productRepository.findDistinctByActived(true, (new Sort(Direction.ASC, "lib"))); 
		
		products = this.productRepository.x(); 
		
       for(Product p : products){
    	
		for (Lot l : p.getLots()) {
		 if (l.getStocklots()!=null && l.getStocklots().size()>0 ) {
		//	if( l.getStocklots().get(0).getQuantity() >0) {
			if( l.isState()==true) {
			                 
			InventoryLine inventoryLine = new InventoryLine();
			inventoryLine.setInventory(inventory);
//			inventoryLine.setInventQte(inventoryLineDto.getQte());
//			inventoryLine.setNote(inventoryLineDto.getNote());
			//Product product = productRepository.findOne(inventoryLineDto.getProductId());
			inventoryLine.setProduct(p);
			inventoryLine.setPrice(p.getPrice());
			inventoryLine.setVatRate(p.getVatRate());
			inventoryLine.setLot(l);
			inventoryLine.setStockQteLot(stockLotRepository.findByLot(l).getQuantity());;
			double qteStock = (stockRepository.findByProductAndLocation(p, location)).getQuantity();
			inventoryLine.setStockQte(qteStock);
			InventoryLineRepository.save(inventoryLine);
			}
		 }
		}
			
		}
		}
		if (addInventoryRequest.getLocationId()==1) {
			Location location = locationRepository.findOne(addInventoryRequest.getLocationId());
			Inventory inventory = new Inventory();
			inventory.setDateInv(addInventoryRequest.getInventoryDate());
			inventory.setState(stateRepository.findOne(1l));//etat brouillon
			inventory.setLocation(location);
			inventory.setUtilisateur(user.get(0));
			inventoryRepository.save(inventory);
			
			
			List<Product> products = new ArrayList<>();
			List<InventoryLine> inventoryLines = new ArrayList<>() ;
			
			//products = this.productRepository.findDistinctByActived(true, (new Sort(Direction.ASC, "lib"))); 
			products = this.productRepository.x(); 
			
			
	       for(Product p : products){
			 
				InventoryLine inventoryLine = new InventoryLine();
				inventoryLine.setInventory(inventory);
//				inventoryLine.setInventQte(inventoryLineDto.getQte());
//				inventoryLine.setNote(inventoryLineDto.getNote());
				//Product product = productRepository.findOne(inventoryLineDto.getProductId());
				inventoryLine.setProduct(p);
				inventoryLine.setPrice(p.getPrice());
				inventoryLine.setVatRate(p.getVatRate());
				//inventoryLine.setLot(l);
				double qteStock = (stockRepository.findByProductAndLocation(p, location)).getQuantity();
				inventoryLine.setStockQte(qteStock);
				InventoryLineRepository.save(inventoryLine);
			
				
			}
			}
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "l'inventaire est ajoutÃ© avec succÃ©s",null);	
	
	}
	
	
	
	@Override
	public MessageResponse updateInventory(UpdateInventoryRequest updateInventoryRequest) {
		  
		List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
		if(user==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		Inventory inventory = inventoryRepository.findOne(updateInventoryRequest.getInventoryId());
		inventory.setUtilisateur(user.get(0));
		inventoryRepository.save(inventory);
		
	    List <InventoryLine> inventoryLines = inventoryLineRepository.findByInventory(inventory);
	    if(inventory.getLocation().getId()==2) {
		for(InventoryLineDto inventoryLineDto : updateInventoryRequest.getInventoryLineDtos()){
			
		
			
			System.out.println("__________________________________In Depot:______________________________________"+inventoryLineDto.getLotId());
			InventoryLine inventoryLine = inventoryLineRepository.findByProductIdAndLotIdAndInventoryId(inventoryLineDto.getProductId(), inventoryLineDto.getLotId(),inventory.getId() );
			inventoryLine.setInventQte(inventoryLineDto.getQte());
			inventoryLine.setDiscard(inventoryLineDto.getDiscard());
			inventoryLine.setNote(inventoryLineDto.getNote());
			InventoryLineRepository.save(inventoryLine);
			
			
		
			}	
		inventoryRepository.save(inventory);
	       }  
	    
	    if(inventory.getLocation().getId()==1) {
			for(InventoryLineDto inventoryLineDto : updateInventoryRequest.getInventoryLineDtos()){
				
				if(inventoryLineDto.getId()!=null) {
				
		        System.out.println("__________________________________In pharmacie:______________________________________");
				InventoryLine inventoryLine = inventoryLineRepository.findByProductIdAndInventory(inventoryLineDto.getProductId(), inventory);
				inventoryLine.setInventQte(inventoryLineDto.getQte());
				inventoryLine.setDiscard(inventoryLineDto.getDiscard());
				inventoryLine.setNote(inventoryLineDto.getNote());
				InventoryLineRepository.save(inventoryLine);
				}
				
			
				}	
			inventoryRepository.save(inventory);
		    }  
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "l'inventaire a été mis a jour avec succÃ©s",null);	
	}
	
	
	
		
		@Override
		public MessageResponse stockRecovery(Long id) {
			  
			List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
			if(user==null){
				RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			
			Inventory inventory = inventoryRepository.findOne(id);
			inventory.setUtilisateur(user.get(0));
			inventory.setState(stateRepository.findOne(2l));//etat valider
			inventoryRepository.save(inventory);
			
			Location location = inventory.getLocation();
			
			//for(InventoryLine inventoryLine : inventory.getInventoryLine()){
			for(InventoryLine inventoryLine : InventoryLineRepository.findByInventory(inventory, (new Sort(Direction.ASC, "product.lib"))) ){  //(new Sort(Direction.ASC, "product")
				
				if(location.getId()==2) {
				  
					
					Stocklot stocklot = stockLotRepository.findByLotAndLocation(inventoryLine.getLot(),location);
					stocklot.setQuantity(stocklot.getQuantity()+inventoryLine.getDiscard());
					stockLotRepository.save(stocklot);
					Stock stock =stockRepository.findByProductAndLocation(inventoryLine.getProduct(), location);
				    stock.setQuantity(stock.getQuantity()+inventoryLine.getDiscard());
					stockRepository.save(stock);
					
				}
				if(location.getId()==1) {
					Stock stock =stockRepository.findByProductAndLocation(inventoryLine.getProduct(), location);
						//stock.setQuantity(inventoryLine.getInventQte()+stock.getQuantity());
					    stock.setQuantity(inventoryLine.getStockQte()+inventoryLine.getDiscard());
						stockRepository.save(stock);
						
						
						
				}
					
					if (inventoryLine.getDiscard()<0) {
					Movement mvt = new Movement();
					mvt.setDate(inventory.getDateInv());
					mvt.setLocation(location);
					mvt.setProduct(inventoryLine.getProduct());
					mvt.setQuantity(inventoryLine.getDiscard());
					mvt.setType("-1");// -1: Exit 1:Entry
					mvt.setDescription("Qte physique > qte théorique:"+"Inventaire N°=||"+ inventory.getId()+
							"||Date Redressement||:"+new Date()+"||Motif||:"+inventoryLine.getNote()+"||Produit Libellé:||"+inventoryLine.getProduct().getLib() );
					//save Movement
					movementRepository.save(mvt);
					
					} 
					if (inventoryLine.getDiscard()>0) {
						Movement mvt = new Movement();
						mvt.setDate(new Date());
						mvt.setLocation(location);
						mvt.setProduct(inventoryLine.getProduct());
						mvt.setQuantity(inventoryLine.getDiscard());
						mvt.setType("1");// -1: Exit 1:Entry
						mvt.setDescription("Qte physique < qte théorique:"+"Inventaire N°=||"+ inventory.getId()+
								"||Date Redressement||:"+new Date()+"||Motif||:"+inventoryLine.getNote()+"||Produit Libellé:||"+inventoryLine.getProduct().getLib() );
						//save Movement
						movementRepository.save(mvt);
						
						}
					
					
				}
			
		
			
			return new MessageResponse(HttpStatus.OK.toString(), null, "l'inventaire a été mis a jour avec succÃ©s",null);	
		}
	
	//******************************************************Jasper List ProductInventory************************************************************************
	@Override
	public File getPdfStock(InventoryPdfRequest inventoryPdfRequest) throws FileNotFoundException{
	
		return this.getPdf(this.getStockFromPdf(inventoryPdfRequest));
	}
	
	private File getPdf(List<InventoryProductJasper> list) throws FileNotFoundException{
		
		JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
		return jasperService.jasperFile(dataSource, "inventoryProduct");
	}
	


	@Override
	//public List<StockJasper> getStockFromPdf(FilterStockReport filterStockReport) {
	public List<InventoryProductJasper> getStockFromPdf(InventoryPdfRequest inventoryPdfRequest) {
		Integer i = 0;
	
		
		List<Product> products ;
		List<InventoryProductJasper> stocksJaspers = new LinkedList<>()  ;
		Inventory inventory = inventoryRepository.findOne(inventoryPdfRequest.getInventoryId());
		if(inventory.getLocation().getId()==2) {
			
			//products = this.productRepository.findDistinctByActived(true, (new Sort(Direction.ASC, "lib"))) ;
			//product mvt
			products = this.productRepository.x() ;
			
			
		   // products.stream().forEach(p->{
		   for(Product p : products) {
			 for (Lot l : p.getLots()) {
				if (l.getStocklots()!=null && l.getStocklots().size()>0 ) {
			//if( l.getStocklots().get(0).getQuantity() >0) {
					if( l.isState()==true) {	
		    InventoryProductJasper stockJasp = new InventoryProductJasper();
			stockJasp.setCodePct(p.getCodePctProd());
			stockJasp.setBarCode(p.getBarCode());
			i=i+1;
			stockJasp.setNumOrd(i);
			stockJasp.setInventorydate(inventory.getDateInv());
			stockJasp.setLocationLbel(locationRepository.findOne(inventory.getLocation().getId()).getLib());
			stockJasp.setPrice(p.getPrice());
			stockJasp.setDesignation(p.getLib());
			stockJasp.setDesignationlot(l.getLabel());
			stocksJaspers.add(stockJasp);
				}   
			 } 
			   }
		   }
		   
		}
		if(inventory.getLocation().getId()==1) {
			
			//products = this.productRepository.findDistinctByActived(true, (new Sort(Direction.ASC, "lib"))) ;
			//product mvt
			products = this.productRepository.x() ;
		   // products.stream().forEach(p->{
		   for(Product p : products) {
			  // for (Lot l : p.getLots()) {
		    InventoryProductJasper stockJasp = new InventoryProductJasper();
			stockJasp.setCodePct(p.getCodePctProd());
			stockJasp.setBarCode(p.getBarCode());
			i=i+1;
			stockJasp.setNumOrd(i);
			stockJasp.setInventorydate(inventory.getDateInv());
			stockJasp.setLocationLbel(locationRepository.findOne(inventory.getLocation().getId()).getLib());
			stockJasp.setPrice(p.getPrice());
			stockJasp.setDesignation(p.getLib());
			stockJasp.setDesignationlot("________");
			stocksJaspers.add(stockJasp);
			
			  // }
		   }
		   
		}
	
		    
		return stocksJaspers;
	}
	
	
	@Override
	public List<Inventory> listInventory() {
		return inventoryRepository.findAll();
	}
	

	@Override
	public InventoryPagination ListInventoryLine(InventoryLineRequest p ) {
		
       JPQLQuery<?> query = new JPAQuery(em); 
		
		//QDistribution distribution = QDistribution.distribution;
		QInventory inventory = QInventory.inventory;
		QInventoryLine inventoryLine = QInventoryLine.inventoryLine;
		query.from(inventoryLine).where(inventoryLine.inventory.id.eq(p.getInventoryId()));
		//query.distinct();	
		System.out.println("___________________1________________________________"+query);
		query.orderBy(inventoryLine.product.lib.asc());
		System.out.println("___________________2________________________________"+query);
		if(p.getPage()!=null){
			query.offset((p.getPage()-1)* p.getLinesNbr());
		    query.limit(p.getLinesNbr());
			//query.offset(0);
			//query.limit(4);
		}
		InventoryPagination invePagination = new InventoryPagination();
		if(p.getPage()!=null){
			if((query.select(inventoryLine).fetchCount())%p.getLinesNbr()==0) {
				invePagination.setPage(p.getPage());
				invePagination.setNbrPages((query.select(inventoryLine).fetchCount())/p.getLinesNbr());
				invePagination.setTotalProducts((query.select(inventoryLine).fetchCount()));
			}else {	
				invePagination.setPage(p.getPage());
				invePagination.setNbrPages(((query.select(inventoryLine).fetchCount())/p.getLinesNbr())+1);	
				invePagination.setTotalProducts((query.select(inventoryLine).fetchCount()));
			}
		}
		List<InventoryLine> list =  query.select(inventoryLine).fetch();
		invePagination.setInventoryLines(list);
		return invePagination;
		//return InventoryLineRepository.findByInventory(inventoryRepository.findOne(id));
	}
	
	@Override
	public Inventory getInventory(Long id) {
		return inventoryRepository.findOne(id);
	}
	
	

	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	//__________________________________________________________________________________________Pdf Edition Inventaire Pharmacie Ou Depot____________________________________________________________________________
	
	@Override
	public File getPdfinventory(Long idPharma, Long idDep) throws FileNotFoundException{
		
		if(idPharma!=0 && idDep !=0) {
		return this.getPdfgloblale(this.getInventoryFromPdf(idPharma, idDep));
		}
		return this.getPdf1(this.getInventoryFromPdf(idPharma, idDep));
	}
	
	private File getPdfgloblale(HashSet<StockJasper> hashSet) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(hashSet,false);
		return jasperService.jasperFile(dataSource, "inventoryglobal");
	}
	private File getPdf1(HashSet<StockJasper> hashSet) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(hashSet,false);
		return jasperService.jasperFile(dataSource, "inventory");
	}
	
	
	@Override
	public LinkedHashSet<StockJasper> getInventoryFromPdf(Long idPharma, Long iddepot) {
	
		  if (idPharma !=0 && iddepot !=0 ) {
		   Inventory invetoryPharma = inventoryRepository.findOne(idPharma);
		   Inventory invetoryDepot = inventoryRepository.findOne(iddepot);
		   LinkedHashSet<InventoryLine> inventoryLines = this.InventoryLineRepository.findByInventory(invetoryDepot, (new Sort(Direction.ASC, "id")));
	        
		    Location pharmacie =locationRepository.findOne(1l);
	        Location depot =locationRepository.findOne(2l);
	     
		    LinkedHashSet<StockJasper> stocksJasper = new LinkedHashSet<>() ;
		    inventoryLines.stream().forEach(inv->{  
			//Stock stock = this.stockRepository.findByProductAndLocation(p, this.locationRepository.findOne(filterStockReport.getStockId()));
			StockJasper stockJasp = new StockJasper();
			stockJasp.setBarCode(productRepository.findOne(inv.getProduct().getId()).getBarCode());
			stockJasp.setCodePct(productRepository.findOne(inv.getProduct().getId()).getCodePctProd());
			//stockJasp.setPrice(productRepository.findOne(inv.getProduct().getId()).getPrice());
			stockJasp.setPrice(inv.getPrice());
			stockJasp.setDateInv(invetoryDepot.getDateInv());
			stockJasp.setLocation("Pharmacie & Dépôt");
			if (idPharma!=null) {
			stockJasp.setQtStockpharma(inv.getInventQte());
			  }
			if (iddepot!=null) {
				if(InventoryLineRepository.findByInventoryAndProduct(invetoryPharma,productRepository.findOne(inv.getProduct().getId())).size()>0) {
					 qtedep = InventoryLineRepository.getSumInventoryprodDep(inv.getProduct().getId(), invetoryPharma.getId());
			          stockJasp.setQtStockDep(qtedep);
				}else {qtedep=0;}
		    }
			stockJasp.setDesignation(productRepository.findOne(inv.getProduct().getId()).getLib());
			stockJasp.setVatRate(productRepository.findOne(inv.getProduct().getId()).getVatRate());
			
			double price = inv.getPrice();

					//productRepository.findOne(inv.getProduct().getId()).getPrice();
			double qtePhar = inv.getInventQte();
			double qteDep = qtedep;
			double prixPhar = inv.getInventQte()*price;
			System.out.println("La quantite des depot ----------------------------"+qtedep);
			System.out.println("La quantite des pharma ----------------------------"+qtePhar);
			double prixDep = qtedep*price;
			double totalhT =price*(qtePhar+qteDep);
			stockJasp.setPriceDep(round(prixDep, 3));
			stockJasp.setPricePh(round(prixPhar,3));
			stockJasp.setTotal((double) (qtePhar+qteDep));
			double totalTtc =(double) (totalhT*(inv.getVatRate()*0.01+1)) ;
			stockJasp.setTotalTtc(round(totalTtc, 3));
			//stockJasp.setTotalTtc((float) (totalTtc+((stockJasp.getPrice()*totalQte)*p.getVatRate())/100));
			stocksJasper.add(stockJasp);
			
		});
		    
		return stocksJasper;
		  }
		  
		  if (idPharma ==0 ) {
			  
			   //Inventory invetoryPharma = inventoryRepository.findOne(idPharma);
			   Inventory invetoryDepot = inventoryRepository.findOne(iddepot);
			   LinkedHashSet<InventoryLine> inventoryLines = this.InventoryLineRepository.findByInventory(invetoryDepot, (new Sort(Direction.ASC, "id")));
		
			 
			    //Location pharmacie =locationRepository.findOne(1l);
		        Location depot =locationRepository.findOne(2l);
		     
			    LinkedHashSet<StockJasper> stocksJasper = new LinkedHashSet<>() ;
			    inventoryLines.stream().forEach(inv->{  
			    
				//Stock stock = this.stockRepository.findByProductAndLocation(p, this.locationRepository.findOne(filterStockReport.getStockId()));
				StockJasper stockJasp = new StockJasper();
				stockJasp.setBarCode(productRepository.findOne(inv.getProduct().getId()).getBarCode());
				stockJasp.setCodePct(productRepository.findOne(inv.getProduct().getId()).getCodePctProd());
				stockJasp.setPrice(inv.getPrice());
				stockJasp.setDateInv(invetoryDepot.getDateInv());
				stockJasp.setLocation("Dépôt");
//				if (idPharma!=null) {
//				stockJasp.setQtStockpharma(inv.getInventQte());
//				  }
				if (iddepot!=null) {

					 stockJasp.setQtStockDep(InventoryLineRepository.findByProductIdAndLotIdAndInventoryId(inv.getProduct().getId(), inv.getLot().getId(), invetoryDepot.getId()).getInventQte());
			    }
				
				stockJasp.setDesignationlot(inv.getLot().getLabel());
				stockJasp.setDesignation(productRepository.findOne(inv.getProduct().getId()).getLib());
				stockJasp.setVatRate(inv.getVatRate());
				
				double price = inv.getPrice();
				double qteDep = InventoryLineRepository.findByProductIdAndLotIdAndInventoryId(inv.getProduct().getId(), inv.getLot().getId(), invetoryDepot.getId()).getInventQte();
				double prixDep = qteDep*price;
				double totalhT =price*(qteDep);
				stockJasp.setPriceDep(prixDep);
				stockJasp.setTotal((double) (qteDep));
				
				double totalTtc =(double) (totalhT*(inv.getVatRate()*0.01+1)); 
				//double totalTtc =(double) (totalhT*(productRepository.findOne(inv.getProduct().getId()).getVatRate()*0.01+1)) ;
				stockJasp.setTotalTtc(round(totalTtc, 3));
				stocksJasper.add(stockJasp);
				
			});
			    
			return stocksJasper;
			  } 
		  
		  if (iddepot ==0 ) {
			
			   Inventory invetoryPharma = inventoryRepository.findOne(idPharma);
			  // Inventory invetoryDepot = inventoryRepository.findOne(iddepot);
			   LinkedHashSet<InventoryLine> inventoryLines = this.InventoryLineRepository.findByInventory(invetoryPharma, (new Sort(Direction.ASC, "id")));
			    Location pharmacie =locationRepository.findOne(1l);
		        Location depot =locationRepository.findOne(2l);
		     
			    LinkedHashSet<StockJasper> stocksJasper = new LinkedHashSet<>() ;
			    inventoryLines.stream().forEach(inv->{  
			    
				//Stock stock = this.stockRepository.findByProductAndLocation(p, this.locationRepository.findOne(filterStockReport.getStockId()));
				StockJasper stockJasp = new StockJasper();
				stockJasp.setBarCode(productRepository.findOne(inv.getProduct().getId()).getBarCode());
				stockJasp.setCodePct(productRepository.findOne(inv.getProduct().getId()).getCodePctProd());
				stockJasp.setPrice(inv.getPrice());
				stockJasp.setDateInv(invetoryPharma.getDateInv());
				stockJasp.setLocation("Pharmacie");
				if (idPharma!=null) {
				//stockJasp.setQtStockpharma(inv.getInventQte());
					stockJasp.setQtStockDep(inv.getInventQte());
				  }

				stockJasp.setDesignation(productRepository.findOne(inv.getProduct().getId()).getLib());
				stockJasp.setVatRate(inv.getVatRate());
				
				double price = inv.getPrice();
				double qtePhar = inv.getInventQte();
				//double qteDep = InventoryLineRepository.findByInventoryAndProduct(invetoryDepot,
				//		  productRepository.findOne(inv.getProduct().getId())).getInventQte();
				double prixPhar = inv.getInventQte()*price;
				//double prixDep = qtePhar*price;
				double totalhT = price*(qtePhar);
				//stockJasp.setPriceDep(prixDep);
				stockJasp.setPricePh(prixPhar);
				stockJasp.setTotal((double) (qtePhar));
				double totalTtc =(double) (totalhT*(inv.getVatRate()*0.01+1)) ;
				stockJasp.setTotalTtc(round(totalTtc, 3));
				//stockJasp.setTotalTtc((float) (totalTtc+((stockJasp.getPrice()*totalQte)*p.getVatRate())/100));
				stocksJasper.add(stockJasp);
				 System.out.println(" -------------------------------------E|*|E--------------------------------------------");
			});
			    
			return stocksJasper;
			  }
		return null;
		
	}



	
	
	
	@Override
	public File getPdfinventoryEcart(Long idPharma, Long idDep) throws FileNotFoundException{
		
		
		return this.getPdf11(this.getInventoryEcartFromPdf(idPharma, idDep));
	}
	
	private File getPdf11(HashSet<StockJasperEcart> hashSet) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(hashSet,false);
		return jasperService.jasperFile(dataSource, "inventoryEcart");
	}
	
	
	
	@Override
	public LinkedHashSet<StockJasperEcart> getInventoryEcartFromPdf(Long idPharma, Long iddepot) {
		
		  
		  if( iddepot==0 ) {
			   Inventory invetoryPharma = inventoryRepository.findOne(idPharma);
			   LinkedHashSet<InventoryLine> inventoryLines = this.InventoryLineRepository.findByInventory(invetoryPharma, (new Sort(Direction.ASC, "id")));
			    //Location pharmacie =locationRepository.findOne(1l);
		        Location depot =locationRepository.findOne(2l);
		     
			    LinkedHashSet<StockJasperEcart> stocksJasper = new LinkedHashSet<>() ;
			    inventoryLines.stream().forEach(inv->{  
			    
				//Stock stock = this.stockRepository.findByProductAndLocation(p, this.locationRepository.findOne(filterStockReport.getStockId()));
			    	double discard =InventoryLineRepository.findByInventoryAndProduct(invetoryPharma,
							  productRepository.findOne(inv.getProduct().getId())).get(0).getDiscard();
			    if (discard!= 0) {
			    Product product = productRepository.findOne(inv.getProduct().getId());
			    StockJasperEcart stockJasp = new StockJasperEcart();
			    stockJasp.setLocale("Pharmacie");
				stockJasp.setCodePct(product.getCodePctProd());
				stockJasp.setDateInv(invetoryPharma.getDateInv());
				//-stockJasp.setBarCode(58d);
				//-stockJasp.setQtStock(58d);
				stockJasp.setPrice(inv.getPrice());
				//-stockJasp.setQtStockpharma(58d);

				if (iddepot!=null) {
				      stockJasp.setQtStockphypharma(InventoryLineRepository.findByInventoryAndProduct(invetoryPharma,
					  productRepository.findOne(inv.getProduct().getId())).get(0).getInventQte());
				      stockJasp.setQtStockpharmaThe(InventoryLineRepository.findByInventoryAndProduct(invetoryPharma,
					   productRepository.findOne(inv.getProduct().getId())).get(0).getStockQte());
				      stockJasp.setEcartStockpha(discard);
				 }
				stockJasp.setDesignation(productRepository.findOne(inv.getProduct().getId()).getLib());
				
				double price = inv.getPrice();
				//double qtePhar = inv.getInventQte();
				double qteDep = InventoryLineRepository.findByInventoryAndProduct(invetoryPharma,
						  productRepository.findOne(inv.getProduct().getId())).get(0).getInventQte();
				//double prixPhar = inv.getInventQte()*price;
				double prixDep = qteDep*price;
				double totalhT =price*(qteDep);
			
				double totalTtc =(double) (totalhT*(inv.getVatRate()*0.01+1)) ;
			    //stockJasp.setTotalTtc(round(totalTtc, 3));
				//stockJasp.setTotalTtc((float) (totalTtc+((stockJasp.getPrice()*totalQte)*p.getVatRate())/100));
				stocksJasper.add(stockJasp);
			    	}
				
			});
			    
			return stocksJasper;
			 } 
		  
		  if ( idPharma ==0 ) {
			  
			   Inventory invetoryDepot = inventoryRepository.findOne(iddepot);
			   LinkedHashSet<InventoryLine> inventoryLines = this.InventoryLineRepository.findByInventory(invetoryDepot, (new Sort(Direction.ASC, "id")));
			    Location pharmacie =locationRepository.findOne(1l);
		        Location depot =locationRepository.findOne(2l);
		     
			    LinkedHashSet<StockJasperEcart> stocksJasper = new LinkedHashSet<>() ;
			    inventoryLines.stream().forEach(inv->{  
			    	
			    	double discard =inventoryLineRepository.findByProductIdAndLotIdAndInventoryId(inv.getProduct().getId(), 
							inv.getLot().getId(), invetoryDepot.getId()).getDiscard();
			    
			    
			    if (discard!= 0) {
			    	Product product = productRepository.findOne(inv.getProduct().getId());
						    StockJasperEcart stockJasp = new StockJasperEcart();
						    stockJasp.setLocale("Dépot");
							stockJasp.setCodePct(product.getCodePctProd());
							stockJasp.setDateInv(invetoryDepot.getDateInv());
							//-stockJasp.setBarCode(58d);
							//-stockJasp.setQtStock(58d);
							stockJasp.setPrice(inv.getPrice());
							//-stockJasp.setQtStockpharma(58d);
				
				if (iddepot!=null) {
//			      stockJasp.setQtStockphypharma(inventoryLineRepository.findByInventoryAndProduct(invetoryDepot,
//						  productRepository.findOne(inv.getProduct().getId())).getInventQte());
					stockJasp.setQtStockphypharma(inventoryLineRepository.findByProductIdAndLotIdAndInventoryId(inv.getProduct().getId(), 
							inv.getLot().getId(), invetoryDepot.getId()).getInventQte());
					stockJasp.setQtStockpharmaThe(inventoryLineRepository.findByProductIdAndLotIdAndInventoryId(inv.getProduct().getId(),
							inv.getLot().getId(), invetoryDepot.getId()).getStockQteLot());
//			              stockJasp.setQtStockpharmaThe(inventoryLineRepository.findByInventoryAndProduct(invetoryDepot,
//						  productRepository.findOne(inv.getProduct().getId())).getStockQte());
			      stockJasp.setEcartStockpha(discard);
			      
		    }
				stockJasp.setDesignation(productRepository.findOne(inv.getProduct().getId()).getLib());
				stockJasp.setDesignationlot(inv.getLot().getLabel());
	
				double price = inv.getPrice();
				double qtePhar = inv.getInventQte();
				double prixPhar = inv.getInventQte()*price;
				double totalhT =price*(qtePhar);
				double totalTtc =(double) (totalhT*(inv.getVatRate()*0.01+1)) ;
				stocksJasper.add(stockJasp);
			    	 }
			});
			    
			return stocksJasper;
			  }
		  
		  
		return null;
	}
	


	
	
	@Override
	public FilterInventoryResponse filterInventory(InventoryRequest filterinv) {

		JPQLQuery<?> query = new JPAQuery(em); 
		
		QInventory inventory = QInventory.inventory;

		query.from(inventory).orderBy(inventory.dateInv.desc());
		
		
		
		query.distinct();	
		if(filterinv.getPage()!=null){
			if(filterinv.getLinesNbr()==null){
				RestExceptionCode code = RestExceptionCode.LINES_NUMBER_REQUIRED_IN_PAGINATION;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			query.offset((filterinv.getPage()-1)*filterinv.getLinesNbr());
			query.limit(filterinv.getLinesNbr());
			
		}
		FilterInventoryResponse filterinvResponse = new FilterInventoryResponse();
		filterinvResponse.setInventorys(query.select(inventory).fetch());
		filterinvResponse.setTotalInventorys(query.select(inventory).fetchCount());
		if(filterinvResponse.getPage()!=null){
			filterinvResponse.setPage(filterinv.getPage());
			filterinvResponse.setNbrPages(filterinvResponse.getTotalInventorys()/filterinv.getLinesNbr());
		}
		return filterinvResponse ;
		
	}
	
	







	
}
