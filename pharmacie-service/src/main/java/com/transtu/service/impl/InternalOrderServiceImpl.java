package com.transtu.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.transtu.core.dto.InternalOrderLineDto;
import com.transtu.core.json.request.InternalOrderFilter;
import com.transtu.core.json.request.InternalOrderRequest;
import com.transtu.core.json.request.InternalordfilterResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.StockMvtfilter;
import com.transtu.core.json.request.UpdateOrderRequest;
import com.transtu.persistence.entities.Day;
import com.transtu.persistence.entities.InternalOrder;
import com.transtu.persistence.entities.InternalOrderLine;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.QInternalOrder;
import com.transtu.persistence.entities.QInternalOrderLine;
import com.transtu.persistence.entities.State;
import com.transtu.persistence.repositories.DayRepository;
import com.transtu.persistence.repositories.InternalOrderLineRepository;
import com.transtu.persistence.repositories.InternalOrderRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.StateRepository;
import com.transtu.persistence.repositories.TypeRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.InternalOrderService;
import com.transtu.service.SequenceService;
import com.transtu.service.utils.ConvertService;




@Service
public class InternalOrderServiceImpl implements InternalOrderService {
     
	@PersistenceContext
	EntityManager em ;
	
	
	@Autowired(required=true)
	LocationRepository locationRepo;
	@Autowired(required=true)
	InternalOrderLineRepository internalOrderLineRepository;
	@Autowired(required=true)
	InternalOrderRepository internalOrderRepository;
	@Autowired(required=true)
	ProductRepository productRepository;
	@Autowired(required=true)
	StateRepository stateRepository;
	@Autowired(required=true)
	ConvertService convertService;
	@Autowired(required=true)
	SequenceService sequenceService;
	@Autowired(required=true)
	ProductRepository productRepo;
	@Autowired
	DayRepository dayRepository;
	@Autowired(required = true)
	TypeRepository typeRepository;
	
	@Override
	public MessageResponse addInternalOrder(InternalOrderRequest internalOrderRequest) {
		
		Location location = this.locationRepo.findOne(internalOrderRequest.getLocationId());
		State state = this.stateRepository.findOne(1l);
		if(location==null){
			RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;	
		}
		if(state==null){
			RestExceptionCode code = RestExceptionCode.STATE_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;	
		}
		
		Day day = dayRepository.findOne(internalOrderRequest.getDayId());
		if(day==null ){
			RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		InternalOrder  internalOrder = new InternalOrder();	
		internalOrder.setDateOrder(new Date());
		internalOrder.setDay(day);
		internalOrder.setNumOrder(this.sequenceService.calculateNumOrder());
		internalOrder.setState(state);
		internalOrder.setLocation(location);
		internalOrder.setType(this.typeRepository.getOne(internalOrderRequest.getTypeId()));
		internalOrder=internalOrderRepository.save(internalOrder);
	
		
		
	
	    for (InternalOrderLineDto internalOrderLineDto : internalOrderRequest.getInternalOrderLines()){
	    	Product product = this.productRepo.findOne(internalOrderLineDto.getProductId());
	    	if(product==null){
				RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;	
			}
	    	if(internalOrderLineDto.getOrderQte()==0){
				RestExceptionCode code = RestExceptionCode.QUANTITY_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;	
			}
		System.out.println("--------------------nobre de ligne ste"+internalOrderRequest.getInternalOrderLines().size());
		InternalOrderLine internalOrderLine = new InternalOrderLine();
		internalOrderLine.setInternalOrder(internalOrder);
		internalOrderLine.setProduct(product);
		internalOrderLine.setOrderQte(internalOrderLineDto.getOrderQte());
		

		internalOrderLineRepository.save(internalOrderLine);
	}
	
	 return new MessageResponse(HttpStatus.OK.toString(), null, "la Commande Interne a été ajouté avec succés",internalOrder.getNumOrder());	
	}
	
	@Override
	public MessageResponse addAssistInternalOrder(InternalOrderRequest internalOrderRequest) {
		
		InternalOrder  internalOrder = new InternalOrder();		
		internalOrder.setDateOrder(new Date());
		internalOrder.setState(this.stateRepository.findOne(internalOrderRequest.getStatId()));
		internalOrder.setLocation(this.locationRepo.findOne(internalOrderRequest.getLocationId()));
		internalOrder.setType(this.typeRepository.getOne(internalOrderRequest.getTypeId()));
		internalOrderRepository.save(internalOrder);
	
		if(internalOrder.getLocation()==null){
			RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;	
		}
		
		for (InternalOrderLineDto internalOrderLineDto : internalOrderRequest.getInternalOrderLines()){
			
			InternalOrderLine internalOrderLine = new InternalOrderLine();
			internalOrderLine.setInternalOrder(internalOrder);
			internalOrderLine.setProduct(this.productRepository.findOne(internalOrderLineDto.getProductId()));
			internalOrderLine.setOrderQte(internalOrderLineDto.getOrderQte());
			

			internalOrderLineRepository.save(internalOrderLine);
		}
		
		 return new MessageResponse(HttpStatus.OK.toString(), null, "la Commande Interne a été ajouté avec succés",internalOrder.getNumOrder());	
		}
	
	@Transactional
	@Override
	public MessageResponse cancelInternalOrder(Long internalOrderId) {
		InternalOrder internalOrder = internalOrderRepository.findOne(internalOrderId);
		if(internalOrder==null){
			RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		 State state = stateRepository.findOne(4l);
		 InternalOrder internalOrder1 = internalOrderRepository.findByState(state);
		 
		 if(internalOrder1!=null){
				RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_ALREADY_DELIVRED;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
		internalOrder.setState(stateRepository.findOne(3l));
		internalOrderRepository.save(internalOrder);
		 return new MessageResponse(HttpStatus.OK.toString(), null, "la commande a été annulée avec succés",internalOrder.getNumOrder());	
	}
	
	@Transactional
	@Override
	public MessageResponse updateOrder(UpdateOrderRequest updateOrderRequest) {
	InternalOrder internalOrder = internalOrderRepository.findOne(updateOrderRequest.getInternalOrderId());
	if(internalOrder==null){
		RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_NOT_FOUND;
		RestException ex = new RestException(code.getError(), code);
		throw ex;
	}
	 if(internalOrder.getState().getId()==4){
			RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_ALREADY_DELIVERED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
	
	for(InternalOrderLineDto InternalOrderLineDto : updateOrderRequest.getInternalOrderLinesDto()){
		Product product = this.productRepository.findOne(InternalOrderLineDto.getProductId());
	    InternalOrderLine internalOrderLine1 = this.internalOrderLineRepository.
			findByInternalOrderAndProduct(internalOrder, product);
	
	if(internalOrderLine1==null){
	InternalOrderLine internalOrderLine = new InternalOrderLine();
	internalOrderLine.setInternalOrder(internalOrder);
	internalOrderLine.setProduct(product);
	internalOrderLine.setOrderQte(InternalOrderLineDto.getOrderQte());
	this.internalOrderLineRepository.save(internalOrderLine);
	}else{
		internalOrderLine1.setOrderQte(InternalOrderLineDto.getOrderQte());
		this.internalOrderLineRepository.save(internalOrderLine1);
	}
	}
	return new MessageResponse(HttpStatus.OK.toString(), null, "la Commande a été modifié avec succés",internalOrder.getNumOrder());
	
	}
	
	
	
	
	
	
	
	@Transactional
	@Override
	public List<InternalOrder> getlistOrder ()
	    {
         return this.internalOrderRepository.findAll(new Sort(Direction.DESC, "dateOrder"));
	     }
	
	
@Override
public InternalordfilterResponse filterInternalOrder(InternalOrderFilter internalOrderFilter) {
	JPQLQuery<?> query = new JPAQuery(em); 
	
	QInternalOrder internalOrder = QInternalOrder.internalOrder;
	QInternalOrderLine internalOrderLine = QInternalOrderLine.internalOrderLine;
	
	query.from(internalOrderLine).orderBy(internalOrder.dateOrder.desc()).distinct()
			.leftJoin(internalOrderLine.internalOrder , internalOrder);
	

	
	if(internalOrderFilter.getMinDate()!=null){
		query.where(internalOrder.dateOrder.after(internalOrderFilter.getMinDate()));
	}
	
	if(internalOrderFilter.getMaxDate()!=null){
		query.where(internalOrder.dateOrder.before(internalOrderFilter.getMaxDate()));
	}
	if(internalOrderFilter.getProductId()!=null){
		query.where(internalOrderLine.product.id.in(internalOrderFilter.getProductId()));
	}
	if(internalOrderFilter.getNumOrder()!=null){
		query.where(internalOrder.numOrder.eq(internalOrderFilter.getNumOrder()));
	}			
	if(internalOrderFilter.getTypeId()!=null){
		query.where(internalOrder.type.id.eq(internalOrderFilter.getTypeId()));
	}
	if(internalOrderFilter.getStateId()!=null){
		query.where(internalOrder.state.id.eq(internalOrderFilter.getStateId()));
	}
	
	//***********************
	 if(internalOrderFilter.getPage()!=null){
   	  query.offset((internalOrderFilter.getPage()-1)*15);
   	  query.limit(15);	    	
     }

	 InternalordfilterResponse internalordfilterResponse = new InternalordfilterResponse();
	 internalordfilterResponse.setInternalOrders( query.select(internalOrder).fetch());
	 internalordfilterResponse.setTotalItemes(query.fetchCount());
     
	//*****************
	
	
//	List<InternalOrder> list = query.select(internalOrder).fetch();
//	Set<InternalOrder> set = new LinkedHashSet<>();
//	set.addAll(list);

	return internalordfilterResponse;
}

@Transactional
@Override
public MessageResponse changeStateInternalOrder(Long internalOrderId,Long stateId ) {
	String msg = null;
	//stateId : 1---->brouillon--*
	//stateId : 2---->valider--*
	//stateId : 3---->annuller--*
	//stateId : 4---->livrer--*
	//stateId : 5---->livrer partiellement--*
	
	
	InternalOrder internalOrder = internalOrderRepository.findOne(internalOrderId);
	
	if(internalOrder==null){
		RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_NOT_FOUND;
		RestException ex = new RestException(code.getError(), code);
		throw ex;
	}
	
	 if(internalOrder.getState().getId()==4){
			RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_ALREADY_DELIVRED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
	//annulation de la validation : retour de l'état valider à l'état brouillon
		 if (stateId==1){
		 internalOrder.setState(stateRepository.findOne(stateId));
		 internalOrderRepository.save(internalOrder);
		 msg= "L'annulation de la validation à été faite avec succès";
		 }
		 //**************************
	 if (stateId==2){
	 internalOrder.setState(stateRepository.findOne(stateId));
	 internalOrderRepository.save(internalOrder);
	 msg= "la commande Interne a été valider avec succés";
	 }
	 if (stateId==3){
	 internalOrder.setState(stateRepository.findOne(stateId));
	 internalOrderRepository.save(internalOrder);
	 msg="la commande Interne a été annuler avec succés";
	
	 }
	 return new MessageResponse(HttpStatus.OK.toString(), null, msg,internalOrder.getNumOrder());	
	 
}





@SuppressWarnings("deprecation")
@Override

public List<InternalOrder> findByCurrentYear(Date date) {
	System.out.println("date---------------systeme"+new Date());
	List<InternalOrder> list = new ArrayList<>();
	for (InternalOrder internalOrder : internalOrderRepository.findAll()){
		if (internalOrder.getDateOrder().getYear()== date.getYear()){
			list.add(internalOrder);
		}
	}
	return list;
	  
}

}
