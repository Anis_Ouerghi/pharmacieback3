package com.transtu.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.transtu.core.dto.ReceptionLineDto;
import com.transtu.core.jasper.ReceptionResponseJasper;
import com.transtu.core.json.request.ExternaOrdRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.ReceptionFilter;
import com.transtu.core.json.request.ReceptionRequest;
import com.transtu.core.json.request.UpdateReceptionRequest;
import com.transtu.persistence.entities.Day;
//import com.transtu.persistence.entities.ExternalDelivery;
import com.transtu.persistence.entities.ExternalOrder;
import com.transtu.persistence.entities.ExternalOrderLine;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Lot;
//import com.transtu.persistence.entities.OrdinaryMovement;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.QReception;
import com.transtu.persistence.entities.QReceptionLine;
import com.transtu.persistence.entities.Reception;
import com.transtu.persistence.entities.ReceptionLine;
import com.transtu.persistence.entities.State;
import com.transtu.persistence.entities.Stock;
import com.transtu.persistence.entities.StockMovement;
import com.transtu.persistence.entities.StockMovementLine;
import com.transtu.persistence.entities.Stocklot;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.persistence.repositories.DayRepository;
//import com.transtu.persistence.repositories.ExternalDeliveryRepository;
import com.transtu.persistence.repositories.ExternalOrderRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.LotRepository;
import com.transtu.persistence.repositories.MovementRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.ReceptionLineRepository;
import com.transtu.persistence.repositories.ReceptionRepository;
import com.transtu.persistence.repositories.ReferenceTypeRepository;
import com.transtu.persistence.repositories.StateRepository;
import com.transtu.persistence.repositories.StockLotRepository;
import com.transtu.persistence.repositories.StockMovementLineRepository;
import com.transtu.persistence.repositories.StockMovementRepository;
import com.transtu.persistence.repositories.StockRepository;
import com.transtu.persistence.repositories.UtilisateurRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.JasperService;
import com.transtu.service.ReceptionService;
import com.transtu.service.SequenceService;
import com.transtu.service.UtilisateurService;
import com.transtu.service.utils.ConvertService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;



@Service
public class ReceptionServiceImpl implements ReceptionService{
  	
	@PersistenceContext
	EntityManager em ;
	
	
	@Autowired(required=true)
	StateRepository stateRepository;
	@Autowired(required=true)
	ExternalOrderRepository externalOrderRepository;
	@Autowired(required=true)
	LocationRepository locationRepository;
	@Autowired(required=true)
	ProductRepository productRepository;
	@Autowired(required=true)
	ReceptionRepository receptionRepository;
	@Autowired(required=true)
	ReceptionLineRepository receptionLineRepository;
	@Autowired(required=true)
	StockRepository stockRepository;
	@Autowired (required=true)
	MovementRepository mouvementrepository;
	@Autowired (required=true)
	SequenceService sequenceService;
//	@Autowired (required=true)
//	ExternalDeliveryRepository externaldeliveryRepository;
	@Autowired (required=true)
	ConvertService convertService;
	@Autowired (required=true)
	DayRepository dayRepository;
	
	@Autowired(required=true)
	StockMovementRepository stockMovementRepository;
	@Autowired(required=true)
	StockMovementLineRepository stockMovementRepositoryLine;
	@Autowired(required=true)
	ReferenceTypeRepository referenceTypeRepository;
	@Autowired(required=true)
	StockLotRepository stockLotRepository;
	@Autowired(required=true)
	LotRepository lotRepository;
	@Autowired(required = true)
	UtilisateurRepository utilisateurRepository;
	@Autowired(required=true)
	UtilisateurService utilisateurService;
	
	@Autowired(required=true)
	JasperService jasperService ;
	
	
	
	public MessageResponse AddReception(ReceptionRequest receptionRequest){
		
		List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
		if(user==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
//	ExternalDelivery externalDelivery =	externalDeliveryRepository.
//			findOne(receptionRequest.getExtrernalDeliveryId());
	
	//findByNumExternalDelivery(receptionRequest.getExtrernalDeliveryNum());
	Location location = locationRepository.findOne(receptionRequest.getLocationId());
		//if (externalDelivery==null){	
			Day day = dayRepository.findOne(receptionRequest.getDayId());
			if(day==null){
				RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
//			ExternalDelivery externaldelivery = new ExternalDelivery();
//			//externaldelivery.setId(receptionRequest.getExtrernalDeliveryId());
//			externaldelivery.setNumExternalDelivery(receptionRequest.getExtrernalDeliveryNum());
//			externaldelivery.setExternalOrder(externalOrderRepository.
//					findOne(receptionRequest.getExtrernalOrderId()));
//			
//			externaldelivery.setDateExternalDelivery(new Date());
//			externaldelivery.setDay(day);
//			externaldelivery.setDateExternalDelivery(new Date());
//			
//			externaldelivery.setState(stateRepository.findOne(6l));
//			//externaldelivery.getExternalOrder().setState(stateRepository.findOne(6l));
//			externalDeliveryRepository.save(externaldelivery);
			
			Reception reception = new Reception();
			reception.setNumReception(sequenceService.calculateReceptionOrder());
			reception.setExternalOrder(externalOrderRepository.findOne(receptionRequest.getExtrernalOrderId()));
			reception.setDateLiv(receptionRequest.getDateLiv());
			reception.setNumExternalDelivery(receptionRequest.getExtrernalDeliveryNum());
			reception.setNumInvoice(receptionRequest.getNumInvoice());
			reception.setDateInvoice(receptionRequest.getDateInvoice());
			reception.setTypeId(receptionRequest.getTypeId());
			reception.setUtilisateur(user.get(0));
			reception.setDateReception(new Date());
			reception.setLocation(location);
			reception.setState(stateRepository.findOne(1l));
			reception.setDay(day);
			
			
			for (ReceptionLineDto receptionLineDto : receptionRequest.getReceptionLinesDto() ){
				Product product =productRepository.findOne(receptionLineDto.getProductId());
	
//				if((receptionLineDto.getLotNumber()==null) || (receptionLineDto.getExpirationDate()==null) || (receptionLineDto.getReceptionQte() <=0 )){
//					RestExceptionCode code = RestExceptionCode.LOT_INFO_NOT_FOUND;
//					RestException ex = new RestException(code.getError(), code);
//					throw ex;
//					
//				}
				
				
				if((receptionLineDto.getLotNumber().isEmpty()==false) &&  (receptionLineDto.getExpirationDate()!=null) && (receptionLineDto.getReceptionQte() > 0)){
					
				ReceptionLine receptionLine = new ReceptionLine();
//				reception.setNumReception("numReception");
				receptionLine.setReception(reception);
				receptionLine.setProduct(product);
				receptionLine.setInvoiceQte(receptionLineDto.getInvoiceQte());
				receptionLine.setReceptionQte(receptionLineDto.getReceptionQte());
				receptionLine.setPrice(receptionLineDto.getPrice());
				receptionLine.setVat(receptionLineDto.getVat());
				receptionLine.setExpirationDate(receptionLineDto.getExpirationDate());
				receptionLine.setLotNumber(receptionLineDto.getLotNumber());
				receptionLineRepository.save(receptionLine);
			}
				
			 }
			   receptionRepository.save(reception);

		
		ExternalOrder externalOrder = this.externalOrderRepository.findOne(receptionRequest.getExtrernalOrderId());
		if(externalOrder==null){
			RestExceptionCode code = RestExceptionCode.EXTERNAL_ORDER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		externalOrder.setState(this.stateRepository.findOne((long) 5));
		this.externalOrderRepository.save(externalOrder);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Reception  a été  ajoutée avec succés","-----");	
		
	}
	@Transactional
	@Override
	public MessageResponse cancelReception(Long id) {
		Reception reception = receptionRepository.findOne(id);
		if(reception==null){
			RestExceptionCode code = RestExceptionCode.RECEPTION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
			
	
		reception.setState(stateRepository.findOne(6l));
		receptionRepository.save(reception);
	
	
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Reception  a été annullée avec succés",reception.getNumReception());	
		}
	
	
	@Transactional
	@Override
	public MessageResponse updateReception(UpdateReceptionRequest updateReceptionRequest) {
	
		Reception reception = receptionRepository.findOne(updateReceptionRequest.getReceptionId());
		
		if(reception==null){
			RestExceptionCode code = RestExceptionCode.RECEPTION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		State state = reception.getState() ;
		
		if((state!=null)&&(state.getId().equals(2))){
			RestExceptionCode code = RestExceptionCode.RECEPTION_ALREADY_VALIDATED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		
		
		reception.setDateInvoice(updateReceptionRequest.getDateInvoice());
		reception.setDateLiv(updateReceptionRequest.getDateInvoice());
		reception.setNumExternalDelivery(updateReceptionRequest.getNumExternalDelivery());
		reception.setNumInvoice(updateReceptionRequest.getNumInvoice());
		receptionRepository.save(reception);
		
//    	ExternalDelivery externalDelivery = externaldeliveryRepository.
//    			findByStateAndNumExternalDelivery(state, reception.getExternalDelivery()
//    					.getNumExternalDelivery());
		
		
//    	if(reception.getExternalDelivery() == null){
//    		RestExceptionCode code = RestExceptionCode.DElIVRY_NOT_OPEN; 
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
//    	};
    	
        Location location  = reception.getLocation();
		
		for(ReceptionLineDto receptionLineDto:updateReceptionRequest.getReceptionLinesDto()){
		      Product product =productRepository.findOne(receptionLineDto.getProductId());
			
			if(product==null){
				RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
		
		     ReceptionLine receptionLine1 = receptionLineRepository.findByProductAndReception(product, reception);
			
			//if(receptionLine1 != null){
				
			double qte = receptionLine1.getReceptionQte();
			receptionLine1.setReceptionQte(receptionLineDto.getReceptionQte());
			receptionLine1.setInvoiceQte(receptionLineDto.getInvoiceQte());
			receptionLine1.setPrice(receptionLineDto.getPrice());
			receptionLine1.setVat(receptionLineDto.getVat());
			receptionLine1.setLotNumber(receptionLineDto.getLotNumber());
			receptionLine1.setExpirationDate(receptionLineDto.getExpirationDate());
			receptionLineRepository.save(receptionLine1);
			
			//Stock stockDepot = this.stockRepository.findByProductAndLocation(product,location);
//			if(stockDepot==null){
//				RestExceptionCode code = RestExceptionCode.STOCK_NOT_FOUND	;
//				RestException ex = new RestException(code.getError(), code);
//				throw ex;
//			}
			//double qt =receptionLineDto.getReceptionQte()-qte;
			//stockDepot.setQuantity(stockDepot.getQuantity()+qt);
			//System.out.println("------------------------///////////-------"+qt);
			//stockRepository.save(stockDepot);
//			Movement movement = new Movement();
//			movement.setDate(new Date());
//			movement.setDescription(" Modification entré du produit vers le stock ");
//			movement.setLocation(location);
//			movement.setProduct(product);
//			movement.setQuantity(receptionLineDto.getReceptionQte());
//			this.mouvementrepository.save(movement);
			//}else 
//				if(receptionLine1 == null){
//				System.out.println("++++++++++++++++++++++++++0++++++++++++++++++++++++++++++++");
//		    {
//				ReceptionLine  receptionLine =new ReceptionLine();
//				receptionLine.setProduct(product);
//				receptionLine.setInvoiceQte(receptionLineDto.getInvoiceQte());
//				receptionLine.setReceptionQte(receptionLineDto.getReceptionQte());
//				receptionLine.setReception(reception);
//				this.receptionLineRepository.save(receptionLine);
//				
//				Stock stockDepot = this.stockRepository.findByProductAndLocation(product,location);
//				if(stockDepot==null){
//					RestExceptionCode code = RestExceptionCode.STOCK_NOT_FOUND	;
//					RestException ex = new RestException(code.getError(), code);
//					throw ex;
//				}
//				
//				stockDepot.setQuantity(stockDepot.getQuantity()+(receptionLineDto.getReceptionQte()));
//				this.stockRepository.save(stockDepot);
//				
//				Movement movement = new Movement();
//				movement.setDate(new Date());
//				movement.setDescription(" Modification entré du produit vers le stock "+stockDepot.getLocation().getLib());
//				movement.setLocation(location);
//				movement.setProduct(product);
//				movement.setQuantity(receptionLineDto.getReceptionQte());
//				this.mouvementrepository.save(movement);
	
			}
		//}
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Reception  a été  modifiéé avec succés",reception.getNumReception());
		}

	
	@Override
	public Set<Reception> filterReception(ReceptionFilter receptionFilter) {
		
		JPQLQuery<?> query = new JPAQuery(em);
		QReception reception = QReception.reception;
		QReceptionLine receptionLine = QReceptionLine.receptionLine;
		
		query.from(receptionLine).orderBy(reception.Id.desc())
		.leftJoin(receptionLine.reception , reception);
		if(receptionFilter.getMinDate()!=null){
			query.where(reception.dateReception.after(receptionFilter.getMinDate()));
		}
		
		if(receptionFilter.getMaxDate()!=null){
			query.where(reception.dateReception.before(receptionFilter.getMaxDate()));
		}
		if(receptionFilter.getProductId()!=null){
			query.where(receptionLine.product.id.in(receptionFilter.getProductId()));
		}
		if(receptionFilter.getNumReception()!=null){
			query.where(reception.numReception.eq(receptionFilter.getNumReception()));
		}			
//		if(receptionFilter.getExternalDeliveryId()!=null){
//			query.where(reception.externalDelivery.Id.eq(receptionFilter.getExternalDeliveryId()));
//		}
		if(receptionFilter.getStateId()!=null){
			query.where(reception.state.id.eq(receptionFilter.getStateId()));
		}
		query.where(reception.state.id.ne((long) 6));
		
		List<Reception> list = query.select(reception).fetch();
		//----
		List<Reception> listrecep= new ArrayList<>(); 
		for(Reception  recep:  query.select(reception).fetch()){
			recep.getReceptionlines().sort(new Comparator<ReceptionLine>() {
				@Override
				public int compare(ReceptionLine o1, ReceptionLine o2) {
					int comparaison =0;
					comparaison=o1.getProduct().getLib().compareToIgnoreCase(o2.getProduct().getLib()) ;
			
					return comparaison;
				}
			});
			
			listrecep.add(recep);
			
		}
		//----
		Set<Reception> set = new LinkedHashSet<>();
		set.addAll(listrecep);
		
		return set; 
	}
	
	 @Transactional
	    @Override
	    public MessageResponse changeStateReception(Long receptionId, Long stateId ) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");
	    	String msg = null;
	    	
	    	List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
			if(user==null){
				RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
	    	//stateId : 1---->brouillon--*
	    	//stateId : 2---->valider--*
	    	//stateId : 3---->annuller--*
	    	//stateId : 4---->livrer--*
	    	//stateId : 5---->livrer partiellement--*
	    	//stateId : 6---->ouvert---*
	    	//stateId : 7---->fermé---*
	    	
	    	Reception reception = receptionRepository.findOne(receptionId);
	    	
	    	if(reception==null){
	    		RestExceptionCode code = RestExceptionCode.RECEPTION_NOT_FOUND;
	    		RestException ex = new RestException(code.getError(), code);
	    		throw ex;
	    	}
	    	
              if(reception.getState().getId()==2){
	    			RestExceptionCode code = RestExceptionCode.RECEPTION_ALREADY_VALIDATED;
	    			RestException ex = new RestException(code.getError(), code);
	    			throw ex;
	    	}
              
	    	 if (stateId==3){
	    		 reception .setState(stateRepository.findOne(stateId));
	    		 reception.setUtilisateur(user.get(0));
	    		 receptionRepository.save(reception);
	    	     msg="la reception  a été annuler avec succés";
	    	
	    	 }
	    	
	    	 if (stateId==2){
	    	//-------------------	MAJ-STOCK-PHARMA--&&--MAJ-STOCK-DEPOT------------//	 
	    		
	    		
	    	
	    		//-------------<>------stok-mvt-entete------<>------------------------- 
	    		 Location locationPharma = locationRepository.findOne(1l);
	     		 Location locationDep = locationRepository.findOne(2l);
	    		
	    		 StockMovement stockMovement = new StockMovement();
	    			stockMovement.setDateMovement(new Date());
	    			stockMovement.setNumMovement(sequenceService.calculateNumMvtOrdEntre());	
	    			stockMovement.setTypeMvt("1");
	    			stockMovement.setUtilisateur(user.get(0));
	    			stockMovement.setDescriminator("Ordinaire");
	    			stockMovement.setLocation(locationDep);
	    			stockMovement.setReferenceType(referenceTypeRepository.findOne(2l));
	    			stockMovement.setReception(reception);
	    			stockMovementRepository.save(stockMovement);
	    	
	    	
	    		
	    		
	    		for (ReceptionLine receptionLines : 
	    			 reception.getReceptionlines()){
	    			    System.out.println("____________________________________valider  reception_____________________________________________"); 
	    			    Stock stockDepot = this.stockRepository.findByProductAndLocation(receptionLines.getProduct(), locationDep);
	    			    stockDepot.setQuantity(stockDepot.getQuantity()+receptionLines.getReceptionQte());
	    			   this.stockRepository.save(stockDepot);
	    			
	    			    //-------------<>------stok-mvt-ligne------<>------------------------- 				
	    				StockMovementLine stockMovementLine= new StockMovementLine();
	    				stockMovementLine.setStockMovement(stockMovement);
	    				
	    				//stockMovementLine.setMotif(motif);
	    				stockMovementLine.setDescription("Mouvement ordinaire-entre du produit vers le stock depot");
	    				stockMovementLine.setStockMovement(stockMovement);
	    				stockMovementLine.setProduct(receptionLines.getProduct());
	    				stockMovementLine.setMovmentQte(receptionLines.getReceptionQte());
	    				stockMovementRepositoryLine.save(stockMovementLine);
	    		        
	    				Product prod = productRepository.findOne(receptionLines.getProduct().getId());
	    				prod.setPrice(receptionLines.getPrice());
	    				productRepository.save(prod);
	    			
	    				
	  
	    				if (lotRepository.findByProductAndLabel(receptionLines.getProduct(), receptionLines.getLotNumber())== null){
	    				Lot	lot = new Lot();
						lot.setDateRefusal(receptionLines.getExpirationDate());
						lot.setLabel(receptionLines.getLotNumber());
						lot.setProduct(receptionLines.getProduct());
						lot.setState(true);
						lotRepository.save(lot);
	    						
						
						Stocklot stocklot = new Stocklot();
						stocklot.setLocation(locationRepository.findOne(2l));
						stocklot.setLot(lot);
						stocklot.setQuantity(receptionLines.getReceptionQte());
						stockLotRepository.save(stocklot);
						
	    				}else {
	    					
//modifier le 29/06/2020
//							Lot	lot2 = new Lot();
//							lot2.setState(true);
//							lotRepository.save(lot2);
	    					
	    					Stocklot stocklot = stockLotRepository.
	    					findByLot(lotRepository.findByProductAndLabel(receptionLines.getProduct(), receptionLines.getLotNumber()));
							stocklot.setLocation(locationRepository.findOne(2l));
							stocklot.setLot(lotRepository.findByProductAndLabel(receptionLines.getProduct(), receptionLines.getLotNumber()));
							stocklot.setQuantity(stocklot.getQuantity()+receptionLines.getReceptionQte());
							stockLotRepository.save(stocklot);
	    					
							
	    					
	    				}
	  //---------------<>----------------------------<>-------------------------------------------------------------------------------	
	    				
	    		 }	
	    		 reception.setState(stateRepository.findOne(2l));
	    		 reception.setUtilisateur(user.get(0));
	    		// ExternalOrder externalOrder = externalOrderRepository.findByExternalDelivery(reception.getExternalDelivery());
	    		// externalOrder.setState(stateRepository.findOne(4l));
	    		// externalOrderRepository.save(externalOrder);
	    		 receptionRepository.save(reception); 
	    		 
	    	     msg="la reception  a été validé avec succés";
	    	
	    	
	    }
	    	 return new MessageResponse(HttpStatus.OK.toString(), null, msg,reception.getNumReception());	
	    	 
	    }
	@Override
	public MessageResponse changeStateReception1(Long receptionId, Long stateId, Long externalDeliveryId,
			String extrernalDeliveryNum, Long externalorderId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public List<ReceptionLine> getReceptionsJasper(long id ) {
		 
		ExternalOrder externalOrder = (ExternalOrder) externalOrderRepository.findOne((long) id);
		ReceptionResponseJasper  receptionResponseJasper = new ReceptionResponseJasper();
		List<ReceptionResponseJasper> receptionResponseJaspers = new ArrayList<>();
		
		int num =0;
		for ( ReceptionLine receptionLine :  receptionLineRepository.findByReceptionExternalOrder(externalOrder)) {
			
			
			
			num=+1;
			receptionResponseJasper.setNumCde(externalOrder.getNumExternalOrder());
			receptionResponseJasper.setDateCde(externalOrder.getDateExterOrder());
			receptionResponseJasper.setNum(num);
			receptionResponseJasper.setCodePctProd(receptionLine.getProduct().getCodePctProd());
			receptionResponseJasper.setLib(receptionLine.getProduct().getLib());
			receptionLine.setPrice(receptionLine.getPrice());
			receptionLine.setVat(receptionLine.getVat());
			receptionResponseJasper.setReceptionQte(receptionLine.getReceptionQte());
			receptionResponseJasper.setExpirationDate(receptionLine.getExpirationDate());
			receptionResponseJasper.setLotNumber(receptionLine.getLotNumber());
		}
		
		return receptionLineRepository.findByReceptionExternalOrder(externalOrder);	
	
	}
	


	
	@Override
	public 	Reception findByReceptionnNum(Long referenceId, String numReception) {
		
		if(referenceId==2) {
			
			 Reception reception=  receptionRepository.findByNumReception(numReception); 
			 if(reception!=null) {
					return reception;
					}
					else {	 		
				    return new Reception();
					}
			 	 		
		} else {
			return new Reception();
		}
		
		
		
	}
	
	
	@Override
	public MessageResponse cancelRecep(Long id) {
		
		Reception reception = receptionRepository.findOne(id);
		if(reception==null){
			RestExceptionCode code = RestExceptionCode.RECEPTION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		if(reception.getState().getId()!=2) {
	      reception.setState(stateRepository.findOne(6l));
	      receptionRepository.save(reception);

		}
	
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Reception  a été spprimé avec succés",reception.getNumReception());	
	
	}
	
	
	
	@Override
	public List<ReceptionResponseJasper> getReceptionsJasper(ExternaOrdRequest externaOrdRequest) {
 
		 
		ExternalOrder externalOrder = (ExternalOrder) externalOrderRepository.findOne(externaOrdRequest.getId());
		List<ReceptionResponseJasper> receptionResponseJaspers = new ArrayList<>();
		
		Map<Product, List<ReceptionLine>> lm = receptionLineRepository.findByReceptionExternalOrder1(externalOrder.getId())
				.stream().distinct().collect(Collectors.groupingBy(ReceptionLine::getProduct));

		Integer num =0;
				for (Entry<Product, List<ReceptionLine>>  entry : lm.entrySet()) {
					System.out.println("Item : " + entry.getKey() + " Count : " + entry.getValue().get(0).getProduct().getLib());
					ReceptionResponseJasper  receptionResponseJasper = new ReceptionResponseJasper();
					num=num+1;
					receptionResponseJasper.setNum(num);
					receptionResponseJasper.setNumCde(externalOrder.getNumExternalOrder());
					receptionResponseJasper.setDateCde(externalOrder.getDateExterOrder());
			    	receptionResponseJasper.setNum(num);
					receptionResponseJasper.setCodePctProd(entry.getValue().get(0).getProduct().getCodePctProd());
					receptionResponseJasper.setLib(entry.getValue().get(0).getProduct().getLib());
					receptionResponseJasper.setPrice(entry.getValue().get(0).getPrice());
					receptionResponseJasper.setVat(entry.getValue().get(0).getVat());
					receptionResponseJasper.setReceptionQte(receptionLineRepository.sumrecepQTE(externaOrdRequest.getId(), entry.getValue().get(0).
							getProduct().getId()));
					receptionResponseJasper.setExpirationDate(entry.getValue().get(0).getExpirationDate());
					receptionResponseJasper.setLotNumber(entry.getValue().get(0).getLotNumber());
				
					receptionResponseJaspers.add(receptionResponseJasper);
					
					
				}
				
				
			receptionResponseJaspers.sort(new Comparator<ReceptionResponseJasper>() {
					@Override
					public int compare(ReceptionResponseJasper o1, ReceptionResponseJasper o2) {
						int comparaison =0;
						comparaison=o1.getLib().compareToIgnoreCase(o2.getLib()) ;
				
						return comparaison;
					}

					
				});
		return receptionResponseJaspers;
	}
	
	//________________________________jasper______________________________
	
	@Override
 public File getPdfListReception(ExternaOrdRequest d ) throws FileNotFoundException{
System.out.println("________________getPdfStocklotMvt____________________");
  return this.getPdfListReception(this.getReceptionsJasper( d ));
}


private File getPdfListReception(List<ReceptionResponseJasper> list) throws FileNotFoundException{
JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);

return jasperService.jasperFile(dataSource, "receptionList");

}






	
	
	
	
	
	 
}
