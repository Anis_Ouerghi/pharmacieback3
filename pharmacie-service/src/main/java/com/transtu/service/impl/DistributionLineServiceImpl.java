package com.transtu.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transtu.core.jasper.MissingProductJasper;
import com.transtu.core.jasper.StockJasper;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.core.json.request.FilterStockReport;
import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.repositories.DistributionLineRepository;
import com.transtu.service.DistributionLineService;
import com.transtu.service.DistributionService;
import com.transtu.service.JasperService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class DistributionLineServiceImpl implements DistributionLineService {

	@Autowired(required = true)
	DistributionService distributionService ;
	
	@Autowired(required = true)
	DistributionLineRepository distributionLineRepository;
	
	@Autowired(required=true)
	JasperService jasperService ;

	private double prixttc =0;
	
	@Override
	public File getPdfMissingProductsByDate(Date start, Date end) throws FileNotFoundException {

		
		return this.getPdf(this.getMissingProductFromPdf(start, end), start, end);
	}
	
	
	
	
	//--------------------------------------------------------------------------------------------
	@Override
	public File getExcelMissingProductsByDate1(DateRequestPdf date) throws FileNotFoundException {

		
		return this.getExcel(this.getMissingProductFromPdf(date.getStart(), date.getEnd()));
	}
	
	@SuppressWarnings("unused")
	private Float calculateTotal(Double price , Double vatRate){
		return (float) (price + ((price*vatRate)/100)) ;
		
	}
	
	private File getPdf(List<MissingProductJasper> list , Date start, Date end) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
		return jasperService.jasperFileWithDateParameter(dataSource, "missingProducts", start, end);
	}
	
	private File getExcel(List<MissingProductJasper> list) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
		return jasperService.jasperFileXlsx(dataSource, "missingProducts");
	}
//*************************************************************************
	

	@Override
	public List<MissingProductJasper> getMissingProductFromPdf(Date start, Date end) {
		System.out.println("_________________________missing product------");
		List<MissingProductJasper> productsJasper = new ArrayList<>();
		List<Distribution> distributions = this.distributionService.findByDateDistBetween(start, end);
		distributions.forEach(distribution->{
			distribution.getDistributionLines()
				.stream()
				.filter(distLine->distLine.getMissingQt()>0)
				
				.forEach(distLine->{
					if(!productsJasper
							.stream()
							.anyMatch(p->p.getBarCode().equals(distLine.getProduct().getCodePctProd()))){
						System.out.println("_________________________missing product------"+distLine.getProduct().getId());
						MissingProductJasper productJasp = new MissingProductJasper();
						productJasp.setBarCode(distLine.getProduct().getCodePctProd());
						productJasp.setDesignation(distLine.getProduct().getLib());
						productJasp.setQt(distLine.getMissingQt());
						productJasp.setPrice(distLine.getProduct().getPrice());
						productJasp.setVatRate(distLine.getProduct().getVatRate());
						//productJasp.setTotal(this.calculateTotal(productJasp.getPrice(), productJasp.getVatRate()));
						
						
						prixttc =productJasp.getPrice()*(productJasp.getVatRate()/100)+productJasp.getPrice();
						System.out.println("------------prixttc--------------"+prixttc);
						productJasp.setTotal(productJasp.getQt()*prixttc);
						productsJasper.add(productJasp);
					}else{
						productsJasper
						.stream()
						.filter(p->p.getBarCode().equals(distLine.getProduct().getCodePctProd())).forEach(p->{
							p.setQt(p.getQt()+distLine.getMissingQt());
						});
					}

				}
			);
		});
		return productsJasper;
	}

	@Override
	public File getExcelMissingProductsByDate(DateRequestPdf date) throws FileNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public File getExcelMissingProductsByDate(Date start, Date end) throws FileNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}}
	
	
	


