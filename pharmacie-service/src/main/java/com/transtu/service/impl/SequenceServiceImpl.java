package com.transtu.service.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transtu.persistence.entities.Sequence;
import com.transtu.persistence.entities.SequenceDelivery;
import com.transtu.persistence.entities.SequenceExternalOrder;
import com.transtu.persistence.entities.SequenceMvtExepEntre;
import com.transtu.persistence.entities.SequenceMvtExepSortie;
import com.transtu.persistence.entities.SequenceMvtOrdEntre;
import com.transtu.persistence.entities.SequenceMvtOrdSorti;
import com.transtu.persistence.entities.SequenceOrder;
import com.transtu.persistence.entities.SequenceReception;
import com.transtu.persistence.entities.sequencePresecription;
import com.transtu.persistence.repositories.SequenceDeliveryRepository;
import com.transtu.persistence.repositories.SequenceExternalOrderRepository;
import com.transtu.persistence.repositories.SequenceMvtExepEntreRepository;
import com.transtu.persistence.repositories.SequenceMvtExepSortieRepository;
import com.transtu.persistence.repositories.SequenceMvtOrdEntreRepository;
import com.transtu.persistence.repositories.SequenceMvtOrdSortRepository;
import com.transtu.persistence.repositories.SequenceOrderRepository;
import com.transtu.persistence.repositories.SequenceReceptionRepository;
import com.transtu.persistence.repositories.SequenceRepository;
import com.transtu.persistence.repositories.sequencePrescriptionRepository;
import com.transtu.service.SequenceService;


@Service
public class SequenceServiceImpl implements SequenceService {

	@Autowired
	SequenceRepository sequenceRepository;
	@Autowired
	SequenceDeliveryRepository sequenceDeliveryRepository;
	@Autowired
	SequenceOrderRepository sequenceOrderRepository;
	@Autowired
	SequenceExternalOrderRepository sequencexternalOrderRepository;
	@Autowired
	SequenceReceptionRepository sequenceReceptionRepository;
	@Autowired
	sequencePrescriptionRepository sequencePresecriptionRepository;
	@Autowired
	SequenceMvtExepSortieRepository sequenceMvtExepSortieRepository;
	@Autowired
	SequenceMvtExepEntreRepository  sequenceMvtExepEntreRepository;
	@Autowired
	SequenceMvtOrdEntreRepository sequenceMvtOrdEntreRepository;
	@Autowired
	SequenceMvtOrdSortRepository sequenceMvtOrdSortRepository;
	
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");

	
	//***Calcule num pressecription
	 
	//***
	
//	@Override
//	public String calculateNumPres(String matriculeAgent) {
//		
//		Sequence seq;
//		Integer  i =this.sequenceRepository.findTopById();
//		Integer num = 0;
//		if(i==null){
//			seq = new Sequence();
//			seq.setLastDate(new Date());
//			seq.setNum(1);
//			System.out.println("--calc num prec----");
//			this.sequenceRepository.save(seq);
//			num=1;
//		}
//		else{
//			///---
//			Sequence sequence =this.sequenceRepository.findOne(i);
//			Calendar totday = Calendar.getInstance();
//		    Calendar c = Calendar.getInstance();
//			c.setTime(sequence.getLastDate());
//						
//			Long diff=(totday.getTimeInMillis()-c.getTimeInMillis())/(24*60*60*1000);
//			///---
//			if(diff==0 ){
//			num = sequence.getNum()+1;
//			seq = new Sequence();
//			seq.setLastDate(new Date());
//			seq.setNum(num);
//			this.sequenceRepository.save(seq);
//			}else {
//				seq = new Sequence();
//				seq.setLastDate(new Date());
//				seq.setNum(1);
//				this.sequenceRepository.save(seq);	
//				num=1;
//			}
//		}
//		return num + "_" + matriculeAgent + "_" + simpleDateFormat.format(new Date());
//		 
//	}
	//*****Calcule num distribution
//	
//	@Override
//	public String calculateNumDist(String matriculeAgent) {
//		
//		sequencePresecription seq;
//		//Integer  i =this.sequencePresecriptionRepository.findTopById();
//		Integer i=1000;
//		System.out.println("-*-*/-*-*--/**");
//		Integer num = 0;
//		if(i==null){  
//			seq = new sequencePresecription();
//			seq.setLasteDate(new Date());
//			seq.setNum(1);
//			System.out.println("--calc num Distribution----");
//			this.sequencePresecriptionRepository.save(seq);
//			num=1;
//		}
//		else{
//			///---
//			sequencePresecription  sequence =this.sequencePresecriptionRepository.findOne(i);
//			Calendar totday = Calendar.getInstance();
//		    Calendar c = Calendar.getInstance();
//			c.setTime(sequence.getLasteDate());
//						
//			Long diff=(totday.getTimeInMillis()-c.getTimeInMillis())/(24*60*60*1000);
//			///---
//			if(diff==0 ){
//			num = sequence.getNum()+1;
//			seq = new sequencePresecription();
//			seq.setLasteDate(new Date());
//			seq.setNum(num);
//			this.sequencePresecriptionRepository.save(seq);
//			}else {
//				seq = new sequencePresecription();
//				seq.setLasteDate(new Date());
//				seq.setNum(1);
//				this.sequencePresecriptionRepository.save(seq);	
//				num=1;
//			}
//		}
//		return num + "_" + matriculeAgent + "_" + simpleDateFormat.format(new Date());
//		 
//	}
	//*****
	
	@Override
	public String calculateNumDest(String matriculeAgent) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		Integer currentYear = c.get(Calendar.YEAR);
		Sequence seq= this.sequenceRepository.findByYear(currentYear);
		Integer num = 0;
		if(seq==null){
			seq = new Sequence(currentYear, 1);
			this.sequenceRepository.save(seq);
			num=1;
		}
		else{
			num = seq.getNum()+1;
			seq.setNum(num);
			this.sequenceRepository.save(seq);
		}
		return num + "_" + matriculeAgent + "_" + simpleDateFormat.format(new Date());
		 
	}

	@Override
	public String calculateNumDelivery(String location) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		Integer currentYear = c.get(Calendar.YEAR);
		SequenceDelivery seq= this.sequenceDeliveryRepository.findByYear(currentYear);
		Integer num = 0;
		if(seq==null){
			seq = new SequenceDelivery(currentYear, 1);
			this.sequenceDeliveryRepository.save(seq);
			num=1;
		}
		else{
			num = seq.getNum()+1;
			seq.setNum(num);
			this.sequenceDeliveryRepository.save(seq);
		}
		return num + "_" + location + "_" + simpleDateFormat.format(new Date());
	}

	@Override
	public String calculateNumOrder() {
		
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			Integer currentYear = c.get(Calendar.YEAR);
			SequenceOrder seq= this.sequenceOrderRepository.findByYear(currentYear);
			Integer num = 0;
			if(seq==null){
				seq = new SequenceOrder(currentYear, 1);
				this.sequenceOrderRepository.save(seq);
				num=1;
			}
			else{
				
				num = seq.getNum()+1;
				seq.setNum(num);
				this.sequenceOrderRepository.save(seq);
			}
			return num + "_" + simpleDateFormat.format(new Date());
			 
		}

	
	public String calculateNumExternalOrder() {
		System.out.println("-----------****-----1---------");
		Calendar c = Calendar.getInstance();
		System.out.println("-----------****-----3---------");
		c.setTime(new Date());
		Integer currentYear = c.get(Calendar.YEAR);
		SequenceExternalOrder seq= this.sequencexternalOrderRepository.findByYear(currentYear);
		Integer num = 0;
		System.out.println("-----------****-----2---------");
		if(seq==null){
			seq = new SequenceExternalOrder(currentYear, 1);
			this.sequencexternalOrderRepository.save(seq);
			num=1;
			System.out.println("-----------****--------------"+num);
		}
		else{
			
			num = seq.getNum()+1;
			System.out.println("-------------------------"+num);
			seq.setNum(num);
			this.sequencexternalOrderRepository.save(seq);
		}
		return num + "_" + simpleDateFormat.format(new Date());
		 
	}

	
public String calculateReceptionOrder() {
		
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		Integer currentYear = c.get(Calendar.YEAR);
		SequenceReception seq= this.sequenceReceptionRepository.findByYear(currentYear);
		Integer num = 0;
		if(seq==null){
			seq = new SequenceReception(currentYear, 1);
			this.sequenceReceptionRepository.save(seq);
			num=1;
		}
		else{
			
			num = seq.getNum()+1;
			seq.setNum(num);
			this.sequenceReceptionRepository.save(seq);
		}
		return num + "_" + simpleDateFormat.format(new Date());
		 
	}



@Override
public String calculateNumMvtExepSortie() {
	Calendar c = Calendar.getInstance();
	c.setTime(new Date());
	Integer currentYear = c.get(Calendar.YEAR);
	SequenceMvtExepSortie seq= this.sequenceMvtExepSortieRepository.findByYear(currentYear);
	Integer num = 0;
	if(seq==null){
		seq = new SequenceMvtExepSortie(currentYear, 1);
		this.sequenceMvtExepSortieRepository.save(seq);
		num=1;
	}
	else{
		
		num = seq.getNum()+1;
		seq.setNum(num);
		this.sequenceMvtExepSortieRepository.save(seq);
	}
	return num + "_"+"S-Exp"+"_"+simpleDateFormat.format(new Date());
}

@Override
public String calculateNumMvtExepEntre() {
	Calendar c = Calendar.getInstance();
	c.setTime(new Date());
	Integer currentYear = c.get(Calendar.YEAR);
	SequenceMvtExepEntre seq= this.sequenceMvtExepEntreRepository.findByYear(currentYear);
	Integer num = 0;
	if(seq==null){
		seq = new SequenceMvtExepEntre(currentYear, 1);
		this.sequenceMvtExepEntreRepository.save(seq);
		num=1;
	}
	else{
		
		num = seq.getNum()+1;
		seq.setNum(num);
		this.sequenceMvtExepEntreRepository.save(seq);
	}
	return num + "_"+"E-Exp"+"_"+simpleDateFormat.format(new Date());
}

@Override
public String calculateNumMvtOrdSortie() {
	Calendar c = Calendar.getInstance();
	c.setTime(new Date());
	Integer currentYear = c.get(Calendar.YEAR);
	SequenceMvtOrdSorti seq= this.sequenceMvtOrdSortRepository.findByYear(currentYear);
	Integer num = 0;
	if(seq==null){
		seq = new SequenceMvtOrdSorti(currentYear, 1);
		this.sequenceMvtOrdSortRepository.save(seq);
		num=1;
	}
	else{
		
		num = seq.getNum()+1;
		seq.setNum(num);
		this.sequenceMvtOrdSortRepository.save(seq);
	}
	
	return num + "_"+"S-Ord"+"_"+simpleDateFormat.format(new Date());
}

@Override
public String calculateNumMvtOrdEntre() {
	Calendar c = Calendar.getInstance();
	c.setTime(new Date());
	Integer currentYear = c.get(Calendar.YEAR);
	SequenceMvtOrdEntre seq= this.sequenceMvtOrdEntreRepository.findByYear(currentYear);
	Integer num = 0;
	if(seq==null){
		seq = new SequenceMvtOrdEntre(currentYear, 1);
		this.sequenceMvtOrdEntreRepository.save(seq);
		num=1;
	}
	else{
		
		num = seq.getNum()+1;
		seq.setNum(num);
		this.sequenceMvtOrdEntreRepository.save(seq);
	}
	
	return num + "_"+"E-Ord"+"_"+ simpleDateFormat.format(new Date());
}


@Override
public String calculatePrescription(String matriculeAgent) {
	
	Calendar c = Calendar.getInstance();
	c.setTime(new Date());
	Integer currentYear = c.get(Calendar.YEAR);
	sequencePresecription seq= this.sequencePresecriptionRepository.findByYear(currentYear);
	Integer num = 0;
	if(seq==null){
		seq = new sequencePresecription(currentYear, 1);
		this.sequencePresecriptionRepository.save(seq);
		num=1;
	}
	else{
		
		num = seq.getNum()+1;
		seq.setNum(num);
		this.sequencePresecriptionRepository.save(seq);
	}
	return num + "_"+matriculeAgent + "_" +simpleDateFormat.format(new Date());
	 
}




	}
	


