package com.transtu.service.impl;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.transtu.core.dto.UsersDto;
import com.transtu.core.json.request.AddDayRequest;
import com.transtu.core.json.request.AddUserDayRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Day;
import com.transtu.persistence.entities.DayType;
import com.transtu.persistence.entities.UserDay;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.persistence.repositories.DayRepository;
import com.transtu.persistence.repositories.DayTypeRepository;
import com.transtu.persistence.repositories.StateRepository;
import com.transtu.persistence.repositories.UserDayRepository;
import com.transtu.persistence.repositories.UtilisateurRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.DayService;

@Service
public class DayServiceImpl implements DayService{
	
	@Autowired(required=true)
	DayRepository dayRepo ;
	
	
	@Autowired(required = true)
	private DayTypeRepository dayTypeRepo;
	
	@Autowired(required = true)
	StateRepository stateRepository;
	
	@Autowired(required = true)
	UserDayRepository userDayRepository;
	
	@Autowired(required = true)
	UtilisateurRepository userRepository;
	
	@Autowired(required = true)
	UtilisateurRepository utilisateurRepository;
	
	@Override
	public MessageResponse addDay(AddDayRequest dayRequest) {
		
		DayType dayType = this.dayTypeRepo.findOne(dayRequest.getDayTypeId());
		if (dayType == null) {
			RestExceptionCode code = RestExceptionCode.FAMILY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Day d = new Day();
		d.setDate(dayRequest.getDate());
		d.setState(dayRequest.isState());
		d.setDayType(dayType);
		dayRepo.save(d);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la journée est ajoutée avec success","");
		
	}

	@Override
	public MessageResponse updateDate(AddDayRequest dayRequest) {
		DayType dayType = this.dayTypeRepo.findOne(dayRequest.getDayTypeId());
		
		if((this.dayRepo.findOne(dayRequest.getId())==null)||(dayType == null)){
			RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Day d = this.dayRepo.findOne(dayRequest.getId());
		d.setDate(dayRequest.getDate());
		d.setState(dayRequest.isState());
		d.setDayType(dayType);
		
		dayRepo.save(d);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la journée est modifiée avec success", d.getDayType().getLibelle());	
	}

	@Override
	public MessageResponse deleteDate(Long idDay) {
		
		if(this.dayRepo.findOne(idDay)==null){
			RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		dayRepo.delete(idDay);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la journée est supprimée avec success",this.dayRepo.findOne(idDay).getDayType().getLibelle());	
	}

	@Override
	public List<Day> getAllDays() {
		return this.dayRepo.findAll(new Sort(Direction.ASC, "date"));
	}

	@Override
	public List<Day> filterDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Day> getDayByStateAndDayType(boolean state, Long dayTypeId) {
		
		return this.dayRepo.findByStateAndDayType(state, this.dayTypeRepo.findOne(dayTypeId));
	}

	
	@Override
	public  void openDaytoFirstAuthentication() {
		
		Date today= new Date();
		DayType dayType =  dayTypeRepo.findOne(1l);
		List<Day> listDays = dayRepo.findByDayType(this.dayTypeRepo.findOne(1l));
		if(!listDays.isEmpty()){
		
		Long max = 0l;
		for ( Day day :  listDays ){
			if(day.getId()>max){
				max=day.getId();
				
			 }
			}
		
		Day day = dayRepo.findOne(max);
		if(day==null ){
			RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
	    Calendar totday = Calendar.getInstance();
	    Calendar c = Calendar.getInstance();
		c.setTime(day.getDate());
					
		Long diff=(totday.getTimeInMillis()-c.getTimeInMillis())/(24*60*60*1000);
		        
		if(diff>0 ){
				
				Day day1 = new Day();
		 		day1.setDate(today);
				day1.setDayType(dayType);
				day1.setState(true);
				dayRepo.save(day1);
				
			 }
		
		}else {
		
		Day day1 = new Day();
		day1.setDate(today);
		day1.setDayType(dayType);
		day1.setState(true);
		dayRepo.save(day1);
		}
		
	}
	@Override
	public MessageResponse addUserDay(AddUserDayRequest addUserDayRequest) {
	
		Day day = new Day();
		day.setDate(addUserDayRequest.getDateDay());
		DayType dayType = dayTypeRepo.findOne(addUserDayRequest.getDayTypeId());
		if(dayType==null ){
			RestExceptionCode code = RestExceptionCode.DAYTYPE_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		  }
		day.setDayType(dayType);
		day.setState(true);
		dayRepo.save(day);
		for(UsersDto usersDto :addUserDayRequest.getUsersDto()){
			UserDay userDay = new UserDay();
			userDay.setDay(day);
			Utilisateur utilisateur= utilisateurRepository.findOne(usersDto.getUserId());
			userDay.setUtilisateur(utilisateur);
			userDayRepository.save(userDay);
		
			
			}
			
		
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "les utilisateur ont été ajouté avec succès a la journée","");
	}
	
	
	@Override
	public MessageResponse cancelUserDay(Long dayId, Long userId) {
		
		Utilisateur user = userRepository.findOne(userId);
		if(user==null ){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Day day =dayRepo.findOne(dayId);
		if(day==null ){
			RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		UserDay userDay =userDayRepository.findByDayAndUtilisateur(day, user);
		userDayRepository.delete(userDay);
		
		 return new MessageResponse(HttpStatus.OK.toString(), null, "les utilisateur ont été annulé avec succès ",user.getUsername());
	}

	@Override
	public void openDayToAuthentication() throws ParseException {

//		SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy",Locale.FRENCH);
//		Date date = new Date();
		final Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -1);
	    Date date =  cal.getTime();
	    List<Day> oldDay = this.dayRepo.findByDateAfter(date) ;
	  
		if(oldDay.size()==0){
			this.createNewDay();
		}else{
			if(DateUtils.isSameDay(oldDay.get(0).getDate(), date)){
			
				this.createNewDay();
				
			}
			 
		}
		 
		
	}
	
	private void createNewDay(){
		Day day = new Day();
		day.setDayType(dayTypeRepo.findOne(1l));
		day.setDate(new Date());
		day.setState(true);
		dayRepo.save(day);
	}
		
}
	


