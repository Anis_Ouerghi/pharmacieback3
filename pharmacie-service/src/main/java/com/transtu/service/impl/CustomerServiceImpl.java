package com.transtu.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Customer;
import com.transtu.persistence.entities.Presentation;
import com.transtu.persistence.repositories.CustomerRepository;
import com.transtu.persistence.repositories.PresentationRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.CustomerService;


@Service
public class CustomerServiceImpl implements CustomerService {

	
	private final static Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired(required = true)
	private CustomerRepository customerRepository;
	@Override
	public MessageResponse addCustomer(String label) {
		logger.info("___________Add customer_______________");
		Customer customer = customerRepository.findByLabel(label);
		if(customer != null ){
			RestExceptionCode code = RestExceptionCode.CUSTOMER_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Customer customers = new Customer() ;
		customers.setLabel(label);
		this.customerRepository.save(customers);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "le client est ajouté avec succés",customers.getLabel());
	}

	@Override
	public List<Customer> getListCustomer() {
		logger.info("___________get list Customer_______________");
		return this.customerRepository.findAll();
	}

	@Override
	public MessageResponse updateCustomer(Long id, String label) {
		Customer customer = customerRepository.findByLabel(label);
		if(customer != null ){
			RestExceptionCode code = RestExceptionCode.CUSTOMER_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Customer customers = this.customerRepository.findOne(id);
		customers.setLabel(label);
		this.customerRepository.save(customers);
		return new MessageResponse(HttpStatus.OK.toString(), null, "le client est modifié avec succés",label);
	}

	@Override
	public MessageResponse deleteCustomer(Long id) {
		Customer customer = this.customerRepository.findOne(id);
		this.customerRepository.delete(customer);
		return new MessageResponse(HttpStatus.OK.toString(), null, "le client est supprimé avec succés",customer.getLabel());
	}

}
