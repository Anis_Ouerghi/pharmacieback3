package com.transtu.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.transtu.core.json.request.ActiveProductByLabelStartWith;
import com.transtu.core.json.request.ActiveProductByLabelStartWithAndDep;
import com.transtu.core.json.request.AddProductRequest;
import com.transtu.core.json.request.DuplcateEmptyResp;
import com.transtu.core.json.request.FilterProductAssistOrderResponse;
import com.transtu.core.json.request.FilterProductResponse;
import com.transtu.core.json.request.FilterProductResponseSpecific;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.ProductAutoCompleteResponse;
import com.transtu.core.json.request.ProductQuantityDep;
import com.transtu.core.json.request.ProductRequestFilter;
import com.transtu.core.json.request.ProductRequestFilterAssistOrder;
import com.transtu.core.json.request.ProductResp;
import com.transtu.core.json.request.ProductResponse;
import com.transtu.core.json.request.ProductResponseSearch;
import com.transtu.core.json.request.UpdateProductRequest;
import com.transtu.core.product.LightProduct;
import com.transtu.persistence.entities.Depot;
import com.transtu.persistence.entities.DistributionLine;
import com.transtu.persistence.entities.InventoryLine;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.QPrescriptionLine;
import com.transtu.persistence.entities.QProduct;
import com.transtu.persistence.entities.Stock;
import com.transtu.persistence.entities.StockMovementLine;
import com.transtu.persistence.repositories.DciRepository;
import com.transtu.persistence.repositories.DepotRepository;
import com.transtu.persistence.repositories.DistributionUnitRepository;
import com.transtu.persistence.repositories.DossageRepository;
import com.transtu.persistence.repositories.FamilyRepository;
import com.transtu.persistence.repositories.FormRepository;
import com.transtu.persistence.repositories.InventoryLineRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.PharmaClassRepository;
import com.transtu.persistence.repositories.PrescriptionLineRepository;
import com.transtu.persistence.repositories.PresentationRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.StockMovementLineRepository;
import com.transtu.persistence.repositories.StockRepository;
import com.transtu.persistence.repositories.TypeRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.LotService;
import com.transtu.service.ProductService;
import com.transtu.service.UtilisateurService;

@Service
public class ProductServiceImpl implements ProductService {
	
	@PersistenceContext
	EntityManager em ;

	private final static Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

	PrescriptionLineRepository prescriptionLineRepo;
	
	@Autowired(required = true)
	private ProductRepository produitRepo;
	
	@Autowired(required = true)
	PharmaClassRepository pharmaClassRepository;


	@Autowired(required = true)
	FamilyRepository FamilyRepository;
	
	
	@Autowired(required = true)
	StockRepository stockRepo;

	@Autowired(required = true)
	TypeRepository typeRepository;
	
	@Autowired(required = true)
	PresentationRepository presentationRepository;
	
	@Autowired(required = true)
	LocationRepository locationRepository;
	
	@Autowired(required = true)
	DepotRepository depotRepository;
	
	@Autowired(required = true)
	DciRepository dciRepository;
	
	@Autowired(required = true)
	DistributionUnitRepository distributionUnitRepository;
	
	@Autowired(required = true)
	DossageRepository DossageRepository;
	
	@Autowired(required = true)
	FormRepository formRepository;
	
	@Autowired(required = true)
	UtilisateurService utilisateurService;
	
	
	@Autowired(required = true)
	StockMovementLineRepository stockMovementLineRepository;
	
	@Autowired(required = true)
	ProductRepository productRepository;
	
	@Autowired(required = true)
	InventoryLineRepository inventoryLineRepository;
	
	
	@Autowired(required = true)
	LotService  lotService;
	
	@Autowired(required = true)
	com.transtu.persistence.repositories.DistributionLineRepository distributionLineRepos;
	
	@Override
	public MessageResponse addProduct(AddProductRequest productRequest) {
		
		
	
		
		if(productRequest.getCodeaBarre() != "" && productRequest.getCodeaBarre() != null ) {
			if (productRepository.findBycodePctProd(productRequest.getCodeaBarre()).size()>0 ) {
				return new MessageResponse(HttpStatus.NOT_ACCEPTABLE.toString(), null, "Le Produit existant","");
//				RestExceptionCode code = RestExceptionCode.CODE_A_BARRE_EXISTED;
//				RestException ex = new RestException(code.getError(), code);
//				throw ex;
			}
			
			}
		
		if(productRequest.getCodePctProd() != "" && productRequest.getCodePctProd() != null ) {
		if (productRepository.findBycodePctProd(productRequest.getCodePctProd()) .size()>0 ) {
			return new MessageResponse(HttpStatus.NOT_ACCEPTABLE.toString(), null, "Le Produit existant","");
//			RestExceptionCode code = RestExceptionCode.CODE_PCT_EXISTED;
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
		}
		
		}
		
		if(productRequest.getLibelle() != "" && productRequest.getLibelle() != null) {
		if (productRepository.findByLibContainingIgnoreCase(productRequest.getLibelle()).size()>0 ) {
			return new MessageResponse(HttpStatus.NOT_ACCEPTABLE.toString(), null, "Le Produit existant","");
//			RestExceptionCode code = RestExceptionCode.LABEL_EXISTED;
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
		}
		}
		
	
		Product p = new Product();
		p.setBarCode(productRequest.getCodeaBarre());
		p.setCodePctProd(productRequest.getCodePctProd());
		p.setActived(true);
	
		p.setLib(productRequest.getLibelle());
		p.setWarningStock(productRequest.getWarningStock());
		p.setMinStock(productRequest.getStockMin());
		p.setSafetyStock(productRequest.getSafetyStock());
		p.setVatRate(productRequest.getVatRate());
		p.setColisage(productRequest.getColisage());
		p.setPrice(productRequest.getPrice());
		
		
		
		
		if (productRequest.getDciId()!=null && productRequest.getDciId()!=0 ){
			p.setDci(dciRepository.findOne(productRequest.getDciId()));
		}
		if (productRequest.getDistributionUnitId()!=null && productRequest.getDistributionUnitId()!=0 ){
			p.setDistributionUnit(distributionUnitRepository.findOne(productRequest.getDistributionUnitId()));
		}
		
		if (productRequest.getDosageId()!=null && productRequest.getDosageId()!=0){
			p.setDossage(DossageRepository.findOne(productRequest.getDosageId()));
			}
		
		if (productRequest.getTypeId()!=null && productRequest.getTypeId()!=0){
			p.setType(typeRepository.findOne(productRequest.getTypeId()));
			
			
		}
		
		if (productRequest.getDepotId()!=null && productRequest.getDepotId()!=0){
			p.setDepot(depotRepository.findOne(productRequest.getDepotId()));
		}
		if (productRequest.getFormId()!=null && productRequest.getFormId()!=0){
			p.setForm(formRepository.findOne(productRequest.getFormId()));
		}
		if (productRequest.getPresentationId()!=null && productRequest.getPresentationId()!=0 ){
			p.setPresentation(presentationRepository.findOne(productRequest.getPresentationId()));
		}
		if (productRequest.getPharmaClassId()!=null && productRequest.getPharmaClassId()!=0 ){
			p.setPharmaClass(pharmaClassRepository.findOne(productRequest.getPharmaClassId()));
		}
		
		this.produitRepo.save(p);
		
		
		Location locationPharma = locationRepository.findOne(1l);
		Location locationDep = locationRepository.findOne(2l);
		if ((locationPharma == null) ||(locationDep == null)  ) {
			RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}

		Stock stock = new Stock();
		stock.setLocation(locationPharma);
		stock.setProduct(p);
		stock.setQuantity(0);
		stockRepo.save(stock);
		Stock stock1 = new Stock();
		stock1.setLocation(locationDep);
		stock1.setProduct(p);
		stock1.setQuantity(0);
		stockRepo.save(stock1);
		
		
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Le Produit est ajouté avec succés",p.getId().toString());
		
	}

	@Override
	public List<ProductResponse> getListProducts() {
		
		long start = System.currentTimeMillis();
		logger.info("___________get list products_______________");
		Location  locationPharma = locationRepository.findOne(1l);
		Location  locationdepot = locationRepository.findOne(2l);
		if ((locationPharma == null) ||(locationdepot == null)  ) {
			RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<ProductResponse> listproductRep = new ArrayList<>();
		
		for (Product product2 : produitRepo.findDistinctByActived(true, (new Sort(Direction.ASC, "lib"))) ){
		ProductResponse productResponse = new ProductResponse();
		
		productResponse.setId(product2.getId());
		productResponse.setBarCode(product2.getBarCode());
		productResponse.setCodePctProd(product2.getCodePctProd());
		productResponse.setLabelSearchStkPha(concatenationLabel(product2.getId()));
		
		productResponse.setLabelSearch(concatenationLabelwithoutQte(product2.getId()));
		productResponse.setLib(product2.getLib());
		productResponse.setActived(product2.isActived());
		
		if(product2.getDci()==null){
			productResponse.setDci(null);
		}
	    else{
			productResponse.setDci(product2.getDci().getLabel());
		}
		
		if(product2.getDepot()==null){
			productResponse.setDepot(null);
		}
		else{
			productResponse.setDepot(product2.getDepot().getLabel());
		}
		
	    if(product2.getDistributionUnit()==null)
	    {
			productResponse.setDistributionUnit(null);
		}
		else{
		productResponse.setDistributionUnit(product2.getDistributionUnit().getLabel());		}
		
		if(product2.getDossage()==null){
			productResponse.setDossage(null);
		}
		else{
			productResponse.setDossage(product2.getDossage().getLabel());
		}
		
		if(product2.getForm()==null){
		productResponse.setForm(null);
		}
		else{
			productResponse.setForm(product2.getForm().getLabel());
		}
		if(product2.getPharmaClass()==null){
			productResponse.setPharmaClass(null);
		}
		else{
			productResponse.setPharmaClass(product2.getPharmaClass().getLabel());
		}
		if(product2.getPresentation()==null){
			productResponse.setPresentation(null);
		}
	
		else{
		productResponse.setPresentation(product2.getPresentation().getLabel());
	    }
		if(product2.getType()==null){
			productResponse.setType(null);
		}
	   else
	   {
			productResponse.setType(product2.getType().getLabel());
	   }
		
		if ((stockRepo.findByProductAndLocation(product2, locationPharma))==null) 
		{
			productResponse.setStockPharma(0);
		}else {
			productResponse.setStockPharma(stockRepo.findByProductAndLocation
				    (product2, locationPharma).getQuantity());
	    }
		
		if ((stockRepo.findByProductAndLocation(product2, locationdepot))==null) 
		{
			productResponse.setStockDep(0);
	     }
		else {
			productResponse.setStockDep(stockRepo.findByProductAndLocation
			    (product2, locationdepot).getQuantity());
		}
		
		
		
		listproductRep.add(productResponse);
		
		}
		
		return listproductRep;
		
		
	}
	
	@Override
	public List<Product> findByBarCode(String code) {
		logger.info("___________get  product by bar  code_______________");
		return produitRepo.findByBarCode(code);
	}
	
	@Override
	public List<Product> getLibStartingWith(String name) {
		logger.info("___________get  product by name_______________");
		
		return produitRepo.findByLibStartingWithIgnoreCase(name);
		
		
	}

	@Override
	public List<Product> getListStockBreak() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public FilterProductAssistOrderResponse filterProductAssitOrder(ProductRequestFilterAssistOrder filterProduct) {
//
//		JPQLQuery<?> query = new JPAQuery(em); 
//		QProduct product = QProduct.product;
//		query.from(product).orderBy(product.lib.asc());
//
//		System.out.println("------------------------------------------------------------------------------------------");
//		if(filterProduct.getTypeId()!=null && filterProduct.getTypeId()!= 0 ){
//			query.where(product.type.id.eq(filterProduct.getTypeId()));
//
//		}
//		if(filterProduct.getDossageId()!=null && filterProduct.getDossageId()!=0){
//			query.where(product.dossage.id.eq(filterProduct.getDossageId()));
//
//		}
//		
//		if(filterProduct.getDciId()!=null && filterProduct.getDciId()!=0){
//		
//			query.where(product.dci.id.eq(filterProduct.getDciId()));
//		}
//		
//		if(filterProduct.getDepotId()!=null && filterProduct.getDepotId()!=0){
//	
//			query.where(product.depot.id.eq(filterProduct.getDepotId()));
//		}
//		
//		if(filterProduct.getPharmaClassId()!=null && filterProduct.getPharmaClassId()!= 0){
//		
//			query.where(product.pharmaClass.id.eq(filterProduct.getPharmaClassId()));
//		}
//		
//		if(filterProduct.getFormId()!=null && filterProduct.getFormId()!=0){
//	
//			query.where(product.form.id.eq(filterProduct.getFormId()));
//		}
//		
//		if(filterProduct.getPresentationId()!=null && filterProduct.getPresentationId()!=0 ){
//			
//			query.where(product.presentation.id.eq(filterProduct.getPresentationId()));
//		}
//
//		
//		if(filterProduct.getCodePctProd()!=null && filterProduct.getCodePctProd()!="" && filterProduct.getCodePctProd()!="0"){
//	
//			query.where(product.codePctProd.contains(filterProduct.getCodePctProd()));
//		}
//		
//		if(filterProduct.getBarCode()!=null && filterProduct.getBarCode() !="" && filterProduct.getBarCode() !="0" ){
//			
//			query.where(product.barCode.eq(filterProduct.getBarCode()));
//		}
//		
//		if(filterProduct.getFamilyId()!=null && filterProduct.getFamilyId()!=0){
//		
//			query.where(product.family.id.eq(filterProduct.getFamilyId()));
//		}
////		
////		if(filterProduct.getLib()!=null){
////			query.where(product.lib.lower().contains(filterProduct.getLib().toLowerCase()));
////		}
//		
////		if(filterProduct.getLib()!=null){
////			query.where(product.lib.lower().contains(filterProduct.getLib().toLowerCase()));
////		}
//		
//		if(filterProduct.getLib()!=null && filterProduct.getLib()!="" ){
//		
//			query.where(product.lib.lower().startsWith(filterProduct.getLib().toLowerCase()));
//		}
//		
//
//		if(filterProduct.getActived()!=null && filterProduct.getActived()!=null){
//			
//			query.where(product.actived.eq(filterProduct.getActived()));
//		}
//		
//		query.distinct();
//		
//		System.out.println("---------------------------------------------------------------------------2---------------");
//	
//		//*** -----------------assist cde-externe------------------------------------------------------------------------
//		
//		if(filterProduct.getAlerte()==true){
//			List<ProductResp> lisprod = new ArrayList<>();
//			FilterProductAssistOrderResponse filterProductResponse = new FilterProductAssistOrderResponse();
//			long i =0;
//			for(Product  product1 : query.select(product).fetch()) {
//				
//				if(product1.getWarningStock() > stockRepo.getSumQteStockDep(product1.getId(), 2l)) {
//					
//					ProductResp p = new ProductResp();
//					p.setId(product1.getId());
//					p.setBarCode(product1.getBarCode());
//					p.setLib(product1.getLib());
//					p.setCodePctProd(product1.getCodePctProd());
//					p.setActived(product1.isActived());
//					p.setVatRate(product1.getVatRate());
//					p.setWauCost(product1.getWauCost());
//					p.setSafetyStock(product1.getSafetyStock());
//					p.setMinStock(product1.getMinStock());
//					p.setWarningStock(product1.getWarningStock());
//					p.setPrice(product1.getPrice());
//					p.setColisage(product1.getColisage());
//					p.setDossage(product1.getDossage());
//					p.setType(product1.getType());
//					p.setDci(product1.getDci());
//					p.setDistributionUnit(product1.getDistributionUnit());
//					p.setForm(product1.getForm());
//					p.setPharmaClass(product1.getPharmaClass());
//					p.setDepot(product1.getDepot());
//					p.setPresentation(product1.getPresentation());
//					p.setStocks(product1.getStocks());
//					Double qteSort = getSumQteMvt("-1", product1 ,locationRepository.findOne(2L),  filterProduct.getSatart(),filterProduct.getEnd()  );	
//					System.out.println("-----------------------------------qte--sort------------------------------------------------------"+qteSort);
//					
//					p.setAverageConsum(qteSort);
//					i =i+1;
//					lisprod.add(p);
//				}
//			}
//			
//			
//			if(filterProduct.getPage()!=null){
//				if(filterProduct.getLinesNbr()==null){
//					RestExceptionCode code = RestExceptionCode.LINES_NUMBER_REQUIRED_IN_PAGINATION;
//					RestException ex = new RestException(code.getError(), code);
//					throw ex;
//				}
//				query.offset((filterProduct.getPage()-1)*filterProduct.getLinesNbr());
//				query.limit(filterProduct.getLinesNbr());
//				
//			}
//			
//			
//			filterProductResponse.setProducts(lisprod);
//			filterProductResponse.setTotalProducts(i);
//			if(filterProduct.getPage()!=null){
//				filterProductResponse.setPage(filterProduct.getPage());
//				filterProductResponse.setNbrPages(i/filterProduct.getLinesNbr());
//			}
//			
//			return filterProductResponse ;
//			
//		//*** ----------------- cde-externe assi security----------------------------
//		}
//		
//		
//		
//		else {
//			
//			//if (filterProduct.getSecurity()==true && filterProduct.getAlerte()==false) {
//		
//			System.out.println("-----------------------------cde-externe assi security---------------------------------------------2---------------");
//		
//		FilterProductAssistOrderResponse filterProductResponse = new FilterProductAssistOrderResponse();
//		List<ProductResp> lisprod = new ArrayList<>();
//		long i =0;
//		for(Product  product1 : query.select(product).fetch()) {
//			
//			Long sumQte =  (long) stockRepo.getSumQteStockDep(product1.getId(), 2l);
//			if(sumQte <= product1.getSafetyStock()  && sumQte  > product1.getWarningStock()) {
//			ProductResp p = new ProductResp();
//			p.setId(product1.getId()); 
//			p.setBarCode(product1.getBarCode());
//			p.setLib(product1.getLib());
//			p.setCodePctProd(product1.getCodePctProd());
//			p.setActived(product1.isActived());
//			p.setVatRate(product1.getVatRate());
//			p.setWauCost(product1.getWauCost());
//			p.setSafetyStock(product1.getSafetyStock());
//			p.setMinStock(product1.getMinStock());
//			p.setWarningStock(product1.getWarningStock());
//			p.setPrice(product1.getPrice());
//			p.setColisage(product1.getColisage());
//			p.setDossage(product1.getDossage());
//			p.setType(product1.getType());
//			p.setDci(product1.getDci());
//			p.setDistributionUnit(product1.getDistributionUnit());
//			p.setForm(product1.getForm());
//			p.setPharmaClass(product1.getPharmaClass());
//			p.setDepot(product1.getDepot());
//			p.setPresentation(product1.getPresentation());
//			p.setStocks(product1.getStocks());
//			p.setStocks(product1.getStocks());	
//			Double qteSort = getSumQteMvt("-1", product1 ,locationRepository.findOne(2L),  filterProduct.getSatart(),filterProduct.getEnd()  );	
//			p.setAverageConsum(qteSort);
//			i =i+1;
//			lisprod.add(p);
//			}
//		}
//		
//		filterProductResponse.setProducts(lisprod);
//		filterProductResponse.setTotalProducts(i);
//		if(filterProduct.getPage()!=null){
//			filterProductResponse.setPage(filterProduct.getPage());
//			filterProductResponse.setNbrPages(i/filterProduct.getLinesNbr());
//		}
//		
//		return filterProductResponse ;
//			}
//		//}
//		//return null;
//	}
//	
	
	
	
	@Override
	public FilterProductAssistOrderResponse filterProductAssitOrder(ProductRequestFilterAssistOrder filterProduct) {


			List<ProductResp> lisprod = new ArrayList<>();
			List<com.transtu.persistence.entities.ProductResp> lisprodByDate = new ArrayList<>();
			//lisprodByDate=distributionLineRepos.prodbyDate(filterProduct.getSatart(),filterProduct.getEnd());
			List<Product> lisproduit = new ArrayList<>();
			
			FilterProductAssistOrderResponse filterProductResponse = new FilterProductAssistOrderResponse();
		
			                                  if(filterProduct.getDepotId()!= null){
			                                	  lisprodByDate =  distributionLineRepos.prodbyDepAndDate(filterProduct.getDepotId(),filterProduct.getSatart(),filterProduct.getEnd());
			                                  }else {
			                                	  lisprodByDate =  distributionLineRepos.prodbyDate(filterProduct.getSatart(),filterProduct.getEnd());
			                                  
			                                  }
			                      			Location pharma = locationRepository.findOne(1l);                                 	                 
			                      			Location depot = locationRepository.findOne(2l);         
	
			for(com.transtu.persistence.entities.ProductResp  product1 : lisprodByDate) {
			
					ProductResp p = new ProductResp();
					p.setId(product1.getId());
					p.setLib(product1.getLib());
					p.setCodePctProd(product1.getCodePctProd());
					p.setVatRate(product1.getVatRate());
					p.setPrice(product1.getPrice());
					p.setColisage(product1.getColisage());
					p.setDepot(product1.getDepot());
					p.setPresentation(product1.getPresentation());
					p.setStocks(stockRepo.findByProductId(product1.getId())); 
				    p.setAverageConsum(product1.getTotalQt());
					lisprod.add(p);
				
			}
			
			
			filterProductResponse.setProducts(lisprod);
			filterProductResponse.setTotalProducts((long) lisprod.size());

			
			return filterProductResponse ;
			
		
		}
		
	
	
	
	@Override
	public int  totalProductAssitOrder(ProductRequestFilterAssistOrder filterProduct) {
		
		return this.productRepository.gettotalUsedProd(filterProduct.getSatart(),filterProduct.getEnd());
		
		
	}
	
	@Override
	public int  totalPerime() {
		
		return this.lotService.listLotInfDate().size();
		
		
	}
	
	
	
	
	
	
	
//public double getSumCons(Product product, Date d1, Date d2) {
//		
//		double sumQteMvt =0;
//		
//		//List <PrescriptionLine > p = prescriptionLineRepo.findByProduct(product);
//		for(DistributionLine distributionLine :distributionLineRepos.findByProduct(product)) {
//		
//			if(distributionLine.getDistribution().getDateDist().after(d1)
//				&& distributionLine.getDistribution().getDateDist().after(d1))
//			
//			    sumQteMvt=sumQteMvt+distributionLine.getToDistribute();
//			
//			System.out.println("----------------------------produit---"+product.getLib());
//			System.out.println("-----------------------------sum ste----"+sumQteMvt);
//		//}
//		    }
//		
//		return sumQteMvt;
//		}
//	public double getSumQteMvt(String TypeMvt , Product product, Location location, Date d1, Date d2) {
//		
//		double sumQteMvt =0;
//		
//		for(StockMovementLine StockMovementLine :stockMovementLineRepository.findByStockMovementTypeMvtAndProductAndStockMovementLocation(TypeMvt, product, location)) {
//		
//			if(StockMovementLine.getStockMovement().getDateMovement().after(d1)
//					&& StockMovementLine.getStockMovement().getDateMovement().before(d2))
//			
//			    sumQteMvt=sumQteMvt+StockMovementLine.getMovmentQte();
//			
//			System.out.println("----------------------------produit---"+product.getLib());
//			System.out.println("-----------------------------sum ste----"+sumQteMvt);
//
//		    }
//		
//		return sumQteMvt;
//		}
	
	@Override
	public ProductResponse findByBarCodespec(String code) {
		
		Location  locationPharma = locationRepository.findOne(1l);
		Location  locationdepot = locationRepository.findOne(2l);
		List<Product> product =produitRepo.findByBarCode(code);
		ProductResponse productResponse = new ProductResponse();
		productResponse.setId(product.get(0).getId());
		productResponse.setBarCode(product.get(0).getBarCode());
		productResponse.setCodePctProd(product.get(0).getCodePctProd());
		productResponse.setLib(concatenationLabel(product.get(0).getId()));
		productResponse.setDci(product.get(0).getDci().getLabel());
		productResponse.setDepot(product.get(0).getDepot().getLabel());
		productResponse.setDistributionUnit(product.get(0).getDistributionUnit().getLabel());
		productResponse.setDossage(product.get(0).getDossage().getLabel());
		//productResponse.setFamily(product.getFamily().getLibelle());
		productResponse.setForm(product.get(0).getForm().getLabel());
		productResponse.setPharmaClass(product.get(0).getPharmaClass().getLabel());
		productResponse.setPresentation(product.get(0).getPharmaClass().getLabel());
		productResponse.setType(product.get(0).getType().getLabel());
		productResponse.setStockPharma(stockRepo.findByProductAndLocation
	    (product.get(0), locationPharma).getQuantity());
		productResponse.setStockDep(stockRepo.findByProductAndLocation
	    (product.get(0), locationdepot).getQuantity());
		System.out.println("////***");
		return productResponse;
	}
	@Override
	public List<ProductResponse> getLibsStartingWith(String name) {
		logger.info("___________get  product by name_______________");
		
		List<Product> listeProduit =  produitRepo.findByLibStartingWithIgnoreCase(name);
		Location  locationPharma = locationRepository.findOne(1l);
		Location  locationdepot = locationRepository.findOne(2l);
		List<ProductResponse> listeProductResp = new ArrayList<>();
		for(Product product:listeProduit ){
			
			ProductResponse productResponse = new ProductResponse();
			productResponse.setId(product.getId());
			productResponse.setBarCode(product.getBarCode());
			productResponse.setCodePctProd(product.getCodePctProd());
			productResponse.setLib(concatenationLabel(product.getId()));
			productResponse.setDci(product.getDci().getLabel());
		
			productResponse.setDepot(product.getDepot().getLabel());
			productResponse.setDistributionUnit(product.getDistributionUnit().getLabel());
			productResponse.setDossage(product.getDossage().getLabel());
			//productResponse.setFamily(product.getFamily().getLibelle());
			productResponse.setForm(product.getForm().getLabel());
			productResponse.setPharmaClass(product.getPharmaClass().getLabel());
			productResponse.setPresentation(product.getPresentation().getLabel());
			productResponse.setType(product.getType().getLabel());
			productResponse.setStockPharma(stockRepo.findByProductAndLocation
					(product, locationPharma).getQuantity());
			productResponse.setStockDep(stockRepo.findByProductAndLocation
					(product, locationdepot).getQuantity());
			listeProductResp.add(productResponse);
			
		}
		
		return listeProductResp;
		
	}
	
	@Override
	public String concatenationLabel(Long id){
		String Lab = null;
		Product product = produitRepo.findOne(id);
		Location  location = locationRepository.findOne(1l);
	
		if((product.getForm()!=null)&&(product.getDossage()!=null)&&(product.getPresentation()!=null)){
		 Lab =product.getLib()+" "+product.getForm().getLabel()+" "+product.getDossage().getLabel()
		+"  "+product.getPresentation().getLabel()+  "("+stockRepo.findByProductAndLocation(product, location).getQuantity()+")";
		 
		 
		} 
		
		if((product.getForm()==null)&&(product.getDossage()==null)&&(product.getPresentation()!=null)){
			 Lab =product.getLib()+" ----- "+" "+ "-----"+product.getPresentation().getLabel()+ 
					 "("+stockRepo.findByProductAndLocation(product, location).getQuantity()+")";
			 
			 
		} 
		
		if((product.getForm()==null)&&(product.getDossage()==null)&&(product.getPresentation()==null)){
			 Lab =product.getLib()+" -----" +" "+ "-----" +" "+ "-----" +" "+ 
					 "("+stockRepo.findByProductAndLocation(product, location).getQuantity()+")";
			 
			 
		} 
		if((product.getForm()==null)&&(product.getDossage()!=null)&&(product.getPresentation()==null)){
			 Lab =product.getLib()+" -----" +" "+product.getDossage().getLabel() +" "+"-----"+ 
					 "("+stockRepo.findByProductAndLocation(product, location).getQuantity()+")";
			 
			 
		} 
		
		if((product.getForm()!=null)&&(product.getDossage()==null)&&(product.getPresentation()!=null)){
			 Lab =product.getLib()+" "+product.getForm().getLabel()+" "+ "-----"+" "+product.getPresentation().getLabel()+" "+"("+stockRepo.findByProductAndLocation(product, location).getQuantity()+")";
			 
			 
		} 
		
		if ((product.getForm()==null)&&((product.getDossage()!=null)&&(product.getPresentation()!=null))){
			Lab =product.getLib()+" "+ "-----"+" "+product.getDossage().getLabel()
					+"  "+product.getPresentation().getLabel()+  "("+stockRepo.findByProductAndLocation(product, location).getQuantity()+")";
			
		}
		
		if ((product.getForm()!=null)&&((product.getDossage()==null)&&(product.getPresentation()==null))){
			Lab =product.getLib()+" "+product.getForm().getLabel()+"-----"+" "+"-----"+" "+"("+stockRepo.findByProductAndLocation(product, location).getQuantity()+")";
					 
			
		}
		if ((product.getForm()!=null)&&((product.getDossage()==null)&&(product.getPresentation()==null))){
			Lab =product.getLib()+" "+product.getForm().getLabel()+"-----" +" "+ "-----" +" "+ "("+stockRepo.findByProductAndLocation(product, location).getQuantity()+")";
					 
			
		}
		
		if ((product.getForm()!=null)&&((product.getDossage()!=null)&&(product.getPresentation()==null))){
			 Lab =product.getLib()+" "+product.getForm().getLabel()+" "+product.getDossage().getLabel()+"-----" +" "+ "("+stockRepo.findByProductAndLocation(product, location).getQuantity()+")";
			
		}
		
		
		return Lab;
		
		
	}
	public String concatenationLabelwithoutQte(Long id){
		Product product = produitRepo.findOne(id);
        String Lab = null;
	
		if((product.getForm()!=null)&&(product.getDossage()!=null)&&(product.getPresentation()!=null)){
			 Lab =product.getLib()+" "+product.getForm().getLabel()+" "+product.getDossage().getLabel()
			+"  "+product.getPresentation().getLabel();
			 
			 
			} 
			
			if((product.getForm()==null)&&(product.getDossage()==null)&&(product.getPresentation()!=null)){
				 Lab =product.getLib()+" ----- " +" "+ "-----"+product.getPresentation().getLabel();
				 
				 
			} 
			
			if((product.getForm()==null)&&(product.getDossage()==null)&&(product.getPresentation()==null)){
				 Lab =product.getLib()+" -----" +" "+ "-----" +" "+ "-----";
				 
				 
			} 
			if((product.getForm()==null)&&(product.getDossage()!=null)&&(product.getPresentation()==null)){
				 Lab =product.getLib()+" "+" -----" +" "+product.getDossage().getLabel()+" "+ "-----";
				 
				 
			} 
			
			if((product.getForm()!=null)&&(product.getDossage()==null)&&(product.getPresentation()!=null)){
				 Lab =product.getLib()+" "+" "+product.getForm().getLabel()+" "+ "-----"+" "+product.getPresentation().getLabel();
			} 
			
			if ((product.getForm()==null)&&((product.getDossage()!=null)&&(product.getPresentation()!=null))){
				Lab =product.getLib()+" "+"-----"+" "+product.getDossage().getLabel();
				
			}
			
			if ((product.getForm()!=null)&&((product.getDossage()==null)&&(product.getPresentation()==null))){
				Lab =product.getLib()+" "+" "+product.getForm().getLabel()+" "+"-----" +" "+"-----";
				
			}
			if ((product.getForm()!=null)&&((product.getDossage()==null)&&(product.getPresentation()==null))){
				Lab =product.getLib()+" "+product.getForm().getLabel()+"-----" +" "+ "-----" ;
			}
			
			if ((product.getForm()!=null)&&((product.getDossage()!=null)&&(product.getPresentation()==null))){
				 Lab =product.getLib()+" "+product.getForm().getLabel()+" "+product.getDossage().getLabel()+" "+"-----";
			}
			
			
		return Lab;
		
	}
	@Override
	public ProductResponse findByBarCodes(String code) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public MessageResponse updateProduct(UpdateProductRequest updateproductRequest) {
		
		Product p = produitRepo.findOne(updateproductRequest.getId());
		if (p == null ) {
			RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
//		Stock stock1 =stockRepo.findByProductAndLocation(product, locationRepository.findOne(1l));
//		Stock stock2 = stockRepo.findByProductAndLocation(product, locationRepository.findOne(2l));
//		if (((stock1.getQuantity()>0)||(stock2.getQuantity()>0)))  {
//			RestExceptionCode code = RestExceptionCode.PRODUCT_ALREADY_USED;
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
//		}
//		List<DistributionLine> distributionLine = distributionLineRepos.findByProduct(product);
//		
//		if (distributionLine.size()>0) {
//			RestExceptionCode code = RestExceptionCode.PRODUCT_ALREADY_USED;
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
//		}
		//*****
		p.setBarCode(updateproductRequest.getCodeaBarre());
		p.setCodePctProd(updateproductRequest.getCodePctProd());
	    p.setActived(true);
		p.setLib(updateproductRequest.getLibelle());
		p.setWarningStock(updateproductRequest.getWarningStock());
		p.setMinStock(updateproductRequest.getStockMin());
		p.setSafetyStock(updateproductRequest.getSafetyStock());
		p.setVatRate(updateproductRequest.getVatRate());
		//p.setFamily(famille);
		p.setPrice(updateproductRequest.getPrice());
		p.setWauCost(updateproductRequest.getWauCost());
		p.setColisage(updateproductRequest.getColisage());
		
		if (updateproductRequest.getDciId()!=null){
			p.setDci(dciRepository.findOne(updateproductRequest.getDciId()));
		}
		if (updateproductRequest.getDistributionUnitId()!=null){
			p.setDistributionUnit(distributionUnitRepository.findOne(updateproductRequest.getDistributionUnitId()));
		}
		
		if (updateproductRequest.getDosageId()!=null){
			p.setDossage(DossageRepository.findOne(updateproductRequest.getDosageId()));
			}
		
		if (updateproductRequest.getTypeId()!=null){
			p.setType(typeRepository.findOne(updateproductRequest.getTypeId()));
		}
		
		if (updateproductRequest.getDepotId()!=null){
			p.setDepot(depotRepository.findOne(updateproductRequest.getDepotId()));
		}
		if (updateproductRequest.getFormId()!=null){
			p.setForm(formRepository.findOne(updateproductRequest.getFormId()));
		}
		if (updateproductRequest.getPresentationId()!=null){
			p.setPresentation(presentationRepository.findOne(updateproductRequest.getPresentationId()));
		}
		if (updateproductRequest.getPharmaClassId()!=null){
			p.setPharmaClass(pharmaClassRepository.findOne(updateproductRequest.getPharmaClassId()));
		}
	
		
		produitRepo.save(p);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Le Produit est modifié avec succés",p.getLib());
		
		}
	
	
	@Override
	public MessageResponse deleteProduct(Long id) {
	
	    Product product = produitRepo.findOne(id);
	  
		if (product == null) {
			RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<StockMovementLine> stockMovementLines = stockMovementLineRepository.findByProduct(product);
		Stock stock1 =stockRepo.findByProductAndLocation(product, locationRepository.findOne(1l));
		Stock stock2 = stockRepo.findByProductAndLocation(product, locationRepository.findOne(2l));
		List<InventoryLine> inventoryLine = inventoryLineRepository.findByProduct(product);
		if ((stock1.getQuantity()>0||(stock2.getQuantity()>0)||(stockMovementLines.size()>0)||(inventoryLine.size()>0)))  {
			
			return new MessageResponse(HttpStatus.NOT_ACCEPTABLE.toString(), null, "Product already used",product.getLib());

		}else {
	
	
	    stockRepo.delete(stock1);
		stockRepo.delete(stock2);
		produitRepo.delete(product);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Le Produit est supprimé avec succés",product.getLib());
		}
		
	
		
	
		
	}
		
		
		
	

	@Override
	public MessageResponse changeProductState(Long id, boolean etat) {
	
	    Product product = produitRepo.findOne(id);
		if (product == null) {
			RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		product.setActived(etat);
	
		produitRepo.save(product);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Le Produit est modifié avec succés",product.getLib());
		
	}

	@Override
	public List<ProductResponseSearch> getListProductsSearch() {
		long start = System.currentTimeMillis();
		//logger.info("___________get list products_______________");
//		Location  locationPharma = locationRepository.findOne(1l);
//		Location  locationdepot = locationRepository.findOne(2l);
//		if ((locationPharma == null) ||(locationdepot == null)  ) {
//			RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
//		}
		List<ProductResponseSearch> listproductRep = new ArrayList<>();
		
		for (Product product2 : produitRepo.findDistinctByActived(true, (new Sort(Direction.ASC, "lib"))) ){
		
		ProductResponseSearch productResponse = new ProductResponseSearch();
		productResponse.setId(product2.getId());
		productResponse.setBarCode(product2.getBarCode());
        productResponse.setLabelSearchStkPha(concatenationLabel(product2.getId()));
		
		//productResponse.setLabelSearch(concatenationLabelwithoutQte(product2.getId()));
		//productResponse.setLib(product2.getLib());
		//productResponse.setActived(product2.isActived());
		listproductRep.add(productResponse);
		}
		
		System.out.println("Run time---------****----------> : " + (System.currentTimeMillis() - start));
		return listproductRep;
		
	
	}
	
	public Product getProductById(Long id) {
		
		
		return produitRepo.findOne(id);
	}

	@Override
	public List<Product> findByDepotAndActived(Long id ,boolean boolean1) {
		Depot dep = depotRepository.findOne(id);
		if (dep == null) {
			RestExceptionCode code = RestExceptionCode.DEPOT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		return produitRepo.findByDepotAndActived(dep, boolean1);
	}

	@Override
	public List<Product> findByDepot(Depot depot) {
		// TODO Auto-generated method stub
		return null;
	}


	
	//-------------------------*/filterProductNew/*--------------------------


		@Override
	public FilterProductResponseSpecific filterProductSpecific(ProductRequestFilter filterProduct) {

		JPQLQuery<?> query = new JPAQuery(em); 
		
		QProduct product = QProduct.product;

		query.from(product).orderBy(product.lib.asc());
		
		if(filterProduct.getTypeId()!=null){
			query.where(product.type.id.eq(filterProduct.getTypeId()));
		}
		if(filterProduct.getDossageId()!=null){
			query.where(product.dossage.id.eq(filterProduct.getDossageId()));
		}
		
		
		
		if(filterProduct.getDciId()!=null){
			query.where(product.dci.id.eq(filterProduct.getDciId()));
		}
		
		if(filterProduct.getDepotId()!=null){
			query.where(product.depot.id.eq(filterProduct.getDepotId()));
		}
		
		if(filterProduct.getPharmaClassId()!=null){
			query.where(product.pharmaClass.id.eq(filterProduct.getPharmaClassId()));
		}
		
		if(filterProduct.getFormId()!=null){
			query.where(product.form.id.eq(filterProduct.getFormId()));
		}
		
		if(filterProduct.getPresentationId()!=null){
			query.where(product.presentation.id.eq(filterProduct.getPresentationId()));
		}

		
		if(filterProduct.getCodePctProd()!=null){
			query.where(product.codePctProd.contains(filterProduct.getCodePctProd()));
		}
		
		if(filterProduct.getBarCode()!=null){
			query.where(product.barCode.eq(filterProduct.getBarCode()));
		}
		
		if(filterProduct.getFamilyId()!=null){
			query.where(product.family.id.eq(filterProduct.getFamilyId()));
		}
		
		if(filterProduct.getLib()!=null){
			query.where(product.lib.contains(filterProduct.getLib()));
		}
		if(filterProduct.getActived()!=null){
			query.where(product.actived.eq(filterProduct.getActived()));
		}
		
		//.orderBy(product.lib.asc())
		query.distinct();	
		if(filterProduct.getPage()!=null){
			if(filterProduct.getLinesNbr()==null){
				RestExceptionCode code = RestExceptionCode.LINES_NUMBER_REQUIRED_IN_PAGINATION;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			query.offset((filterProduct.getPage()-1)*filterProduct.getLinesNbr());
			query.limit(filterProduct.getLinesNbr());
			
		}
		FilterProductResponseSpecific filterProductResponseSpecific = new FilterProductResponseSpecific();
		List <Product> listProd =new ArrayList<>();
		listProd.addAll(query.select(product).fetch());
		List <ProductResponse> listProdResp =new ArrayList<>();
		for (Product p : listProd){
			System.out.println("---------------------2-----"+p.getBarCode());
			ProductResponse productResponse = new ProductResponse();
			productResponse.setId(p.getId());
			productResponse.setBarCode(p.getBarCode());
			productResponse.setCodePctProd(p.getCodePctProd());
		//	productResponse.setActived(p.isActived());
			productResponse.setLib(p.getLib());
			productResponse.setWarningStock(0);
			productResponse.setSafetyStock(p.getSafetyStock());
			productResponse.setMinStock(p.getMinStock());
			productResponse.setVatRate(p.getVatRate());
			productResponse.setPrice(p.getPrice());
			productResponse.setWauCost(p.getWauCost());
			productResponse.setColisage(p.getColisage());
			productResponse.setVatRate(p.getVatRate());
			
			if(p.getDci()!=null){
			productResponse.setDci(p.getDci().getLabel());
			}
			
			if(p.getDepot()!=null){
			productResponse.setDepot(p.getDepot().getLabel());
			}
			if(p.getDistributionUnit()!=null){
			productResponse.setDistributionUnit(p.getDistributionUnit().getLabel());
			}
			
			if(p.getDossage()!=null){
			productResponse.setDossage(p.getDossage().getLabel());
			}
			
			if(p.getForm()!=null){
			productResponse.setForm(p.getForm().getLabel());
			}
			if(p.getPharmaClass()!=null){
			productResponse.setPharmaClass(p.getPharmaClass().getLabel());
			}
			if(p.getPresentation()!=null){
			productResponse.setPresentation(p.getPresentation().getLabel());
			}
			Stock stockPharma =stockRepo.findByProductAndLocation(productRepository.findOne(p.getId()), locationRepository.findOne(1l));
			Stock stockDepot = stockRepo.findByProductAndLocation(productRepository.findOne(p.getId()), locationRepository.findOne(2l));
			productResponse.setStockPharma(stockPharma.getQuantity());
			productResponse.setStockDep(stockDepot.getQuantity());
			
			if(p.getType()!=null){
			productResponse.setType(p.getType().getLabel());
			}
			listProdResp.add(productResponse);	
		}
		
		filterProductResponseSpecific.setProducts(listProdResp);
		filterProductResponseSpecific.setTotalProducts(query.select(product).fetchCount());
		//filterProductResponse.setProducts(query.select(product).fetch());
		//filterProductResponse.setTotalProducts(query.select(product).fetchCount());
		if(filterProduct.getPage()!=null){
			filterProductResponseSpecific.setPage(filterProduct.getPage());
			filterProductResponseSpecific.setNbrPages(filterProductResponseSpecific.getTotalProducts()/filterProduct.getLinesNbr());
		}
		return filterProductResponseSpecific ;
		
	}

		@Override
		public List<ProductAutoCompleteResponse> getProductsForAutoComplete(Boolean status) {
			List<ProductAutoCompleteResponse> listProducts = new ArrayList<>();
			for(Product p : this.produitRepo.findDistinctByActived(status, null)){
				listProducts.add(new ProductAutoCompleteResponse(p.getId(),p.getLib(), p.getVatRate(), p.getPrice()));
				System.out.println("-------auto-complet ------"+p.getPrice());
			}
			return listProducts;
		}

		@Override
		public List<LightProduct> getActivedProductLabelbStartingWith(ActiveProductByLabelStartWith activeProductByLabelStartWith) {
			List <LightProduct> listProducts =new ArrayList<>();
			for(Product p : this.produitRepo.findByLibStartingWithIgnoreCaseAndActived(activeProductByLabelStartWith.getLabel(), activeProductByLabelStartWith.getActive())){
			
				//listProducts.add(new LightProduct( p.getId(), p.getBarCode(), p.getCodePctProd(), p.getLib(), p.isActived(), p.getPresentation(), p.getStocks()));
				
				listProducts.add(new LightProduct(p.getId(), p.getBarCode(), p.getCodePctProd(), p.getLib(), p.getVatRate(), p.getPrice(), p.isActived(), p.getPresentation(), p.getStocks()));
			}
			return listProducts;
		}
		
		@Override
		public List<LightProduct> getActivedProductLabelbStartingWithByDep(ActiveProductByLabelStartWithAndDep activeProductByLabelStartWith) {
		
			//---------------------- bydep---------------------
			if(activeProductByLabelStartWith.getDepID()!=null) {
			List <LightProduct> listProducts =new ArrayList<>();
			if (activeProductByLabelStartWith.getActive()==true) {
			for(Product p : this.produitRepo.findByLibStartingWithIgnoreCaseAndDepot(activeProductByLabelStartWith.getLabel(),depotRepository.findOne(activeProductByLabelStartWith.getDepID()) )){
		      if (p.isActived()==true) {
				
				listProducts.add(new LightProduct(p.getId(), p.getBarCode(), p.getCodePctProd(), p.getLib(), p.getVatRate(), p.getPrice(), p.isActived(), p.getPresentation(), p.getStocks()));
		      }
		      }
			return listProducts;
			
			
			} else 
				for(Product p : this.produitRepo.findByLibStartingWithIgnoreCaseAndDepot(activeProductByLabelStartWith.getLabel(),depotRepository.findOne(activeProductByLabelStartWith.getDepID()) )){
				      if (p.isActived()==false) {
						
						listProducts.add(new LightProduct(p.getId(), p.getBarCode(), p.getCodePctProd(), p.getLib(), p.getVatRate(), p.getPrice(), p.isActived(), p.getPresentation(), p.getStocks()));
				      }
				      }
					return listProducts;
					//---------------------- bytype---------------------		
			} else  {
				
				
				
				
				List <LightProduct> listProducts =new ArrayList<>();
				if (activeProductByLabelStartWith.getActive()==true) {
				for(Product p : this.produitRepo.findByLibStartingWithIgnoreCaseAndType(activeProductByLabelStartWith.getLabel(),typeRepository.findOne(activeProductByLabelStartWith.getTypeID()) )){
			      if (p.isActived()==true) {
					
					listProducts.add(new LightProduct(p.getId(), p.getBarCode(), p.getCodePctProd(), p.getLib(), p.getVatRate(), p.getPrice(), p.isActived(), p.getPresentation(), p.getStocks()));
			      }
			      }
				return listProducts;
				
				
				} else 
					for(Product p : this.produitRepo.findByLibStartingWithIgnoreCaseAndType(activeProductByLabelStartWith.getLabel(),typeRepository.findOne(activeProductByLabelStartWith.getTypeID()) )){
					      if (p.isActived()==false) {
							
							listProducts.add(new LightProduct(p.getId(), p.getBarCode(), p.getCodePctProd(), p.getLib(), p.getVatRate(), p.getPrice(), p.isActived(), p.getPresentation(), p.getStocks()));
					      }
					      }
						return listProducts;
				
				
				
			}
			
			
			
		} 
		
		
		@Override
		public DuplcateEmptyResp verifEmptyAndDuplicate() {
			
			DuplcateEmptyResp duplcateEmptyResp = new DuplcateEmptyResp();
            Long d =0l;
            Long e =0l;
            Long Size =0l;
		for(Product prod1 : this.produitRepo.findDistinctByActived(true, (new Sort(Direction.ASC, "lib"))) ){
			
			String codeabare =prod1.getBarCode();
			 if (productRepository.findByCodBare(codeabare).size()>1 ) {
				d =d+1;
		
			 }
			 if (productRepository.findByCodBare(codeabare).size()==0) {
				e =e+1;
			} 
			 if(d>0 && e>0) {
				 
				 duplcateEmptyResp.setState(false);
				 duplcateEmptyResp.setNumberDup(d);
				 duplcateEmptyResp.setNumberEmpty(e);
				 
			 }else 
				 if(d>0) {
					 
				 duplcateEmptyResp.setState(false);
				 duplcateEmptyResp.setNumberDup(d);
		         } 
			     if(e>0) {
			    	
			     duplcateEmptyResp.setState(false);
			     duplcateEmptyResp.setNumberEmpty(e);
			     }       
			      if(d==0 && e ==0) {
			    	
			     duplcateEmptyResp.setState(true);
				 duplcateEmptyResp.setNumberDup(d);
				 duplcateEmptyResp.setNumberEmpty(e);
			       }
			 
			 
		}
	return duplcateEmptyResp;
	}
		
		
		
	
		
		
		
		
		
		
		@Override
		public FilterProductResponse filterProduct(ProductRequestFilter filterProduct) {

			JPQLQuery<?> query = new JPAQuery(em); 
			
			QProduct product = QProduct.product;

			query.from(product).orderBy(product.lib.asc());
			
			if(filterProduct.getProductId()!=null && filterProduct.getProductId()!= 0 ){
				query.where(product.id.eq(filterProduct.getProductId()));

			}
			
			if(filterProduct.getTypeId()!=null && filterProduct.getTypeId()!= 0 ){
				query.where(product.type.id.eq(filterProduct.getTypeId()));

			}
			if(filterProduct.getDossageId()!=null && filterProduct.getDossageId()!=0){
				query.where(product.dossage.id.eq(filterProduct.getDossageId()));

			}
			
			if(filterProduct.getDciId()!=null && filterProduct.getDciId()!=0){
			
				query.where(product.dci.id.eq(filterProduct.getDciId()));
			}
			
			if(filterProduct.getDepotId()!=null && filterProduct.getDepotId()!=0){
		
				query.where(product.depot.id.eq(filterProduct.getDepotId()));
			}
			
			if(filterProduct.getPharmaClassId()!=null && filterProduct.getPharmaClassId()!= 0){
			
				query.where(product.pharmaClass.id.eq(filterProduct.getPharmaClassId()));
			}
			
			if(filterProduct.getFormId()!=null && filterProduct.getFormId()!=0){
		
				query.where(product.form.id.eq(filterProduct.getFormId()));
			}
			
			if(filterProduct.getPresentationId()!=null && filterProduct.getPresentationId()!=0 ){
				
				query.where(product.presentation.id.eq(filterProduct.getPresentationId()));
			}

			
			if(filterProduct.getCodePctProd()!=null && filterProduct.getCodePctProd()!="" && filterProduct.getCodePctProd()!="0"){
		
				query.where(product.codePctProd.contains(filterProduct.getCodePctProd()));
			}
			
			if(filterProduct.getBarCode()!=null && filterProduct.getBarCode() !="" && filterProduct.getBarCode() !="0" ){
				
				query.where(product.barCode.eq(filterProduct.getBarCode()));
			}
			
			if(filterProduct.getFamilyId()!=null && filterProduct.getFamilyId()!=0){
			
				query.where(product.family.id.eq(filterProduct.getFamilyId()));
			}
//			
//			if(filterProduct.getLib()!=null){
//				query.where(product.lib.lower().contains(filterProduct.getLib().toLowerCase()));
//			}
			
//			if(filterProduct.getLib()!=null){
//				query.where(product.lib.lower().contains(filterProduct.getLib().toLowerCase()));
//			}
			
			if(filterProduct.getLib()!=null && filterProduct.getLib()!="" ){
			
				query.where(product.lib.lower().startsWith(filterProduct.getLib().toLowerCase()));
			}
			

			if(filterProduct.getActived()!=null && filterProduct.getActived()!=null){
				
				query.where(product.actived.eq(filterProduct.getActived()));
			}
			
			query.distinct();	
			if(filterProduct.getPage()!=null){
				if(filterProduct.getLinesNbr()==null){
					RestExceptionCode code = RestExceptionCode.LINES_NUMBER_REQUIRED_IN_PAGINATION;
					RestException ex = new RestException(code.getError(), code);
					throw ex;
				}
				query.offset((filterProduct.getPage()-1)*filterProduct.getLinesNbr());
				query.limit(filterProduct.getLinesNbr());
				
			}
			FilterProductResponse filterProductResponse = new FilterProductResponse();
			filterProductResponse.setProducts(query.select(product).fetch());
			filterProductResponse.setTotalProducts(query.select(product).fetchCount());
			if(filterProduct.getPage()!=null){
				filterProductResponse.setPage(filterProduct.getPage());
				filterProductResponse.setNbrPages(filterProductResponse.getTotalProducts()/filterProduct.getLinesNbr());
			}
			return filterProductResponse ;
			
		}
		
	
		
		@Override
		public ProductQuantityDep existProductByDepot(Long depotId, boolean boolean1) {
			
			Depot dep = depotRepository.findOne(depotId);
			
			if (dep == null) {
				RestExceptionCode code = RestExceptionCode.DEPOT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			List<Product> products = productRepository.findByDepotAndActived(dep, boolean1);
		
			ProductQuantityDep productQunatity  = new ProductQuantityDep();
			productQunatity.setQunatity(products.size());
			
					return productQunatity;
		
		
		}

		@Override
		public int totalPerime(ProductRequestFilterAssistOrder filterProduct) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		
}

