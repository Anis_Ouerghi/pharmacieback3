package com.transtu.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Dosage;
import com.transtu.persistence.entities.Presentation;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.repositories.DossageRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.DossageService;
@Service
public class DossageServiceImpl implements DossageService {
	
	
	private final static Logger logger = LoggerFactory.getLogger(DossageServiceImpl.class);

	@Autowired(required = true)
	private DossageRepository dossageRepo;
	@Autowired(required = true)
	ProductRepository  productRepository;

	@Override
	public MessageResponse addossage(String label) {
		logger.info("___________Add dossage_______________");
		Dosage dossage = dossageRepo.findByLabel(label);
		if(dossage != null ){
			RestExceptionCode code = RestExceptionCode.DOSSAGE_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Dosage dossages = new Dosage() ;
		dossages.setLabel(label);
		this.dossageRepo.save(dossages);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "le dossage est ajouté avec succés",dossages.getLabel());
	}

	@Override
	public List<Dosage> getListDossage() {
		return this.dossageRepo.findAll(new Sort(Direction.ASC, "label"));
	}
	
	@Override
	public MessageResponse updateDossage(Long id, String label) {
		Dosage dossage = dossageRepo.findByLabel(label);
		if(dossage != null ){
			RestExceptionCode code = RestExceptionCode.DOSSAGE_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		Dosage dossages = this.dossageRepo.findOne(id);
		if(dossages == null ){
			RestExceptionCode code = RestExceptionCode.DOSSAGE_NOT_FOND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		dossages.setLabel(label);
		this.dossageRepo.save(dossages);
		return new MessageResponse(HttpStatus.OK.toString(), null, "le dossage est modifié avec succés",label);
		
	}

	@Override
	public MessageResponse deleteDossage(Long id) {
		Dosage dosage =dossageRepo.findOne(id);
		if(dosage == null ){
			RestExceptionCode code = RestExceptionCode.DOSSAGE_NOT_FOND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<Product> product =productRepository.findByDossage(dosage);
		if(product.size()>0 ){
			RestExceptionCode code = RestExceptionCode.DOSAGE_ALREADY_USED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Dosage dossage = this.dossageRepo.findOne(id);
		if(dossage == null ){
			RestExceptionCode code = RestExceptionCode.DOSSAGE_NOT_FOND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		this.dossageRepo.delete(dossage);
		return new MessageResponse(HttpStatus.OK.toString(), null, "le dossage  est supprimé avec succés",dosage.getLabel());
	}

}
