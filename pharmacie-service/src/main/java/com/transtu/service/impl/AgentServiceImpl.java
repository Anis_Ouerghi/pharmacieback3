package com.transtu.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.transtu.core.json.request.AgentSTT;
//import com.transtu.core.json.request.Stt__AGENT;
import com.transtu.persistence.entities.Agent;
import com.transtu.service.AgentService;


@Service
public class AgentServiceImpl  implements AgentService {
	
	@Override
	public  Agent getAgent( String matri) throws JsonParseException, JsonMappingException, IOException {
	
	
		Agent agentx = new Agent() ;
		ObjectMapper mapper = new ObjectMapper();
		List<AgentSTT> listet = new ArrayList<>();
		   RestTemplate restTemplate = new RestTemplate();
		    String json = restTemplate.getForObject
		    		("http://10.0.2.2:8282/agent/all", String.class);
		    listet= mapper.readValue(json, new TypeReference<List<AgentSTT>>() {});
		    int i = Integer.parseInt(matri); 
	   for (AgentSTT agent : listet){
		   if (agent.getId()== i ){
			  agentx.setNom(agent.getNom());
			  agentx.setPrenom(agent.getPrenom());
			  agentx.setMatricule(agent.getId());
			 
		   }
		   
		   
	   }
		 
		
		return agentx;	
	}

	

	@Override
	public  Agent getDetailAgent( String matri) throws JsonParseException, JsonMappingException, IOException {
	    Agent agentx = new Agent() ;
		ObjectMapper mapper = new ObjectMapper();
		List<AgentSTT> listet = new ArrayList<>();
//		AgentSTT agentstt = new AgentSTT();
		   RestTemplate restTemplate = new RestTemplate();
		   String json = restTemplate.getForObject
				//("http://transtu.tn:8387/agent/"+matri, String.class);
		    		("http://10.0.2.2:8282/agent/"+matri, String.class);
		   listet= mapper.readValue(json, new TypeReference<List<AgentSTT>>() {});
			  for (AgentSTT agent : listet){
					  agentx.setNom(agent.getNom());
					  agentx.setPrenom(agent.getPrenom());
					  agentx.setMatricule(agent.getMatric());
					  agentx.setFullName(agent.getIdentite());
				   }
				return agentx;
		   }
		   
	 
	  
		 
		
	
	




	



}
