package com.transtu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.DistributionUnit;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.repositories.DistributionUnitRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.DistributionUnitService;
@Service
public class DistributionUnitServiceImpl implements DistributionUnitService{

	@Autowired(required = true)
	DistributionUnitRepository distributionUnitRepo;
	@Autowired(required = true)
	ProductRepository productRepository;
	
	@Override
	public MessageResponse addDistributionUnit(String label) {
		
		DistributionUnit distributionunit =this.distributionUnitRepo.findByLabelContainingIgnoreCase(label);
		if(distributionunit !=null){
			RestExceptionCode code = RestExceptionCode.DISTRIBUTION_UNIT_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		this.distributionUnitRepo.save(new DistributionUnit(label));
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Unite de distribution ajouté avec succes",label);
	}

	@Override
	public MessageResponse updateDistributionUnit(DistributionUnit distUnit) {
		
		this.findById(distUnit.getId());
		DistributionUnit oldDistUnit = this.distributionUnitRepo.findByLabelContainingIgnoreCase(distUnit.getLabel()) ;
		
		if( (oldDistUnit!=null) && (oldDistUnit.getLabel().equals(distUnit.getLabel()))&&(!oldDistUnit.getId().equals(distUnit.getId()))){
			RestExceptionCode code = RestExceptionCode.DISTRIBUTION_UNIT_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		this.distributionUnitRepo.save(distUnit);
		return new MessageResponse(HttpStatus.OK.toString(), null, "Unite de distribution modifie avec succes",this.distributionUnitRepo.save(distUnit).getLabel());
	}

	@Override
	public MessageResponse deleteDistributionUnit(Long id) {
		DistributionUnit distributionUnit = distributionUnitRepo.findOne(id);
		if(distributionUnit == null ){
			RestExceptionCode code = RestExceptionCode.DISTRIBUTION_UNIT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<Product> product =productRepository.findByDistributionUnit(distributionUnit);
		if(product.size()>0 ){
			RestExceptionCode code = RestExceptionCode.DISTRIBUTION_UNIT_ALREADY_USED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		this.distributionUnitRepo.delete(this.findById(id));
		return new MessageResponse(HttpStatus.OK.toString(), null, "Unite de distribution supprimé avec succes",distributionUnit.getLabel());	
	}

	@Override
	public DistributionUnit findById(Long id) {
		DistributionUnit distUnit = this.distributionUnitRepo.findOne(id) ;
		this.isExist(distUnit);
		return distUnit ;
	}

	@Override
	public DistributionUnit findByLabel(String label) {
		DistributionUnit distUnit = this.distributionUnitRepo.findByLabelContainingIgnoreCase(label);
		this.isExist(distUnit);
		return distUnit;
	}

	@Override
	public List<DistributionUnit> findAllDistributionUnits() {
		return this.distributionUnitRepo.findAll(new Sort(Direction.ASC, "label"));
	}
	
	public void isExist(DistributionUnit distUnit){
		if(distUnit == null ){
			RestExceptionCode code = RestExceptionCode.DISTRIBUTION_UNIT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
	}

}
