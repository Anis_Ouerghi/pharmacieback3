package com.transtu.service.impl;

import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Dosage;
import com.transtu.persistence.entities.Motif;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.repositories.MotifRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.MotifService;


@Service
public class MotifServiceImpl  implements MotifService{

	private final static Logger logger = LoggerFactory.getLogger(MotifServiceImpl.class);
	
	@Autowired(required = true)
	private MotifRepository motifRepo;
	@Override
	public MessageResponse addMotif(String label) {
		logger.info("___________Add dossage_______________");
		Motif motif = motifRepo.findByLabel(label);
		if(motif != null ){
			RestExceptionCode code = RestExceptionCode.MOTIF_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Motif motifs = new Motif() ;
		motifs.setLabel(label);
		this.motifRepo.save(motifs);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "le motif est ajouté avec succés",label);
	}

	@Override
	public List<Motif> getListMotif() {
		// TODO Auto-generated method stub
		return this.motifRepo.findAll(new Sort(Direction.ASC, "label"));
	}

	@Override
	public MessageResponse updateMotif(Long id, String label) {
		Motif motif = motifRepo.findByLabel(label);
		if(motif != null ){
			RestExceptionCode code = RestExceptionCode.MOTIF_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		Motif motifs = this.motifRepo.findOne(id);
		if(motifs == null ){
			RestExceptionCode code = RestExceptionCode.MOTIF_NOT_FOND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		motifs.setLabel(label);
		this.motifRepo.save(motifs);
		return new MessageResponse(HttpStatus.OK.toString(), null, "le dossage est modifié avec succés",label);
	}

	@Override
	public MessageResponse deleteMotif(Long id) {
		Motif motif = this.motifRepo.findOne(id);
		if(motif == null ){
			RestExceptionCode code = RestExceptionCode.MOTIF_NOT_FOND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
//		List<Product> product =productRepository.findByDossage(dosage);
//		if(product.size()>0 ){
//			RestExceptionCode code = RestExceptionCode.DOSAGE_ALREADY_USED;
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
//		}
//		Dosage dossage = this.dossageRepo.findOne(id);
//		if(dossage == null ){
//			RestExceptionCode code = RestExceptionCode.DOSSAGE_NOT_FOND;
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
//		}
		this.motifRepo.delete(motif);
		return new MessageResponse(HttpStatus.OK.toString(), null, "le Motif  est supprimé avec succés",motif.getLabel());
	}
	}


