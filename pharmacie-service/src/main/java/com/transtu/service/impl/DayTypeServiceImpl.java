package com.transtu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.AddDayTypeRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.DayType;
import com.transtu.persistence.repositories.DayTypeRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.DayTypeService;




@Service
public class DayTypeServiceImpl implements DayTypeService {
	
	@Autowired(required = true)
	private DayTypeRepository dayTypeRepo;
	
	@Override
	public List<DayType> getAllDaysType() {
		return this.dayTypeRepo.findAll(new Sort(Direction.ASC, "libelle"));
	}
	
	
	@Override
	public MessageResponse addDayType(AddDayTypeRequest daytypeRequest) {
		
	DayType dayType= dayTypeRepo.findByLibelle(daytypeRequest.getLibelle());
	
	if(dayType!=null){
		RestExceptionCode code = RestExceptionCode.DAYTYPE_ALREADY_TAKEN;
		RestException ex = new RestException(code.getError(), code);
		throw ex;
	}
		DayType d = new DayType();
		d.setId(daytypeRequest.getId());
		d.setLibelle(daytypeRequest.getLibelle());
		dayTypeRepo.save(d);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la journée type est ajoutée avec success",d.getLibelle());	
	}
	
	@Override
	public MessageResponse deleteDayType(Long idDayType) {
		if(this.dayTypeRepo.findOne(idDayType)==null){
			RestExceptionCode code = RestExceptionCode.DAYTYPE_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		dayTypeRepo.delete(idDayType);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la journée type est supprimée avec success",this.dayTypeRepo.findOne(idDayType).getLibelle());	
	}
	
	
	

}
