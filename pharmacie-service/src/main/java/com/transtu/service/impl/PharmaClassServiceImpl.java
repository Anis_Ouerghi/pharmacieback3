package com.transtu.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.PharmaClass;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.repositories.PharmaClassRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.PharmaService;
import com.transtu.service.UtilisateurService;


@Service
public class PharmaClassServiceImpl implements PharmaService {
	
	private final static Logger logger = LoggerFactory.getLogger(PharmaClassServiceImpl.class);
	
	@Autowired(required = true)
	UtilisateurService utilisateurService;
	@Autowired(required = true)
	private PharmaClassRepository pharmaRepo;
	@Autowired(required = true)
	ProductRepository productRepository;
	
	@Override
	public MessageResponse addpharmaClass(String label) {
		logger.info("___________Add pharmaClass_______________");
		PharmaClass pharmaClass = pharmaRepo.findByLabel(label);
		
		if(pharmaClass != null ){
			RestExceptionCode code = RestExceptionCode.PHARMACLASS_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		PharmaClass pharmaClasse = new PharmaClass() ;
		pharmaClasse.setLabel(label);
		this.pharmaRepo.save(pharmaClasse);
		
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "la classe produit est ajouté avec succés",pharmaClasse.getLabel());
	}

	@Override
	public List<PharmaClass> getListPharmaClass() {
		logger.info("___________get list pharmaClass_______________");
		
		return this.pharmaRepo.findAll(new Sort(Direction.ASC, "label"));
	}
	
	public MessageResponse updatepharmaClass(Long id, String label) {
		PharmaClass pharmaClass = pharmaRepo.findByLabel(label);
		if(pharmaClass != null ){
			RestExceptionCode code = RestExceptionCode.PHARMACLASS_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		PharmaClass pharmaClasse = this.pharmaRepo.findOne(id);
		if(pharmaClasse == null ){
			RestExceptionCode code = RestExceptionCode.PHARMACLASS_NOT_FOND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		pharmaClasse.setLabel(label);
		this.pharmaRepo.save(pharmaClasse);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la classe produit est modifié avec succés",pharmaClass.getLabel());
		
	}
	public MessageResponse deletepharmaClass(Long id) {
		PharmaClass pharmaClass = pharmaRepo.findOne(id);
		if(pharmaClass == null ){
			RestExceptionCode code = RestExceptionCode.PHARMACLASS_NOT_FOND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<Product> product =productRepository.findByPharmaClass(pharmaClass);
		if(product.size()>0 ){
			RestExceptionCode code = RestExceptionCode.PHARMA_CLASS_ALREADY_USED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		this.pharmaRepo.delete(pharmaClass);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la classe produit est supprimé avec succés",pharmaClass.getLabel());
		
	}
	
}

	

