package com.transtu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Form;
import com.transtu.persistence.entities.PharmaClass;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.repositories.FormRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.StockRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.FormService;

@Service
public class FormServiceImpl implements FormService{

	@Autowired(required = true)
	FormRepository formRepo;
	@Autowired(required = true)
	ProductRepository productRepository;
	@Autowired(required = true)
	StockRepository stockRepository;
	
	@Override
	public MessageResponse addForm(String label) {
		Form form=this.formRepo.findByLabel(label);
		if(form!=null){
			RestExceptionCode code = RestExceptionCode.FORM_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		this.formRepo.save(new Form(label));
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Form ajouté avec succes",label);	
	}

	@Override
	public MessageResponse updateForm(Long id, String label) {
		Form form = formRepo.findByLabel(label);
		if(form != null ){
			RestExceptionCode code = RestExceptionCode.FORM_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Form forme = this.formRepo.findOne(id);
		if(forme == null ){
			RestExceptionCode code = RestExceptionCode.FORM_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		forme.setLabel(label);
		this.formRepo.save(forme);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la forme produit est modifié avec succés",forme.getLabel());	
	}

	@Override
	public MessageResponse deleteForm(Long id) {
		Form form = formRepo.findOne(id);
		if(form == null ){
			RestExceptionCode code = RestExceptionCode.FORM_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<Product> product =productRepository.findByForm(form);
		if(product.size()>0){
			RestExceptionCode code = RestExceptionCode.FORM_ALREADY_USED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		this.formRepo.delete(this.findById(id));
		return new MessageResponse(HttpStatus.OK.toString(), null, "Form supprimé avec succes",form.getLabel());	
	}

	@Override
	public Form findById(Long id) {
		Form form = this.formRepo.findOne(id) ;
		if(form==null){
			RestExceptionCode code = RestExceptionCode.FORM_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		return form ;
	}

	@Override
	public Form findByLabel(String label) {
		Form form = this.formRepo.findByLabel(label);
		if(form==null){
			RestExceptionCode code = RestExceptionCode.FORM_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		return form;
	}

	@Override
	public List<Form> findAllForms() {
		
		return formRepo.findAll(new Sort(Direction.ASC, "label"));
	}

	
}
