package com.transtu.service.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.AddFamilyRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Family;
import com.transtu.persistence.entities.Form;
import com.transtu.persistence.entities.Presentation;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.repositories.FamilyRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.FamilyService;

@Service
public class FamilyServiceImpl implements FamilyService{
	
	@Autowired(required = true)
	private FamilyRepository familyRepo;
	@Autowired(required = true)
	ProductRepository  productRepository;
	@Override
	public MessageResponse addFamily(String label) {
		if(this.familyRepo.findByLibelle(label)!=null){
			RestExceptionCode code = RestExceptionCode.FORM_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Family family = new Family();
		family.setLibelle(label);
		this.familyRepo.save(family);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Famille ajouté avec succes","");	
	
	}
	
	@Override
	public List<Family> getListFamily() {
		
		return this.familyRepo.findAll();
	}
	
	@Override
	public MessageResponse updateFamily(Long id, String label) {
		Family family = familyRepo.findByLibelle(label);
		if(family != null ){
			RestExceptionCode code = RestExceptionCode.FAMILY_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Family family1 = this.familyRepo.findOne(id);
		if(family1 == null ){
			RestExceptionCode code = RestExceptionCode.FAMILY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		family1.setLibelle(label);;
		this.familyRepo.save(family1);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Famille  a été modifié avec succés","");
		
	}
	
	@Override
	public MessageResponse deleteFamily(Long id) {
		Family family= familyRepo.findOne(id);
		if(family == null ){
			RestExceptionCode code = RestExceptionCode.FAMILY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<Product> product =productRepository.findByFamily(family);
		if(product.size()>0 ){
			RestExceptionCode code = RestExceptionCode.FAMILY_ALREADY_USED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		this.familyRepo.delete(familyRepo.findOne(id));
		return new MessageResponse(HttpStatus.OK.toString(), null, "famille supprimé avec succes","");	
	}

}
