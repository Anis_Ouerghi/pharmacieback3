package com.transtu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Dci;
import com.transtu.persistence.entities.Depot;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.repositories.DepotRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.DepotService;

@Service
public class DepotServiceImpl implements DepotService{

	@Autowired(required = true)
	DepotRepository depotRepo;
	
	@Autowired(required = true)
	ProductRepository productRepository;
	
	
	@Override
	public MessageResponse addDepot(String label) {
		Depot depot = this.depotRepo.findByLabel(label);
		if(depot!=null){
			RestExceptionCode code = RestExceptionCode.DEPOT_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		this.depotRepo.save(new Depot(label));
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Depot ajouté avec succes",label);
	}

	@Override
	public MessageResponse updateDepot(Long id, String label) {
		Depot depot = depotRepo.findByLabel(label);
		if(depot != null ){
			RestExceptionCode code = RestExceptionCode.DEPOT_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Depot depots = this.depotRepo.findOne(id);
		if(depots == null ){
			RestExceptionCode code = RestExceptionCode.DEPOT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		depots.setLabel(label);
		this.depotRepo.save(depots);
		return new MessageResponse(HttpStatus.OK.toString(), null, "le depot produit est modifié avec succés",depots.getLabel());
	}

	@Override
	public MessageResponse deleteDepot(Long id) {
		Depot depot = depotRepo.findOne(id);
		if(depot==null){
			RestExceptionCode code = RestExceptionCode.DEPOT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<Product> product =productRepository.findByDepot(depot);
		if(product.size()>0){
			RestExceptionCode code = RestExceptionCode.DEPOT_ALREADY_USED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		this.depotRepo.delete(this.findById(id));
		return new MessageResponse(HttpStatus.OK.toString(), null, "Depot supprimé avec succes",depot.getLabel());
	}

	@Override
	public Depot findById(Long id) {
		Depot depot = this.depotRepo.findOne(id) ;
		this.isExist(depot);
		return depot ;
	}

	@Override
	public Depot findByLabel(String label) {
		Depot depot = this.depotRepo.findByLabel(label) ;
		this.isExist(depot);
		return depot ;
	}

	@Override
	public List<Depot> findAllDepots() {
		return this.depotRepo.findAll(new Sort(Direction.ASC, "label"));
	}
	
	public void isExist(Depot depot){
		if(depot == null ){
			RestExceptionCode code = RestExceptionCode.DEPOT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
	}


}
