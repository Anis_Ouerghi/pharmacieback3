package com.transtu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.transtu.persistence.entities.Location;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.service.LocationService;

@Service
public class LocationServiceImpl implements LocationService {
	
	@Autowired(required = true)
	LocationRepository locationRepo ; 

	@Override
	public List<Location> findAllLocations() {
		return this.locationRepo.findAll(new Sort(Direction.ASC, "lib"));
	}

}
