//package com.transtu.service.impl;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Service;
//
//import com.transtu.core.json.request.MessageResponse;
//import com.transtu.persistence.entities.ExternalDelivery;
//import com.transtu.persistence.entities.ExternalOrder;
//import com.transtu.persistence.entities.State;
//import com.transtu.persistence.repositories.ExternalDeliveryRepository;
//import com.transtu.persistence.repositories.ExternalOrderRepository;
//import com.transtu.persistence.repositories.StateRepository;
//import com.transtu.rest.exception.RestException;
//import com.transtu.rest.exception.RestExceptionCode;
//import com.transtu.service.ExternalDeliveryService;
//
//
//@Service
//public class ExternalDilevryServiceImpl implements ExternalDeliveryService{
//
//	@Autowired(required=true)
//	ExternalDeliveryRepository  externalDeliveryRepository;
//	@Autowired(required=true)
//	StateRepository stateRepository;
//	@Autowired(required=true)
//	ExternalOrderRepository externalOrderRepository;
//	public MessageResponse fenceDelivery (Long Id){
//	
//		ExternalDelivery externalDelivery = externalDeliveryRepository.findOne(Id);
//		if(externalDelivery==null){
//			RestExceptionCode code = RestExceptionCode.EXTERNAL_DELIVERY_NOT_FOUND;
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
//		}
//	
//		State state = stateRepository.findOne(7l);//----------------------------------------------->close
//		if(state==null){
//			RestExceptionCode code = RestExceptionCode.STATE_NOT_FOUND;
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
//		}
//
//		externalDelivery.setState(state);
//		externalDeliveryRepository.save(externalDelivery);
////		externalOrderRepository.findByExternalDelivery(externalDelivery)
////		.setState(stateRepository.findOne(6));
//		 ExternalOrder externalOrder = externalOrderRepository.findOne
//		(externalDelivery.getExternalOrder().getId());
//		 externalOrder.setState(stateRepository.findOne(4l));
//		 externalOrderRepository.save(externalOrder);
//		return new MessageResponse(HttpStatus.OK.toString(), null, "la livraison externe est fermée",externalDelivery.getNumExternalDelivery());	
//		
//	}
//	
//	public MessageResponse openDelivery (Long Id){
//		
//			ExternalDelivery externalDelivery = externalDeliveryRepository.findOne(Id);
//			if(externalDelivery==null){
//				RestExceptionCode code = RestExceptionCode.EXTERNAL_DELIVERY_NOT_FOUND;
//				RestException ex = new RestException(code.getError(), code);
//				throw ex;
//			}
//			
//			State state = stateRepository.findOne(8l);//----------------------------------------------->open
//			if(state==null){
//				RestExceptionCode code = RestExceptionCode.STATE_NOT_FOUND;
//				RestException ex = new RestException(code.getError(), code);
//				throw ex;
//			}
//			
//			externalDelivery.setState(state);
//			externalDelivery.getExternalOrder().setState(stateRepository.findOne(5l));
//			externalDeliveryRepository.save(externalDelivery);
//			
//			return new MessageResponse(HttpStatus.OK.toString(), null, "la livraison externe est ouverte",externalDelivery.getNumExternalDelivery());	
//			
//		}
//	public MessageResponse cancelDelivery (Long Id){
//		
//			ExternalDelivery externalDelivery = externalDeliveryRepository.findOne(Id);
//			if(externalDelivery==null){
//				RestExceptionCode code = RestExceptionCode.EXTERNAL_DELIVERY_NOT_FOUND;
//				RestException ex = new RestException(code.getError(), code);
//				throw ex;
//			}
//	
//			State state = stateRepository.findOne(3l);
//			if(state==null){
//				RestExceptionCode code = RestExceptionCode.STATE_NOT_FOUND;
//				RestException ex = new RestException(code.getError(), code);
//				throw ex;
//			}
//		
//			externalDelivery.setState(state);
//			externalDeliveryRepository.save(externalDelivery);
//			
//			return new MessageResponse(HttpStatus.OK.toString(), null, "la livraison externe a été annullée",externalDelivery.getNumExternalDelivery());	
//			
//		}
//	
//	
//}
