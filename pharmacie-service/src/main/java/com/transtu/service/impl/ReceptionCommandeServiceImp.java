//package com.transtu.service.impl;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.transtu.core.dto.ExternalOrderLineDto;
//import com.transtu.core.dto.ReceptionLineDto;
//import com.transtu.core.json.request.ExternalOrderResponse;
//import com.transtu.persistence.entities.ExternalOrder;
//import com.transtu.persistence.entities.ExternalOrderLine;
//import com.transtu.persistence.entities.Product;
//import com.transtu.persistence.repositories.ExternalOrderLineRepository;
//import com.transtu.persistence.repositories.ExternalOrderRepository;
//import com.transtu.persistence.repositories.ProductRepository;
//import com.transtu.persistence.repositories.ReceptionLineRepository;
//import com.transtu.service.ReceptionCommandeService;
//
//
//@Service
//public class ReceptionCommandeServiceImp  implements ReceptionCommandeService{
//	
//	@Autowired 
//	ExternalOrderRepository externalOrderRepository;
//	@Autowired
//	ExternalOrderLineRepository externalOrderLineRepository;
//	@Autowired
//	ReceptionLineRepository receptionLineRepository;
//	@Autowired
//	ProductRepository productRepository;
//	
//	@Override
//	public ExternalOrderResponse ListReceptionCommande(Long externalOrderId) {
//		ExternalOrder ExternalOrder = externalOrderRepository.findOne(externalOrderId);
//		
//		ExternalOrderResponse externalOrderResponse= new ExternalOrderResponse();
//		externalOrderResponse.setStatId(ExternalOrder.getState().getId());
//		externalOrderResponse.setLocationId(ExternalOrder.getLocation().getId());
//		List<ExternalOrderLineDto> list = new ArrayList<>();
//		
//		for (ExternalOrderLine externalOrderLine : 
//			externalOrderLineRepository.findByExternalOrder(ExternalOrder)){
//			ExternalOrderLineDto externalOrderLineDto = new ExternalOrderLineDto();
//			externalOrderLineDto.setOrderQte(externalOrderLine.getOrderQte());
//			externalOrderLineDto.setProductId(externalOrderLine.getProduct().getId());
//			list.add(externalOrderLineDto);
//		}
//	
//		externalOrderResponse.setExternalOrderLinesDto(list);
//		
//		List<ReceptionLineDto> listreception= new ArrayList<>();
//		for (Product  product : productRepository.findByExternOrder(externalOrderId) ){
//			ReceptionLineDto receptionLineDto =new ReceptionLineDto();
//			receptionLineDto.setProductId(product.getId());
//			receptionLineDto.setInvoiceQte(0);
//			receptionLineDto.setPrice(0);
//			receptionLineDto.setReceptionQte(productRepository.
//					findSumQteProductByExternOrder(externalOrderId, product.getId()));
//			
//			listreception.add(receptionLineDto);
//		}
//		externalOrderResponse.setReceptionLineDtos(listreception);;
//		// TODO Auto-generated method stub
//		return externalOrderResponse;
//	}
//
//}
