package com.transtu.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.transtu.core.dto.InternalDeliveryLineDto;
import com.transtu.core.dto.InternalOrderLineDto;
import com.transtu.core.jasper.DeliveryJasper;
import com.transtu.core.jasper.StockJasperEcart;

import com.transtu.core.json.request.InterDeliveryFilterResponse;
import com.transtu.core.json.request.InternalDeliveryFilter;
import com.transtu.core.json.request.InternalDeliveryRequest;
import com.transtu.core.json.request.InternalordfilterResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateDeliveryRequest;
import com.transtu.persistence.entities.Customer;
import com.transtu.persistence.entities.Day;
import com.transtu.persistence.entities.InternalDelivery;
import com.transtu.persistence.entities.InternalDeliveryLine;
import com.transtu.persistence.entities.InternalOrder;
import com.transtu.persistence.entities.InternalOrderLine;
import com.transtu.persistence.entities.Inventory;
import com.transtu.persistence.entities.InventoryLine;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Movement;
//import com.transtu.persistence.entities.OrdinaryMovement;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.QInternalDelivery;
import com.transtu.persistence.entities.QInternalDeliveryLine;
import com.transtu.persistence.entities.Reception;
import com.transtu.persistence.entities.Stock;
import com.transtu.persistence.entities.StockMovement;
import com.transtu.persistence.entities.StockMovementLine;
import com.transtu.persistence.entities.Stocklot;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.persistence.repositories.CustomerRepository;
import com.transtu.persistence.repositories.DayRepository;
import com.transtu.persistence.repositories.InternalDeliveryLineRepository;
import com.transtu.persistence.repositories.InternalDeliveryRepository;
import com.transtu.persistence.repositories.InternalOrderLineRepository;
import com.transtu.persistence.repositories.InternalOrderRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.LotRepository;
import com.transtu.persistence.repositories.MovementRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.ReferenceTypeRepository;
import com.transtu.persistence.repositories.StateRepository;
import com.transtu.persistence.repositories.StockLotRepository;
import com.transtu.persistence.repositories.StockMovementLineRepository;
import com.transtu.persistence.repositories.StockMovementRepository;
import com.transtu.persistence.repositories.StockRepository;
import com.transtu.persistence.repositories.UtilisateurRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.CustomerService;
import com.transtu.service.InternalDeliveryService;
import com.transtu.service.JasperService;
import com.transtu.service.SequenceService;
import com.transtu.service.UtilisateurService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class InternalDeliveryserviceImpl implements InternalDeliveryService {
	
	@PersistenceContext
	EntityManager em ;
	
	@Autowired(required=true)
	InternalDeliveryLineRepository internalDeliveryLineRepo;

	@Autowired(required=true)
	InternalDeliveryRepository internalDeliveryRepo;
	
	@Autowired(required=true)
	InternalOrderRepository internalOrderRepo;
	
	@Autowired(required=true)
	LocationRepository locationRepo;
	
	@Autowired(required=true)
	ProductRepository productRepo;
	
	@Autowired(required=true)
	StockRepository stockRepo;
	
	@Autowired(required=true)
	MovementRepository movementRepo;
	
	@Autowired(required=true)
	StateRepository stateRepo;
	
	@Autowired(required=true)
	InternalOrderLineRepository internalOrderLineRepo;

	@Autowired(required=true)
	SequenceService sequenceService ;
	
	@Autowired(required=true)
    CustomerService customerService;
	
	@Autowired(required=true)
	CustomerRepository customerRepository;
	
	@Autowired(required=true)
	DayRepository dayRepository;
	
	@Autowired(required=true)
	LotRepository LotRepository;
	
	@Autowired(required=true)
	StockMovementRepository stockMovementRepository;
	@Autowired(required=true)
	StockMovementLineRepository stockMovementRepositoryLine;
	@Autowired(required=true)
	ReferenceTypeRepository referenceTypeRepository;
	@Autowired(required=true)
	InternalDeliveryLineRepository internalDeliveryLineRepository;
	@Autowired(required = true)
	UtilisateurRepository utilisateurRepository;
	@Autowired(required=true)
	UtilisateurService utilisateurService;
	@Autowired(required=true)
	LotRepository lotRepository;
	
	@Autowired(required=true)
	StockLotRepository stockLotRepository;
	
	@Autowired(required=true)
	JasperService jasperService;
	
	@Transactional
	@Override
	public MessageResponse addInternalDelivery(InternalDeliveryRequest internalDeliveryRequest) {
		
		List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
		if(user==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		
	 if (internalDeliveryRequest.getInternalOrderId() != null) {
		 InternalOrder internalOrder = this.internalOrderRepo.findOne(internalDeliveryRequest.getInternalOrderId()); 
			 
		Location location = this.locationRepo.findOne(internalDeliveryRequest.getLocationId());
		Customer customer = this.customerRepository.findOne(internalDeliveryRequest.getCustomerId());
		
		if(location==null){
			RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		Day day = dayRepository.findOne(internalDeliveryRequest.getDayId());
	
		if(day==null ){
			RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		if(internalOrder==null){
		RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_NOT_FOUND;
		RestException ex = new RestException(code.getError(), code);
		throw ex;
		}
		
		if(internalOrder.getState().getId()==4){
		RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_ALREADY_DELIVERED;
		RestException ex = new RestException(code.getError(), code);
		throw ex;
		}
		
		
		InternalDelivery internalDelivery = new InternalDelivery();
		internalDelivery.setDateDelivery(new Date());
		internalDelivery.setInternalOrder(internalOrder);
		internalDelivery.setDay(day);
		internalDelivery.setUtilisateur(user.get(0));
		internalDelivery.setCustomer(customer);;
		internalDelivery.setLocation(location);
		internalDelivery.setState(this.stateRepo.findOne(1l));
		internalDelivery.setNumDelivery(this.sequenceService.calculateNumDelivery(location.getLib()));
		internalDelivery = this.internalDeliveryRepo.save(internalDelivery);
		
	
		
		
		for(InternalDeliveryLineDto internalDeliveryLineDto : internalDeliveryRequest.getInternalDeliveryLinesDto()){
			Product product = this.productRepo.findOne(internalDeliveryLineDto.getProductId());
			
			if(product==null){
				RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			Stock stockDepot = this.stockRepo.findByProductAndLocation(product, location);
	
			
			if(stockDepot==null){
				RestExceptionCode code = RestExceptionCode.STOCK_NOT_FOUND	;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			InternalOrderLine internalOrderLine = this.internalOrderLineRepo.findByInternalOrderAndProduct(internalOrder, product);
		
			if(internalOrderLine==null){
				RestExceptionCode code = RestExceptionCode.INCONSISTENT_DATA_STOCK;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			
			if(internalDeliveryLineDto.getQte()== 0 || internalDeliveryLineDto.getLot()==null ){
				RestExceptionCode code = RestExceptionCode.INCONSISTENT_DATA_;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			
			if((internalOrderLine.getOrderQte()<internalDeliveryLineDto.getQte())||
					(stockDepot.getQuantity()<internalDeliveryLineDto.getQte())
					||(stockLotRepository.findByLot(internalDeliveryLineDto.getLot()).getQuantity()<internalDeliveryLineDto.getQte())){
				
			
				RestExceptionCode code = RestExceptionCode.INCONSISTENT_DATA_STOCK;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
		
			InternalDeliveryLine internalDeliveryLine = new InternalDeliveryLine();
			internalDeliveryLine.setInternalDelivery(internalDelivery);
			internalDeliveryLine.setProduct(product);
			internalDeliveryLine.setLot(internalDeliveryLineDto.getLot());
			internalDeliveryLine.setQte(internalDeliveryLineDto.getQte());
			internalDeliveryLine = this.internalDeliveryLineRepo.save(internalDeliveryLine);
			
		}
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "la livraison est ajoutÃ© avec succÃ©s",internalDelivery.getNumDelivery());	
		 
		 } else {
				
			    Location locationPharma = locationRepo.findOne(1l);
     		    Location locationDep = locationRepo.findOne(2l);
			    Location location = this.locationRepo.findOne(internalDeliveryRequest.getLocationId());
				Customer customer = this.customerRepository.findOne(internalDeliveryRequest.getCustomerId());
				
				if(location==null){
					RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
					RestException ex = new RestException(code.getError(), code);
					throw ex;
				}
				
				Day day = dayRepository.findOne(internalDeliveryRequest.getDayId());
			
				if(day==null ){
					RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
					RestException ex = new RestException(code.getError(), code);
					throw ex;
				}
				
				
				
//				if(internalOrder==null){
//				RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_NOT_FOUND;
//				RestException ex = new RestException(code.getError(), code);
//				throw ex;
//				}
				
//				if(internalOrder.getState().getId()==4){
//				RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_ALREADY_DELIVERED;
//				RestException ex = new RestException(code.getError(), code);
//				throw ex;
//				}
//				
				
				InternalDelivery internalDelivery = new InternalDelivery();
				internalDelivery.setDateDelivery(new Date());
				internalDelivery.setInternalOrder(null);
				internalDelivery.setUtilisateur(user.get(0));
				internalDelivery.setDay(day);
				internalDelivery.setCustomer(customer);
				internalDelivery.setLocation(location);
				internalDelivery.setState(this.stateRepo.findOne(4l));
			
				internalDelivery.setNumDelivery(this.sequenceService.calculateNumDelivery(location.getLib()));
				internalDelivery = this.internalDeliveryRepo.save(internalDelivery);
				
				
				StockMovement stockMovement = new StockMovement();
    			stockMovement.setDateMovement(new Date());
    			stockMovement.setNumMovement(sequenceService.calculateNumMvtOrdSortie());	
    			stockMovement.setTypeMvt("-1");
    			stockMovement.setDescriminator("Ordinaire");
    			stockMovement.setUtilisateur(user.get(0));
    			stockMovement.setLocation(locationPharma);
    			stockMovement.setReferenceType(referenceTypeRepository.findOne(5l));//livraison interne (sortie pharmacie)
    			stockMovement.setInternalDelivery(internalDelivery);
    			stockMovementRepository.save(stockMovement);
    		  //---------------<>----------------------------<>-------------------
    			
//    			StockMovement stockMovement1 = new StockMovement();
//    			stockMovement1.setDateMovement(new Date());
//    			stockMovement1.setNumMovement(sequenceService.calculateNumMvtExepEntre());	
//    			stockMovement1.setTypeMvt("1");
//    			stockMovement1.setDescriminator("Ordinaire");
//    			stockMovement1.setLocation(locationDep);
//    			stockMovement1.setReferenceType(referenceTypeRepository.findOne(3l));
//    			stockMovement1.setInternalDelivery(internalDelivery);
//    			stockMovementRepository.save(stockMovement1);
				
				
				for(InternalDeliveryLineDto internalDeliveryLineDto : internalDeliveryRequest.getInternalDeliveryLinesDto()){
					Product product = this.productRepo.findOne(internalDeliveryLineDto.getProductId());
					
					if(product==null){
						RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
						RestException ex = new RestException(code.getError(), code);
						throw ex;
					}
					Stock stockDepot = this.stockRepo.findByProductAndLocation(product, location);
					
					if(stockDepot==null){
						RestExceptionCode code = RestExceptionCode.STOCK_NOT_FOUND	;
						RestException ex = new RestException(code.getError(), code);
						throw ex;
					}
//					InternalOrderLine internalOrderLine = this.internalOrderLineRepo.findByInternalOrderAndProduct(internalOrder, product);
//					if(internalOrderLine==null){
//						RestExceptionCode code = RestExceptionCode.INCONSISTENT_DATA_STOCK;
//						RestException ex = new RestException(code.getError(), code);
//						throw ex;
//					}
					
//					if((internalOrderLine.getOrderQte()<internalDeliveryLineDto.getQte())||(stockDepot.getQuantity()<internalDeliveryLineDto.getQte())){
//						RestExceptionCode code = RestExceptionCode.INCONSISTENT_DATA_STOCK;
//						RestException ex = new RestException(code.getError(), code);
//						throw ex;
//					}
					
					Stock stockPharmacie = this.stockRepo.findByProductAndLocation(product, locationPharma);
					InternalDeliveryLine internalDeliveryLine = new InternalDeliveryLine();
					internalDeliveryLine.setInternalDelivery(internalDelivery);
					internalDeliveryLine.setProduct(product);
					//internalDeliveryLine.setQte(internalDeliveryLineDto.getQte());
					
					if(stockPharmacie.getQuantity()<internalDeliveryLineDto.getQte()) {
					
					internalDeliveryLine.setMissingqte(internalDeliveryLineDto.getQte()-stockPharmacie.getQuantity());
					internalDeliveryLine.setQte(stockPharmacie.getQuantity());
					internalDeliveryLine = this.internalDeliveryLineRepo.save(internalDeliveryLine);
                    stockPharmacie.setQuantity(0);
    				this.stockRepo.save(stockPharmacie);
					}
					
					
					if(stockPharmacie.getQuantity()>=internalDeliveryLineDto.getQte()) {
					internalDeliveryLine.setMissingqte(0);
					internalDeliveryLine.setQte(internalDeliveryLineDto.getQte());
					internalDeliveryLine = this.internalDeliveryLineRepo.save(internalDeliveryLine);
	                stockPharmacie.setQuantity(stockPharmacie.getQuantity()-internalDeliveryLine.getQte());
	    			this.stockRepo.save(stockPharmacie);
    				
					}
				    //-------------<>------stok-mvt-ligne------<>-------------------------<> 				
					StockMovementLine stockMovementLine= new StockMovementLine();
					stockMovementLine.setStockMovement(stockMovement);
					stockMovementLine.setDescription("Mouvement ordinaire-sortie de produit du pharmacie vers District : "+ internalDelivery.getNumDelivery()
					+"le produit est:"+product.getLib());
					stockMovementLine.setStockMovement(stockMovement);
					stockMovementLine.setProduct(product);
					stockMovementLine.setMovmentQte(internalDeliveryLineDto.getQte());
					stockMovementRepositoryLine.save(stockMovementLine);
					
					
					
					
					//---------------------------Concerne l'etat livré 
//					stockDepot.setQuantity(stockDepot.getQuantity()-internalDeliveryLine.getQte());
//					this.stockRepo.save(stockDepot);
//					Stock stockPharmacie = this.stockRepo.findByProductAndLocation(product, internalOrder.getLocation());
//					stockPharmacie.setQuantity(stockPharmacie.getQuantity()+internalDeliveryLine.getQte());
//					this.stockRepo.save(stockPharmacie);
//					Movement movement = new Movement();
//					movement.setDate(new Date());
//					movement.setDescription("sortie du produit vers le stock "+stockPharmacie.getLocation().getLib());
//					movement.setLocation(location);
//					movement.setProduct(product);
//					movement.setQuantity(internalDeliveryLine.getQte());
//					
//					this.movementRepo.save(movement);
					
				}
//				   internalOrder.setState(this.stateRepo.findByType("livrÃ©"));
//				   this.internalOrderRepo.save(internalOrder);
				   //----------------------------------------------------------------------
				return new MessageResponse(HttpStatus.OK.toString(), null, "la livraison est ajoutÃ© avec succÃ©s",internalDelivery.getNumDelivery());	 
			 
			 
			 
		 }	
	}

	@Transactional
	@Override
	public MessageResponse deleteInternalDelivery(Long internalDeliveryId) {
		InternalDelivery internalDelivery = this.internalDeliveryRepo.findOne(internalDeliveryId);
		
		if(internalDelivery==null){
			RestExceptionCode code = RestExceptionCode.INTERNAL_DELIVERY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		if(internalDelivery.getState().getId()==4){
			RestExceptionCode code = RestExceptionCode.INTERNAL_DELIVERY_ALREADY_DELIVRED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		if(internalDelivery.getState().getId()==4){
			RestExceptionCode code = RestExceptionCode.INTERNAL_DELIVERY_ALREADY_DELIVRED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		if(internalDelivery.getState().getId()==2){
			RestExceptionCode code = RestExceptionCode.INTERNAL_DELIVERY_ALREADY_VALIDATE;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		//List<InternalDelivery> list = new ArrayList<>();
				//internalDeliveryLineRepository.findByInternalDelivery(internalDelivery);
		for (InternalDeliveryLine internalDeliveryLine :internalDeliveryLineRepository.findByInternalDelivery(internalDelivery)) {
			
			internalDeliveryLineRepo.delete(internalDeliveryLine);
			
		}
		internalDeliveryRepo.delete(internalDelivery);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "la livraison a été supprimée avec succÃ©s",internalDelivery.getNumDelivery());	
	}

	@Transactional
	@Override
	public MessageResponse updateInternalDelviery(UpdateDeliveryRequest updateDeliveryRequest) {
		InternalDelivery internalDelivery = this.internalDeliveryRepo.findOne(updateDeliveryRequest.getInternalDeliveryId());
		if(internalDelivery==null){
			RestExceptionCode code = RestExceptionCode.INTERNAL_DELIVERY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		if(internalDelivery.getState().getId()==4){
			RestExceptionCode code = RestExceptionCode.INTERNAL_DELIVERY_ALREADY_DELIVRED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		for(InternalDeliveryLineDto internalDeliveryLineDto : updateDeliveryRequest.getInternalDeliveryLinesDto()){
			Product product = this.productRepo.findOne(internalDeliveryLineDto.getProductId());
			if(product==null){
				RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			List<InternalDeliveryLine> internalDeliveryLines = this.internalDeliveryLineRepo.
					findByProductAndInternalDeliveryAndLot(product, internalDelivery, internalDeliveryLineDto.getLot());
			if(internalDeliveryLines==null){
				RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_EXISTING_IN_THIS_DELIVERY;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			
			System.out.println("-------------------------------------internalDeliveryLineDto.getLot())--------------------------------------------"+internalDeliveryLineDto.getLot());
			//Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationRepo.findOne(2l));
			System.out.println("---------------------------------------"+internalDeliveryLines);
			//stockDepot.setQuantity((stockDepot.getQuantity()+internalDeliveryLines.get(0).getQte())-internalDeliveryLineDto.getQte());
		//	this.stockRepo.save(stockDepot);
			 
			//Stocklot stocklot = this.stockLotRepository.findByLot(internalDeliveryLines.get(0).getLot());
			//stocklot.setQuantity(stocklot.getQuantity()+internalDeliveryLines.get(0).getQte()-internalDeliveryLineDto.getQte());
			
			// TODO fix it
			//Stock stockPharmacie = this.stockRepo.findByProductAndLocation(internalDeliveryLines.get(0).getProduct(), internalDelivery.getInternalOrder().getLocation());
			//stockPharmacie.setQuantity((stockPharmacie.getQuantity()-internalDeliveryLines.get(0).getQte())+internalDeliveryLineDto.getQte());
		//	this.stockRepo.save(stockPharmacie);
			
			internalDeliveryLines.get(0).setQte(internalDeliveryLineDto.getQte());
			this.internalDeliveryLineRepo.save(internalDeliveryLines.get(0));
			
		}
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "la livraison est modifiÃ© avec succÃ©s",internalDelivery.getNumDelivery());	
	}

    @Override
    public InterDeliveryFilterResponse filterInternalDelivery(InternalDeliveryFilter internalDeliveryFilter) {
          JPQLQuery<?> query = new JPAQuery(em); 
          
          QInternalDelivery internalDelivery = QInternalDelivery.internalDelivery;
          QInternalDeliveryLine internalDeliveryLine = QInternalDeliveryLine.internalDeliveryLine;
         
          query.from(internalDeliveryLine).orderBy(internalDelivery.dateDelivery.desc())
                        .leftJoin(internalDeliveryLine.internalDelivery , internalDelivery);
  
          if(internalDeliveryFilter.getMinDate()!=null){
                 query.where(internalDelivery.dateDelivery.after(internalDeliveryFilter.getMinDate()));
                
          }
          if(internalDeliveryFilter.getMaxDate()!=null){
                 query.where(internalDelivery.dateDelivery.before(internalDeliveryFilter.getMaxDate()));
                 
          }
          //if(internalDeliveryFilter.getProductId().size()>0){
          if(internalDeliveryFilter.getProductId()!=null){
                 query.where(internalDeliveryLine.product.id.in(internalDeliveryFilter.getProductId()));
          
               
            }
          if(internalDeliveryFilter.getNumDelivery().isEmpty()== false){
                 query.where(internalDelivery.numDelivery.eq(internalDeliveryFilter.getNumDelivery()));
               
          }
          if(internalDeliveryFilter.getInternalDeliveryId()!=null){
                 query.where(internalDelivery.Id.eq(internalDeliveryFilter.getInternalDeliveryId()));
                
          } 
          
      System.out.println("-----------internalDeliveryFilter.getCustomerId()---------"
          		+ "----------------------"+internalDeliveryFilter.getCustomerId());
          if(internalDeliveryFilter.getCustomerId()!=null){
              query.where(internalDelivery.customer.id.eq(internalDeliveryFilter.getCustomerId()));
         
          } 
          if(internalDeliveryFilter.getNumInternalOrder()!=null){
              query.where(internalDelivery.internalOrder.numOrder.eq(internalDeliveryFilter.getNumInternalOrder()));
            
          } 
          
          if(internalDeliveryFilter.getInternalOrderyId()!=null){
              query.where(internalDelivery.internalOrder.Id.eq(internalDeliveryFilter.getInternalOrderyId()));
            
          } 
         
          if(internalDeliveryFilter.getStateId()!=null){
              query.where(internalDelivery.state.id.eq(internalDeliveryFilter.getStateId()));
         
          } 
          
          
          
        //***********************
     	 if(internalDeliveryFilter.getPage()!=null){
        	  query.offset((internalDeliveryFilter.getPage()-1)*20);
        	  query.limit(20);	    	
          }

     	InterDeliveryFilterResponse interDeliveryFilterResponse = new InterDeliveryFilterResponse();
     	interDeliveryFilterResponse.setInternalDeliveryrs(query.select(internalDelivery).distinct().fetch());
     	interDeliveryFilterResponse.setTotalItemes(query.fetchCount());
          
     	//*****************
          
//		List<InternalDelivery> list = query.select(internalDelivery).fetch();
//		Set<InternalDelivery> set = new LinkedHashSet<>();
//		set.addAll(list);
//		
//		List<InternalDeliveryFilterResponse> internalDeliveriesResponse = new ArrayList<>();
//		for(InternalDelivery  delivery : set){
//			InternalDeliveryFilterResponse internalDeliveryResponse = new InternalDeliveryFilterResponse();
//			internalDeliveryResponse.setDeliveryNumber(delivery.getNumDelivery());
//			internalDeliveryResponse.setOrderNumber(delivery.getInternalOrder().getNumOrder());
//			internalDeliveryResponse.setProductNumber(delivery.getInternalDeliveryLines().size());		
//			for(InternalDeliveryLine deliveryLine : delivery.getInternalDeliveryLines()){
//            internalDeliveryResponse.getProductsId().add(deliveryLine.getId());
//			}
//			internalDeliveriesResponse.add(internalDeliveryResponse);
//		}
		
		return interDeliveryFilterResponse;
	}

    
    @Transactional
    @Override
    public MessageResponse changeStateInternalDelivery(Long internalDeliveryId,Long stateId ) {
    	
    	List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
		if(user==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");
    	String msg = null;
    	
    	//stateId : 1---->brouillon--*
    	//stateId : 2---->valider--*
    	//stateId : 3---->annuller--*
    	//stateId : 4---->livrer--*
    	//stateId : 5---->livrer partiellement--*
    	
    	
    	InternalDelivery internalDelivery = internalDeliveryRepo.findOne(internalDeliveryId);
    	
    	if(internalDelivery==null){
    		RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_NOT_FOUND;
    		RestException ex = new RestException(code.getError(), code);
    		throw ex;
    	}
    	
    	 InternalOrder internalOrder= internalOrderRepo.findOne(internalDelivery.getInternalOrder().getId());
    	 if(internalOrder.getState().getId()==4){
 			RestExceptionCode code = RestExceptionCode.INTERNAL_ORDER_ALREADY_DELIVRED;
 			RestException ex = new RestException(code.getError(), code);
 			throw ex;
 		}
    	 if(internalDelivery.getState().getId()==4){
    			RestExceptionCode code = RestExceptionCode.INTERNAL_DELIVERY_ALREADY_DELIVRED;
    			RestException ex = new RestException(code.getError(), code);
    			throw ex;
    		}
    	//annulation de la validation : retour de l'état valider à l'état brouillon
		 if (stateId==1){
			 internalDelivery.setState(stateRepo.findOne(stateId));
			 internalDelivery.setUtilisateur(user.get(0));
			 internalDeliveryRepo.save(internalDelivery);
		 msg= "L'annulation de la validation à été faite avec succès";
		 }
		 //**************************
    	 if (stateId==2){
    		 
    		
    		 internalDelivery.setState(stateRepo.findOne(stateId));
    		 internalDelivery.setUtilisateur(user.get(0));
    		 internalDeliveryRepo.save(internalDelivery);
    	 msg= "la livraison Interne a été valider avec succés";
    	 }
    	 if (stateId==3){
    		 internalDelivery.setState(stateRepo.findOne(stateId));
    		 internalDelivery.setUtilisateur(user.get(0));
    		 internalDeliveryRepo.save(internalDelivery);
    	 msg="la livraison Interne a été annuler avec succés";
    	
    	 }
    	
    	 
    	 if (stateId==4){
    	//-------------------	MAJ-STOCK-PHARMA--&&--MAJ-STOCK-DEPOT------------//	
    		 
    		  //-------------<>------stok-mvt-entete------<>------------------------- 
    		 Location locationPharma = locationRepo.findOne(1l);
     		 Location locationDep = locationRepo.findOne(2l);
    		
    		    StockMovement stockMovement = new StockMovement();
    			stockMovement.setDateMovement(new Date());
    			stockMovement.setNumMovement(sequenceService.calculateNumMvtOrdSortie());	
    			stockMovement.setTypeMvt("-1");
    			stockMovement.setUtilisateur(user.get(0));
    	
    			stockMovement.setDescriminator("Ordinaire");
    			stockMovement.setLocation(locationDep);
    			stockMovement.setReferenceType(referenceTypeRepository.findOne(3l));
    			stockMovement.setInternalDelivery(internalDelivery);
    			stockMovementRepository.save(stockMovement);
    		  //---------------<>----------------------------<>-------------------
    			
    			StockMovement stockMovement1 = new StockMovement();
    			stockMovement1.setDateMovement(new Date());
    			stockMovement1.setNumMovement(sequenceService.calculateNumMvtOrdEntre());	
    			stockMovement1.setTypeMvt("1");
    			stockMovement.setUtilisateur(user.get(0));
    		
    			stockMovement1.setDescriminator("Ordinaire");
    			stockMovement1.setLocation(locationPharma);
    			stockMovement1.setReferenceType(referenceTypeRepository.findOne(4l));
    			stockMovement1.setInternalDelivery(internalDelivery);
    			stockMovementRepository.save(stockMovement1);
    		  //---------------<>----------------------------<>-------------------
    		
    		 for (InternalDeliveryLine internalDeliveryLines : 
    			 internalDelivery.getInternalDeliveryLines()){
    			 
    			     	        List<InternalDeliveryLine>	internalDeliveryLines1	= internalDeliveryLineRepo.
    			findByProductAndInternalDeliveryAndLot(internalDeliveryLines.getProduct(),internalDelivery, internalDeliveryLines.getLot());
    	
    			 
    			 
    			 if (internalDeliveryLines1.size()>1) {
    			    RestExceptionCode code = RestExceptionCode.QUANTITY_LOWER_DELIVRED_QUANTITY;
 		 			RestException ex = new RestException(code.getError(), code);
 		 			throw ex;
    			 }
    			 
    			 
    			 if(internalDeliveryLines.getLot()!= null) {
     				Stocklot stocklot =this.stockLotRepository.findByLot(internalDeliveryLines.getLot());
     				
     			 if(stocklot.getQuantity() >= internalDeliveryLines.getQte() ) {
     				stocklot.setQuantity(stocklot.getQuantity()-internalDeliveryLines.getQte());
     				this.stockLotRepository.save(stocklot);
     				}
     			else {
     					
     				RestExceptionCode code = RestExceptionCode.QUANTITY_LOWER_DELIVRED_QUANTITY;
     		 		RestException ex = new RestException(code.getError(), code);
     		 		throw ex;
     			   }
     				
     			}
    			 Stock stockDepot = this.stockRepo.findByProductAndLocation(internalDeliveryLines.getProduct(), locationDep);
    			 stockDepot.setQuantity(stockDepot.getQuantity()-internalDeliveryLines.getQte());
    			 Stock stockPharmacie = this.stockRepo.findByProductAndLocation(internalDeliveryLines.getProduct(), locationPharma);
    			 stockPharmacie.setQuantity(stockPharmacie.getQuantity()+internalDeliveryLines.getQte());
    				this.stockRepo.save(stockPharmacie);
    				this.stockRepo.save(stockDepot);
                 
    				Movement movement = new Movement();
    				movement.setDate(new Date());
    				movement.setDescription("sortie du produit vers le stock "+stockPharmacie.getLocation().getLib());
    				movement.setLocation(locationDep);
    				movement.setType("-1");
    				movement.setProduct(internalDeliveryLines.getProduct());
    				movement.setQuantity(internalDeliveryLines.getQte());
    				
    				this.movementRepo.save(movement);
    				
   //-------------<>------stok-mvt-ligne------<>-------------------------<> 				
    				StockMovementLine stockMovementLine= new StockMovementLine();
    				stockMovementLine.setStockMovement(stockMovement);
    				//stockMovementLine.setMotif(motif);
    				stockMovementLine.setDescription("Mouvement ordinaire-sortie du produit de depot  vers le stock pharmacie"+ internalDelivery.getNumDelivery());
    				stockMovementLine.setStockMovement(stockMovement);
    				stockMovementLine.setProduct(internalDeliveryLines.getProduct());
    				stockMovementLine.setMovmentQte(internalDeliveryLines.getQte());
    				stockMovementRepositoryLine.save(stockMovementLine);
    		
  //---------------<>----------------------------<>-----------------------<>	
    				StockMovementLine stockMovementLine1= new StockMovementLine();
					stockMovementLine1.setStockMovement(stockMovement1);
					//stockMovementLine.setMotif(motif);
					stockMovementLine1.setDescription("Mouvement ordinaire-entre de produit de depot avers le pharmacie : "+ internalDelivery.getNumDelivery());
					stockMovementLine1.setStockMovement(stockMovement1);
					stockMovementLine1.setProduct(internalDeliveryLines.getProduct());
					stockMovementLine1.setMovmentQte(internalDeliveryLines.getQte());
					stockMovementRepositoryLine.save(stockMovementLine1);
	
    		 }
    		 internalOrder.setState(stateRepo.findOne(4l));
    		 internalOrderRepo.save(internalOrder);
    		 internalDelivery.setState(stateRepo.findOne(4l));
    		 internalDeliveryRepo.save(internalDelivery);
    	     msg="la livraison Interne a été receptionné avec succés";
    	
    	
    }
    	 return new MessageResponse(HttpStatus.OK.toString(), null, msg,internalDelivery.getNumDelivery());	
    	 
    }
    
    @Override
	public 	InternalDelivery findByInternalDeliveryNum(Long referenceId, String Deliverynum) {
	
    	if(referenceId==3) {
			
    		 InternalDelivery internalDelivery=  internalDeliveryRepo.findByNumDelivery(Deliverynum); 
			 if(internalDelivery!=null) {
					return internalDelivery;
					}
					else {	 		
				    return new InternalDelivery();
					}
			 
			 
		} else {
			 return new InternalDelivery();
		}

	}
    
    
    
    @Override
	public File get(Long customerId, Date maxDate, Date minDate) throws FileNotFoundException{
    	  System.out.println("customerId 44"+customerId);
		
		return this.getPdf(this.getInternalDeliveryFromPdf(customerId,  maxDate, minDate));
	}
	
	private File getPdf(HashSet<DeliveryJasper> hashSet) throws FileNotFoundException{
		  System.out.println("customerId 66666");
		JRDataSource dataSource = new JRBeanCollectionDataSource(hashSet,false);
		return jasperService.jasperFile(dataSource, "livraison");
	}
	
	
	
	@Override
	public LinkedHashSet<DeliveryJasper> getInternalDeliveryFromPdf(Long customerId, Date maxDate, Date minDate) {
		
		  System.out.println("customerId  2"+customerId);
			    LinkedHashSet<DeliveryJasper> deliveryJaspers = new LinkedHashSet<>() ;
			    filterJasperInternalDelivery( customerId,  maxDate,  minDate).stream().forEach(del->{  
			    
			    	
			    //  deliveryJasper.setCustomerName(del.getCustomer().getLabel());
			    //	deliveryJasper.setNumDelivery(del.getNumDelivery()); 
			    //	deliveryJasper.setDateDelivery(del.getDateDelivery());
			    //  deliveryJaspers.add(deliveryJasper);
			    	
			    	del.getInternalDeliveryLines().stream().forEach(line->{ 
			    	
			    		DeliveryJasper deliveryJasper = new DeliveryJasper();  
			    		deliveryJasper.setCustomerName(del.getCustomer().getLabel());
				    	deliveryJasper.setNumDelivery(del.getNumDelivery()); 
				    	deliveryJasper.setDateDelivery(del.getDateDelivery());
			    		deliveryJasper.setDesignation(line.getProduct().getLib());	
			    		deliveryJasper.setProductCode(line.getProduct().getCodePctProd());
			    		deliveryJasper.setMissingqte(line.getMissingqte());
			    		deliveryJasper.setDateEnd(maxDate);
			    		deliveryJasper.setDateStart(minDate);
			    		deliveryJasper.setQte(line.getQte());
			    		deliveryJaspers.add(deliveryJasper);
			    	});
			    	
			    	
			    	
			});
			    
			return deliveryJaspers;
			  
		  
		  
		  
		
	}
	
	
	
	
	
	 @Override
	    public List<InternalDelivery> filterJasperInternalDelivery(Long customerId, Date dateMin,Date dateMax) {
	          JPQLQuery<?> query = new JPAQuery(em); 
	  
	          
	          System.out.println("customerId"+customerId);
	          QInternalDelivery internalDelivery = QInternalDelivery.internalDelivery;
	          QInternalDeliveryLine internalDeliveryLine = QInternalDeliveryLine.internalDeliveryLine;
	         
	          query.from(internalDeliveryLine).orderBy(internalDelivery.dateDelivery.desc())
	                        .leftJoin(internalDeliveryLine.internalDelivery , internalDelivery).orderBy(internalDelivery.dateDelivery.desc());
	  
	          if(dateMin!=null){
	                 query.where(internalDelivery.dateDelivery.after(dateMin));
	                
	          }
	          if(dateMax!=null){
	                 query.where(internalDelivery.dateDelivery.before(dateMax));
	                 
	          }
	          
	        

	          if(customerId!=null){
	              query.where(internalDelivery.customer.id.eq(customerId));
	         
	          } 
	        
	          
	         // List<InternalDelivery>  listInternalDeliverys ;
	         // listInternalDeliverys.set(0, query.select(internalDelivery).distinct().fetch())
	      
			return query.select(internalDelivery).distinct().orderBy(internalDelivery.Id.desc()).fetch();
		}

    
    
}

