package com.transtu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.Doctor;
import com.transtu.persistence.entities.Prescription;
import com.transtu.persistence.repositories.DoctorRepository;
import com.transtu.persistence.repositories.PrescriptionLineRepository;
import com.transtu.persistence.repositories.PrescriptionRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.DoctorService;

@Service
public class DoctorServiceImpl implements DoctorService {
	
	
	
	@Autowired
	private DoctorRepository doctorRepository;
	@Autowired
	private PrescriptionRepository  prescriptionRepository;
	
	@Override
	public List<Doctor> getListDoctors() {
		
//		this.doctorRepository.findAll(new Sort(Direction.ASC, "name")).forEach(item->{
//			if("d".equals(item.getName())) {
//				
//			}
//			
//		});
		
		return this.doctorRepository.findAll(new Sort(Direction.ASC, "name"));
		
		
		
	}
	
	
	

	@Override
	public MessageResponse addDoctor(Doctor doctor) {
		
		Doctor doctor1 = this.doctorRepository.findByName(doctor.getName());
		if(doctor1!=null){
			RestExceptionCode code = RestExceptionCode.EXISTING_DISTRICT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		this.doctorRepository.save(doctor);
		return new MessageResponse(HttpStatus.OK.toString(), null, "Medecin a été ajouté avec success",doctor.getName());	

	}
	
	
	@Override
	public MessageResponse deleteDoctor(Long idDoctor) {
		Doctor doctor = this.doctorRepository.findOne(idDoctor);
		List<Prescription> prescriptions =  prescriptionRepository.findByDoctorId(idDoctor);
		if(doctor==null){
			RestExceptionCode code = RestExceptionCode.DISTRICT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		if(prescriptions.size()>0) 
		{return new MessageResponse(HttpStatus.NOT_ACCEPTABLE.toString(), null, "Medecin déja utilisé",doctor.getName());}
		else{  
		this.doctorRepository.delete(idDoctor);
		return new MessageResponse(HttpStatus.OK.toString(), null, "Medecin a été supprimé avec success",doctor.getName()); }
		}
	

}
