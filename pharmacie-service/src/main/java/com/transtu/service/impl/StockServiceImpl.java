package com.transtu.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.transtu.core.jasper.StockJasper;
import com.transtu.core.json.request.FilterStockReport;
import com.transtu.core.json.request.ProductResponse;
import com.transtu.core.json.request.StockFilterRequest;
import com.transtu.core.json.request.StockResponse;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.QLocation;
import com.transtu.persistence.entities.QProduct;
import com.transtu.persistence.entities.QStock;
import com.transtu.persistence.entities.Stock;
import com.transtu.persistence.repositories.ControlStockRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.StockRepository;
import com.transtu.service.JasperService;
import com.transtu.service.StockService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


 @Service
public class StockServiceImpl  implements StockService{

@Autowired(required = true)
private StockRepository stockRepository;
@Autowired(required = true)
private ControlStockRepository controlStockRepository;
@Autowired(required = true)
ProductRepository productRepository;
@Autowired(required = true)
LocationRepository locationRepository;
@Autowired(required=true)
JasperService jasperService ;

private final static Logger logger = LoggerFactory.getLogger(StockServiceImpl.class);
	

	@PersistenceContext
	EntityManager em ;

	@Override
	public List<StockResponse> getListStockBreak() {
		
		List<StockResponse> list = new ArrayList<>();
		for (Stock stk : this.stockRepository.getListStockBreak()){
		StockResponse st = new StockResponse() ;
		
		st.setProductResponse(new ProductResponse(stk.getProduct().getId(), stk.getProduct().getBarCode(),
		stk.getProduct().getCodePctProd(), stk.getProduct().getLib()));
		st.setMinStock(this.controlStockRepository.findOne(stk.getId()).getMinStock());
		st.setQuantity(stk.getQuantity());
		list.add(st);
			
			
		}
		logger.info("________get list Stock break_______________");
		 return  list;
	}
	

	@Override
	public List<StockResponse> getTotalListStockBreak() {

		List<StockResponse> list = new ArrayList<>();
		for (Product prod : this.productRepository.getListProductEventful()){
		
			StockResponse stockResponse = new StockResponse();
		if(prod.getSafetyStock() >= stockRepository.getSumQteStock(prod.getId())){
			stockResponse.setProductResponse(new ProductResponse(prod.getId(), prod.getBarCode(), prod.getCodePctProd(),
					prod.getLib()));
			stockResponse.setSafetyStock(prod.getSafetyStock());
			stockResponse.setSumStock(stockRepository.getSumQteStock(prod.getId()));
		list.add(stockResponse);
		
		}
			
		}
		logger.info("________get list Total Stock break_______________");
		 return  list;
	}


	@Override
	public Double getQteByProductAndLocation(Long idProduct, Long idLocation) {
		Location location = this.locationRepository.findOne(idLocation);
		Product product = this.productRepository.findOne(idProduct);
		
		return this.stockRepository.findByProductAndLocation(product, location).getQuantity();
	}

	
	
	
//	@Override
//	public List<ProductResponse> getTotalListStockBreak() {
//
//		List<ProductResponse> list = new ArrayList<>();
//		for (Product prod : this.productRepository.getListProductEventful()){
//		
//		ProductResponse productResponse = new ProductResponse();
//		if(prod.getSafetyStock() >= stockRepository.getSumQteStock(prod.getId())){
//			productResponse.setId(prod.getId());
//			productResponse.setBarCode(prod.getBarCode());
//			productResponse.setCodePctProd(prod.getCodePctProd());
//			productResponse.setLib(prod.getLib());
//			productResponse.setSafetystock(prod.getSafetyStock());
//			productResponse.setSumstock(stockRepository.getSumQteStock(prod.getId()));
//		list.add(productResponse);
//		
//		}
//			
//		}
//		logger.info("________get list Total Stock break_______________");
//		 return  list;
//	}
	
	
	@Override
	public File getPdfStock(FilterStockReport filterStockReport) throws FileNotFoundException{
		return this.getPdf(this.getStockFromPdf(filterStockReport));
	}
	
	private File getPdf(List<StockJasper> list) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
		return jasperService.jasperFile(dataSource, "stock");
	}
	
	//----------------------------------------------------------------------/**********************/----------------------------------------------------------------------------------//
	@Override
	public File getXlsxStock(FilterStockReport filterStockReport) throws FileNotFoundException{
		return this.getXlsx(this.getStockFromPdf(filterStockReport));
	}
	
	private File getXlsx(List<StockJasper> list) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
		return jasperService.jasperFileXlsx(dataSource, "stock");
	}


	@Override
	public List<StockJasper> getStockFromPdf(FilterStockReport filterStockReport) {
		
		List<Product> products = new ArrayList<>();
		if(filterStockReport.getProductsId()!=null && filterStockReport.getProductsId().size()>0 && filterStockReport.getProductsId().get(0)!=0 && 
				filterStockReport.getProductsId().isEmpty()==false){
			products = this.productRepository.findByIdIn(filterStockReport.getProductsId());
		}else{
			if (filterStockReport.isFullProduct()==true ) {
				System.out.println("-----------isFullProduct--------");
			products = this.productRepository.findDistinctByActived(true, (new Sort(Direction.ASC, "lib"))) ;
			}else {
				
				System.out.println("-----------isFullProductfalse--------");
				products = this.productRepository.getListProductSupp();
			}
		}
	        Location pharmacie =locationRepository.findOne(1l);
	        Location depot =locationRepository.findOne(2l);
		    List<StockJasper> stocksJasper = new ArrayList<>() ;
		     products.stream().forEach(p->{
			//Stock stock = this.stockRepository.findByProductAndLocation(p, this.locationRepository.findOne(filterStockReport.getStockId()));
			StockJasper stockJasp = new StockJasper();
			stockJasp.setBarCode(p.getBarCode());
			stockJasp.setCodePct(p.getCodePctProd());
			stockJasp.setPrice(p.getPrice());
			stockJasp.setQtStockpharma(stockRepository.findByProductAndLocation(p, pharmacie).getQuantity());
			stockJasp.setQtStockDep(stockRepository.findByProductAndLocation(p, depot).getQuantity());
			stockJasp.setDesignation(p.getLib());
			double totalQte =stockRepository.findByProductAndLocation(p, pharmacie).getQuantity()+stockRepository.findByProductAndLocation(p, depot).getQuantity();
			stockJasp.setQtStock(totalQte);
			stockJasp.setDateInv(new Date());
			double totalTht =(double)(stockJasp.getPrice()*totalQte);
			stockJasp.setTotal((double) totalTht);//*(1+(p.getVatRate()/100)))
			stockJasp.setVatRate(p.getVatRate());
			double totalTtc =(double) (totalTht*(p.getVatRate()*0.01+1)) ;
			stockJasp.setTotalTtc(round(totalTtc, 3));
			//stockJasp.setTotalTtc((float) (totalTtc+((stockJasp.getPrice()*totalQte)*p.getVatRate())/100));
			stocksJasper.add(stockJasp);
			
		});
		return stocksJasper;
	}

	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	@Override
	public List<Product> filterStock(StockFilterRequest stockFilterRequest) {
		JPQLQuery<?> query = new JPAQuery(em); 
		
		QStock stock = QStock.stock ;
		QProduct product = QProduct.product ; 
		QLocation location = QLocation.location ; 
		
		query.from(stock)
				.leftJoin(stock.product, product); 
		if(stockFilterRequest.getProductsId()!=null){
			query.where(product.id.in(stockFilterRequest.getProductsId()));
		}
		if(stockFilterRequest.getLocationId()!=null){
			query.where(stock.location.id.eq(stockFilterRequest.getLocationId()));
		}
		
		query.distinct();
		List<Product> list =  query.select(product).fetch();
		
		return list;
	}
	
	
	
	
	
}
