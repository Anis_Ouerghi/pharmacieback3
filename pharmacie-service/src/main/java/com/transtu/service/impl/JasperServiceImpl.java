/**
 * 
 */
package com.transtu.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.transtu.service.JasperService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;



@Service
public class JasperServiceImpl implements JasperService {
	
	
	@Override
	public File jasperFile1(JRDataSource dataSource , String object) throws FileNotFoundException{
		
		try {
			
		System.out.println("------------------------------------File jasperFile1-----------------------------------------------");
		Map<String, Object> parameters = new HashMap<String, Object>();
		JasperPrint jasperPrint = JasperFillManager.fillReport(this.getClass().getResourceAsStream("/jasper/"+object+".jasper"), parameters, dataSource);
        /* Write content to PDF file */
		JRPdfExporter exporter = new JRPdfExporter();
		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		
		 File file = new File("pdf") ;
		 if (!file.exists()) {
				if (file.mkdirs()) {
					System.out.println("Multiple directories are created!");
				} else {
					System.out.println("Failed to create multiple directories!");
				}
			}

		
		exporter.setExporterOutput(
		  new SimpleOutputStreamExporterOutput("pdf/"+object+".pdf"));
		exporter.exportReport();
		 System.out.println("set file");


       

    
	} catch (JRException ex) {
        ex.printStackTrace();
    }
	
	 File file = new File("pdf/"+object+".pdf") ;
	 System.out.println("load file");

        
      return file ;
	}
	
	
	
	
	
	
	@Override
	public File jasperFile(JRDataSource dataSource , String object) throws FileNotFoundException{
		try {
		Map<String, Object> parameters = new HashMap<String, Object>();
		JasperPrint jasperPrint = JasperFillManager.fillReport(this.getClass().getResourceAsStream("/jasper/"+object+".jasper"), parameters, dataSource);

        /* Write content to PDF file */
		JRPdfExporter exporter = new JRPdfExporter();
		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		
		 File file = new File("pdf") ;
		 if (!file.exists()) {
				if (file.mkdirs()) {
					System.out.println("Multiple directories are created!");
				} else {
					System.out.println("Failed to create multiple directories!");
				}
			}

		
		exporter.setExporterOutput(
		  new SimpleOutputStreamExporterOutput("pdf/"+object+".pdf"));
		exporter.exportReport();
		 System.out.println("set file");


       

    
	} catch (JRException ex) {
        ex.printStackTrace();
    }
	
	 File file = new File("pdf/"+object+".pdf") ;
	 System.out.println("load file");

        
      return file ;
	}

	
	
			
		@Override
		public File jasperFileXlsx(JRDataSource dataSource , String object) throws FileNotFoundException{
			
			
			try {
				Map<String, Object> parameters = new HashMap<String, Object>();
				JasperPrint jasperPrint = JasperFillManager.fillReport(this.getClass().getResourceAsStream("/jasper/"+object+".jasper"), parameters, dataSource);
			
		        /* Write content to excel file */
				JRXlsxExporter exporter = new JRXlsxExporter();
				exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
				
//				SimpleXlsxReportConfiguration xlsReportConfiguration = new SimpleXlsxReportConfiguration();
//		        xlsReportConfiguration.setOnePagePerSheet(false);
//		        xlsReportConfiguration.setRemoveEmptySpaceBetweenRows(true);
//		        xlsReportConfiguration.setDetectCellType(false);
//		        xlsReportConfiguration.setWhitePageBackground(false);
//		        exporter.setConfiguration((XlsxReportConfiguration) xlsReportConfiguration);
				
				 File file = new File("xlsx") ;
				 if (!file.exists()) {
						if (file.mkdirs()) {
							System.out.println("Multiple directories are created!");
						} else {
							System.out.println("Failed to create multiple directories!");
						}
					}

				
				exporter.setExporterOutput(
				  new SimpleOutputStreamExporterOutput("xlsx/"+object+".xlsx"));
				exporter.exportReport();
				 System.out.println("set file");


		       

		    
			} catch (JRException ex) {
		        ex.printStackTrace();
		    }
			
			 File file = new File("xlsx/"+object+".xlsx") ;
			 System.out.println("load file");

			 
		      return file ;
			}
		@Override
		public File jasperFileWithDateParameter(JRDataSource dataSource , String object , Date start , Date end) throws FileNotFoundException{
			try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("start", start) ;
			parameters.put("end", end) ;
            JasperPrint jasperPrint = JasperFillManager.fillReport(this.getClass().getResourceAsStream("/jasper/"+object+".jasper"), parameters, dataSource);
			//JasperPrint jasperPrint = JasperFillManager.fillReport(this.getClass().getResource("/jasper/"+object+".jasper").getPath(), parameters, dataSource);
	        /* Write content to PDF file */
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(
			  new SimpleOutputStreamExporterOutput("pdf/"+object+".pdf"));
			exporter.exportReport();
			 System.out.println("set file");


	       

	    
		} catch (JRException ex) {
	        ex.printStackTrace();
	    }
		
		 File file = new File("pdf/"+object+".pdf") ;
		 System.out.println("load file");

	        
	      return file ;
		}
		
		
		@Override
		public File jasperFileExcelWithDateParameter(JRDataSource dataSource , String object , Date start , Date end) throws FileNotFoundException{
			try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("start", start) ;
			parameters.put("end", end) ;
            JasperPrint jasperPrint = JasperFillManager.fillReport(this.getClass().getResourceAsStream("/jasper/"+object+".jasper"), parameters, dataSource);
			//JasperPrint jasperPrint = JasperFillManager.fillReport(this.getClass().getResource("/jasper/"+object+".jasper").getPath(), parameters, dataSource);
	        /* Write content to PDF file */
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(
			  new SimpleOutputStreamExporterOutput("xlsx/"+object+".xlsx"));
			exporter.exportReport();
			 System.out.println("set file");


	       

	    
		} catch (JRException ex) {
	        ex.printStackTrace();
	    }
		
		 File file = new File("pdf/"+object+".pdf") ;
		 System.out.println("load file");

	        
	      return file ;
		}






	
}

