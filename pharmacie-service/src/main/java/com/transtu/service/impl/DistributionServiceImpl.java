 package com.transtu.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.dto.DistributionLineDto;
import com.transtu.core.dto.PrescriptionLineDto;
import com.transtu.core.jasper.AgentJasper;
import com.transtu.core.jasper.DistributionJasper;
import com.transtu.core.jasper.DistributionLineJasper;
import com.transtu.core.jasper.PresecriptionResponseJasper;
import com.transtu.core.jasper.ResponseRatioJasper;
import com.transtu.core.jasper.UserDistStatestiqueJasper;
import com.transtu.core.json.request.DistributionRequest;
import com.transtu.core.json.request.DistributionRequestPdf;
import com.transtu.core.json.request.DistrubtionResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.PrescriptionFilterRequest;
import com.transtu.core.json.request.PresecriptionRequestjasper;
import com.transtu.core.json.request.updateDistrictDistribution;
import com.transtu.core.json.request.updateDoctorPrescription;
import com.transtu.persistence.entities.Agent;
import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.DistributionLine;
import com.transtu.persistence.entities.District;
import com.transtu.persistence.entities.Doctor;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.LogPresecription;
import com.transtu.persistence.entities.Prescription;
import com.transtu.persistence.entities.PrescriptionLine;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.Stock;
import com.transtu.persistence.entities.StockMovement;
import com.transtu.persistence.entities.StockMovementLine;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.persistence.repositories.DayRepository;
import com.transtu.persistence.repositories.DistributionLineRepository;
import com.transtu.persistence.repositories.DistributionRepository;
import com.transtu.persistence.repositories.DistributionStateRepository;
import com.transtu.persistence.repositories.DistrictRepository;
import com.transtu.persistence.repositories.DoctorRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.LogPresecriptionRepository;
import com.transtu.persistence.repositories.MovementRepository;
import com.transtu.persistence.repositories.PrescriptionLineRepository;
import com.transtu.persistence.repositories.PrescriptionRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.ReferenceTypeRepository;
import com.transtu.persistence.repositories.RoleRepository;
import com.transtu.persistence.repositories.StockMovementLineRepository;
import com.transtu.persistence.repositories.StockMovementRepository;
import com.transtu.persistence.repositories.StockRepository;
import com.transtu.persistence.repositories.UtilisateurRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.AgentService;
import com.transtu.service.DistributionService;
import com.transtu.service.JasperService;
import com.transtu.service.PrescriptionService;
import com.transtu.service.SequenceService;
import com.transtu.service.UtilisateurService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class DistributionServiceImpl implements DistributionService{
	
	private static final Distribution Null = null;

	private static final Distribution NullableValues = null;
	
	double prix=0;

	@Autowired(required=true)
	DistrictRepository districtRepo ; 
	
	@Autowired(required=true)
	DayRepository dayRepo ;
	
	@Autowired(required=true)
	DoctorRepository doctorRepository;
	
	@Autowired(required=true)
	PrescriptionRepository prescriptionRepo;

	@Autowired(required=true)
	DistributionRepository distributionRepository ; 
	
	
	@Autowired(required=true)
	LogPresecriptionRepository  logPresecriptionRepository;
	
	@Autowired(required=true)
	DistributionLineRepository distLineRepo ; 
	
	@Autowired(required=true)
	LocationRepository locationRepo;
	
	@Autowired(required=true)
	SequenceService sequenceService;
	
	@Autowired(required=true)
	ProductRepository productRepo ; 
		@Autowired(required=true)
	MovementRepository movementRepo; 
	
	@Autowired(required=true)
	StockRepository stockRepo; 
	
	@Autowired(required=true)
	AgentService agentService;
	
	@Autowired(required=true)
	PrescriptionLineRepository prescriptionLineRepository;
	
	@Autowired(required=true)
	JasperService jasperService ;
	
	@Autowired(required=true)
	PrescriptionService prescriptionService ;
	
	@Autowired(required=true)
	DistributionLineRepository  distributionLineRepo;
	
	@Autowired(required=true)
	StockMovementRepository stockMovementRepository;
	@Autowired(required=true)
	StockMovementLineRepository stockMovementRepositoryLine;
	@Autowired(required=true)
	ReferenceTypeRepository referenceTypeRepository;
	@Autowired(required = true)
	UtilisateurRepository utilisateurRepository;
	@Autowired(required = true)
	UtilisateurService utilisateurService;
	@Autowired(required = true)
	DistributionStateRepository distributionStateRepo;
	@Autowired(required = true)
	StockMovementLineRepository  stockMovementLineRepo;
	@Autowired(required = true)
	RoleRepository  roleRepository;

	private double x;
	@Override
	public DistrubtionResponse addDistribution(DistributionRequest distributionRequest) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");
		
		List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
		if(user==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		Distribution distribution = new Distribution() ;
		distribution.setPrescription(this.prescriptionRepo.findOne(distributionRequest.getPrescriptionId()));
		
		if(distribution.getPrescription()==null){
			RestExceptionCode code = RestExceptionCode.PRESCRIPTION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		distribution.setDay(this.dayRepo.findOne(distributionRequest.getDayId()));
		
		if(distribution.getDay()==null){
			RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		if(distributionRequest.getDistrictId()!=null){
			distribution.setDistrict(this.districtRepo.findOne(distributionRequest.getDistrictId()));
			
			if(distribution.getDistrict()==null){
				RestExceptionCode code = RestExceptionCode.DISTRICT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;			
			}
		}
		
		distribution.setLocation(this.locationRepo.findOne(distributionRequest.getLocationId()));
		if(distribution.getLocation()==null){
			RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;	
		}
		
		distribution.setDistributionNumber(this.sequenceService.calculateNumDest(distribution.getPrescription().getAgentId()));
		distribution.setDateDist(new Date());
		distribution.setUtilisateur(user.get(0));
		//distribution.setDoctor(doctorRepository.findOne(distributionRequest.getDoctorId()));
		distribution=this.distributionRepository.save(distribution);
		List<DistributionLine> list = new ArrayList<>();
		List<DistributionLineDto> listdto = new ArrayList<>();
			
		
		 //-------------<>------stok-mvt-entete------<>------------------------- 
		 Location locationPharma = locationRepo.findOne(1l);
		 Location locationDep = locationRepo.findOne(2l);
		 String numMvt;
			if(stockMovementRepository.getTopId()==null){
			 numMvt=1+"-"+locationDep.getLib()+"-"+simpleDateFormat.format(new Date());
			
			}else {
				 numMvt=stockMovementRepository.getTopId()+1+"-"+locationDep.getLib()+"-"+simpleDateFormat.format(new Date());
			
			}
			
		    StockMovement stockMovement = new StockMovement();
			stockMovement.setDateMovement(new Date());
			stockMovement.setNumMovement(sequenceService.calculateNumMvtOrdSortie());	
			stockMovement.setTypeMvt("-1");
			stockMovement.setDay(distribution.getDay());
			stockMovement.setUtilisateur(distribution.getUtilisateur());
			stockMovement.setDescriminator("Ordinaire");
			stockMovement.setLocation(locationPharma);
			stockMovement.setReferenceType(referenceTypeRepository.findOne(1l));
			stockMovement.setDistribution(distribution);
			stockMovementRepository.save(stockMovement);
		  //---------------<>----------------------------<>-------------------	
		
		
		for(PrescriptionLineDto distributionLineDto : distributionRequest.getDistributionLines()){
			
			DistributionLine distributionLine = new DistributionLine();
			distributionLine.setDistribution(distribution);
			distributionLine.setProduct(this.productRepo.findOne(distributionLineDto.getProductId()));
			distributionLine.setPosologie(distributionLineDto.getPosologie());
			distributionLine.setNextDistributionDate(distributionLineDto.getNextDistributionDate());
			if(distributionLineDto.getProductId()==null){
				RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;	
			}
			
		//	Stock stock = this.stockRepo.findByProductAndLocation(distributionLine.getProduct(), distribution.getLocation());
			Stock stock = this.stockRepo.findByProductAndLocation(productRepo.findOne(distributionLineDto.getProductId()), locationRepo.findOne(1l));
//			if(stock==null){
//				RestExceptionCode code = RestExceptionCode.STOCK_NOT_FOUND;
//				RestException ex = new RestException(code.getError(), code);
//				throw ex;
//			}
			
			if(stock.getQuantity()<distributionLineDto.getWantedQt()){
				distributionLine.setDeliveredQt(stock.getQuantity());
				distributionLine.setMissingQt(distributionLineDto.getWantedQt()-stock.getQuantity());
				stock.setQuantity(0);
			}
			
			if(stock.getQuantity()>=distributionLineDto.getWantedQt()){
				stock.setQuantity(stock.getQuantity()-distributionLineDto.getWantedQt());
				distributionLine.setDeliveredQt(distributionLineDto.getWantedQt());
				distributionLine.setMissingQt(0);
				
			}
			this.distLineRepo.save(distributionLine);
			this.stockRepo.save(stock);
			list.add(distributionLine);
			
		
			//-------------<>------stok-mvt-ligne------<>------------------------- 			
			System.out.println("-------stok-mvt-ligne-----------------1----------");
			StockMovementLine stockMovementLine= new StockMovementLine();
			stockMovementLine.setStockMovement(stockMovement);
			
			//stockMovementLine.setMotif(motif);
			stockMovementLine.setDescription("Mouvement ordinaire-Sortie de produit Ordonnance : "+ distribution.getDistributionNumber());
			stockMovementLine.setStockMovement(stockMovement);
			stockMovementLine.setProduct(distributionLine.getProduct());
			System.out.println("-------stok-mvt-ligne-----------------1----------"+distributionLine.getDeliveredQt());
			stockMovementLine.setMovmentQte(distributionLine.getDeliveredQt());
			stockMovementRepositoryLine.save(stockMovementLine);
	 
          //---------------<>----------------------------<>-------------------
			
			
		}
		//ressssssssssssssssssssssssssssssssponse
//		DistrubtionResponse distrubtionResponse  = new DistrubtionResponse();
//		distrubtionResponse.setDistributionNumber(distribution.getDistributionNumber());
//		distrubtionResponse.setDateDist(distribution.getDateDist());
//		distrubtionResponse.setLocationlib(distribution.getLocation().getLib());
//		List<DistributionLineDto> listdistributionLineDtos = new ArrayList<>();
//		for(DistributionLine distributionLine1 : list){
//			DistributionLineDto distributionLineDtos  = new DistributionLineDto();
//			
//			distributionLineDtos.setDeliveredQt(distributionLine1.getDeliveredQt());
//			distributionLineDtos.setMissingQt(distributionLine1.getMissingQt());
//		//	distributionLineDtos.setToDistrubute(0);
//			distributionLineDtos.setWantedQt(0);
//			listdistributionLineDtos.add(distributionLineDtos);
//			
//		}
//		distrubtionResponse.setDistributionLineDtos(listdistributionLineDtos);
		//response-----------------------------------
		
		
		// methode to set distributed for completed prescription line
		Prescription prescription = prescriptionRepo.findOne(distributionRequest.getPrescriptionId());
					
		for(PrescriptionLine prescriptionLine:prescriptionLineRepository.findByPrescription(prescription)){
						//for(DistributionLine distributionLine:distributionLineRepo.findBydistribution_Prescription(prescription)){
							double d=sumQteByProdAndPres(prescription, prescriptionLine.getProduct());
							System.out.println("la somme est "+d);
							if( (prescriptionLine.getTotalQt()>d) ){
								
								prescriptionLine.setDistributed(false);
								System.out.println("----------->d-------------------"+prescriptionLine.getTotalQt());
							 
							}
							if((prescriptionLine.getTotalQt()<=d) ){
								prescriptionLine.setDistributed(true);
								System.out.println("----------<d----------------------"+prescriptionLine.getTotalQt());
								
							}
							prescriptionLineRepository.save(prescriptionLine);
							prescription.setDistributed(etatPrescription(prescription));
							System.out.println("------"+etatPrescription(prescription));
							prescriptionRepo.save(prescription);
							
						//}
					}
		List<DistributionLineDto> listdistributionLineDtos = new ArrayList<>();
	     
		for(DistributionLine distributionLine : distributionLineRepo.findByDistribution(distribution)){
		
			DistributionLineDto distributionLineDtos  = new DistributionLineDto();
		    Product product = distributionLine.getProduct();
			distributionLineDtos.setDeliveredQt(distributionLine.getDeliveredQt());
			System.out.println("-------------*-*-*-*-*-*--*------------distributionLineDto.missing----------------------------"+distributionLine.getMissingQt());
			distributionLineDtos.setMissingQt(distributionLine.getMissingQt());
			distributionLineDtos.setPeriodic(prescriptionLineRepository.findByPrescriptionAndProduct(prescription, product).getIsPeriodic());
			distributionLineDtos.setToDistribute(distributionLine.getToDistribute());
			distributionLineDtos.setWantedQt(prescriptionLineRepository.findByPrescriptionAndProduct(prescription, product).getTotalQt());
			distributionLineDtos.setProductId(distributionLine.getProduct().getId());
			distributionLineDtos.setPosologie(distributionLine.getPosologie());
			distributionLineDtos.setConsumesQte(this.sumQteByProdAndPres(prescription, product));
			distributionLineDtos.setNextDistributionDate(distributionLine.getNextDistributionDate());
			distributionLineDtos.setDistNumber(prescriptionLineRepository.findByPrescriptionAndProduct(prescription, product).getDistNumber());
			//distributionLineDtos.setPeriodicity(prescriptionLineRepo.findByPrescriptionAndProduct(prescription, product).getPeriodicity());
			listdistributionLineDtos.add(distributionLineDtos);
		
		}
		
		prescription.setDistributed(etatPrescription(prescription));
		prescriptionRepo.save(prescription);
		
		DistrubtionResponse distrubtionResponse  = new DistrubtionResponse();
		distrubtionResponse.setDistributionNumber(distribution.getDistributionNumber());
		distrubtionResponse.setAgentId(prescription.getAgentId());
		distrubtionResponse.setDateDist(distribution.getDateDist());
		distrubtionResponse.setLocationlib(distribution.getLocation().getLib());
		distrubtionResponse.setDistributionLineDtos(listdistributionLineDtos);
		return distrubtionResponse;
	}

	
	
	
	
	@Override
	public List<Distribution> listDistributionByAgent(String AgentID) {
			
		return distributionRepository.findByPrescription_AgentId(AgentID);
		}
	
	
	
	public double sumQteByProdAndPres(Prescription prescription, Product product ){
		double somme = 0;
		for (DistributionLine distributionLine : distributionLineRepo.findBydistribution_Prescription(prescription))
				
			{
			if(distributionLine.getProduct().equals(product))
			somme = somme + distributionLine.getDeliveredQt()+distributionLine.getMissingQt();
		}
		return somme;
	}
	
	public double sumQteByProdAndPrescreptionRes(Prescription prescription, Product product ){
		double somme = 0;
		for (DistributionLine distributionLine : distributionLineRepo.
			findByDistributionPrescriptionAndProduct(prescription, product)){
			somme = somme + distributionLine.getDeliveredQt()+distributionLine.getMissingQt();
			
		}
		return somme;
	}
	
	public Boolean etatPrescription(Prescription prescription ){
		boolean etat = true ;
		for (PrescriptionLine prescriptionLine : prescriptionLineRepository.findByPrescription(prescription))
		{
				
			if(prescriptionLine.getDistributed()==false){
				etat =false;
			}
		}
		return etat;
	
	
	}


		@Override
		public File getPdfDistributionByAgent(DistributionRequestPdf date ) throws FileNotFoundException {
			return this.getPdf(this.getDistributionsByAgentFromPdf(date), date);
		}
		
		
		private File getPdf(List<DistributionJasper> list,DistributionRequestPdf date) throws FileNotFoundException{
			JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
			return jasperService.jasperFileWithDateParameter(dataSource, "distributionByAgent",date.getStart(), date.getEnd());
		}
		
//		private File getPdf(List<DistributionJasper> list , DateRequestPdf date) throws FileNotFoundException{
//			JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
//			System.out.println("-----------------distributionByAgentx-------************************************------");
//			return jasperService.jasperFileWithDateParameter(dataSource, "distributionByAgent", date.getStart(), date.getEnd());
//		}


		//******************************distributionglobaleByAgent**************************
		
		private File getPdf1(List<DistributionJasper> list , DistributionRequestPdf date) throws FileNotFoundException{
			JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
			System.out.println("-----------------distributionglobaleByAgent--------------------");
			return jasperService.jasperFileWithDateParameter(dataSource, "distributionglobaleByAgent", date.getStart(), date.getEnd());
		}

		@Override
		public File getPdfDistributionByAgents(DistributionRequestPdf date ) throws FileNotFoundException {
			
			return this.getPdf1(this.getDistributionsByAgentSFromPdf(date), date);
		}
		
      //*********************************----------------------*********************************


		@Override
		public List<Distribution> findByDateDistBetween(Date start, Date end) {
			return this.distributionRepository.findByDateDistBetween(start, end);
		}






		
		
		@Override
		public Distribution findByDistrubutionNum(Long referenceId, String distrubutionNum) {
			
			if(referenceId==1) {
				
				Distribution distribution=  distributionRepository.findByDistributionNumber(distrubutionNum);
				if(distribution!=null) {
				return distribution;
				}
				else {	 		
			    return new Distribution();
				}
				
				
			}else{
				return new Distribution();
			
			}}
			
		
		
		





		@Override
		public List<DistributionJasper> getDistributionsFromPdf(DistributionRequestPdf date) {
			List<Distribution> distributions = this.findByDateDistBetween(date.getStart(), date.getEnd());
			List<DistributionJasper> distributionsJasper = new ArrayList<>();
			
			distributions.forEach(d->{
				DistributionJasper distJasper = new DistributionJasper() ;
				distJasper.setDate(d.getDateDist());
				
				d.getDistributionLines().forEach(dLine->{
					DistributionLineJasper distributionLineJasper = new DistributionLineJasper();
					distributionLineJasper.setDesignation(dLine.getProduct().getLib());
					distributionLineJasper.setBarCode(dLine.getProduct().getBarCode());
					distributionLineJasper.setMissingQt(dLine.getMissingQt());
					distributionLineJasper.setWantedQt(dLine.getToDistribute());
					
				
					distJasper.getDistributionLines().add(distributionLineJasper);
				});
				AgentJasper agentJasper = new AgentJasper();
				Agent agent = new Agent();
				try {
					 agent = this.agentService.getDetailAgent(this.prescriptionRepo.findByDistributionIn(d).getAgentId());
					 
				} catch (IOException e) {
					e.printStackTrace();
				}
				agentJasper.setName(agent.getFullName());
				agentJasper.setAgentId(agent.getMatricule());
				distJasper.setAgent(agentJasper);
				
				distributionsJasper.add(distJasper);
			});
			return distributionsJasper;
		}





		@Override
		public List<DistributionJasper> getDistributionsByAgentFromPdf(DistributionRequestPdf date) {
			
			double a =0d;
			System.out.println("--------------Distribution by Agent..........");
			if(date.getAgentId()==null || date.getAgentId()=="" ) {
			PrescriptionFilterRequest prescFilter = new PrescriptionFilterRequest();
			prescFilter.setDateMin(date.getStart());
			prescFilter.setDateMax(date.getEnd());
			//prescFilter.setAgentId(date.getAgentId());
			prescFilter.setProduits(new ArrayList<>());
			List<DistributionJasper> distributionsJasper  = new ArrayList<>() ; 
			this.prescriptionService.presecriptionHistoryFilter(prescFilter).forEach(p->{
				p.getDistribution().forEach(d->{
					DistributionJasper distJasper = new DistributionJasper() ;
					distJasper.setDate(d.getDateDist());
					System.out.println("------num dis ----------------....--"+d.getDistributionNumber());
					
					//------
					AgentJasper agentJasper = new AgentJasper();
					Agent agent = new Agent();
//					System.out.println("/*------------8------****-----");
					try {
						 agent = this.agentService.getDetailAgent(d.getPrescription().getAgentId());
//						 
					} catch (IOException e) {
						e.printStackTrace();
					}
					System.out.println("/*------------8------****-----");
				
					distJasper.setNumberDist(null);
					
					System.out.println("--------------size----------");
					agentJasper.setName(agent.getFullName());
					agentJasper.setAgentId(agent.getMatricule());
					distJasper.setAgent(agentJasper);
					distJasper.setDistNum(d.getDistributionNumber());
					distJasper.setMontant(this.getMontantTotalDistAgent(d));
					distributionsJasper.add(distJasper);
					//-----
					
					d.getDistributionLines().forEach(dLine->{
						DistributionLineJasper distributionLineJasper = new DistributionLineJasper();
						distributionLineJasper.setDesignation(dLine.getProduct().getLib());
						distributionLineJasper.setBarCode(dLine.getProduct().getBarCode());
						distributionLineJasper.setMissingQt(dLine.getMissingQt());
						distributionLineJasper.setWantedQt(dLine.getToDistribute());
						
						///**
						distributionLineJasper.setPrice(dLine.getProduct().getPrice());
						distributionLineJasper.setLivredQt(dLine.getDeliveredQt());
						distributionLineJasper.setVatRate(dLine.getProduct().getVatRate());
						//distributionLineJasper.setDistNum(dLine.getDistribution().getDistributionNumber());
						distJasper.getDistributionLines().add(distributionLineJasper);
						
					});
	   System.out.println("--------------Distribution by Agent..xx........");
//					AgentJasper agentJasper = new AgentJasper();
//					
//					Agent agent = new Agent();
//					
//					try {
//						 agent = this.agentService.getDetailAgent(date.getAgentId());
//						 
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				
//				
//					distJasper.setNumberDist(distributionRepository.findByPrescriptionAgentIdAndDateDistBetween(date.getAgentId(),date.getStart(), date.getEnd()).size());
//					agentJasper.setName(agent.getFullName());
//					agentJasper.setAgentId(agent.getMatricule());
//					distJasper.setAgent(agentJasper);
//					distributionsJasper.add(distJasper);
				});
			});
			return distributionsJasper;
			
			}else {
				
				
				PrescriptionFilterRequest prescFilter = new PrescriptionFilterRequest();
				prescFilter.setDateMin(date.getStart());
				prescFilter.setDateMax(date.getEnd());
				prescFilter.setAgentId(date.getAgentId());
				prescFilter.setProduits(new ArrayList<>());
				List<DistributionJasper> distributionsJasper  = new ArrayList<>() ; 
				this.prescriptionService.presecriptionHistoryFilter(prescFilter).forEach(p->{
					p.getDistribution().forEach(d->{
						DistributionJasper distJasper = new DistributionJasper() ;
						distJasper.setDate(d.getDateDist());
						System.out.println("------num dis -- by agent---------------"+d.getDistributionNumber());
						
						//------
//						AgentJasper agentJasper = new AgentJasper();
//						Agent agent = new Agent();
////						System.out.println("/*------------8------****-----");
//						try {
//							 agent = this.agentService.getDetailAgent(d.getPrescription().getAgentId());
////							 
//						} catch (IOException e) {
//							e.printStackTrace();
//						}
//						System.out.println("/*------------8------****-----");
//					
//						distJasper.setNumberDist(distributionRepository.findByPrescriptionAgentIdAndDateDistBetween(date.getAgentId(),date.getStart(), date.getEnd()).size());
//						
//						System.out.println("--------------size----------");
//						agentJasper.setName(agent.getFullName());
//						agentJasper.setAgentId(agent.getMatricule());
//						distJasper.setAgent(agentJasper);
//						distJasper.setDistNum(d.getDistributionNumber());
//						distJasper.setMontant(this.getMontantTotalDistAgent(d));
//						distributionsJasper.add(distJasper);
						//-----
						
						d.getDistributionLines().forEach(dLine->{
							DistributionLineJasper distributionLineJasper = new DistributionLineJasper();
							distributionLineJasper.setDesignation(dLine.getProduct().getLib());
							distributionLineJasper.setBarCode(dLine.getProduct().getBarCode());
							distributionLineJasper.setMissingQt(dLine.getMissingQt());
							distributionLineJasper.setWantedQt(dLine.getToDistribute());
							
							///**
							distributionLineJasper.setPrice(dLine.getProduct().getPrice());
							distributionLineJasper.setLivredQt(dLine.getDeliveredQt());
							distributionLineJasper.setVatRate(dLine.getProduct().getVatRate());
							//distributionLineJasper.setDistNum(dLine.getDistribution().getDistributionNumber());
							distJasper.getDistributionLines().add(distributionLineJasper);
							
							 x= x + ((dLine.getDeliveredQt()+dLine.getMissingQt())*dLine.getProduct().getPrice())+
										(dLine.getDeliveredQt()+dLine.getMissingQt())*(dLine.getProduct().getVatRate()/100);
							System.out.println("-----//-----.................................................prix"
									+ "-----------"+((dLine.getDeliveredQt()+dLine.getMissingQt())*dLine.getProduct().getPrice())+
									(dLine.getDeliveredQt()+dLine.getMissingQt())*(dLine.getProduct().getVatRate()/100));
							
							
							System.out.println("-----ddd....................."+x);
							
						
							
						});
		
						AgentJasper agentJasper = new AgentJasper();
						
						Agent agent = new Agent();
						
//						try {
//							 agent = this.agentService.getDetailAgent(date.getAgentId());
//							 
//						} catch (IOException e) {
//							e.printStackTrace();
//						}
					
					
						distJasper.setNumberDist(distributionRepository.findByPrescriptionAgentIdAndDateDistBetween(date.getAgentId(),date.getStart(), date.getEnd()).size());
						agentJasper.setName(agent.getFullName());
						agentJasper.setAgentId(agent.getMatricule());
						distJasper.setAgent(agentJasper);
						distJasper.setDistNum(d.getDistributionNumber());
						distJasper.setMontant(x);
						System.out.println("-----------------sssssssssssssssss -------"
								+ "-----------"+x);
						
						distributionsJasper.add(distJasper);
					});
				});
				return distributionsJasper;
			}
		}
		
		
		
		
		//------------------------------liste jasper detaille----------------------------------------//
		@Override
		public List<DistributionJasper> getDistributionsByAgentSFromPdf(DistributionRequestPdf date) {
			
			System.out.println("-----------------agent id ------------------"+date.getAgentId());
			
			if(date.getAgentId()==null || date.getAgentId()=="" ) {
			PrescriptionFilterRequest prescFilter = new PrescriptionFilterRequest();
			prescFilter.setDateMin(date.getStart());
			prescFilter.setDateMax(date.getEnd());
			//prescFilter.setAgentId(date.getAgentId());
			prescFilter.setProduits(new ArrayList<>());
			List<DistributionJasper> distributionsJasper  = new ArrayList<>() ; 
			this.prescriptionService.presecriptionHistoryFilter(prescFilter).forEach(p->{
				p.getDistribution().forEach(d->{
					DistributionJasper distJasper = new DistributionJasper() ;
					distJasper.setDate(d.getDateDist());
					System.out.println("------num dis ------------------"+d.getDistributionNumber());
					
					
					AgentJasper agentJasper = new AgentJasper();
					Agent agent = new Agent();
					System.out.println("/*------------8----bbbbbbb--****-----");
					//try {
						System.out.println("/*------------1------****-----");
						 //agent = this.agentService.getDetailAgent(d.getPrescription().getAgentId());
//					    
						 System.out.println("/*------------2------****-----");
					//} catch (IOException e) {
					//	e.printStackTrace();
					//}
					System.out.println("/*------------8------****-----");
				
					distJasper.setNumberDist(null);
					
					System.out.println("--------------size----------");
//					agentJasper.setName(agent.getFullName());
//					agentJasper.setAgentId(agent.getMatricule());
					agentJasper.setName("name");
					agentJasper.setAgentId(222);
					distJasper.setAgent(agentJasper);
					distJasper.setDistNum(d.getDistributionNumber());
					distJasper.setMontant(this.getMontantTotalDistAgent(d));
					distributionsJasper.add(distJasper);
					//-----
					
					d.getDistributionLines().forEach(dLine->{
						System.out.println("/*------------85-----****-----");
						DistributionLineJasper distributionLineJasper = new DistributionLineJasper();
						distributionLineJasper.setDesignation(dLine.getProduct().getLib());
						distributionLineJasper.setBarCode(dLine.getProduct().getBarCode());
						distributionLineJasper.setMissingQt(dLine.getMissingQt());
						distributionLineJasper.setWantedQt(dLine.getToDistribute());
						System.out.println("/*------------14------****-----");
						///**
						distributionLineJasper.setPrice(dLine.getProduct().getPrice());
						distributionLineJasper.setLivredQt(dLine.getDeliveredQt());
						distributionLineJasper.setVatRate(dLine.getProduct().getVatRate());
						//distributionLineJasper.setDistNum(dLine.getDistribution().getDistributionNumber());
						distJasper.getDistributionLines().add(distributionLineJasper);
						System.out.println("/*-----------55-1------****-----");
					});
	
//					AgentJasper agentJasper = new AgentJasper();
//					
//					Agent agent = new Agent();
//					
//					try {
//						 agent = this.agentService.getDetailAgent(date.getAgentId());
//						 
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				
//				
//					distJasper.setNumberDist(distributionRepository.findByPrescriptionAgentIdAndDateDistBetween(date.getAgentId(),date.getStart(), date.getEnd()).size());
//					agentJasper.setName(agent.getFullName());
//					agentJasper.setAgentId(agent.getMatricule());
//					distJasper.setAgent(agentJasper);
//					distributionsJasper.add(distJasper);
				});
			});
			return distributionsJasper;
			
			}else {
				
				System.out.println("matricule agent nooooooooooooooooooo null");
				
				PrescriptionFilterRequest prescFilter = new PrescriptionFilterRequest();
				prescFilter.setDateMin(date.getStart());
				prescFilter.setDateMax(date.getEnd());
				prescFilter.setAgentId(date.getAgentId());
				prescFilter.setProduits(new ArrayList<>());
				List<DistributionJasper> distributionsJasper  = new ArrayList<>() ; 
				this.prescriptionService.presecriptionHistoryFilter(prescFilter).forEach(p->{
					p.getDistribution().forEach(d->{
						DistributionJasper distJasper = new DistributionJasper() ;
						distJasper.setDate(d.getDateDist());
						System.out.println("------num dis ------------------"+d.getDistributionNumber());
						
						//------
//						AgentJasper agentJasper = new AgentJasper();
//						Agent agent = new Agent();
////						System.out.println("/*------------8------****-----");
//						try {
//							 agent = this.agentService.getDetailAgent(d.getPrescription().getAgentId());
////							 
//						} catch (IOException e) {
//							e.printStackTrace();
//						}
//						System.out.println("/*------------8------****-----");
//					
//						distJasper.setNumberDist(distributionRepository.findByPrescriptionAgentIdAndDateDistBetween(date.getAgentId(),date.getStart(), date.getEnd()).size());
//						
//						System.out.println("--------------size----------");
//						agentJasper.setName(agent.getFullName());
//						agentJasper.setAgentId(agent.getMatricule());
//						distJasper.setAgent(agentJasper);
//						distJasper.setDistNum(d.getDistributionNumber());
//						distJasper.setMontant(this.getMontantTotalDistAgent(d));
//						distributionsJasper.add(distJasper);
						//-----
						
						d.getDistributionLines().forEach(dLine->{
							DistributionLineJasper distributionLineJasper = new DistributionLineJasper();
							distributionLineJasper.setDesignation(dLine.getProduct().getLib());
							distributionLineJasper.setBarCode(dLine.getProduct().getBarCode());
							distributionLineJasper.setMissingQt(dLine.getMissingQt());
							distributionLineJasper.setWantedQt(dLine.getToDistribute());
							
							///**
							distributionLineJasper.setPrice(dLine.getProduct().getPrice());
							distributionLineJasper.setLivredQt(dLine.getDeliveredQt());
							distributionLineJasper.setVatRate(dLine.getProduct().getVatRate());
							//distributionLineJasper.setDistNum(dLine.getDistribution().getDistributionNumber());
							distJasper.getDistributionLines().add(distributionLineJasper);
							
						});
		
						AgentJasper agentJasper = new AgentJasper();
						
						Agent agent = new Agent();
						
						try {
							 agent = this.agentService.getDetailAgent(date.getAgentId());
							 
						} catch (IOException e) {
							e.printStackTrace();
						}
					
					
						distJasper.setNumberDist(distributionRepository.findByPrescriptionAgentIdAndDateDistBetween(date.getAgentId(),date.getStart(), date.getEnd()).size());
						agentJasper.setName(agent.getFullName());
						agentJasper.setAgentId(agent.getMatricule());
						distJasper.setAgent(agentJasper);
						distJasper.setDistNum(d.getDistributionNumber());
						distJasper.setMontant(this.getMontantTotalDistAgent(d));
						distributionsJasper.add(distJasper);
					});
				});
				return distributionsJasper;
			}
		}
		////////////////////////////////////////////////////
		@Override
		public Double getMontantTotalDistAgent(Distribution d) {
		 double montant =0d;
			 for(DistributionLine distributionLine : distributionLineRepo.findByDistribution(d) ) {
				double qte = distributionLine.getToDistribute();
			    double price= distributionLine.getProduct().getPrice();
			    double prixttc = price+(price*distributionLine.getProduct().getVatRate()/100);
				double montantTTC = qte*prixttc;
				montant= montant+montantTTC;
				System.out.println("------montant---------"+montant);
			 }
			
			
			return montant;
		}





		@Override
		public File getPdfDistributions(DistributionRequestPdf date) throws FileNotFoundException {
			// TODO Auto-generated method stub
			return null;
		}



		@Override
		public MessageResponse updateDistrictDistribution(updateDistrictDistribution  updateDistrictDistribution){
			List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
			Distribution distribution = this.distributionRepository.findOne(updateDistrictDistribution.getDitributionId());
			if(distribution==null){
				RestExceptionCode code = RestExceptionCode.LINE_DISTRIBUTION_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			District OldDistrict = distribution.getDistrict();
			District district = districtRepo.findOne(updateDistrictDistribution.getDitrictId());
			if(district==null){
				RestExceptionCode code = RestExceptionCode.DISTRICT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			distribution.setDistrict(district);
			distributionRepository.save(distribution);
			
			
			LogPresecription logPresecription = new LogPresecription();
			logPresecription.setDateUpdate(new Date());
			logPresecription.setAdresseIp(utilisateurService.getCurrentAdresseIp());
			logPresecription.setDesignation("Le District Num: "+OldDistrict.getLib()+"de L'Ordonnance N°: "+distribution.getDistributionNumber()+"a été modifier par Le District Num:"
			+district.getLib()+"a la date:"+new Date()+"");
			logPresecription.setUserName(user.get(0).getUsername());
			logPresecriptionRepository.save(logPresecription);
		
			  return new MessageResponse(HttpStatus.OK.toString(), null, "La distribution a été modifier avec success",distribution.getDistributionNumber());
		}


		
		
		@Override
		public MessageResponse updateDoctorPresrcrption(updateDoctorPrescription  updateDoctorPrescript){
			List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
			Prescription prescription = this.prescriptionRepo.findOne(updateDoctorPrescript.getPrescriptionId());
			if(prescription==null){
				RestExceptionCode code = RestExceptionCode.LINE_DISTRIBUTION_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			//Doctor OldDoctor = prescription.getDoctor();
			Doctor doctor = doctorRepository.findOne(updateDoctorPrescript.getDoctorId());
			if(doctor==null){
				RestExceptionCode code = RestExceptionCode.DISTRICT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			prescription.setDoctor(doctor);;
			prescriptionRepo.save(prescription);
			
			
			LogPresecription logPresecription = new LogPresecription();
			logPresecription.setDateUpdate(new Date());
			logPresecription.setAdresseIp(utilisateurService.getCurrentAdresseIp());
			logPresecription.setDesignation("de L'Ordonnance N°: "+prescription.getPrescriptionNumber()+"a été modifier par Le District Num:"
			+doctor.getName()+"a la date:"+new Date()+"");
			logPresecription.setUserName(user.get(0).getUsername());
			logPresecriptionRepository.save(logPresecription);
		
			  return new MessageResponse(HttpStatus.OK.toString(), null, "La distribution a été modifier avec success",prescription.getPrescriptionNumber());
		}





		@Override
		public MessageResponse deleteDistribution(Long id)  {
			
			Distribution distribution = distributionRepository.findOne(id);
			
			
			List<DistributionLine> distributionLines =  distributionLineRepo.findByDistribution(distribution);
			distributionStateRepo.delete(distribution.getDistributionStates());
			StockMovement stockMovement = stockMovementRepository.findByDistribution(distribution);
			
			if (stockMovement != null) {	
			 for(StockMovementLine movementLine : stockMovement.getStockMovementLine()) {
				 if(movementLine!= null) {
				 stockMovementLineRepo.delete(movementLine);
				 }	
			 }
			 stockMovementRepository.delete(stockMovement);
			}
			
			for(DistributionLine distributionLine :  distributionLines) {
				
//_______________________________________________log_______________________________________________
				
				List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
				if(user==null){
					RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
					RestException ex = new RestException(code.getError(), code);
					throw ex;
				}
				
				LogPresecription logPresecription = new LogPresecription();
				logPresecription.setDateUpdate(new Date());
				logPresecription.setAdresseIp(utilisateurService.getCurrentAdresseIp());
				logPresecription.setDesignation("La distribution Num :  "+distributionLine.getDistribution().getDistributionNumber()
						+"DistributionId : "  +distributionLine.getDistribution().getId()
						+"Presecription Num : "  +distribution.getPrescription().getPrescriptionNumber()
						+"Agent Num: "  +distribution.getPrescription().getAgentId()
						+"ProduitId :"  +distributionLine.getProduct().getId()
						+"Quantité_manquante :" +distributionLine.getMissingQt()
						+"la distributionLine a été supprimer :"
						+"a la date:" +new Date()+"");
				logPresecription.setUserName(user.get(0).getUsername());
				logPresecriptionRepository.save(logPresecription);
				
//_______________________________________________log_______________________________________________
				distributionLineRepo.delete(distributionLine);
			
			}
			
			distributionRepository.delete(distribution);			
			 return new MessageResponse(HttpStatus.OK.toString(), null, "La distribution a été supprimé avec success",null);
		}


		@Override
		public MessageResponse deleteDistributionLine(Long id)  {
	
//_______________________________________________log_______________________________________________
			DistributionLine distributionLine = distributionLineRepo.findOne(id);
			List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
			if(user==null){
				RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			
			LogPresecription logPresecription = new LogPresecription();
			logPresecription.setDateUpdate(new Date());
			logPresecription.setAdresseIp(utilisateurService.getCurrentAdresseIp());
			logPresecription.setDesignation("La distribution Num: "+distributionLine.getDistribution().getDistributionNumber()
			+"DistributionId :" +distributionLine.getDistribution().getId()
			+"ProduitId :" +distributionLine.getProduct().getId()
			+"Quantité_manquante :" +distributionLine.getMissingQt()
			+"la distributionLine a été supprimer :"
			+"a la date :" +new Date()+"");
			logPresecription.setUserName(user.get(0).getUsername());
			logPresecriptionRepository.save(logPresecription);
//_______________________________________________log_______________________________________________
			
			distributionLineRepo.delete(distributionLine);
			
			
			 return new MessageResponse(HttpStatus.OK.toString(), null, "La distributionLigne a été supprimé avec success",null);
		}
	

	
		
		@Override
		public List<UserDistStatestiqueJasper> findByDateDistAndUtilisateur(Date start , Date end) {
			List<UserDistStatestiqueJasper> distStatestiqueJaspers = new ArrayList<>();
			ResponseRatioJasper responseRatioJasperDist=this.getMontantTotalDist(start, end);
			ResponseRatioJasper responseRatioJasperMissing = this.getMontantTotalMissing(start, end);
			
			for(Utilisateur user: utilisateurRepository.findAll()) {
				if(roleRepository.findByUser(user)==roleRepository.findOne(1l)||roleRepository.findByUser(user)==roleRepository.findOne(2l)) {
				UserDistStatestiqueJasper  distStatestiqueJasper = new UserDistStatestiqueJasper();
				distStatestiqueJasper.setName(user.getFirstname()+"."+user.getLastname());
				distStatestiqueJasper.setNbreDist((double) distributionRepository.findByDateDistBetweenAndUtilisateur(start, end, user).size());
				distStatestiqueJasper.setTotalnbreDist((double) distributionRepository.findByDateDistBetween(start, end).size());
				distStatestiqueJasper.setStart(start);
				distStatestiqueJasper.setEnd(end);
				distStatestiqueJasper.setTotalDist(responseRatioJasperDist.getMontant());
				distStatestiqueJasper.setNbredelivred(responseRatioJasperDist.getNbreProduit());
				distStatestiqueJasper.setTotalMissing(responseRatioJasperMissing.getMontant());
				distStatestiqueJasper.setNbreMissing(responseRatioJasperMissing.getNbreProduit());
				distStatestiqueJaspers.add(distStatestiqueJasper);
				}
				
				
			}
			 return distStatestiqueJaspers;
		}
		
		
		
		private File getPdf(List<UserDistStatestiqueJasper> list) throws FileNotFoundException{
			JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
			System.out.println("-----------------distributionStat--------------------");
			return jasperService.jasperFile(dataSource, "distributionStat");
		}

		@Override
		public File getPdfPdfDistStatestique(Date start , Date end ) throws FileNotFoundException {
			System.out.println("-------------------------1-----------------------");
			return this.getPdf(this.findByDateDistAndUtilisateur(start, end));
		}

		
		
		public ResponseRatioJasper getMontantTotalDist(Date start, Date end) {
			 double montantdist =0d;
			 double n=0;
				 for(DistributionLine distributionLine : distLineRepo.findByDistributionDateDistBetween(start, end) ) {
					double qte = distributionLine.getDeliveredQt();
				    double price= distributionLine.getProduct().getPrice();
				    double prixttc = price+(price*distributionLine.getProduct().getVatRate()/100);
					double montantTTC = qte*prixttc;
					montantdist= montantdist+montantTTC;
					n=qte+n;
					//System.out.println("-----montantdist---------"+montantdist);
				 }
				 ResponseRatioJasper  responseRatioJasper  = new ResponseRatioJasper();
				 responseRatioJasper.setMontant(montantdist);
				 responseRatioJasper.setNbreProduit(n);
				  return responseRatioJasper;
			}
		
		public ResponseRatioJasper getMontantTotalMissing(Date start, Date end) {
			 double montantmissing =0d;
			 double n=0;
				 for(DistributionLine distributionLine : distLineRepo.findByDistributionDateDistBetween(start, end) ) {
					 if (distributionLine.getMissingQt()>0) {
					double qte = distributionLine.getMissingQt();
				    double price= distributionLine.getProduct().getPrice();
				    double prixttc = price+(price*distributionLine.getProduct().getVatRate()/100);
					double montantTTC = qte*prixttc;
					montantmissing= montantmissing+montantTTC;
					n=qte+n;
					
					 }
					}
				 ResponseRatioJasper  responseRatioJasper  = new ResponseRatioJasper();
				 responseRatioJasper.setMontant(montantmissing);
				 responseRatioJasper.setNbreProduit(n);
				 return responseRatioJasper;
				
					}
		
		}
		


