package com.transtu.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.PharmaClass;
import com.transtu.persistence.entities.Presentation;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.repositories.PharmaClassRepository;
import com.transtu.persistence.repositories.PresentationRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.PresentationService;


@Service
public class PresentationServiceImp implements PresentationService{
	
	
	private final static Logger logger = LoggerFactory.getLogger(PresentationServiceImp.class);

	@Autowired(required = true)
	private PresentationRepository presentationRepo;
	@Autowired(required = true)
	ProductRepository productRepository;
	@Override
	
	public MessageResponse addpresentation(String label) {
		logger.info("___________Add presentaion_______________");
		Presentation presentation = presentationRepo.findByLabel(label);
		System.out.println("------3");
		if(presentation != null ){
			RestExceptionCode code = RestExceptionCode.PRESENTATION_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		Presentation presentations = new Presentation(label) ;
		this.presentationRepo.save(presentations);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la presentation est ajouté avec succés",label);
	}

	
	@Override
	public List<Presentation> getListPresentation() {
		logger.info("___________get list presentation_______________");
		return this.presentationRepo.findAll(new Sort(Direction.ASC, "label"));
	}
	
	public MessageResponse updatePresentation(Long id, String label) {
		Presentation presentation = presentationRepo.findByLabel(label);
		if(presentation != null ){
			RestExceptionCode code = RestExceptionCode.PRESENTATION_EXISTED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Presentation presentations = this.presentationRepo.findOne(id);
		if(presentations == null ){
			RestExceptionCode code = RestExceptionCode.PRESENTATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		presentations.setLabel(label);
		this.presentationRepo.save(presentations);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Presentation est modifié avec succés",label);
		
	}
	
	public MessageResponse deletePresentation(Long id) {
		Presentation presentation = presentationRepo.findOne(id);
		if(presentation == null ){
			RestExceptionCode code = RestExceptionCode.PRESENTATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		List<Product> product =productRepository.findByPresentation(presentation);
		if(product.size()>0 ){
			RestExceptionCode code = RestExceptionCode.PRESENTATION_ALREADY_USED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		
		this.presentationRepo.delete(presentation);
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Presentation est supprimé avec succés",presentation.getLabel());
		
	}



}
