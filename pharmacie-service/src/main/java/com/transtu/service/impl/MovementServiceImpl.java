package com.transtu.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.transtu.core.jasper.MovementInJasper;
import com.transtu.core.jasper.MovementJasper;
import com.transtu.core.jasper.MovementOutJasper;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.persistence.entities.Movement;
import com.transtu.persistence.repositories.MovementRepository;
import com.transtu.persistence.repositories.PrescriptionRepository;
import com.transtu.service.AgentService;
import com.transtu.service.JasperService;
import com.transtu.service.MovementService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class MovementServiceImpl implements MovementService {
	
	@Autowired(required = true)
	MovementRepository movementRepo ; 
	
	@Autowired(required = true)
	JasperService jasperService ; 
	
	@Autowired(required = true)
	AgentService agentService ;
	
	@Autowired(required = true)
	PrescriptionRepository prescriptionRepo;
	
 

	@Override
	public File getPdfReportAllMovements() throws FileNotFoundException {
		return this.getPdf(this.movementRepo.findAll());
	}
	
	private File getPdf(List<Movement> list) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(list);
		return jasperService.jasperFile(dataSource, "movement");
	}

	@Override
	public File getPdfReportMovementsBetween(DateRequestPdf date) throws FileNotFoundException {
		
		return this.getPdfMovements(this.getMovementsBetween(date), date);
	}
	
	
	private void setMovementJasper(Movement movement , MovementJasper movementJasper) throws JsonParseException, JsonMappingException, IOException{
		if(movement.getType().equals("0")){ // sortie
			MovementOutJasper movOut = new MovementOutJasper();
			movOut.setDate(movement.getDate());
			movOut.setDeliveredQt(movement.getQuantity());
			movOut.setAgent(this.agentService.getDetailAgent((this.prescriptionRepo.findByDistributionIn(movement.getDistribution()).getAgentId())).getFullName());
			movementJasper.getMovementsOut().add(movOut);
		}else{
			MovementInJasper movIn= new MovementInJasper();
			movIn.setReceptionDate(movement.getDate());
			movIn.setQtReceived(movement.getQuantity());
			movementJasper.getMovementsIn().add(movIn);
		}
	}
	
	public int getIndiceProduct(List<MovementJasper> movementsJasper , String productCode){
		for(MovementJasper movJasper : movementsJasper){
			if(movJasper.getBarCode().equals(productCode)){
				return movementsJasper.indexOf(movJasper);
			}
		}

		return -1 ; 
	}
	
	private File getPdfMovements(List<MovementJasper> list , DateRequestPdf date) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
		return jasperService.jasperFileWithDateParameter(dataSource, "movementByProduct", date.getStart(), date.getEnd());
	}
	
	private void adjustMovementJasper(List<MovementJasper> movementsJasper){
		movementsJasper.forEach(movementJasper->{
			if(movementJasper.getMovementsIn().size()>movementJasper.getMovementsOut().size()){
				while(movementJasper.getMovementsIn().size()!=movementJasper.getMovementsOut().size()){
					movementJasper.getMovementsOut().add(new MovementOutJasper());
				}
			}else if(movementJasper.getMovementsIn().size()<movementJasper.getMovementsOut().size()){
				while(movementJasper.getMovementsIn().size()!=movementJasper.getMovementsOut().size()){
					movementJasper.getMovementsIn().add(new MovementInJasper());
				
				}
			}
		});
	}
	
	public void calculTotal(List<MovementJasper> movements){
		movements.forEach(mov->{
			mov.getMovementsIn().forEach(movIn->{
				if(movIn.getQtReceived()!=null){
					mov.setTotalIn(mov.getTotalIn()+movIn.getQtReceived());
				}
			});
			mov.getMovementsOut().forEach(movOut->{
				if(movOut.getDeliveredQt()!=null){
					mov.setTotalOut(mov.getTotalOut()+movOut.getDeliveredQt());
				}
			});
		});
	}

	@Override
	public List<MovementJasper> getMovementsBetween(DateRequestPdf date) {
		
		
		List<Movement> movements = this.movementRepo.findByDateBetween(date.getStart(), date.getEnd());
		List<MovementJasper> movementsJasper = new ArrayList<>();
		
		movements.forEach(movement->{
			if(movementsJasper
					.stream()
					.anyMatch(movementJasper->
					movementJasper.getBarCode().equals(movement.getProduct().getBarCode()))
			){
					MovementJasper movJasper =  movementsJasper.get(this.getIndiceProduct(movementsJasper, movement.getProduct().getBarCode())) ;
					try {
						this.setMovementJasper(movement, movJasper);
					} catch (IOException e) {
						e.printStackTrace();
					}

				
			}else{
				MovementJasper movJasper = new MovementJasper() ; 
				movJasper.setBarCode(movement.getProduct().getBarCode());
				movJasper.setCodePct(movement.getProduct().getCodePctProd());
				movJasper.setDesignation(movement.getProduct().getLib());
					try {
						this.setMovementJasper(movement, movJasper);
					} catch (IOException e) {
						e.printStackTrace();
					}
					movementsJasper.add(movJasper);
	
			}
		});
		
		
		
		this.adjustMovementJasper(movementsJasper);
		this.calculTotal(movementsJasper);
		
		return movementsJasper;
	}
	
	

}
