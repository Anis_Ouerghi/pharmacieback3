package com.transtu.service.impl;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.transtu.core.dto.DistributionLineDto;
import com.transtu.core.dto.ordonnanceNbre;
import com.transtu.core.jasper.PresecriptionResponseJasper;
import com.transtu.core.jasper.StockJasper;
import com.transtu.core.jasper.StockLotJasper;
import com.transtu.core.json.request.ConsultationRequest;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.core.json.request.DistrubtionResponse;
import com.transtu.core.json.request.FilterStockReport;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.PrescriptionFilterRequest;
import com.transtu.core.json.request.PrescriptionRequest;
import com.transtu.core.json.request.PresecriptionRequestSituationjasper;
import com.transtu.core.json.request.PresecriptionRequestjasper;
import com.transtu.core.json.request.updateAgentPresecription;
import com.transtu.core.presecriptionDisplay.AgentD;
import com.transtu.core.presecriptionDisplay.DistributionD;
import com.transtu.core.presecriptionDisplay.DistributionLineD;
import com.transtu.core.presecriptionDisplay.FiltrePresecriptionResponse;
import com.transtu.core.presecriptionDisplay.PrescriptionD;
import com.transtu.core.presecriptionDisplay.PrescriptionLineD;
//import com.transtu.persistence.entities.Agent;
import com.transtu.persistence.entities.Day;
import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.DistributionLine;
import com.transtu.persistence.entities.DistributionState;
import com.transtu.persistence.entities.District;
import com.transtu.persistence.entities.Doctor;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.LogPresecription;
import com.transtu.persistence.entities.Prescription;
import com.transtu.persistence.entities.PrescriptionLine;
import com.transtu.persistence.entities.PresecriptionRepJasper;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.QDistribution;
import com.transtu.persistence.entities.QPrescription;
import com.transtu.persistence.entities.QPrescriptionLine;
import com.transtu.persistence.entities.State;
import com.transtu.persistence.entities.Stock;
import com.transtu.persistence.entities.StockMovement;
import com.transtu.persistence.entities.StockMovementLine;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.persistence.repositories.DayRepository;
import com.transtu.persistence.repositories.DistributionLineRepository;
import com.transtu.persistence.repositories.DistributionRepository;
import com.transtu.persistence.repositories.DistributionStateRepository;
import com.transtu.persistence.repositories.DistrictRepository;
import com.transtu.persistence.repositories.DoctorRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.LogPresecriptionRepository;
import com.transtu.persistence.repositories.MovementRepository;
import com.transtu.persistence.repositories.PrescriptionLineRepository;
import com.transtu.persistence.repositories.PrescriptionRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.ReferenceTypeRepository;
import com.transtu.persistence.repositories.SequenceMvtOrdSortRepository;
import com.transtu.persistence.repositories.StateRepository;
import com.transtu.persistence.repositories.StockMovementLineRepository;
import com.transtu.persistence.repositories.StockMovementRepository;
import com.transtu.persistence.repositories.StockRepository;
import com.transtu.persistence.repositories.UtilisateurRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.AgentService;
import com.transtu.service.DistributionService;
import com.transtu.service.JasperService;
//import com.transtu.service.AgentService;
import com.transtu.service.PrescriptionService;
import com.transtu.service.SequenceService;
import com.transtu.service.UtilisateurService;
import com.transtu.service.utils.ConvertService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
@Service
public class PrescriptionServiceImpl implements PrescriptionService{


	private final static Logger logger = LoggerFactory.getLogger(PrescriptionServiceImpl.class);
		
	@PersistenceContext
	EntityManager em ;
	
	@Autowired(required=true)
	ProductRepository productRepo;

	@Autowired(required=true)
	StockRepository stockRepo;

	@Autowired(required=true)
	PrescriptionLineRepository prescriptionLineRepo ;

	@Autowired(required=true)
	DistributionRepository distributionRepo;

	@Autowired(required=true)
	MovementRepository movementRepo ;


	@Autowired(required=true)
	DistributionLineRepository distributionLineRepo ;

	@Autowired(required=true)
	DayRepository dayRepo ; 
	

	@Autowired(required=true)
	LogPresecriptionRepository logPresecriptionRepository ; 


	@Autowired(required=true)
	LocationRepository locationRepo ; 

	@Autowired(required=true)
	DistrictRepository districtRepo ; 
	@Autowired(required=true)
	SequenceService sequenceService ;
	@Autowired(required=true)
	SequenceMvtOrdSortRepository sequenceMvtOrdSortRepository;
	@Autowired(required=true)
    ConvertService convertService;
	
	@Autowired(required=true)
	StateRepository stateRepository;
	
	@Autowired(required=true)
	DistributionStateRepository distributionStateRepository;
	
	@Autowired(required=true)
	DoctorRepository doctorRepository;
	
	@Autowired(required=true)
	AgentService agentService;
	ConsultationRequest consultationRequest; 
	
	@Autowired(required=true)
	StockMovementRepository stockMovementRepository;
	@Autowired(required=true)
	StockMovementLineRepository stockMovementRepositoryLine;
	@Autowired(required=true)
	ReferenceTypeRepository referenceTypeRepository;
	
	@Autowired(required = true)
	UtilisateurRepository utilisateurRepository;
	

	@Autowired(required = true)
	UtilisateurService utilisateurService;
	@Autowired(required = true)
	DistributionService distributionService;
	
	@Autowired(required = true)
	PrescriptionRepository prescriptionRepo;
	
	@Autowired(required = true)
	JasperService jasperService;
	
	
	
	
	
	@SuppressWarnings("unused")
	@Transactional
	@Override
	public DistrubtionResponse addPrescription(PrescriptionRequest prescriptionRequest){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");

		List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
		if(user==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		//verify existing location & district
		Location location = this.locationRepo.findOne(prescriptionRequest.getLocationId());
		Distribution distribution = new Distribution();
		Day day = this.dayRepo.findOne(prescriptionRequest.getDayId());
		if(location==null){
			RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		if(day==null){
			RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
	
		if(prescriptionRequest.getDistrictId()!=null){
			District district = this.districtRepo.findOne(prescriptionRequest.getDistrictId());
			if(district==null){
				RestExceptionCode code = RestExceptionCode.DISTRICT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			distribution.setDistrict(district);
			
			
		}
		if((prescriptionRequest.getType().equals("ordinaire"))&&(prescriptionRequest.getDistributionLinesDto().stream().anyMatch(line->line.isPeriodic()==true))){
			RestExceptionCode code = RestExceptionCode.INCONSISTENT_DATA_STOCK;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		if((prescriptionRequest.getType().equals("partiel"))&&(!prescriptionRequest.getDistributionLinesDto().stream().anyMatch(line->line.isPeriodic()==true))){
			RestExceptionCode code = RestExceptionCode.INCONSISTENT_DATA_STOCK;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}

		//add prescription
		Prescription prescription = new Prescription();
		prescription.setPrescriptionNumber(sequenceService.calculatePrescription(prescriptionRequest.getAgentId()));
		prescription.setPrescriptionDate(new Date());
		if(prescriptionRequest.getDoctorId()!=null &&  prescriptionRequest.getDoctorId()!=0 ) {
		prescription.setDoctor(doctorRepository.findOne(prescriptionRequest.getDoctorId()));
		}else {prescription.setDoctor(doctorRepository.findOne(1l));}
		prescription.setAgentId(prescriptionRequest.getAgentId());
		prescription.setAgentSituation(prescriptionRequest.getAgentSituation());
		prescription.setType(prescriptionRequest.getType());
		//save prescription
		prescription=prescriptionRepo.save(prescription);
		
		
		//add distribution
		distribution.setLocation(location);
		distribution.setDay(day);
	    distribution.setUtilisateur(user.get(0));
		distribution.setDateDist(new Date());
		distribution.setPrescription(prescription);
	    distribution.setDistributionNumber(sequenceService.calculateNumDest(prescriptionRequest.getAgentId()));
		// save distribution
		
		distribution=distributionRepo.save(distribution);
		
		//add distribution State
				State state = stateRepository.findOne(1l);
				DistributionState distributionState = new DistributionState();
				distributionState.setState(state);
				distributionState.setDistribution(distribution);
				distributionState.setDateState(new Date());
				List<DistributionState> listDistributionState = new ArrayList<>();
				listDistributionState.add(distributionState);
				distributionStateRepository.save(distributionState);
				

				 //-------------<>------stok-mvt-entete------<>------------------------- 
				 Location locationPharma = locationRepo.findOne(1l);
				 Location locationDep = locationRepo.findOne(2l);
				 String numMvt;
					if(stockMovementRepository.getTopId()==null){
					 numMvt=1+"-"+locationDep.getLib()+"-"+simpleDateFormat.format(new Date());
					
					}else {
						 numMvt=stockMovementRepository.getTopId()+1+"-"+locationDep.getLib()+"-"+simpleDateFormat.format(new Date());
					
					}
					
				    StockMovement stockMovement = new StockMovement();
					stockMovement.setDateMovement(new Date());
					stockMovement.setNumMovement(sequenceService.calculateNumMvtOrdSortie());	
					stockMovement.setTypeMvt("-1");
					stockMovement.setDay(day);
					stockMovement.setUtilisateur(distribution.getUtilisateur());
					stockMovement.setDescriminator("Ordinaire");
					stockMovement.setLocation(locationPharma);
					stockMovement.setReferenceType(referenceTypeRepository.findOne(1l));
					stockMovement.setDistribution(distribution);
					stockMovementRepository.save(stockMovement);
				  //---------------<>----------------------------<>-------------------	
			
		List<DistributionLine> list = new ArrayList<>();
		if(!prescriptionRequest.getDistributionLinesDto().isEmpty()){
		
			for(DistributionLineDto linePresc : prescriptionRequest.getDistributionLinesDto()){
				Product produit = this.productRepo.findOne(linePresc.getProductId());
				if(produit==null){
					RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
					RestException ex = new RestException(code.getError(), code);
					throw ex;
				}
			
				Stock stock = stockRepo.findByProductAndLocation(produit, location);
			
				if(stock == null){
					RestExceptionCode code = RestExceptionCode.STOCK_NOT_FOUND	;
					RestException ex = new RestException(code.getError(), code);
					throw ex;
				}
				double qStock = stock.getQuantity();
				double deliveredQt = linePresc.getDeliveredQt();//qte livre a calculer
				double missingQt = linePresc.getMissingQt();//qte manquant a calculer 
				PrescriptionLine prescriptionLine = new PrescriptionLine();
				prescriptionLine.setPrescription(prescription);
				prescriptionLine.setProduct(produit);
				//------modif
				prescriptionLine.setPosologie(linePresc.getPosologie());
			
				prescriptionLine.setTotalQt(linePresc.getWantedQt());//**qte demandÃƒÂ©
				prescriptionLine.setDistNumber(linePresc.getDistNumber());
				prescriptionLine.setIsPeriodic(linePresc.isPeriodic());
				if(!linePresc.isPeriodic()){prescriptionLine.setIsPeriodic(true);} else{
					prescriptionLine.setIsPeriodic(false);}
				//save PrescriptionLine
				prescriptionLineRepo.save(prescriptionLine);
		
				if(!linePresc.isPeriodic()){
					
					//if {(((missingQt > 0 && qStock == deliveredQt) || (missingQt == 0 && qStock >= deliveredQt)&& deliveredQt !=0)&& (linePresc.getWantedQt() ==deliveredQt +missingQt)){
					
					
						DistributionLine distributionLine = new DistributionLine();
						distributionLine.setNextDistributionDate(linePresc.getNextDistributionDate());
						if(stock.getQuantity()>=linePresc.getWantedQt()){
						distributionLine.setMissingQt(0);
						distributionLine.setDeliveredQt(linePresc.getWantedQt());
						if (prescriptionLine.getTotalQt()< distributionLine.getToDistribute()){}
						distributionLine.setDistribution(distribution);
						distributionLine.setProduct(produit);
						//--------------Modif
						distributionLine.setPosologie(linePresc.getPosologie());
						stock.setQuantity(stock.getQuantity()-linePresc.getWantedQt());
						}else{
						distributionLine.setMissingQt(linePresc.getWantedQt()-stock.getQuantity());
						distributionLine.setDeliveredQt(stock.getQuantity());
						distributionLine.setDistribution(distribution);
						distributionLine.setProduct(produit);
						//--------------Modif
						distributionLine.setPosologie(linePresc.getPosologie());
						stock.setQuantity(0);	
						}
					
						// save DistributionLine
						distributionLineRepo.save(distributionLine);
						
//						Movement mvt = new Movement();
//						mvt.setDate(new Date());
//						mvt.setLocation(location);
//						mvt.setProduct(produit);
//						mvt.setQuantity(distributionLine.getDeliveredQt());
//						mvt.setType("-1");// -1: Sortie 1:EntrÃƒÂ©e
//						mvt.setDescription("Sortie de produit Ordonnance : "+ distribution.getDistributionNumber());
//						//save Movement
//						movementRepo.save(mvt);
						
						//-------------<>------stok-mvt-ligne------<>------------------------- 			
					
						StockMovementLine stockMovementLine= new StockMovementLine();
						stockMovementLine.setStockMovement(stockMovement);
						
						//stockMovementLine.setMotif(motif);
						stockMovementLine.setDescription("Mouvement ordinaire-Sortie de produit Ordonnance : "+ distribution.getDistributionNumber());
						stockMovementLine.setStockMovement(stockMovement);
						stockMovementLine.setProduct(produit);
						stockMovementLine.setMovmentQte(distributionLine.getDeliveredQt());
						stockMovementRepositoryLine.save(stockMovementLine);
				 
			          //---------------<>----------------------------<>-------------------
					
						//save Stock  pharmacy
						stockRepo.save(stock);

//					}else{
//		
//						RestExceptionCode code = RestExceptionCode.INCONSISTENT_DATA_STOCK;
//						RestException ex = new RestException(code.getError(), code);
//						throw ex;
//					
//					}


				}else{
					
					
					//if{(((missingQt > 0 && qStock == deliveredQt) || (missingQt == 0 && qStock >= deliveredQt))&& (linePresc.getWantedQt() > deliveredQt +missingQt)){ // verify && deliveredQt !=0)
						//consultationRequest =convertService.calculateQte(linePresc.getProductId(), 
								//linePresc.getWantedQt(), prescriptionRequest.getLocationId());
					
//						if(1 > 0){
							
							
							DistributionLine distributionLine = new DistributionLine();
							distributionLine.setNextDistributionDate(linePresc.getNextDistributionDate());
							if(stock.getQuantity()>=linePresc.getToDistribute()){
							distributionLine.setMissingQt(0);
							distributionLine.setDeliveredQt(linePresc.getToDistribute());
							distributionLine.setToDistribute(linePresc.getToDistribute());
							distributionLine.setDistribution(distribution);
							distributionLine.setProduct(produit);
							//--------------Modif
							distributionLine.setPosologie(linePresc.getPosologie());
							stock.setQuantity(stock.getQuantity()-linePresc.getToDistribute());
							
							}else{
								distributionLine.setNextDistributionDate(linePresc.getNextDistributionDate());
								distributionLine.setMissingQt(linePresc.getToDistribute()-stock.getQuantity());
								distributionLine.setDeliveredQt(stock.getQuantity());
								distributionLine.setToDistribute(linePresc.getToDistribute());
								distributionLine.setDistribution(distribution);
								distributionLine.setProduct(produit);
								//--------------Modif
								distributionLine.setPosologie(linePresc.getPosologie());
								stock.setQuantity(0);
								
								
							}
							// save DistributionLine
							distributionLineRepo.save(distributionLine);
//							Movement mvt = new Movement();
//							mvt.setDate(new Date());
//							mvt.setLocation(location);
//							mvt.setProduct(produit);
//							mvt.setQuantity(distributionLine.getDeliveredQt());
//							mvt.setType("-1");// -1: Exit 1:Entry
//							mvt.setDescription("Sortie de produit Ordonnance : "+ distribution.getDistributionNumber());
//							//save Movement
//							movementRepo.save(mvt);
							
							//-------------<>------stok-mvt-ligne------<>------------------------- 			
				
							StockMovementLine stockMovementLine= new StockMovementLine();
							stockMovementLine.setStockMovement(stockMovement);
							
							//stockMovementLine.setMotif(motif);
							stockMovementLine.setDescription("Mouvement ordinaire-Sortie de produit Ordonnance : "+ distribution.getDistributionNumber());
							stockMovementLine.setStockMovement(stockMovement);
							stockMovementLine.setProduct(produit);
							stockMovementLine.setMovmentQte(distributionLine.getDeliveredQt());
							stockMovementRepositoryLine.save(stockMovementLine);
					 
				          //---------------<>----------------------------<>-------------------
							//save Stock  pharmacy
							stockRepo.save(stock);
						
						
							list.add(distributionLine);
						}

						
				
				
//					}else{
//					
//						RestExceptionCode code = RestExceptionCode.INCONSISTENT_DATA_STOCK;
//						RestException ex = new RestException(code.getError(), code);
//						throw ex;
//					}
					
				//}
   
			}
			
			List<DistributionLineDto> listdistributionLineDtos = new ArrayList<>();
	     System.out.println("le cle primaire est "+distribution.getId());
			for(DistributionLine distributionLineDto : distributionLineRepo.findByDistribution(distribution)){
				
			    Product product = distributionLineDto.getProduct();
				DistributionLineDto distributionLineDtos  = new DistributionLineDto();
				distributionLineDtos.setDeliveredQt(distributionLineDto.getDeliveredQt());
				distributionLineDtos.setMissingQt(distributionLineDto.getMissingQt());
				distributionLineDtos.setPeriodic(prescriptionLineRepo.findByPrescriptionAndProduct(prescription, product).getIsPeriodic());
				distributionLineDtos.setToDistribute(distributionLineDto.getToDistribute());
				distributionLineDtos.setWantedQt(prescriptionLineRepo.findByPrescriptionAndProduct(prescription, product).getTotalQt());
				distributionLineDtos.setProductId(distributionLineDto.getProduct().getId());
				distributionLineDtos.setPosologie(distributionLineDto.getPosologie());
				distributionLineDtos.setConsumesQte(this.sumQteByProdAndPrescreptionRes(prescription, product));
				distributionLineDtos.setDistNumber(prescriptionLineRepo.findByPrescriptionAndProduct(prescription, product).getDistNumber());
				//distributionLineDtos.setPeriodicity(prescriptionLineRepo.findByPrescriptionAndProduct(prescription, product).getPeriodicity());
				listdistributionLineDtos.add(distributionLineDtos);
			
			}
			
			
		
			DistrubtionResponse distrubtionResponse  = new DistrubtionResponse();
			distrubtionResponse.setDistributionNumber(prescription.getPrescriptionNumber());
			distrubtionResponse.setAgentId(prescription.getAgentId());
			distrubtionResponse.setDateDist(prescription.getPrescriptionDate());
			distrubtionResponse.setLocationlib(distribution.getLocation().getLib());
			distrubtionResponse.setDistributionLineDtos(listdistributionLineDtos);
			
			
			
			// methode to set distributed for completed prescription line
			
			for(PrescriptionLine prescriptionLine:prescriptionLineRepo.findByPrescription(prescription)){
				for(DistributionLine distributionLine:distributionLineRepo.findBydistribution_Prescription(prescription)){
					double d=sumQteByProdAndPres(distribution, distributionLine.getProduct());
					if(prescriptionLine.getProduct().
							equals(distributionLine.getProduct())&& (prescriptionLine.getTotalQt()>d
									) ){
						prescriptionLine.setDistributed(false);
					
					}
					if(prescriptionLine.getProduct().
							equals(distributionLine.getProduct())&& (prescriptionLine.getTotalQt()<=d
									) ){
						prescriptionLine.setDistributed(true);
						
					}
				    }
			}
			
			prescription.setDistributed(etatPrescription(prescription));
			prescriptionRepo.save(prescription);
			return distrubtionResponse;
			
			
			
		}
		else{
			
			RestExceptionCode code = RestExceptionCode.LINE_PRECRIPTION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
	}
	
	
	@Override
	public MessageResponse CancelDistrubtion(Long  id){
		
		 Distribution distribution = distributionRepo.findOne(id);
		 State state = stateRepository.findByType("annullÃ©");
		 DistributionState distributionState = new DistributionState();
		 distributionState.setDateState(new Date());
		 distributionState.setDistribution(distribution);
		 distributionState.setState(state);
		 distributionStateRepository.save(distributionState);
		 
		
		 for(DistributionLine distributionLine : distributionLineRepo.findByDistribution(distribution))	{
		     distributionLine.setDeliveredQt(0); 
		     distributionLine.setMissingQt(0);
		     distributionLineRepo.save(distributionLine); 
			 Stock stock = stockRepo.findByProductAndLocation(distributionLine.getProduct(), distribution.getLocation());
			 stock.setQuantity(stock.getQuantity()+distributionLine.getDeliveredQt());
			 
//			 Movement mvt = new Movement();
//				mvt.setDate(new Date());
//				mvt.setLocation(distribution.getLocation());
//				mvt.setProduct(distributionLine.getProduct());
//				mvt.setQuantity(distributionLine.getDeliveredQt());
//				mvt.setType("1");// -1: Exit 1:Entry
//				mvt.setDescription("Retour de produit Ordonnance : "+ distribution.getDistributionNumber());
//				//save Movement
//				movementRepo.save(mvt);
		 }	 
		 
       return new MessageResponse(HttpStatus.OK.toString(), null, "La distribution a ÃƒÂ©tÃƒÂ©  supprimÃƒÂ© avec success",distribution.getDistributionNumber());
		
		
	}
	
	
	
	
	@Override
	public FiltrePresecriptionResponse prescriptionFilter(PrescriptionFilterRequest p)
			throws JsonParseException, JsonMappingException, IOException{

		JPQLQuery<?> query = new JPAQuery(em); 
		
		//QDistribution distribution = QDistribution.distribution;
		QPrescription presc = QPrescription.prescription;
		QPrescriptionLine prescriptionLine = QPrescriptionLine.prescriptionLine;

		
		query.from(prescriptionLine).orderBy(presc.prescriptionDate.desc())
		//.orderBy(presc.id.desc())
				.leftJoin( prescriptionLine.prescription,presc);
				//.leftJoin(distribution.prescription, presc);
		 if(p.getDateMin()!=null ){
			Calendar c = Calendar.getInstance();
			c.setTime(p.getDateMin());
			//c.add(Calendar.DATE, -1);
			c.add(Calendar.DATE, 0);
			Date newDatemin = c.getTime(); 
			System.out.println(" date min"+newDatemin);
			query.where(presc.prescriptionDate.after(newDatemin));
			//or(presc.prescriptionDate.eq(newDatemin)));
			}
          if(p.getDateMax()!=null){
			Calendar c = Calendar.getInstance();
			c.setTime(p.getDateMax());
			c.add(Calendar.DATE, 1);
			Date newDatemax = c.getTime();
			query.where(presc.prescriptionDate.before(newDatemax));
		
			}
          if(p.getProduits().size()>0 && p.getProduits().get(0)!=0){
			query.where(prescriptionLine.product.id.in(p.getProduits()));
			}
          if(p.getAgentId()!=""  && p.getAgentId()!=null){
			query.where(presc.agentId.eq(p.getAgentId()));
			}			
			
//		List<Prescription> list =  query.select(presc).fetch();
//		Set<Prescription> set = new HashSet<>();
//		set.addAll(list);
//		return set;
		query.distinct();	
		if(p.getPage()!=null){
			query.offset((p.getPage()-1)* p.getLinesNbr());
			query.limit(p.getLinesNbr());
		}
		
		List<Prescription> list =  query.select(presc).fetch();
		Set<Prescription> set = new LinkedHashSet<>();
		set.addAll(list);
        System.out.println("taille  liste "+set.size());
		List<PrescriptionD> listDisplay = new ArrayList<>();
		FiltrePresecriptionResponse filtrePresecriptionResponse = new FiltrePresecriptionResponse();
		List<PrescriptionLineD> listDisplayline = new ArrayList<>();
		List<DistributionD> listDistributionDisplay = new ArrayList<>();
		List<DistributionLineD> listDistributionLineDisplay = new ArrayList<>();
		PrescriptionD prescriptionDisplay1;
		
		for (Prescription prescription : set){
			
			    PrescriptionD prescriptionDisplay = new PrescriptionD();
				prescriptionDisplay.setId(prescription.getId());
				prescriptionDisplay.setPrescriptionDate(prescription.getPrescriptionDate());
			    AgentD agentdDisplay = new AgentD();
				agentdDisplay.setNom(agentService.getDetailAgent(prescription.getAgentId()).getNom());
				agentdDisplay.setPrenom(agentService.getDetailAgent(prescription.getAgentId()).getPrenom());
				agentdDisplay.setMatricule(agentService.getDetailAgent(prescription.getAgentId()).getMatricule());
				prescriptionDisplay.setAgent(agentdDisplay);
				prescriptionDisplay.setPrescriptionNumber(prescription.getPrescriptionNumber());
				prescriptionDisplay.setType(prescription.getType());
				prescriptionDisplay.setDistributed(prescription.getDistributed());
				
				

				
				prescriptionDisplay.setDistribution(prescription.getDistribution());
				listDisplay.add(prescriptionDisplay);

				filtrePresecriptionResponse.setPrescriptionDs(listDisplay);
				if(p.getPage()!=null){
					if((query.select(presc).fetchCount())%p.getLinesNbr()==0) {
				     filtrePresecriptionResponse.setPage(p.getPage());
				     filtrePresecriptionResponse.setTotalPages((query.select(presc).fetchCount())/p.getLinesNbr());
				     filtrePresecriptionResponse.setTotalElements((query.select(presc).fetchCount()));
					}else {	
				      filtrePresecriptionResponse.setPage(p.getPage());
				      filtrePresecriptionResponse.setTotalPages(((query.select(presc).fetchCount())/p.getLinesNbr())+1);	
				      filtrePresecriptionResponse.setTotalElements((query.select(presc).fetchCount()));
					}
				}
				
		}
	
		return filtrePresecriptionResponse ;
	
	
	}

	

	@Override
	public List<Prescription> getPresecriptionByAgent(String agentId, Long id) {
		
		return this.prescriptionRepo.findByAgentIdAndId(agentId, id);
	}
	
	
	@Override
	public List<PrescriptionLine> getAllPrescriptionLines() {
		
		return this.prescriptionLineRepo.findAll();
	}
	
		

	public double sumQteByProdAndPres(Distribution distribution, Product product ){
		double somme = 0;
		for (DistributionLine distributionLine : distributionLineRepo.
				findByDistributionAndProduct(distribution, product)){
			somme = somme + distributionLine.getDeliveredQt()+distributionLine.getMissingQt();
		}
		return somme;
	}
	
	
	public double sumQteByProdAndPrescreptionRes(Prescription prescription, Product product ){
		double somme = 0;
		for (DistributionLine distributionLine : distributionLineRepo.
				findByDistributionPrescriptionAndProduct(prescription, product)){
			somme = somme + distributionLine.getDeliveredQt()+distributionLine.getMissingQt();
			
		}
		return somme;
	}
	
	public Boolean etatPrescription(Prescription prescription ){
		boolean etat = true ;
		for (PrescriptionLine prescriptionLine : prescriptionLineRepo.findByPrescription(prescription))
		{
				
			if(prescriptionLine.getDistributed()==false){
				etat =false;
			}
		}
		return etat;
		
	}
	
	
	@Override
	public Set<Prescription> presecriptionHistoryFilter(PrescriptionFilterRequest p){
		
		JPQLQuery<?> query = new JPAQuery(em); 
		QDistribution distribution = QDistribution.distribution;
		QPrescription presc = QPrescription.prescription;
		QPrescriptionLine prescriptionLine = QPrescriptionLine.prescriptionLine;
		
		query.from(prescriptionLine).orderBy(presc.prescriptionDate.desc())
				.leftJoin( prescriptionLine.prescription,presc);
				//.leftJoin(distribution.prescription, presc);
//		query.from(prescriptionLine , distribution)
//		        .leftJoin( prescriptionLine.prescription,presc)
//		        .leftJoin(distribution.prescription, presc);
		
		if(p.getDateMin()!=null){
			query.where(presc.prescriptionDate.after(p.getDateMin()));
		}
		if(p.getDateMax()!=null){
			query.where(presc.prescriptionDate.before(p.getDateMax()));
		}
		if(p.getProduits().size()>0 && p.getProduits()!=null ){
			query.where(prescriptionLine.product.id.in(p.getProduits()));
		}
		
		if(p.getAgentId()!=null && p.getAgentId()!="" ){
			query.where(presc.agentId.eq(p.getAgentId()));
		}			
		query.distinct();
		//query.orderBy(presc.prescriptionDate.desc());
		if(p.getPage()!=null){
			query.offset((p.getPage()-1)*10);
			query.limit(10);
		}
	List<Prescription> list =  query.select(presc).fetch();
	Set<Prescription> set = new LinkedHashSet<>();
	for (Prescription prescription: list){
		
		set.add(prescription);
		
	}

	
	//set.addAll(list);
	return  set;
	}
	
	//******************************************************************test jasperrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
	@Override
	public Set<Prescription> presecriptionHistoryFiltertest(PrescriptionFilterRequest p){
		
		JPQLQuery<?> query = new JPAQuery(em); 
		
		QDistribution distribution = QDistribution.distribution;
		QPrescription presc = QPrescription.prescription;
		QPrescriptionLine prescriptionLine = QPrescriptionLine.prescriptionLine;
		
		query.from(prescriptionLine).orderBy(presc.prescriptionDate.desc()).groupBy(presc.agentId,presc.id)
				.leftJoin( prescriptionLine.prescription,presc);
				//.leftJoin(distribution.prescription, presc);
//		query.from(prescriptionLine , distribution)
//		        .leftJoin( prescriptionLine.prescription,presc)
//		        .leftJoin(distribution.prescription, presc);
		
		if(p.getDateMin()!=null){
			query.where(presc.prescriptionDate.after(p.getDateMin()));
		}
		if(p.getDateMax()!=null){
			query.where(presc.prescriptionDate.before(p.getDateMax()));
		}
		if(p.getProduits().size()>0 && p.getProduits()!=null ){
			query.where(prescriptionLine.product.id.in(p.getProduits()));
		}
		
		if(p.getAgentId()!=null && p.getAgentId()!="" ){
			query.where(presc.agentId.eq(p.getAgentId()));
		}			
		//query.distinct();
		//query.orderBy(presc.prescriptionDate.desc());
		if(p.getPage()!=null){
			query.offset((p.getPage()-1)*10);
			query.limit(10);
		}
	List<Prescription> list =  query.select(presc).fetch();
	Set<Prescription> set = new LinkedHashSet<>();
	for (Prescription prescription: list){
		System.out.println("-------------"+prescription.getId());
		set.add(prescription);
		
	}
	for (Prescription prescription: set){
		System.out.println("--------set----"+prescription.getId());
		
	}
	
	//set.addAll(list);
	return  set;
	}	
	
	//*********************************************************************************************************************************
	
	
	@Override
	public void changeStatedistributed(Long id, Boolean distributed){
		
		Prescription prescription = this.prescriptionRepo.findOne(id);
		
		if(prescription==null){
			RestExceptionCode code = RestExceptionCode.PRESCRIPTION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		if (distributed == true){
		prescription.setDistributed(true);
		}else{
		prescription.setDistributed(false);
		}
		prescriptionRepo.save(prescription);
		
		
	}


	@Override
	public MessageResponse deletePresecription(Long id)  {
	
		Prescription prescription = prescriptionRepo.findOne(id);
		for (Distribution distribution : distributionRepo.findByPrescription(prescription)) {
		distributionService.deleteDistribution(distribution.getId());
		}
		
	    for (PrescriptionLine prescriptionLine :  prescription.getPrescriptionLine() ) {

	    	
			prescriptionLineRepo.delete(prescriptionLine);
		}
		
		
		//_______________________________________________log_______________________________________________
		
		List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
		if(user==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		LogPresecription logPresecription = new LogPresecription();
		logPresecription.setDateUpdate(new Date());
		logPresecription.setAdresseIp(utilisateurService.getCurrentAdresseIp());
		logPresecription.setDesignation("La prescription Num: "+prescription.getPrescriptionNumber()+"Agent-N°: "+prescription.getAgentId()+"a été supprimer:"
		+"a la date:"+new Date()+"");
		logPresecription.setUserName(user.get(0).getUsername());
		logPresecriptionRepository.save(logPresecription);
//_______________________________________________log_______________________________________________
	    prescriptionRepo.delete(prescription);
		 return new MessageResponse(HttpStatus.OK.toString(), null, "La presecription a été supprimé avec success",null);
	}


	@Override
	public MessageResponse updateAgentPresecription(updateAgentPresecription updateAgentPresecription) {
		List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
		Prescription prescription = this.prescriptionRepo.findOne(updateAgentPresecription.getPrescriptionId());
		String OldMatriculeAgent= prescription.getAgentId();
		prescription.setAgentId(updateAgentPresecription.getAgentId());
		prescriptionRepo.save(prescription);
		
		LogPresecription logPresecription = new LogPresecription();
		logPresecription.setDateUpdate(new Date());
		logPresecription.setAdresseIp(utilisateurService.getCurrentAdresseIp());
		logPresecription.setDesignation("Le Matricule agent: "+OldMatriculeAgent+"de L'Ordonnance N°: "+prescription.getPrescriptionNumber()+"a Ã©tÃ© modifier par Le Matricule agent:"
		+updateAgentPresecription.getAgentId()+"a la date:"+new Date()+"");
		
		logPresecription.setUserName(user.get(0).getUsername());
		logPresecriptionRepository.save(logPresecription);
		  return new MessageResponse(HttpStatus.OK.toString(), null, "La presecription a Ã©tÃ© modifier avec succÃ©e",prescription.getPrescriptionNumber());
	}
	
	
	

	
	//_______________________________________________Jasper report Accident de travail_______________________________________________________________________________//
	
		@Override
		public List<PresecriptionResponseJasper> presecriptionDisease(PresecriptionRequestjasper presecriptionRequestjasper ) {
			
			List<PresecriptionResponseJasper> preslistjasper = new ArrayList<>();
			
			if(presecriptionRequestjasper.getEnd()!=null && presecriptionRequestjasper.getEnd()!=null && 
					presecriptionRequestjasper.getDiseaseId()!= null) {
				
			List<PresecriptionRepJasper> presecriptionList = prescriptionRepo.findByPresDateBetweenAndDoctorId(
			presecriptionRequestjasper.getDiseaseId(),
			presecriptionRequestjasper.getStart()
			, presecriptionRequestjasper.getEnd());
			
		
			presecriptionList.forEach(item->{
				System.out.println(item.getAgentSituation());
				PresecriptionResponseJasper presRespJasper = new PresecriptionResponseJasper();
				presRespJasper.setDateend(presecriptionRequestjasper.getEnd());
				presRespJasper.setDatestart(presecriptionRequestjasper.getStart());
				presRespJasper.setDateord(item.getPrescriptionDate());
				presRespJasper.setDistNum(item.getPrescriptionNumber());
				presRespJasper.setMatricule(item.getAgentId());
				presRespJasper.setMontantOrd(item.getMontantOrd());
				presRespJasper.setTotalnbreOrd(presecriptionList.size());
				presRespJasper.setMaladie(item.getAgentSituation());
				preslistjasper.add(presRespJasper);
			    });
			//return preslistjasper;
			}
			
			if(presecriptionRequestjasper.getEnd()!=null && presecriptionRequestjasper.getEnd()!=null && 
					presecriptionRequestjasper.getDiseaseId() == 0) {
			List<PresecriptionRepJasper> presecriptionList = prescriptionRepo.findByPreMaldiDateBetween(presecriptionRequestjasper.getStart()
			, presecriptionRequestjasper.getEnd());
			
			
			presecriptionList.forEach(item->{
				PresecriptionResponseJasper presRespJasper = new PresecriptionResponseJasper();
				presRespJasper.setDateend(presecriptionRequestjasper.getEnd());
				presRespJasper.setDatestart(presecriptionRequestjasper.getStart());
				presRespJasper.setDateord(item.getPrescriptionDate());
				presRespJasper.setDistNum(item.getPrescriptionNumber());
				presRespJasper.setMatricule(item.getAgentId());
				//presRespJasper.setMaladie(item.getDoctor().getSpecialty());
				//presRespJasper.setMontantOrd(prescriptionLineRepo.totalpricePre(item.getId()));
				presRespJasper.setMontantOrd(item.getMontantOrd());
				presRespJasper.setTotalnbreOrd(presecriptionList.size());
				if(item.getAgentSituation()!=null) {
				presRespJasper.setMaladie(item.getAgentSituation());
				}
				preslistjasper.add(presRespJasper);
			    });
		//	return preslistjasper;
			}
		
			
             return preslistjasper;
			
			
			}
		
		
			
		@Override
		public File getPdfpresDisease(PresecriptionRequestjasper presecriptionRequestjasper) throws FileNotFoundException{
			
			return this.getPdf(this.presecriptionDisease(presecriptionRequestjasper));
		}
		
		private File getPdf(List<PresecriptionResponseJasper> list) throws FileNotFoundException{
			JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
			
			return jasperService.jasperFile(dataSource, "prestat");
		}
		
		
		
	//______________________________________________________________________________________________________________________________//
	
	
		
		
		
		
		
		
	
		
		@Override
		public ordonnanceNbre getNbreOrdonnance() {
			ordonnanceNbre ordonnNbre  = new ordonnanceNbre();
			ordonnNbre.setNbreDist(distributionLineRepo.countDistributionLine());
			ordonnNbre.setNbrePre(prescriptionLineRepo.countPrescriptionLine());
			return ordonnNbre;
		}
			
		
		
		
		//_______________________________________________Jasper report ordonnance  actif/retraité_______________________________________________________________________________//
		
			@Override
			public List<PresecriptionResponseJasper> presecriptionSituation(PresecriptionRequestSituationjasper presecriptionRequestjasper ) {
				
				List<PresecriptionResponseJasper> preslistjasper = new ArrayList<>();
				
				if(presecriptionRequestjasper.getEnd()!=null && presecriptionRequestjasper.getEnd()!=null && 
						presecriptionRequestjasper.getSituation()!= "" && presecriptionRequestjasper.getSituation()!= null ) {
					
//				List<Prescription> presecriptionList = prescriptionRepo.findByPrescriptionDateBetweenAndAgentSituation(presecriptionRequestjasper.getStart()
//				, presecriptionRequestjasper.getEnd(), presecriptionRequestjasper.getSituation());
				
				
				
				List<PresecriptionRepJasper> presecriptionList = prescriptionRepo.findByPresDateBetweenAndAgentSituation(presecriptionRequestjasper.getStart()
						, presecriptionRequestjasper.getEnd(), presecriptionRequestjasper.getSituation());
				
				presecriptionList.forEach(item->{
					PresecriptionResponseJasper presRespJasper = new PresecriptionResponseJasper();
					presRespJasper.setDateend(presecriptionRequestjasper.getEnd());
					presRespJasper.setDatestart(presecriptionRequestjasper.getStart());
					presRespJasper.setDateord(item.getPrescriptionDate());
					presRespJasper.setDistNum(item.getPrescriptionNumber());
					presRespJasper.setMatricule(item.getAgentId());
					//presRespJasper.setMontantOrd(prescriptionLineRepo.totalpricePre(item.getId()));
					presRespJasper.setMontantOrd(item.getMontantOrd());
					presRespJasper.setTotalnbreOrd(presecriptionList.size());
					presRespJasper.setMaladie(item.getAgentSituation());
					preslistjasper.add(presRespJasper);
				    });
				//return preslistjasper;
				}
				
				if(presecriptionRequestjasper.getEnd()!=null && presecriptionRequestjasper.getEnd()!=null && 
						presecriptionRequestjasper.getSituation().equalsIgnoreCase("All") ) {
//				List<Prescription> presecriptionList = prescriptionRepo.findByPrescriptionDateBetween(presecriptionRequestjasper.getStart()
//				, presecriptionRequestjasper.getEnd());
				
					List<PresecriptionRepJasper> presecriptionList = prescriptionRepo.findByPrescrStDateBetween(presecriptionRequestjasper.getStart()
				, presecriptionRequestjasper.getEnd());
					
				presecriptionList.forEach(item->{
					PresecriptionResponseJasper presRespJasper = new PresecriptionResponseJasper();
					presRespJasper.setDateend(presecriptionRequestjasper.getEnd());
					presRespJasper.setDatestart(presecriptionRequestjasper.getStart());
					presRespJasper.setDateord(item.getPrescriptionDate());
					presRespJasper.setDistNum(item.getPrescriptionNumber());
					presRespJasper.setMatricule(item.getAgentId());
					//presRespJasper.setMaladie(item.getAgentSituation());
					presRespJasper.setMontantOrd(item.getMontantOrd());
					presRespJasper.setTotalnbreOrd(presecriptionList.size());
					if(item.getAgentSituation()!=null) {
					presRespJasper.setMaladie(item.getAgentSituation());
					}
					preslistjasper.add(presRespJasper);
				    });
			//	return preslistjasper;
				}
			
				
	             return preslistjasper;
				
				
				}
			
			
				
			@Override
			public File getPdfpresSituation(PresecriptionRequestSituationjasper presecriptionRequestjasper) throws FileNotFoundException{
				
				return this.getPdfpresSituation(this.presecriptionSituation(presecriptionRequestjasper));
			}
			
			private File getPdfpresSituation(List<PresecriptionResponseJasper> list) throws FileNotFoundException{
				JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
				
				return jasperService.jasperFile(dataSource, "prestatSituation");
			}
			
			
			
		//______________________________________________________________________________________________________________________________//

	
}




