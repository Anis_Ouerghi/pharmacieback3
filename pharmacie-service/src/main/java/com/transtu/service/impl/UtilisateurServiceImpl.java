package com.transtu.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.transtu.core.dto.AuthenticationResponseDTO;
import com.transtu.core.dto.UserRoleDto;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdatePasswordRequest;
import com.transtu.core.json.request.UserRequest;
import com.transtu.persistence.entities.Role;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.persistence.repositories.DayRepository;
import com.transtu.persistence.repositories.DayTypeRepository;
import com.transtu.persistence.repositories.RoleRepository;
import com.transtu.persistence.repositories.UtilisateurRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.UtilisateurService;
@Service

public class UtilisateurServiceImpl  implements UtilisateurService{
	
	
	
	
	@Autowired
	@Qualifier("sessionRegistry")
	private SessionRegistry sessionRegistry;
	
	@Autowired(required=true)
	UtilisateurRepository utilisateurRepository;
	@Autowired(required=true)
	RoleRepository roleRepository;
	@Autowired(required=true)
	DayRepository dayRepository;
	@Autowired(required=true)
	DayTypeRepository dayTypeRepository;
	
	public MessageResponse addUser(UserRequest userRequest) {
		List<Utilisateur> user1 = utilisateurRepository.findByUsernameContainingIgnoreCase(userRequest.getUsername());
		if(user1.size()>0){
			RestExceptionCode code = RestExceptionCode.USER_NAME_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;	
		}
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(userRequest.getPassword());
		Utilisateur user = new Utilisateur();
		user.setUsername(userRequest.getUsername());
		user.setLastname(userRequest.getLastname());
		user.setFirstname(userRequest.getFirstname());
		user.setPassword(hashedPassword);
		user.setEmail(userRequest.getEmail());
		user.setActived(true);
		utilisateurRepository.save(user);
		
		for (UserRoleDto userRoleDto : userRequest.getUserRoleDtos()){
			Role role = roleRepository.findOne(userRoleDto.getRoleId());
			if(role==null){
				RestExceptionCode code = RestExceptionCode.ROLE_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;	
			}
		// Role userRole = new Role();
         List <Role> roles = new ArrayList<Role>();
         roles.add(role);
         user.setRoles(roles);
         utilisateurRepository.save(user);
		}
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "l'utilisateur a été ajouté avec succés",user.getUsername());	
		
	}
	
	

	@Override
	public List<Utilisateur> getListUsers() {
		// TODO Auto-generated method stub
		return utilisateurRepository.findAll();
	}
	
	@Override
	public MessageResponse activateUser (Long Id){
	
		Utilisateur  utilsateur = utilisateurRepository.findOne(Id);
		if(utilsateur==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		
		utilsateur.setActived(true);
		utilisateurRepository.save(utilsateur);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "l'utlisateur  est activé",utilsateur.getUsername());	
		
	}
	
	@Override
	public MessageResponse desactivateUser (Long Id){
		
		Utilisateur  utilsateur = utilisateurRepository.findOne(Id);
		if(utilsateur==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		utilsateur.setActived(false);
		utilisateurRepository.save(utilsateur);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "l'utlisateur  est désactivé",utilsateur.getUsername());	
		
	}
	
	@Override
	public MessageResponse deleteUser (Long Id){
		
		Utilisateur  utilsateur = utilisateurRepository.findOne(Id);
		if(utilsateur==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		
		
		utilisateurRepository.delete(utilsateur);;
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "l'utlisateur  est supprimé",utilsateur.getUsername());	
		
	}
	
	
	public MessageResponse updateUser(UserRequest userRequest) {
		Utilisateur user = utilisateurRepository.findOne(userRequest.getId());
		//BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		//String hashedPassword = passwordEncoder.encode(userRequest.getPassword());
		user.setUsername(userRequest.getUsername());
		user.setLastname(userRequest.getLastname());
		user.setFirstname(userRequest.getFirstname());
		//user.setPassword(hashedPassword);
		user.setEmail(userRequest.getEmail());
		//user.setActived(false);
		List <Role> roles = new ArrayList<Role>();
		for (UserRoleDto userRoleDto : userRequest.getUserRoleDtos()){
			Role role = roleRepository.findOne(userRoleDto.getRoleId());
			if(role==null){
				RestExceptionCode code = RestExceptionCode.ROLE_INEXISTANT;
				RestException ex = new RestException(code.getError(), code);
				throw ex;	
			}  
         roles.add(role);
         user.setRoles(roles);
         utilisateurRepository.save(user);
		}
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "l'utilisateur a été modifier  avec succés",user.getUsername());	
		
	}
	
	
	@Override
	public MessageResponse updatePassword(UpdatePasswordRequest updatePasswordRequest) {
		
		Utilisateur user = utilisateurRepository.findOne(updatePasswordRequest.getId());
		
		if(user==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
	
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		 boolean result = passwordEncoder.matches( updatePasswordRequest.getOldPassword(),user.getPassword());

		if (result==true){   
			String newPassword = passwordEncoder.encode(updatePasswordRequest.getNewPassword());
			user.setPassword(newPassword);
			utilisateurRepository.save(user);
			return new MessageResponse(HttpStatus.OK.toString(), null, "le mot de passe a été modifier avec succés ",user.getUsername());
		}
		else 
		{	
	    return new MessageResponse(HttpStatus.NOT_ACCEPTABLE.toString(), null, "verifier que le mot passe entré n'est pas conforme ",user.getUsername());
			
		}
		
		
		
	}

	@Override
	
public String getCurrentUserDetails() {
	
		AuthenticationResponseDTO authenticationResponseDTO=(AuthenticationResponseDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return  authenticationResponseDTO.getUserName();	
	   //return utilisateurRepository.findOne(1l).getUsername();
	
		
	}

public Utilisateur getCurrentUserDetail() {
	
	
	List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(this.getCurrentUserDetails());
	if(user==null){
		RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
		RestException ex = new RestException(code.getError(), code);
		throw ex;
	}
	this.getListCurrentUserDetails();
	return  user.get(0);

	
}

	
public void getListCurrentUserDetails() {
		
		List<Object> principals = sessionRegistry.getAllPrincipals(); 
	
		List<String> usersNamesList = new ArrayList<String>();

		for (Object principal: principals) {
		   
			if (principal instanceof User) {
		        usersNamesList.add(((User) principal).getUsername());  
		        
	
		    }
		}
		
	}	
	

@Override
public String getCurrentAdresseIp() {
		
//	 Object details =
//    SecurityContextHolder.getContext().getAuthentication().getDetails();
//
//    String ipAddress = null;
//             if (details instanceof WebAuthenticationDetails)
//    ipAddress = ((WebAuthenticationDetails) details).getRemoteAddress();

		return  request.getRemoteAddr();
		
		}

@Autowired
private HttpServletRequest request;



@Override
public List<Utilisateur> getListUsersByDay(Long id, Date date) {
	// TODO Auto-generated method stub
	return null;
}



//public List<Utilisateur> getListUsersByDay( Long id,  Date date) {
//	
//	
//	LocalDate localDate1 = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//
//	DayType dayType = dayTypeRepository.findOne(id);
//
//	List<Utilisateur> l  = new ArrayList<>();
//	
//	for (Day day : dayRepository.findAll()) {
//		
//		if ( (day.getDayType().getId()==id) && day.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().equals(localDate1)) {
//			
//			l=utilisateurRepository.findByUserDaysDay(day);
//			
//		}
//	}
//	
//	return l;
//	
//}










}
