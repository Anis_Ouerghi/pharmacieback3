package com.transtu.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.type.ReferenceType;
import com.transtu.core.json.request.AddExceptionalMovement;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.MovementResponse;
import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.InternalDelivery;
//import com.transtu.persistence.entities.ExceptionalMovement;
import com.transtu.persistence.entities.Movement;
import com.transtu.persistence.entities.Reception;
import com.transtu.persistence.repositories.DistributionRepository;
//import com.transtu.persistence.repositories.ExceptionalMovementRepository;
import com.transtu.persistence.repositories.InternalDeliveryRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.MovementRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.ReceptionRepository;
import com.transtu.persistence.repositories.ReferenceTypeRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.ExceptionalMovementService;


@Service
public class ExceptionalMovementImpl implements ExceptionalMovementService {
	
	@Autowired(required = true)
	DistributionRepository distributionRepository;
	
	@Autowired(required = true)
	InternalDeliveryRepository InternalDeliveryRepository;
	
//	@Autowired(required = true)
//	ExceptionalMovementRepository exceptionalMovementRepository;
	
	@Autowired(required = true)
	MovementRepository movementRepository;
	
	@Autowired(required = true)
	LocationRepository locationRepository;
	

	@Autowired(required = true)
	ReceptionRepository receptionRepository;
	
	@Autowired(required = true)
	ProductRepository productRepository;
	
	@Autowired(required = true)
    ReferenceTypeRepository referenceTypeRepository;
	@Override
	public MessageResponse addExceptionalMovement(AddExceptionalMovement addExceptionalMovement) {
		
		Movement exceptionalMovement = new Movement();
		exceptionalMovement.setDate(new Date());
//		exceptionalMovement.setDescription(addExceptionalMovement.getDescription());
//		exceptionalMovement.setDistribution(distributionRepository.
//				findOne(addExceptionalMovement.getDistributionId()));
//		//exceptionalMovement.setInternalDelivery(InternalDeliveryRepository.
//			//	findOne(addExceptionalMovement.getInternalDeliveryId()));
//    	exceptionalMovement.setLocation(locationRepository
//    			.findOne(addExceptionalMovement.getLocationId()));
//    	exceptionalMovement.setProduct(productRepository.findOne(addExceptionalMovement.getProductId()));
//		exceptionalMovement.setQuantity(addExceptionalMovement.getQuantityId());
//		//exceptionalMovement.setReception(receptionRepository.findOne(addExceptionalMovement.getReceptionId()));
//		//exceptionalMovement.setReferenceType(referenceTypeRepository.findOne(addExceptionalMovement.getReferenceTypeId()));
//		exceptionalMovement.setType(addExceptionalMovement.getType());
		
		 
		movementRepository.save(exceptionalMovement);
		if(exceptionalMovement.getType() == "-1"){
			
		}else {
			
			
		      }
		return new MessageResponse(HttpStatus.OK.toString(), null, "Mouvement exeptionel est ajouté avec succés","");
	}

	@Override
	public List<MovementResponse> getListExceptionalMovement() {
		List<Movement> lmvt;
		MovementResponse mvtresp= new MovementResponse(); 
		List<MovementResponse> listMvt = new ArrayList<>();
		for( Movement mvt : movementRepository.findAll() ){
			mvtresp.setDate(mvt.getDate());
			mvtresp.setLocation(locationRepository.findOne(mvt.getLocation()
					.getId()).getLib());
			mvtresp.setDescription(mvt.getDescription());
			mvtresp.setProduct(productRepository.findOne(mvt.getProduct().getId()).getLib());
			listMvt.add(mvtresp);
		}
		
		return listMvt;	
		
	}

	@Override
	public MessageResponse updateExceptionalMovement(Long id, String label) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageResponse deleteExceptionalMovement(Long id) {
		// TODO Auto-generated method stub
		return null;
	}


//	@Override
//	public MessageResponse deleteExceptionalMovement(Long id) {
//		ExceptionalMovement exceptionalMovement =exceptionalMovementRepository.findOne(id) ;
//		if(exceptionalMovement == null ){
//			RestExceptionCode code = RestExceptionCode.EXEPTIONEL_MOVEMENT_NOT_FOUND;
//			RestException ex = new RestException(code.getError(), code);
//			throw ex;
//		}
//		exceptionalMovementRepository.delete(id);
//		return new MessageResponse(HttpStatus.OK.toString(), null, "Mouvement exeptionel est supprimé avec succés");
//	}
	

}
