package com.transtu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.District;
import com.transtu.persistence.repositories.DistrictRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.DistrictService;

@Service
public class DistrictServiceImpl implements DistrictService {

	@Autowired
	private DistrictRepository districtRepository;
	@Override
	public List<District> getListDistrict() {
		return this.districtRepository.findAll(new Sort(Direction.ASC, "lib"));
	}
	
	
	@Override
	public MessageResponse addDistrict(District district) {
		
		District district2 = this.districtRepository.findByLib(district.getLib());
		if(district2!=null){
			RestExceptionCode code = RestExceptionCode.EXISTING_DISTRICT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		this.districtRepository.save(district);
		return new MessageResponse(HttpStatus.OK.toString(), null, "district est ajouté avec success",district.getLib());	

	}
	@Override
	public MessageResponse updateDistrict(District district) {
		if(this.districtRepository.findOne(district.getId())==null){
			RestExceptionCode code = RestExceptionCode.DISTRICT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		if((this.districtRepository.findByLib(district.getLib())!=null)&&(this.districtRepository.findByLib(district.getLib()).getId()!=district.getId())){
			RestExceptionCode code = RestExceptionCode.EXISTING_DISTRICT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		this.districtRepository.save(district);
		return new MessageResponse(HttpStatus.OK.toString(), null, "district est modifié avec success",district.getLib());
	}
	@Override
	public MessageResponse deleteDistrict(Long idDistrict) {
		District district = this.districtRepository.findOne(idDistrict);
		if(district==null){
			RestExceptionCode code = RestExceptionCode.DISTRICT_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		this.districtRepository.delete(idDistrict);
		return new MessageResponse(HttpStatus.OK.toString(), null, "district est supprimé avec success",district.getLib());
	}


	@Override
	public District findDistrictByLib(String lib) {
		return this.districtRepository.findByLib(lib);
	}

}
