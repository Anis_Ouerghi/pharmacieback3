package com.transtu.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.rowset.serial.SerialException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.transtu.core.dto.StockMovementLineDto;
import com.transtu.core.jasper.MovementInJasper;
import com.transtu.core.jasper.MovementJasper;
import com.transtu.core.jasper.MovementOutJasper;
import com.transtu.core.jasper.MovementsJaspersResponse;
import com.transtu.core.json.request.AddExceptionalMovement;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.StockMovementFilter;
import com.transtu.core.json.request.StockMvtfilter;
import com.transtu.persistence.entities.Day;
import com.transtu.persistence.entities.Inventory;
import com.transtu.persistence.entities.InventoryLine;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Lot;
import com.transtu.persistence.entities.Motif;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.QStockMovement;
import com.transtu.persistence.entities.QStockMovementLine;
import com.transtu.persistence.entities.State;
import com.transtu.persistence.entities.Stock;
import com.transtu.persistence.entities.StockMovement;
import com.transtu.persistence.entities.StockMovementLine;
import com.transtu.persistence.entities.Stocklot;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.persistence.repositories.DayRepository;
import com.transtu.persistence.repositories.DistributionRepository;
import com.transtu.persistence.repositories.InternalDeliveryRepository;
import com.transtu.persistence.repositories.InventoryLineRepository;
import com.transtu.persistence.repositories.InventoryRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.LotRepository;
import com.transtu.persistence.repositories.MotifRepository;
import com.transtu.persistence.repositories.MovementRepository;
import com.transtu.persistence.repositories.PrescriptionRepository;
import com.transtu.persistence.repositories.PresentationRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.ReceptionRepository;
import com.transtu.persistence.repositories.ReferenceTypeRepository;
import com.transtu.persistence.repositories.StateRepository;
import com.transtu.persistence.repositories.StockLotRepository;
import com.transtu.persistence.repositories.StockMovementLineRepository;
import com.transtu.persistence.repositories.StockMovementRepository;
import com.transtu.persistence.repositories.StockRepository;
import com.transtu.persistence.repositories.UtilisateurRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.AgentService;
import com.transtu.service.JasperService;
import com.transtu.service.SequenceService;
import com.transtu.service.StockMovementService;
import com.transtu.service.UtilisateurService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


@Service
public class StockMovementServiceImpl implements StockMovementService {
    
	@PersistenceContext
	EntityManager em ;
	
	@Autowired(required=true)
	SequenceService sequenceService ;
	
	@Autowired
	LocationRepository LocationRepository;
	
	@Autowired
	StockRepository stockRepo;
	
	@Autowired(required = true)
	DistributionRepository distributionRepository;
	
	@Autowired(required = true)
	InternalDeliveryRepository InternalDeliveryRepository;
	
	@Autowired(required = true)
	AgentService agentService ;
	
	@Autowired
	DayRepository dayRepository;
	
	@Autowired
	StateRepository StateRepository;
	
	@Autowired
	MotifRepository motifRepository;
	
	@Autowired(required = true)
	MovementRepository movementRepository;
	
	@Autowired(required = true)
	LocationRepository locationRepository;
	@Autowired(required = true)
	StockMovementRepository stockMovementRepository;

	@Autowired(required = true)
	ReceptionRepository receptionRepository;
	
	@Autowired(required = true)
	ProductRepository productRepository;
	
	@Autowired(required = true)
	StockMovementLineRepository stockMovementLineRepository;
	
	@Autowired(required = true)
    ReferenceTypeRepository referenceTypeRepository;
	
	@Autowired(required = true)
	JasperService jasperService ; 
	
	@Autowired(required = true)
	PrescriptionRepository prescriptionRepo;
	
	@Autowired(required = true)
	UtilisateurService utilisateurService;
	
	@Autowired(required = true)
	UtilisateurRepository utilisateurRepository;
	
	@Autowired(required = true)
	PresentationRepository  presentationRepository;
	
	@Autowired(required = true)
	InventoryLineRepository  inventoryLineRepository;
	
	@Autowired(required = true)
	InventoryRepository  inventoryRepository;
	
	@Autowired(required = true)
	LotRepository  lotRepository;
	
	@Autowired(required = true)
	StockLotRepository  stockLotRepository;
	
	@Override
	public MessageResponse addExceptionalStockMovement(AddExceptionalMovement addExceptionalMovement) {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");
		
		List<Utilisateur> user = utilisateurRepository.findByUsernameContainingIgnoreCase(utilisateurService.getCurrentUserDetails());
		if(user==null){
			RestExceptionCode code = RestExceptionCode.USER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		Location location= this.LocationRepository.findOne(addExceptionalMovement.getLocationId());
		State state = this.StateRepository.findOne(1l);
	
		
		if(location==null){
			RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		if(state==null){
			RestExceptionCode code = RestExceptionCode.STATE_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		Day day = dayRepository.findOne(addExceptionalMovement.getDayId());
		if(day==null ){
			RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		String numMvt ;
		if (addExceptionalMovement.getTypeMvt().equals("-1")){
			numMvt=sequenceService.calculateNumMvtExepSortie();
		}
		else {
			numMvt=sequenceService.calculateNumMvtExepEntre();	
		}
			
		StockMovement stockMovement = new StockMovement();
		stockMovement.setDateMovement(new Date());
		stockMovement.setNumMovement(numMvt);
		stockMovement.setUtilisateur(user.get(0));
		stockMovement.setTypeMvt(addExceptionalMovement.getTypeMvt());
		stockMovement.setDay(day);
		stockMovement.setDescriminator("Exceptionnel");
		stockMovement.setLocation(location);
		stockMovement.setDistribution(distributionRepository
				.findOne(addExceptionalMovement.getDistributionId()));
		
		stockMovement.setInternalDelivery(InternalDeliveryRepository
				.findOne(addExceptionalMovement.getInternalDeliveryId()));
		
		stockMovement.setReception(receptionRepository
				.findOne(addExceptionalMovement.getReceptionId()));
		stockMovement.setReferenceType(referenceTypeRepository
				.findOne(addExceptionalMovement.getReferenceTypeId()));
		
		stockMovementRepository.save(stockMovement);
		
		Location locationPharma = locationRepository.findOne(1l);
		Location locationDep = locationRepository.findOne(2l);
		
		for (StockMovementLineDto stockMovementLineDto : addExceptionalMovement.getStockMovementLineDtos() ){
			Product product = productRepository.findOne(stockMovementLineDto.getProductId());
			if(product==null){
				RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			if(stockMovementLineDto.getMovmentQte()==0){
				RestExceptionCode code = RestExceptionCode.QUANTITY_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			Motif motif=motifRepository.findOne(stockMovementLineDto.getMotifId());
			if(motif==null){
				RestExceptionCode code = RestExceptionCode.MOTIF_NOT_FOND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			StockMovementLine stockMovementLine= new StockMovementLine();
			stockMovementLine.setStockMovement(stockMovement);
			
			stockMovementLine.setMotif(motif);
			stockMovementLine.setDescription(stockMovementLineDto.getDescription());
			stockMovementLine.setProduct(product);
			if(stockMovementLineDto.getLotId()!=null || stockMovementLineDto.getLotId()==0) {
			stockMovementLine.setLot(lotRepository.findOne(stockMovementLineDto.getLotId()));
		    }
			stockMovementLine.setMovmentQte(stockMovementLineDto.getMovmentQte());
			stockMovementLineRepository.save(stockMovementLine);
			
			//-----------------------<>------------------------------MAJ-Stock----------------------<>------------------<>--------
			
			//--------->1-Distribution
			//--------->2-reception
			//--------->3-internal-delivery
			
			
			
			if (addExceptionalMovement.getTypeMvt().equals("1")){
				
				//-------------<>-----<>-------3-internal-delivery--------------------------------------stock+
				if (addExceptionalMovement.getReferenceTypeId()==3 ){
					Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
					 stockDepot.setQuantity(stockDepot.getQuantity()+stockMovementLineDto.getMovmentQte());
			         Stock stockPharmacie = this.stockRepo.findByProductAndLocation(product, locationPharma);
					 stockPharmacie.setQuantity(stockPharmacie.getQuantity()-stockMovementLineDto.getMovmentQte());
					 this.stockRepo.save(stockPharmacie);
					 this.stockRepo.save(stockDepot);
				}
				
				//-------------<>-----<>-------1-Distribution------------------------------------<>-----------------stock+
				if (addExceptionalMovement.getReferenceTypeId()==1){
					Stock stockPharmacie = this.stockRepo.findByProductAndLocation(product, locationPharma);
					stockPharmacie.setQuantity(stockPharmacie.getQuantity()+stockMovementLineDto.getMovmentQte());
					this.stockRepo.save(stockPharmacie);
					}
				
				
				//-------------<>-----<>-------2-Reception------------------------------------<>----------------stock+
				if (addExceptionalMovement.getReferenceTypeId()==2 ){
					
	              Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
				  stockDepot.setQuantity(stockDepot.getQuantity()+stockMovementLineDto.getMovmentQte());
				  this.stockRepo.save(stockDepot);
				}
				
				if(addExceptionalMovement.getReferenceTypeId()==0) {
					//-------------<>-----<>--------autre Reference--&& location pharama 1--------entre a la pharmacie--------------------------<>
					if ((addExceptionalMovement.getLocationId()==1)){
						System.out.println("---------addExceptionalMovement.getTypeMvt()-------------"+addExceptionalMovement.getTypeMvt());
					  Stock stockPharma = this.stockRepo.findByProductAndLocation(product, locationPharma);
					  stockPharma.setQuantity(stockPharma.getQuantity()+stockMovementLineDto.getMovmentQte());
					  this.stockRepo.save(stockPharma);
					}
					
					  //-------------<>-----<>--------autre Reference--&& location depot  2--------entre au dep----------------------------<>
					
					if (addExceptionalMovement.getLocationId()==2){
						  System.out.println("-----------------------------Autre reference entre au depot-------------------------------------");
						  Lot lot = lotRepository.findOne(stockMovementLineDto.getLotId());
						  if(lot==null ){
								RestExceptionCode code = RestExceptionCode.LOT_INFO_NOT_FOUND;
								RestException ex = new RestException(code.getError(), code);
								throw ex;
						}
						  
						  
						 Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
						 stockDepot.setQuantity(stockDepot.getQuantity()+stockMovementLineDto.getMovmentQte());
						 this.stockRepo.save(stockDepot);
						  
						 Stocklot stocklot = stockLotRepository.findByLot(lot);
						 stocklot.setQuantity(stocklot.getQuantity()+stockMovementLineDto.getMovmentQte());;
						 stockLotRepository.save(stocklot);
						 
						 
							
					}					
				}
				
				
			}else if (addExceptionalMovement.getTypeMvt().equals("-1")){
				
				
				//-------------<>-----<>-------3-internal-delivery--------------------------------------stock-
				if (addExceptionalMovement.getReferenceTypeId()==3 ){
				  Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
				  stockDepot.setQuantity(stockDepot.getQuantity()-stockMovementLineDto.getMovmentQte());
		          Stock stockPharmacie = this.stockRepo.findByProductAndLocation(product, locationPharma);
				  stockPharmacie.setQuantity(stockPharmacie.getQuantity()+stockMovementLineDto.getMovmentQte());
				  this.stockRepo.save(stockPharmacie);
				  this.stockRepo.save(stockDepot);
				}
				
				//-------------<>-----<>-------1-Distribution------------------------------------<>----------------stock
				if (addExceptionalMovement.getReferenceTypeId()==1){
					Stock stockPharmacie = this.stockRepo.findByProductAndLocation(product, locationPharma);
					stockPharmacie.setQuantity(stockPharmacie.getQuantity()-stockMovementLineDto.getMovmentQte());
					this.stockRepo.save(stockPharmacie);
					}
				
				//-------------<>-----<>-------2-Reception------------------------------------<>----------------stock+
				if (addExceptionalMovement.getReferenceTypeId()==2 ){
					
	              Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
				  stockDepot.setQuantity(stockDepot.getQuantity()-stockMovementLineDto.getMovmentQte());
				  this.stockRepo.save(stockDepot);
				}
				
				
				if(addExceptionalMovement.getReferenceTypeId()==0) {
					//-------------<>-----<>--------autre Reference--&& location pharama 1--------sortie a la pharmacie--------------------------<>
					if ((addExceptionalMovement.getLocationId()==1)){
						System.out.println("---------addExceptionalMovement.getTypeMvt()-------------"+addExceptionalMovement.getTypeMvt());
					  Stock stockPharma = this.stockRepo.findByProductAndLocation(product, locationPharma);
					  stockPharma.setQuantity(stockPharma.getQuantity()-stockMovementLineDto.getMovmentQte());
					  this.stockRepo.save(stockPharma);
					}
					
					
					  //-------------<>-----<>--------autre Reference--&& location depot  2--------sortie au dep----------------------------<>
					
					if (addExceptionalMovement.getLocationId()==2){
						  System.out.println("----------------------------------------------Autre reference sortie dep----------------------------");
						  Lot lot = lotRepository.findOne(stockMovementLineDto.getLotId());
						  Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
						  Stocklot stocklot = stockLotRepository.findByLot(lot);
						  if(lot==null || (stockDepot.getQuantity() < stockMovementLineDto.getMovmentQte()) || (stockDepot.getQuantity()<stocklot.getQuantity())){
								RestExceptionCode code = RestExceptionCode.LOT_INFO_NOT_FOUND;
								RestException ex = new RestException(code.getError(), code);
								throw ex;
							}
						    if(stockDepot.getQuantity() >= stockMovementLineDto.getMovmentQte()) {
						     stockDepot.setQuantity(stockDepot.getQuantity()-stockMovementLineDto.getMovmentQte());
						     this.stockRepo.save(stockDepot);
						    }
		
						     if(stocklot.getQuantity() >= stockMovementLineDto.getMovmentQte()) {
							 stocklot.setQuantity(stocklot.getQuantity()-stockMovementLineDto.getMovmentQte());;
							 this.stockLotRepository.save(stocklot);
						     }
							
					}
						
					}
				
			}
			
			
			//-------------<>-----<>-------3-internal-delivery-----------------------------<>---------------------------
//			if (addExceptionalMovement.getTypeMvt().equals("1") && addExceptionalMovement.getReferenceTypeId()==3 
//					&& addExceptionalMovement.getDescriminator().equals("Exeptionnel")  ){
//			System.out.println("---**************************------------------1-------------");
//			 Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
//			 stockDepot.setQuantity(stockDepot.getQuantity()+stockMovementLineDto.getMovmentQte());
//	         Stock stockPharmacie = this.stockRepo.findByProductAndLocation(product, locationPharma);
//			 stockPharmacie.setQuantity(stockPharmacie.getQuantity()-stockMovementLineDto.getMovmentQte());
//			 this.stockRepo.save(stockPharmacie);
//			 this.stockRepo.save(stockDepot);
//				}
//		
//			if (addExceptionalMovement.getTypeMvt().equals("-1") && addExceptionalMovement.getReferenceTypeId()==3 
//					&& addExceptionalMovement.getDescriminator().equals("Exeptionnel")  ){
//				System.out.println("---**************************------------------2-------------");
//			  Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
//			  stockDepot.setQuantity(stockDepot.getQuantity()-stockMovementLineDto.getMovmentQte());
//	          Stock stockPharmacie = this.stockRepo.findByProductAndLocation(product, locationPharma);
//			  stockPharmacie.setQuantity(stockPharmacie.getQuantity()+stockMovementLineDto.getMovmentQte());
//			  this.stockRepo.save(stockPharmacie);
//			  this.stockRepo.save(stockDepot);
//				}	
		
			//-------------<>-----<>-------1-Distribution------------------------------------<>----------------
//			if (addExceptionalMovement.getTypeMvt().equals("1") && addExceptionalMovement.getReferenceTypeId()==1 
//					&& addExceptionalMovement.getDescriminator().equals("Exeptionnel")  ){
//				System.out.println("---**************************------------------3-------------");
//				Stock stockPharmacie = this.stockRepo.findByProductAndLocation(product, locationPharma);
//				stockPharmacie.setQuantity(stockPharmacie.getQuantity()+stockMovementLineDto.getMovmentQte());
//				this.stockRepo.save(stockPharmacie);
//				}
//			
//			
//				if (addExceptionalMovement.getTypeMvt().equals("-1") && addExceptionalMovement.getReferenceTypeId()==1 
//					&& addExceptionalMovement.getDescriminator().equals("Exeptionnel")  ){
//					
//				Stock stockPharmacie = this.stockRepo.findByProductAndLocation(product, locationPharma);
//				stockPharmacie.setQuantity(stockPharmacie.getQuantity()-stockMovementLineDto.getMovmentQte());
//				this.stockRepo.save(stockPharmacie);
//				}
			//-------------<>-----<>-------2-Reception------------------------------------<>---------------
//			if (addExceptionalMovement.getTypeMvt().equals("1") && addExceptionalMovement.getReferenceTypeId()==2 
//					&& addExceptionalMovement.getDescriminator().equals("Exeptionnel") ){
//				System.out.println("---**************************------------------4-------------");
//              Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
//			  stockDepot.setQuantity(stockDepot.getQuantity()+stockMovementLineDto.getMovmentQte());
//			  this.stockRepo.save(stockDepot);
//			}
//			
//			if (addExceptionalMovement.getTypeMvt().equals("-1") && addExceptionalMovement.getReferenceTypeId()==2 
//					&& addExceptionalMovement.getDescriminator().equals("Exeptionnel")){
//				System.out.println("---**************************------------------5-------------");
//			  Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
//			  stockDepot.setQuantity(stockDepot.getQuantity()-stockMovementLineDto.getMovmentQte());
//			  this.stockRepo.save(stockDepot);
//				
//			}
			
			//-------------<>-----<>--------autre Reference--&& location pharama 1--------sortie de la pharmacie--------------------------<>
//			if ((addExceptionalMovement.getTypeMvt().equals("1")) && (addExceptionalMovement.getDescriminator().equals("Exeptionnel"))
//					&& (addExceptionalMovement.getLocationId()==1 )&&( addExceptionalMovement.getReferenceTypeId()==0)){
//				System.out.println("---------addExceptionalMovement.getTypeMvt()-------------"+addExceptionalMovement.getTypeMvt());
//			  Stock stockPharma = this.stockRepo.findByProductAndLocation(product, locationPharma);
//			
//			  stockPharma.setQuantity(stockPharma.getQuantity()-stockMovementLineDto.getMovmentQte());
//			  this.stockRepo.save(stockPharma);
//			  
//				
//			}
//			
//			//-------------<>-----<>--------autre Reference--&& location pharama 1--------entre  a la pharmacie--------------------------<>
//			if (addExceptionalMovement.getTypeMvt().equals("-1") && addExceptionalMovement.getDescriminator().equals("Exeptionnel") && 
//					addExceptionalMovement.getLocationId()==1 && addExceptionalMovement.getReferenceTypeId()==0)
//				
//			
//			{
//				System.out.println("---**************************------------------8-------------");	
//			  Stock stockPharma = this.stockRepo.findByProductAndLocation(product, locationPharma);
//			  stockPharma.setQuantity(stockPharma.getQuantity()+stockMovementLineDto.getMovmentQte());
//			  this.stockRepo.save(stockPharma);
//			  
//				
//			}
			
			//-------------<>-----<>--------autre Reference--&& location depot 2--------sortie  de la dep--------------------------<>
			
//			if ((addExceptionalMovement.getTypeMvt().equals("-1")) && (addExceptionalMovement.getDescriminator().equals("Exeptionnel")) &&
//					(addExceptionalMovement.getLocationId()==2)&& (addExceptionalMovement.getReferenceTypeId()==0) ){
//				System.out.println("---**************************------------------9-------------");
//				  Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
//				  stockDepot.setQuantity(stockDepot.getQuantity()-stockMovementLineDto.getMovmentQte());
//				  this.stockRepo.save(stockDepot);
//					
//			}
//			
//	       //-------------<>-----<>--------autre Reference--&& location depot  2--------entre au dep----------------------------<>
//			
//			if ((addExceptionalMovement.getTypeMvt().equals("1")) && (addExceptionalMovement.getDescriminator().equals("Exeptionnel")) &&
//					(addExceptionalMovement.getLocationId()==2 && (addExceptionalMovement.getReferenceTypeId()==0)) ){
//				  System.out.println("---**************************------------------10-------------");
//				  Stock stockDepot = this.stockRepo.findByProductAndLocation(product, locationDep);
//				  stockDepot.setQuantity(stockDepot.getQuantity()+stockMovementLineDto.getMovmentQte());
//				  this.stockRepo.save(stockDepot);
//					
//			}
			
		}
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Mouvement exeptionel est ajouté avec succés",stockMovement.getNumMovement());
	}
	
	
	//------------------------------------------------------------------------------------------------------
	
	
	
	
	
	@Override
    public Set<StockMovement> filterexceptionalMovemen(StockMovementFilter stockMovementFilter) {
          
		  JPQLQuery<?> query = new JPAQuery(em); 
          QStockMovement stockMovement = QStockMovement.stockMovement;
          QStockMovementLine stockMovementLine = QStockMovementLine.stockMovementLine;
         
          query.from(stockMovementLine).orderBy(stockMovement.dateMovement.desc())
                        .leftJoin(stockMovementLine.stockMovement , stockMovement);
  
          if(stockMovementFilter.getMinDate()!=null){
        	  
                 query.where(stockMovement.dateMovement.after(stockMovementFilter.getMinDate()));
                
          }
          if(stockMovementFilter.getMaxDate()!=null){
        	  
        	  query.where(stockMovement.dateMovement.before(stockMovementFilter.getMaxDate()));
         }
         
          if(stockMovementFilter.getProductId()!=null){
        	 
                 query.where(stockMovementLine.product.id.in(stockMovementFilter.getProductId()));
                 
               
            }
//          if(stockMovementFilter.getNumMovement().isEmpty()== false){
//        	 
//                 query.where(stockMovement.numMovement.eq(stockMovementFilter.getNumMovement()));
//                 
//          }
          
          if(stockMovementFilter.getMovementId()!=null){
        	  
                 query.where(stockMovement.Id.eq(stockMovementFilter.getMovementId()));
                  
          } 
          if(stockMovementFilter.getReceptionId()!=null){
        	  
              query.where(stockMovement.Id.eq(stockMovementFilter.getReceptionId()));
              
          }
          if(stockMovementFilter.getInternalDeliveryId()!=null){
              query.where(stockMovement.internalDelivery.Id.eq(stockMovementFilter.getInternalDeliveryId()));
             
          }
          if(stockMovementFilter.getDistributionId()!=null){
              query.where(stockMovement.distribution.Id.eq(stockMovementFilter.getDistributionId()));
             
          } 
          
          if(stockMovementFilter.getReferenceTypeId()!=null){
              query.where(stockMovement.referenceType.id.eq(stockMovementFilter.getReferenceTypeId()));
         
          } 
         
//          if(stockMovementFilter.getDescriminator()!=null){
//              query.where(stockMovement.descriminator.eq(stockMovementFilter.getDescriminator()));
//            
//          } 
          if(stockMovementFilter.getDescriminator()!=null){
              query.where(stockMovement.descriminator.eq(stockMovementFilter.getDescriminator()));
            
          } 
          if(stockMovementFilter.getTypeMovement()!=null){
              query.where(stockMovement.typeMvt.eq(stockMovementFilter.getTypeMovement()));
            
          } 
         
		List<StockMovement> list = query.select(stockMovement).fetch();
		Set<StockMovement> set = new LinkedHashSet<>();
		set.addAll(list);
		
//		List<InternalDeliveryFilterResponse> internalDeliveriesResponse = new ArrayList<>();
//		for(InternalDelivery  delivery : set){
//			InternalDeliveryFilterResponse internalDeliveryResponse = new InternalDeliveryFilterResponse();
//			internalDeliveryResponse.setDeliveryNumber(delivery.getNumDelivery());
//			internalDeliveryResponse.setOrderNumber(delivery.getInternalOrder().getNumOrder());
//			internalDeliveryResponse.setProductNumber(delivery.getInternalDeliveryLines().size());
		
//			for(InternalDeliveryLine deliveryLine : delivery.getInternalDeliveryLines()){
		      //internalDeliveryResponse.getProductsId().add(deliveryLine.getId());
//			}
//			internalDeliveriesResponse.add(internalDeliveryResponse);
//		}
		
		return set;
	}
	@Override
	public List<StockMovement> getListExceptionalStockMovement() {
		
		return stockMovementRepository.findAll() ;	
		
	}
	
	
	@Override
	public File getMvtPdf(DateRequestPdf date) throws FileNotFoundException {
		
		return this.getPdfMovements(this.getMovementsFromPdf(date), date);
	}
	
	
	
	private void setMovementJasper(StockMovementLine stockMovementLine , MovementJasper movementJasper) throws JsonParseException, JsonMappingException, IOException{
		
		if(stockMovementLine.getStockMovement().getTypeMvt().equals("0")){   //sortie
			MovementOutJasper movOut = new MovementOutJasper();
			movOut.setDate(stockMovementLine.getStockMovement().getDateMovement());
			movOut.setDeliveredQt(stockMovementLine.getMovmentQte());			
			movOut.setAgent(
					this.agentService
						.getDetailAgent(this.prescriptionRepo.findByDistributionIn(stockMovementLine.getStockMovement().getDistribution()).getAgentId())
						.getFullName());
			movementJasper.getMovementsOut().add(movOut);
		}else{																 
			MovementInJasper movIn= new MovementInJasper();
			movIn.setNumber(stockMovementLine.getStockMovement().getNumMovement());
			movIn.setQtReceived(stockMovementLine.getMovmentQte());
			movIn.setReceptionDate(stockMovementLine.getStockMovement().getDateMovement());
			movIn.setReferenceBl(stockMovementLine.getStockMovement().getReferenceType().getLabel());
			movementJasper.getMovementsIn().add(movIn);
		}
	}
	
	
	
	private int getIndiceProduct(List<MovementJasper> movementsJasper , String productCode){
		for(MovementJasper movJasper : movementsJasper){
			if(movJasper.getBarCode().equals(productCode)){
				return movementsJasper.indexOf(movJasper);
			}
		}

		return -1 ; 
	}
	
	
	private File getPdfMovements(List<MovementJasper> list , DateRequestPdf date) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
		return jasperService.jasperFileWithDateParameter(dataSource, "movementByProduct", date.getStart(), date.getEnd());
	}
	
	private void adjustMovementJasper(List<MovementJasper> movementsJasper){
		movementsJasper.forEach(movementJasper->{
			if(movementJasper.getMovementsIn().size()>movementJasper.getMovementsOut().size()){
				while(movementJasper.getMovementsIn().size()!=movementJasper.getMovementsOut().size()){
					movementJasper.getMovementsOut().add(new MovementOutJasper());
				}
			}else if(movementJasper.getMovementsIn().size()<movementJasper.getMovementsOut().size()){
				while(movementJasper.getMovementsIn().size()!=movementJasper.getMovementsOut().size()){
					movementJasper.getMovementsIn().add(new MovementInJasper());
				
				}
			}
		});
	}
	
	private void calculTotal(List<MovementJasper> movements){
		movements.forEach(mov->{
			mov.getMovementsIn().forEach(movIn->{
				if(movIn.getQtReceived()!=null){
					mov.setTotalIn(mov.getTotalIn()+movIn.getQtReceived());
				}
			});
			mov.getMovementsOut().forEach(movOut->{
				if(movOut.getDeliveredQt()!=null){
					mov.setTotalOut(mov.getTotalOut()+movOut.getDeliveredQt());
				}
			});
		});
	}
	@Override
	public List<MovementJasper> getMovementsFromPdf(DateRequestPdf date) {
		List<StockMovement> stockMovements = this.stockMovementRepository.findBydateMovementBetween(date.getStart(), date.getEnd());
		List<MovementJasper> movementsJasper = new ArrayList<>();
		
		stockMovements.forEach(movement->{
			movement.getStockMovementLine().forEach(movementLine->{
				if(movementsJasper
						.stream()
						.anyMatch(movJasper->movJasper.getBarCode().equals(movementLine.getProduct().getBarCode()))
				){
					MovementJasper movJasper =  movementsJasper.get(this.getIndiceProduct(movementsJasper, movementLine.getProduct().getBarCode()));
					try {
						this.setMovementJasper(movementLine, movJasper);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}else{
					MovementJasper movJasper = new MovementJasper() ; 
					movJasper.setBarCode(movementLine.getProduct().getBarCode());
					movJasper.setCodePct(movementLine.getProduct().getCodePctProd());
					movJasper.setDesignation(movementLine.getProduct().getLib());
					try {
						this.setMovementJasper(movementLine, movJasper);
					} catch (IOException e) {
						e.printStackTrace();
					}
					movementsJasper.add(movJasper);
				}
			});
		});
		
		this.adjustMovementJasper(movementsJasper);
		this.calculTotal(movementsJasper);
		return movementsJasper;
	}
	

	
	@Override
	public StockMvtfilter filterExceptionalStockMovement(StockMovementFilter stockMovementFilter) {
		
		JPQLQuery<?> query = new JPAQuery(em); 
        
        QStockMovement stockMovement = QStockMovement.stockMovement;
        QStockMovementLine stockMovementLine = QStockMovementLine.stockMovementLine;
       
        query.from(stockMovementLine).orderBy(stockMovement.dateMovement.desc())
                      .leftJoin(stockMovementLine.stockMovement , stockMovement);
        
         if(stockMovementFilter.getMinDate()!=null){
            query.where(stockMovement.dateMovement.after(stockMovementFilter.getMinDate()));  
	     }
         
	     if(stockMovementFilter.getMaxDate()!=null){
	   	  query.where(stockMovement.dateMovement.before(stockMovementFilter.getMaxDate()));
	     }
	     
	     if(stockMovementFilter.getProductId()!=null){
            query.where(stockMovementLine.product.id.in(stockMovementFilter.getProductId()));  
	      }
	     
	      if(stockMovementFilter.getNumMovement()!=null){
	        query.where(stockMovement.numMovement.eq(stockMovementFilter.getNumMovement()));
	      }
	      
	      if(stockMovementFilter.getMovementId()!=null){  
	             query.where(stockMovement.Id.eq(stockMovementFilter.getMovementId()));      
	      } 
	      
	      if(stockMovementFilter.getReceptionId()!=null){
	          query.where(stockMovement.Id.eq(stockMovementFilter.getReceptionId()));
	      }
	      
	      if(stockMovementFilter.getInternalDeliveryId()!=null){
	          query.where(stockMovement.internalDelivery.Id.eq(stockMovementFilter.getInternalDeliveryId()));
	      }
	      
	      if(stockMovementFilter.getDistributionId()!=null){
	          query.where(stockMovement.distribution.Id.eq(stockMovementFilter.getDistributionId()));
	         
	      } 
	      
	      if(stockMovementFilter.getReferenceTypeId()!=null){
	          query.where(stockMovement.referenceType.id.eq(stockMovementFilter.getReferenceTypeId()));
	     
	      }
	      
	      if(stockMovementFilter.getDayId()!=null){
	    	  query.where(stockMovement.day.id.eq(stockMovementFilter.getDayId()));
	      }
	      
	      if(stockMovementFilter.getDescriminator()!=null){
	    	  query.where(stockMovement.descriminator.eq(stockMovementFilter.getDescriminator()));
	    	  
	      }
	      
	      if(stockMovementFilter.getReceptionId()!=null){
	    	  query.where(stockMovement.reception.Id.eq(stockMovementFilter.getReceptionId()));
	      }
	      
	      if(stockMovementFilter.getTypeMovement()!=null){
	    	  query.where(stockMovement.typeMvt.eq(stockMovementFilter.getTypeMovement()));
	      }
	      
	      query.distinct().orderBy(stockMovement.dateMovement.desc());
	      
	      
	      if(stockMovementFilter.getPage()!=null){
	    	  query.offset((stockMovementFilter.getPage()-1)*15);
	    	  query.limit(15);	    	
	      }
     
	      StockMvtfilter stockMvtfilter = new StockMvtfilter();
	      stockMvtfilter.setStockmvts( query.select(stockMovement).fetch());
	      stockMvtfilter.setTotalItemes(query.fetchCount());
	      
		return stockMvtfilter;
	}
	@Override
	public Blob getMvtBlob(DateRequestPdf date) throws FileNotFoundException, SerialException, SQLException {
		
		File f = this.getPdfMovements(this.getMovementsFromPdf(date), date);

		byte[] data = new byte[(int) f.length()];
		Blob blob = new javax.sql.rowset.serial.SerialBlob(data);
		System.out.println(blob.getBytes(1, 100));
		return blob;
	}
	
	
	//--------------------------------------------------stk-mvt-jasper-------------------------------------------------------------------------
	
	
		@Override
		public List<MovementsJaspersResponse>  filterExceptionalStockMovements(StockMovementFilter stockMovementFilter) {
			
			
			JPQLQuery<?> query = new JPAQuery(em); 
			System.out.println("_________________________________________________________filterExceptionalStockMovements_______dd___________________________________________________________________");
	        QStockMovement stockMovement = QStockMovement.stockMovement;
	        QStockMovementLine stockMovementLine = QStockMovementLine.stockMovementLine;
	        //    .groupBy(stockMovementLine.product,stockMovementLine.id).orderBy(stockMovementLine.product.id.desc())
                query.from(stockMovementLine).where(stockMovementLine.movmentQte.gt(0)).orderBy(stockMovementLine.id.asc())
	                      .leftJoin(stockMovementLine.stockMovement , stockMovement);
	       
	         if(stockMovementFilter.getMinDate()!=null){
	            query.where(stockMovement.dateMovement.after(stockMovementFilter.getMinDate()));  
		     }
	        
		     if(stockMovementFilter.getMaxDate()!=null ){
		   	  query.where(stockMovement.dateMovement.before(stockMovementFilter.getMaxDate()));
		     }
		     ///stockMovementFilter.getProductId()!=null
		     if( stockMovementFilter.getProductId()!=null && stockMovementFilter.getProductId().isEmpty() != true ){
		    	 
	            query.where(stockMovementLine.product.id.in(stockMovementFilter.getProductId()));  
		      }
		     
		     if(stockMovementFilter.getUserId()!=null ){
		            query.where(stockMovement.utilisateur.id.eq(stockMovementFilter.getUserId()));  
			      }
		     
		      if(stockMovementFilter.getNumMovement()!=null){
		        query.where(stockMovement.numMovement.eq(stockMovementFilter.getNumMovement()));
		      }
		      
		      if(stockMovementFilter.getLocaleId()!=null){
			        query.where(stockMovement.location.id.eq(stockMovementFilter.getLocaleId()));
			      }
		      if(stockMovementFilter.getMovementId()!=null){  
		             query.where(stockMovement.Id.eq(stockMovementFilter.getMovementId()));      
		      } 
		      
		      if(stockMovementFilter.getReceptionId()!=null){
		          query.where(stockMovement.Id.eq(stockMovementFilter.getReceptionId()));
		      }
		      
		      if(stockMovementFilter.getInternalDeliveryId()!=null){
		          query.where(stockMovement.internalDelivery.Id.eq(stockMovementFilter.getInternalDeliveryId()));
		      }
		      
		      if(stockMovementFilter.getDistributionId()!=null){
		          query.where(stockMovement.distribution.Id.eq(stockMovementFilter.getDistributionId()));
		         
		      } 
		      
		      if(stockMovementFilter.getReferenceTypeId()!=null){
		          query.where(stockMovement.referenceType.id.eq(stockMovementFilter.getReferenceTypeId()));
		     
		      }
		      
		      if(stockMovementFilter.getDayId()!=null){
		    	  query.where(stockMovement.day.id.eq(stockMovementFilter.getDayId()));
		      }
		      
		      if(stockMovementFilter.getDescriminator()!=null){
		    	  query.where(stockMovement.descriminator.eq(stockMovementFilter.getDescriminator()));
		    	  
		      }
		      
		      if(stockMovementFilter.getReceptionId()!=null){
		    	  query.where(stockMovement.reception.Id.eq(stockMovementFilter.getReceptionId()));
		      }
		      
		      if(stockMovementFilter.getTypeMovement()!=null){
		    	  query.where(stockMovement.typeMvt.eq(stockMovementFilter.getTypeMovement()));
		      }

		      query.distinct();
		      
              System.out.println("_________________________________MovementsJaspersResponse________________________________________");

              
             if(stockMovementFilter.getExpress()== true) {
		      List<MovementsJaspersResponse> listMvtsJaspersResponse = new ArrayList<>();
		     
		
		    	  for( StockMovementLine stkMvtLine :query.select(stockMovementLine).fetch()){ 
		    		 
  
//		    	    if((stkMvtLine.getStockMovement().getTypeMvt().equals("1")== true && stkMvtLine.getStockMovement().getLocation()== locationRepository.
//		    					  findOne(stockMovementFilter.getLocaleId())
//			    				  &&stkMvtLine.getMovmentQte()>0)||
//			    		          ((stkMvtLine.getStockMovement().getTypeMvt().equals("-1")== true && stkMvtLine.getStockMovement().getLocation()== locationRepository.
//			    		          findOne(stockMovementFilter.getLocaleId())
//			    				  &&stkMvtLine.getMovmentQte()>0))){
		    			  
		    		  MovementsJaspersResponse  movementsJaspersResponse = new MovementsJaspersResponse();
		  
		    		 // if(stkMvtLine.getStockMovement().getDateMovement()!= null){  
		    		  movementsJaspersResponse.setDateMvt(stkMvtLine.getStockMovement().getDateMovement()); 
		    		 // }
		    		 // if(stkMvtLine.getProduct()!= null){
		    		  movementsJaspersResponse.setCodePct(stkMvtLine.getProduct().getCodePctProd());
		    		 // }
		    		 
		    		  movementsJaspersResponse.setDesignation(stkMvtLine.getProduct().getLib()); 
		    		  
		    		  if(stkMvtLine.getStockMovement().getReferenceType()!= null){
		    		  
		    		  movementsJaspersResponse.setRefference(referenceTypeRepository.findOne(stkMvtLine.getStockMovement().getReferenceType().getId()).getLabel()); 
		    		 
		    			  
		    		  if (stkMvtLine.getStockMovement().getDistribution()!=null) {
		    			  
		    			  movementsJaspersResponse.setRefference(referenceTypeRepository.findOne(stkMvtLine.getStockMovement().getReferenceType().getId()).getLabel()+":"+stkMvtLine.getStockMovement().getDistribution().getPrescription().getAgentId());
		    			  
		    		  }
		    		  }
		    		  
		    		  if(stkMvtLine.getStockMovement().getUtilisateur()!= null){
		    		  String lastfirstname = utilisateurRepository.findOne(stkMvtLine.getStockMovement().getUtilisateur().getId()).getLastname()+"."
                      +utilisateurRepository.findOne(stkMvtLine.getStockMovement().getUtilisateur().getId()).getFirstname().toUpperCase();
		    		  movementsJaspersResponse.setUser(lastfirstname);
		    		  }
	    		  
		    		  //*****quantite stock initial
		    		  
		    		
		    		  if (inventoryLineRepository.findByProductIdAndInventoryDateInvAndInventoryLocationId(stkMvtLine.getProduct().getId(),
	    					  stockMovementFilter.getMinDate(),stockMovementFilter.getLocaleId()) != null){    
		    			
		    			  movementsJaspersResponse.setInitialStock( inventoryLineRepository.findByProductIdAndInventoryDateInvAndInventoryLocationId(stkMvtLine.getProduct().getId(),
		    					  stockMovementFilter.getMinDate(),stockMovementFilter.getLocaleId()).getInventQte());
		    			  
		    		  }else {
		    			

		    			  movementsJaspersResponse.setInitialStock(0d);
		    			  
		    		  }
		    		  //**************************************************
		    		  if(stkMvtLine.getStockMovement().getTypeMvt().equals("1")== true ){
		    		
		    		  movementsJaspersResponse.setEnterQte(stkMvtLine.getMovmentQte());
		  	  
		    		  }
		    		  if(stkMvtLine.getStockMovement().getTypeMvt().equals("-1")== true ){
		    		
		    		   movementsJaspersResponse.setOutQte(stkMvtLine.getMovmentQte());
		    		  
		    		  } 
		    		 
		    		  
		    		 listMvtsJaspersResponse.add(movementsJaspersResponse);}
            
		    	  
			       return listMvtsJaspersResponse;
			       
             }
		
			
			
             else {
			      List<MovementsJaspersResponse> listMvtsJaspersResponse = new ArrayList<>();
			     
			     // InventoryLine inventoryLine = inventoryLineRepository.findByInventoryDateInv( stockMovementFilter.getMinDate());
			      
			      
			    	  for( StockMovementLine stkMvtLine :query.select(stockMovementLine).fetch()){ 
			    		 
	  
//			    	    if((stkMvtLine.getStockMovement().getTypeMvt().equals("1")== true && stkMvtLine.getStockMovement().getLocation()== locationRepository.
//			    					  findOne(stockMovementFilter.getLocaleId())
//				    				  &&stkMvtLine.getMovmentQte()>0)||
//				    		          ((stkMvtLine.getStockMovement().getTypeMvt().equals("-1")== true && stkMvtLine.getStockMovement().getLocation()== locationRepository.
//				    		          findOne(stockMovementFilter.getLocaleId())
//				    				  &&stkMvtLine.getMovmentQte()>0))){
			    			  
			    		  MovementsJaspersResponse  movementsJaspersResponse = new MovementsJaspersResponse();
			  
			    		 // if(stkMvtLine.getStockMovement().getDateMovement()!= null){  
			    		  movementsJaspersResponse.setDateMvt(stkMvtLine.getStockMovement().getDateMovement()); 
			    		 // }
			    		 // if(stkMvtLine.getProduct()!= null){
			    		  movementsJaspersResponse.setCodePct(stkMvtLine.getProduct().getCodePctProd());
			    		 // }
			    		 
			    		  movementsJaspersResponse.setDesignation(stkMvtLine.getProduct().getLib()); 
			    		  
			    		  if(stkMvtLine.getStockMovement().getReferenceType()!= null){
			    		  
			    		  movementsJaspersResponse.setRefference(referenceTypeRepository.findOne(stkMvtLine.getStockMovement().getReferenceType().getId()).getLabel()); 
			    		 
			    			  
			    		  if (stkMvtLine.getStockMovement().getDistribution()!=null) {
			    			  
			    			  movementsJaspersResponse.setRefference(referenceTypeRepository.findOne(stkMvtLine.getStockMovement().getReferenceType().getId()).getLabel()+":"+stkMvtLine.getStockMovement().getDistribution().getPrescription().getAgentId());
			    			  
			    		  }
			    		  }
			    		  
//			    		  if(stkMvtLine.getStockMovement().getUtilisateur()!= null){
//			    		  String lastfirstname = utilisateurRepository.findOne(stkMvtLine.getStockMovement().getUtilisateur().getId()).getLastname()+"."
//	                      +utilisateurRepository.findOne(stkMvtLine.getStockMovement().getUtilisateur().getId()).getFirstname().toUpperCase();
//			    		  movementsJaspersResponse.setUser(lastfirstname);
//			    		  }
		    		  
			    		  //*****quantite stock initial
			    		  
			    		
			    		 // if (inventoryLine == null){    
			    			  movementsJaspersResponse.setInitialStock(0d);
			    		 // }else {

			    			//  movementsJaspersResponse.setInitialStock( inventoryLineRepository.findByProductIdAndInventoryDateInvAndInventoryLocationId(stkMvtLine.getProduct().getId(),
			    			//		  stockMovementFilter.getMinDate(),stockMovementFilter.getLocaleId()).getInventQte());
			    			  
			    		 // }
			    		  //**************************************************
			    		  if(stkMvtLine.getStockMovement().getTypeMvt().equals("1")== true ){
			    		
			    		  movementsJaspersResponse.setEnterQte(stkMvtLine.getMovmentQte());
			  	  
			    		  }
			    		  if(stkMvtLine.getStockMovement().getTypeMvt().equals("-1")== true ){
			    		
			    		   movementsJaspersResponse.setOutQte(stkMvtLine.getMovmentQte());
			    		  
			    		  } 
			    		 
			    		  
			    		  listMvtsJaspersResponse.add(movementsJaspersResponse);}
	            
			    	  
				       return listMvtsJaspersResponse;
				       
	             }
		}

		@Override
		public File getPdfStockMvt(StockMovementFilter stockMovementFilter) throws FileNotFoundException{
			System.out.println("________________getPdfStockMvt____________________");
			return this.getPdf(this.filterExceptionalStockMovements(stockMovementFilter));
		}

		
		private File getPdf(List<MovementsJaspersResponse> list) throws FileNotFoundException{
			JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
			System.out.println("____stock------------mvt______");
			return jasperService.jasperFile(dataSource, "stockmvt");
			
		}
		
		
		
		@Override
		public File getXlsxStockMvt(StockMovementFilter stockMovementFilter) throws FileNotFoundException{
			System.out.println("-----------f--gg3---------");
			return this.getXlsx(this.filterExceptionalStockMovements(stockMovementFilter));
		}
		
	
		private File getXlsx(List<MovementsJaspersResponse> list) throws FileNotFoundException{
			JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
			System.out.println("-----------pdf-f2-------");
			return jasperService.jasperFileXlsx(dataSource, "stockmvt");
			
		}
		
	
}
