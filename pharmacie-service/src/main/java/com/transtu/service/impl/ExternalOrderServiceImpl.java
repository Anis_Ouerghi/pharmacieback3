package com.transtu.service.impl;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.transtu.core.dto.ExternalOrderLineDto;
import com.transtu.core.json.request.ExternaOrderRequest;
import com.transtu.core.json.request.ExternalOrderFilter;
import com.transtu.core.json.request.ExternalOrderFilterResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateExternalOrderListRequest;
import com.transtu.core.json.request.UpdateExternalOrderRequest;
import com.transtu.persistence.entities.Day;
import com.transtu.persistence.entities.ExternalOrder;
import com.transtu.persistence.entities.ExternalOrderLine;
import com.transtu.persistence.entities.Location;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.QExternalOrder;
import com.transtu.persistence.entities.QExternalOrderLine;
//import com.transtu.persistence.entities.QExternalOrder;
//import com.transtu.persistence.entities.QExternalOrderLine;
import com.transtu.persistence.entities.State;
import com.transtu.persistence.repositories.DayRepository;
import com.transtu.persistence.repositories.ExternalOrderLineRepository;
import com.transtu.persistence.repositories.ExternalOrderRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.StateRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.ExternalOrderService;
import com.transtu.service.SequenceService;



 

@Service
public class ExternalOrderServiceImpl implements ExternalOrderService{
	@PersistenceContext
	EntityManager em ;
	
	@Autowired
	LocationRepository LocationRepository;
	
	@Autowired
	ProductRepository productRepository;
	@Autowired
	ExternalOrderLineRepository externalOrderLineRepository; 
	@Autowired
	ExternalOrderRepository externalOrderRepository;
	@Autowired
	SequenceService sequenceService;
	@Autowired
	DayRepository dayRepository;
	@Autowired 
	StateRepository StateRepository;
	@Override
	public MessageResponse addExternalOrder(ExternaOrderRequest externaOrderRequest) {
		
		Location location= this.LocationRepository.findOne(externaOrderRequest.getLocationId());
		State state = this.StateRepository.findOne(1l);
		if(location==null){
			RestExceptionCode code = RestExceptionCode.LOCATION_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		if(state==null){
			RestExceptionCode code = RestExceptionCode.STATE_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		Day day = dayRepository.findOne(externaOrderRequest.getDayId());
		if(day==null ){
			RestExceptionCode code = RestExceptionCode.DAY_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		ExternalOrder externalOrder = new ExternalOrder();
		externalOrder.setLocation(location);
		externalOrder.setState(state);
		externalOrder.setDateExterOrder(new Date());
		externalOrder.setNumExternalOrder(sequenceService.calculateNumExternalOrder());
		externalOrder.setDay(day);
		externalOrderRepository.save(externalOrder);
		for (ExternalOrderLineDto externalOrderLineDto : externaOrderRequest.getExternalOrderLinesDto() ){
			Product product = productRepository.findOne(externalOrderLineDto.getProductId());
			if(product==null){
				RestExceptionCode code = RestExceptionCode.PRODUCT_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			if(externalOrderLineDto.getOrderQte()==0){
				RestExceptionCode code = RestExceptionCode.QUANTITY_NOT_FOUND;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			
			ExternalOrderLine externalOrderLine  = new ExternalOrderLine();
			externalOrderLine.setExternalOrder(externalOrder);
			externalOrderLine.setProduct(product);
			externalOrderLine.setPrice(externalOrderLineDto.getPrice());;
			externalOrderLine.setOrderQte(externalOrderLineDto.getOrderQte());
			externalOrderLine.setVatRate(externalOrderLineDto.getVatRate());;
			externalOrderLineRepository.save(externalOrderLine);
			
		}	
		return new  MessageResponse(HttpStatus.OK.toString(), null, "la Commande Externe N°"+externalOrder.getNumExternalOrder()+"a été ajoutée avec succés",externalOrder.getNumExternalOrder());
		 
	}	
	
	
	public MessageResponse CancelExternalOrder(Long externalOrderId ){
		ExternalOrder externalOrder = this.externalOrderRepository.findOne(externalOrderId);
		if(externalOrder==null){
			RestExceptionCode code = RestExceptionCode.EXTERNAL_ORDER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		State state =this.StateRepository.findByType("annullé");
		if(state==null){
			RestExceptionCode code = RestExceptionCode.STATE_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		externalOrder.setState(state);
		externalOrderRepository.save(externalOrder);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Commande Externe été annulée avec succés",externalOrder.getNumExternalOrder());	
			
	}
	
	public MessageResponse UpdateExternalOrder(UpdateExternalOrderRequest updateExternalOrderRequest ){
		
		ExternalOrder externalOrder = externalOrderRepository.
				findOne(updateExternalOrderRequest.getExternalOrderId());
		if(externalOrder==null){
			RestExceptionCode code = RestExceptionCode.EXTERNAL_ORDER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex ;
		}
		 if(externalOrder.getState().getId()==4){
				RestExceptionCode code = RestExceptionCode.EXTERNAL_ORDER_ALREADY_DELIVRED;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
		for(ExternalOrderLineDto externalOrderLineDto : updateExternalOrderRequest.getExternalOrderLinesDto()){
			Product product = productRepository.findOne(externalOrderLineDto.getProductId());
			ExternalOrderLine externalOrderLine = externalOrderLineRepository.
			findByProductAndExternalOrder(product, externalOrder);
			
			if(externalOrderLine ==null){
				ExternalOrderLine externalOrderline = new ExternalOrderLine();
				externalOrderline.setExternalOrder(externalOrder);
				externalOrderline.setProduct(product);
				externalOrderline.setOrderQte(externalOrderLineDto.getOrderQte());
				externalOrderline.setPrice(externalOrderLineDto.getPrice());
				externalOrderline.setVatRate(externalOrderLineDto.getVatRate());
				externalOrderLineRepository.save(externalOrderline);
			} else {
				externalOrderLine.setOrderQte(externalOrderLineDto.getOrderQte());
				externalOrderLineRepository.save(externalOrderLine);
			}
		}
		 

		
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Commande Externe été modifiéé avec succés",externalOrder.getNumExternalOrder());	
}
	
public MessageResponse UpdateExternalOrderList(UpdateExternalOrderListRequest updateExternalOrderListRequest ){
		
		ExternalOrder externalOrder = externalOrderRepository.
				findOne(updateExternalOrderListRequest.getExternalOrderId());
		if(externalOrder==null){
			RestExceptionCode code = RestExceptionCode.EXTERNAL_ORDER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex ;
		}
		 if(externalOrder.getState().getId()==4){
				RestExceptionCode code = RestExceptionCode.EXTERNAL_ORDER_ALREADY_DELIVRED;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
		for(ExternalOrderLineDto externalOrderLineDto : updateExternalOrderListRequest.getExternalOrderLinesDto()){
			Product product = productRepository.findOne(externalOrderLineDto.getProductId());
			ExternalOrderLine externalOrderLine = externalOrderLineRepository.
			findByProductAndExternalOrder(product, externalOrder);
			
			if(externalOrderLine ==null){
				ExternalOrderLine externalOrderline = new ExternalOrderLine();
				externalOrderline.setProduct(product);
				externalOrderline.setOrderQte(externalOrderLineDto.getOrderQte());
				externalOrderLineRepository.save(externalOrderline);
			} else {
				externalOrderLine.setOrderQte(externalOrderLineDto.getOrderQte());
				externalOrderLineRepository.save(externalOrderLine);
			}
		}
		 

		
		return new MessageResponse(HttpStatus.OK.toString(), null, "la Commande Externe été modifiéé avec succés",externalOrder.getNumExternalOrder());	
}
	
	@Override
	public ExternalOrderFilterResponse filterExternalOrder(ExternalOrderFilter externalOrderFilter) {
		JPQLQuery<?> query = new JPAQuery(em);
		QExternalOrder externalOrder = QExternalOrder.externalOrder;
		QExternalOrderLine externalOrderLine = QExternalOrderLine.externalOrderLine;
		
		
		query.from(externalOrderLine)
		.leftJoin(externalOrderLine.externalOrder , externalOrder).orderBy(externalOrder.dateExterOrder.desc());
		
		if(externalOrderFilter.getMinDate()!=null){
			query.where(externalOrder.dateExterOrder.after(externalOrderFilter.getMinDate()));
		}
		
		if(externalOrderFilter.getMaxDate()!=null){
			query.where(externalOrder.dateExterOrder.before(externalOrderFilter.getMaxDate()));
		}
	
		if(externalOrderFilter.getProductId()!=null){
			query.where(externalOrderLine.product.id.in(externalOrderFilter.getProductId()));
		}
		if(externalOrderFilter.getNumExternalOrder()!=null){
			query.where(externalOrder.numExternalOrder.eq(externalOrderFilter.getNumExternalOrder()));
		}			
		if(externalOrderFilter.getExternalOrderId()!=null){
			query.where(externalOrder.Id.eq(externalOrderFilter.getExternalOrderId()));
		}
		
		if(externalOrderFilter.getStateId()!=null){
			query.where(externalOrder.state.id.eq(externalOrderFilter.getStateId()));
		}
		
		query.where(externalOrder.state.id.ne((long) 6));
		
		query.distinct();
		if(externalOrderFilter.getPage()!=null){
			if(externalOrderFilter.getLinesNbr()==null){
				RestExceptionCode code = RestExceptionCode.LINES_NUMBER_REQUIRED_IN_PAGINATION;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			
			query.offset((externalOrderFilter.getPage()-1)*externalOrderFilter.getLinesNbr());
			query.limit(externalOrderFilter.getLinesNbr());
			
		}
		
	
		List<ExternalOrder> list = query.select(externalOrder).fetch();		
		List<ExternalOrder> listexord= new ArrayList<>(); 
		for(ExternalOrder  exord:  query.select(externalOrder).fetch()){
				exord.getExternalOrderLines().sort(new Comparator<ExternalOrderLine>() {
				@Override
				public int compare(ExternalOrderLine o1, ExternalOrderLine o2) {
					int comparaison =0;
					comparaison=o1.getProduct().getLib().compareToIgnoreCase(o2.getProduct().getLib()) ;
			
					return comparaison;
				}
			});
			
			listexord.add(exord);	
		}
		
		//listexord.stream().filter(l->l.getExternalDelivery()==null).forEach(l->l.setExternalDelivery(new ExternalDelivery()));
		ExternalOrderFilterResponse externalOrderFilterResponse = new ExternalOrderFilterResponse();
		externalOrderFilterResponse.setExternalOrders(list);
		externalOrderFilterResponse.setTotalExternalOrders(query.select(externalOrder).fetchCount());
		if(externalOrderFilter.getPage()!=null){
			
			if(externalOrderFilter.getPage()!=null){
				if((query.select(externalOrder).fetchCount())%externalOrderFilter.getLinesNbr()==0) {
					externalOrderFilterResponse.setPage(externalOrderFilter.getPage());
					externalOrderFilterResponse.setNbrPages((query.select(externalOrder).fetchCount())/externalOrderFilter.getLinesNbr());
					externalOrderFilterResponse.setTotalExternalOrders((query.select(externalOrder).fetchCount()));
				}else {	
				
					externalOrderFilterResponse.setPage(externalOrderFilter.getPage());
					externalOrderFilterResponse.setNbrPages(((query.select(externalOrder).fetchCount())/externalOrderFilter.getLinesNbr())+1);	
					externalOrderFilterResponse.setTotalExternalOrders((query.select(externalOrder).fetchCount()));
				}
			}
//			externalOrderFilterResponse.setPage(externalOrderFilter.getPage());
//			externalOrderFilterResponse.setNbrPages(externalOrderFilterResponse.getTotalExternalOrders()/externalOrderFilter.getLinesNbr());
		}
		return externalOrderFilterResponse; 
	}
	@Transactional
	@Override
	public MessageResponse changeStateExternalOrder(Long externalOrderId,Long stateId ) {
		String msg = null;
		
		//stateId : 1---->brouillon--*
		//stateId : 2---->valider--*
		//stateId : 3---->annuller--*
		//stateId : 4---->livrer--*
		//stateId : 5---->livrer partiellement--*
		//stateId : 6---->supprimer--*
		//stateId : 7---->close--*
		//stateId : 8---->open--*
		
		ExternalOrder externalOrder = externalOrderRepository.findOne(externalOrderId);
				
		
		if(externalOrder==null){
			RestExceptionCode code = RestExceptionCode.EXTERNAL_ORDER_NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		
		 if(externalOrder.getState().getId()==4){
				RestExceptionCode code = RestExceptionCode.EXTERNAL_ORDER_ALREADY_DELIVRED;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
		 if (stateId==2){
		 externalOrder.setState(StateRepository.findOne(stateId));
		 externalOrderRepository.save(externalOrder);
		 msg= "la commande Externe a été valider avec succés";
		 }
		 if (stateId==3){
			 externalOrder.setState(StateRepository.findOne(stateId));
			 externalOrderRepository.save(externalOrder);
		 msg="la commande Externe a été annuler avec succés";
		
		 }
		 else{
			 State state = this.StateRepository.findOne(stateId);
			 if(state == null){
				 RestExceptionCode code = RestExceptionCode.STATE_NOT_FOUND;
					RestException ex = new RestException(code.getError(), code);
					throw ex;
			 }
			 externalOrder.setState(state);
			 this.externalOrderRepository.save(externalOrder);
		 }
		 return new MessageResponse(HttpStatus.OK.toString(), null, "le status a été changé avec succes",externalOrder.getNumExternalOrder());	
		 
	}
	
	
	@Transactional
	@Override
	public MessageResponse deleteExternalOrderLine(Long externalOrderId) {
		
	ExternalOrderLine externalOrderLine =externalOrderLineRepository.findOne(externalOrderId);
	
	 if(externalOrderLine == null){
		 RestExceptionCode code = RestExceptionCode.EXTERNAL_ORDER_LINE__NOT_FOUND;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
	 }
	 
	 externalOrderLineRepository.delete(externalOrderLine);
	 return new MessageResponse(HttpStatus.OK.toString(), null, "la ligne commande externe "
	 		+ "a été supprimé avec succes ",externalOrderLine.getId().toString());	
	
	}
	
	}
