package com.transtu.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.AddRoleRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.Role;
import com.transtu.persistence.repositories.RoleRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.RoleService;

@Service
public class RoleServiceImpl  implements RoleService{
	

	private final static Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);
	@Autowired
	RoleRepository roleRepository;
	
	@Override
	public MessageResponse addRole(AddRoleRequest roleRequest) {
		logger.info("AddRoleRequest ===>" + roleRequest.toString());
		Role role1=roleRepository.findByRole(roleRequest.getRole());
		if(role1!=null){
			RestExceptionCode code = RestExceptionCode.ROLE_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Role role = new Role();
		role.setDiscription(roleRequest.getDiscription());
		role.setRole(roleRequest.getRole());
		roleRepository.save(role);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Le Role a été ajouté avec succés",role.getRole());
		
	}
	
	@Override
	public List<Role> getListRoles() {
		logger.info("___________get list products_______________");
		return this.roleRepository.findAll();
	}
	
	@Override
	public MessageResponse deleteRole(Long Id) {
		logger.info("DeleteRoleRequest ===>");
		Role role = roleRepository.findOne(Id);
		roleRepository.delete(role);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Le Role a été suprimé avec succés",role.getRole());

}
	
	@Override
	public MessageResponse updateRole(AddRoleRequest roleRequest) {
		Role role1=roleRepository.findByRole(roleRequest.getRole());
		if(role1!=null){
			RestExceptionCode code = RestExceptionCode.ROLE_EXISTANT;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
		Role role =roleRepository.findOne(roleRequest.getId());
		role.setDiscription(roleRequest.getDiscription());
		role.setRole(roleRequest.getRole());
		roleRepository.save(role);
		return new MessageResponse(HttpStatus.OK.toString(), null, "Le Role a été modifié avec succés",role.getRole());

}
}