package com.transtu.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.transtu.core.jasper.StockLotJasper;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateLotRequest;
import com.transtu.persistence.entities.Lot;
import com.transtu.persistence.entities.QLot;
import com.transtu.persistence.repositories.LotRepository;
import com.transtu.service.JasperService;
import com.transtu.service.LotService;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
@Service
public class LotServiceImpl implements LotService {
	
	@PersistenceContext
	EntityManager em ;
	
	@Autowired(required = true)
	LotRepository lotRepository ; 

	
	@Autowired(required = true)
	JasperService jasperService;
	
	@Override
	public List<Lot> findAllLots() {
		return this.lotRepository.findAll();
	}

	
	@Override
	public MessageResponse updateLot(Long id, boolean state) {
		
		Lot lot =lotRepository.findOne(id);
		lot.setState(state);
		lotRepository.save(lot);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Le lot est modifier avec succé",lot.getLabel());
	}
	

	
	@Override
	public MessageResponse updateDateLabelLot(UpdateLotRequest Oldlot) {
		
		Lot lot =lotRepository.findOne(Oldlot.getId());
		
		lot.setLabel(Oldlot.getLabel());
		lot.setDateRefusal(Oldlot.getDateRefusal());
		lotRepository.save(lot);
		
		return new MessageResponse(HttpStatus.OK.toString(), null, "Le lot est modifier avec succé",lot.getLabel());
	}
	
	
	@Override
	public List<Lot> ListActivatelLot() {
		
		return 	lotRepository.findByState(true);
	}
	

	
	
	@Override
	public List<Lot> listLotBetwenDate(DateRequestPdf dateRequestPdf ) {
		
    JPQLQuery<?> query = new JPAQuery(em); 	
    QLot lot = QLot.lot;
     
    System.out.println("--------------------------****"+dateRequestPdf.getEnd());
//    Calendar c = Calendar.getInstance();
//	c.setTime(minDate);
//	//c.add(Calendar.DATE, -1);
//	c.add(Calendar.DATE, 0);
//	Date minDate1 = c.getTime();
//	
//	  Calendar c1 = Calendar.getInstance();
//		c1.setTime(maxDte);
//		//c.add(Calendar.DATE, -1);
//		c.add(Calendar.DATE, 0);
//		Date maxDte1 = c.getTime();
     List<Lot> L = new ArrayList<>();
     query.from(lot).orderBy(lot.product.lib.asc()).
     where(lot.dateRefusal.after(dateRequestPdf.getStart()).and(lot.dateRefusal.before(dateRequestPdf.getEnd())));
      L = query.select(lot).fetch();
   return L;
	
	}


	
	@Override
	        public File getPdfStockLotMvt(DateRequestPdf DateRequestPdf ) throws FileNotFoundException{
		System.out.println("________________getPdfStocklotMvt____________________");
		                    return this.getPdf(this.getStockLotFromPdf(DateRequestPdf));
	}

	
	private File getPdf(List<StockLotJasper> list) throws FileNotFoundException{
		JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);
		
		return jasperService.jasperFile(dataSource, "stockLot");
		
	}
	
	@Override
	public List<StockLotJasper> getStockLotFromPdf(DateRequestPdf dateRequestPdf) {
	
		List<Lot> lots = new ArrayList<>();
		lots = this.listLotBetwenDate(dateRequestPdf);
		
	       // Location pharmacie =locationRepository.findOne(1l);
	       // Location depot =locationRepository.findOne(2l);
		    List<StockLotJasper> stocksJasper = new ArrayList<>() ;
		    lots.stream().forEach(l->{
			//Stock stock = this.stockRepository.findByProductAndLocation(p, this.locationRepository.findOne(filterStockReport.getStockId()));
		    if(l.getStocklots() !=null && l.getStocklots().size()>0  ) {
		    if(l.isState()==true){
			                            	
		    StockLotJasper stockJasp = new StockLotJasper();
		    stockJasp.setDatemin(dateRequestPdf.getStart());
		    stockJasp.setDatemax(dateRequestPdf.getEnd());
			stockJasp.setCodePct(l.getProduct().getCodePctProd());
			System.out.println("-----lot-pr---"+l.getProduct().getCodePctProd());
			stockJasp.setQtStockDep(l.getStocklots().get(0).getQuantity());
			stockJasp.setDesignation(l.getProduct().getLib());
			if(l.getDateRefusal()!=null) {
			stockJasp.setDateInv(l.getDateRefusal());
			}
			stockJasp.setLot(l.getLabel());
			//stockJasp.setTotalTtc((float) (totalTtc+((stockJasp.getPrice()*totalQte)*p.getVatRate())/100));
			stocksJasper.add(stockJasp);
			}
		    	}
		});
		return stocksJasper;
	}
	
	
	
	//_____________________________________________________________________Périmé________________________________________________________
	
	
	@Override
 public File getPdfPerime( ) throws FileNotFoundException{
System.out.println("________________getPdfStocklotMvt____________________");
  return this.getPdfPerime(this.getPerimeFromPdf());
}


private File getPdfPerime(List<StockLotJasper> list) throws FileNotFoundException{
JRDataSource dataSource = new JRBeanCollectionDataSource(list,false);

return jasperService.jasperFile(dataSource, "stockLot");

}
	
	
	
	
@Override
	public List<Lot> listLotInfDate( ) {
		
    JPQLQuery<?> query = new JPAQuery(em); 	
    QLot lot = QLot.lot;
    List<Lot> L = new ArrayList<>();
    query.from(lot).orderBy(lot.product.lib.asc()).
    where(lot.dateRefusal.before(new Date()).and(lot.state.eq(true)));
    L = query.select(lot).fetch();
    System.out.println("______________________________________________lot size1____________________________________________________"+L.size());
    return L;
	
	}
	
	@Override
	public List<StockLotJasper> getPerimeFromPdf() {
		
		List<Lot> lots = new ArrayList<>();
		lots = this.listLotInfDate();
		    List<StockLotJasper> stocksJasper = new ArrayList<>() ;
		    lots.stream().forEach(l->{
		    if(l.getStocklots() !=null && l.getStocklots().size()>0 ) {                       	
		    StockLotJasper stockJasp = new StockLotJasper();
		    stockJasp.setDatemin(null);
		    stockJasp.setDatemax(new Date());
			stockJasp.setCodePct(l.getProduct().getCodePctProd());
			stockJasp.setQtStockDep(l.getStocklots().get(0).getQuantity());
			stockJasp.setDesignation(l.getProduct().getLib());
			if(l.getDateRefusal()!=null) {
			stockJasp.setDateInv(l.getDateRefusal());
			}
			stockJasp.setLot(l.getLabel());
			
			stocksJasper.add(stockJasp);
			}
		    
		});
		return stocksJasper;
	}



	
}
