package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.DistributionUnit;

public interface DistributionUnitService {

	public MessageResponse addDistributionUnit(String label);
	public MessageResponse updateDistributionUnit(DistributionUnit distUnit);
	public MessageResponse deleteDistributionUnit(Long id);
	public DistributionUnit findById(Long id);
	public DistributionUnit findByLabel(String label);
	public List<DistributionUnit> findAllDistributionUnits();
	
}
