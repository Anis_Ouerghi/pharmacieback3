package com.transtu.service;

import java.util.Date;
import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdatePasswordRequest;
import com.transtu.core.json.request.UserRequest;
import com.transtu.persistence.entities.Utilisateur;

public interface UtilisateurService {

	public MessageResponse addUser(UserRequest userRequest);
	public List<Utilisateur> getListUsers();
	public MessageResponse activateUser(Long Id);
	public MessageResponse desactivateUser(Long Id);
	public MessageResponse deleteUser(Long Id);
	public MessageResponse updateUser(UserRequest userRequest);
	public MessageResponse updatePassword(UpdatePasswordRequest updatePasswordRequest);
	public String getCurrentUserDetails() ;
	public void getListCurrentUserDetails();
	public List<Utilisateur> getListUsersByDay(Long id,  Date date);
	public Utilisateur getCurrentUserDetail();
	public String getCurrentAdresseIp();
	
}
