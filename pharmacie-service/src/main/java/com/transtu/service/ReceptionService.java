package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Set;

import com.transtu.core.jasper.ReceptionResponseJasper;
import com.transtu.core.json.request.ExternaOrdRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.ReceptionFilter;
import com.transtu.core.json.request.ReceptionRequest;
import com.transtu.core.json.request.UpdateReceptionRequest;
import com.transtu.persistence.entities.Reception;
import com.transtu.persistence.entities.ReceptionLine;

public interface ReceptionService {
	
	public MessageResponse AddReception(ReceptionRequest receptionRequest);

	public MessageResponse cancelReception(Long id);

	public MessageResponse updateReception(UpdateReceptionRequest updateReceptionRequest);

	public Set<Reception> filterReception(ReceptionFilter receptionFilter);

	public MessageResponse changeStateReception(Long receptionId, Long stateId);


	public MessageResponse changeStateReception1(Long receptionId, Long stateId, Long externalDeliveryId,
			String extrernalDeliveryNum, Long externalorderId);

	public Reception findByReceptionnNum(Long referenceId, String numReception);

	public MessageResponse cancelRecep(Long id);

	public List<ReceptionLine> getReceptionsJasper(long id);

	public List<ReceptionResponseJasper> getReceptionsJasper(ExternaOrdRequest externaOrdRequest);

	File getPdfListReception(ExternaOrdRequest d) throws FileNotFoundException;

	
	

	
}
