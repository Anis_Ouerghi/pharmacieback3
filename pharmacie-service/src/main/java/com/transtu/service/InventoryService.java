package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import com.transtu.core.jasper.InventoryProductJasper;
import com.transtu.core.jasper.StockJasper;
import com.transtu.core.jasper.StockJasperEcart;
import com.transtu.core.json.request.AddInventoryRequest;
import com.transtu.core.json.request.FilterInventoryResponse;
import com.transtu.core.json.request.InventoryLineRequest;
import com.transtu.core.json.request.InventoryPagination;
import com.transtu.core.json.request.InventoryPdfRequest;
import com.transtu.core.json.request.InventoryRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateInventoryRequest;
import com.transtu.persistence.entities.Inventory;
import com.transtu.persistence.entities.InventoryLine;

public interface InventoryService {

	public MessageResponse addInventory(AddInventoryRequest addInventoryRequest);
	public File getPdfStock(InventoryPdfRequest inventoryPdfRequest) throws FileNotFoundException;
	public List<InventoryProductJasper> getStockFromPdf(InventoryPdfRequest inventoryPdfRequest);
	public List<Inventory> listInventory();
	public Inventory getInventory(Long id);
	public MessageResponse updateInventory(UpdateInventoryRequest updateInventoryRequest);
	public MessageResponse stockRecovery(Long id);
	public InventoryPagination ListInventoryLine(InventoryLineRequest p);
	public HashSet<StockJasper> getInventoryFromPdf(Long idPharma, Long iddepot);
	public File getPdfinventory(Long idPharma, Long iddepo) throws FileNotFoundException;
	public LinkedHashSet<StockJasperEcart> getInventoryEcartFromPdf(Long idPharma, Long iddepot);
	public File getPdfinventoryEcart(Long idPharma, Long idDep) throws FileNotFoundException;
	FilterInventoryResponse filterInventory(InventoryRequest filterProduct);
	
	


}
