package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import com.transtu.core.jasper.MovementJasper;
import com.transtu.core.json.request.DateRequestPdf;

public interface MovementService {
	File getPdfReportAllMovements() throws FileNotFoundException;
	
	File getPdfReportMovementsBetween(DateRequestPdf date) throws FileNotFoundException;
	
	List<MovementJasper> getMovementsBetween(DateRequestPdf date);



}
