package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Dci;

public interface DciService {
	
	public MessageResponse addDci(String dci);
	public MessageResponse deleteDci(Long id);
	public Dci findById(Long id);
	public Dci findByLabel(String label);
	public List<Dci> findAllDci();
	public MessageResponse updateDci(Long id, String label);

}
