package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

import com.transtu.core.jasper.DistributionJasper;
import com.transtu.core.jasper.PresecriptionResponseJasper;
import com.transtu.core.jasper.UserDistStatestiqueJasper;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.core.json.request.DistributionRequest;
import com.transtu.core.json.request.DistributionRequestPdf;
import com.transtu.core.json.request.DistrubtionResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.PresecriptionRequestjasper;
import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.Prescription;
import com.transtu.core.json.request.updateDistrictDistribution;
import com.transtu.core.json.request.updateDoctorPrescription;

public interface DistributionService {
	public DistrubtionResponse addDistribution(DistributionRequest distribution);
    public List<Distribution> listDistributionByAgent(String AgentID);
    public List<Distribution> findByDateDistBetween(Date start , Date end);
    public File getPdfDistributions(DistributionRequestPdf date) throws FileNotFoundException;
	//File getPdfDistributionByAgent(DateRequestPdf date, String agentId) throws FileNotFoundException;
	public List<DistributionJasper> getDistributionsFromPdf(DistributionRequestPdf date ) ; 
	//List<DistributionJasper> getDistributionsByAgentFromPdf(DateRequestPdf date, String agentId) ;
	public Distribution findByDistrubutionNum(Long referenceId, String distrubutionNum);
	public Double getMontantTotalDistAgent(Distribution d);
	public List<DistributionJasper> getDistributionsByAgentFromPdf(DistributionRequestPdf date);
	public File getPdfDistributionByAgent(DistributionRequestPdf date) throws FileNotFoundException;
	public List<DistributionJasper> getDistributionsByAgentSFromPdf(DistributionRequestPdf date);
	public File getPdfDistributionByAgents(DistributionRequestPdf date) throws FileNotFoundException;
	public MessageResponse updateDistrictDistribution(updateDistrictDistribution updateDistrictDistribution);
	public MessageResponse deleteDistributionLine(Long id);
	public MessageResponse deleteDistribution(Long id);
	public List<UserDistStatestiqueJasper> findByDateDistAndUtilisateur(Date start, Date end);
	public File getPdfPdfDistStatestique(Date start, Date end) throws FileNotFoundException;
	public MessageResponse updateDoctorPresrcrption(updateDoctorPrescription updateDoctorPrescript);
	
	
}
