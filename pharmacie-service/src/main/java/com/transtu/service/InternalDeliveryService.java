package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.transtu.core.json.request.InterDeliveryFilterResponse;
import com.transtu.core.json.request.InternalDeliveryFilter;
import com.transtu.core.json.request.InternalDeliveryRequest;
import com.transtu.core.json.request.InternalordfilterResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateDeliveryRequest;
import com.transtu.persistence.entities.InternalDelivery;


public interface InternalDeliveryService {
	
	public MessageResponse addInternalDelivery(InternalDeliveryRequest internalDeliveryRequest);
	public MessageResponse deleteInternalDelivery(Long internalDeliveryId);
	public MessageResponse updateInternalDelviery(UpdateDeliveryRequest updateDeliveryRequest);
	public InterDeliveryFilterResponse filterInternalDelivery(InternalDeliveryFilter internalDeliveryFilter);
	public MessageResponse changeStateInternalDelivery(Long internalOrderId, Long stateId);
	public InternalDelivery findByInternalDeliveryNum(Long referenceId, String Deliverynum);
	List<InternalDelivery> filterJasperInternalDelivery(Long customerId, Date dateMin, Date dateMax);
	LinkedHashSet<com.transtu.core.jasper.DeliveryJasper> getInternalDeliveryFromPdf(Long customerId, Date maxDate, Date minDate);
	File get(Long customerId, Date maxDate, Date minDate) throws FileNotFoundException;
	
}
