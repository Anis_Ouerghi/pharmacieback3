package com.transtu.service;

import java.util.List;



import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.PharmaClass;


public interface PharmaService {

	public MessageResponse addpharmaClass(String label);
	public List<PharmaClass> getListPharmaClass();
	public MessageResponse updatepharmaClass(Long id, String label); 
	public MessageResponse deletepharmaClass(Long id);
		
}
