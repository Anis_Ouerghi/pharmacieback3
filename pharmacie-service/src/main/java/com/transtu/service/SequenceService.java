package com.transtu.service;

public interface SequenceService {
	
	
	public String calculateNumDelivery(String location);
	public String calculateNumOrder();
	public String calculateReceptionOrder();
	public String calculateNumExternalOrder();
	public String calculateNumMvtExepSortie();
	public String calculateNumMvtExepEntre();
	public String calculateNumMvtOrdSortie();
	public String calculateNumMvtOrdEntre();
	String calculatePrescription(String matriculeAgent);
	String calculateNumDest(String matriculeAgent);
}
