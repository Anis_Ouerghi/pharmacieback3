package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.District;

public interface DistrictService {

	public List<District> getListDistrict();
	public MessageResponse addDistrict(District district);
	public MessageResponse updateDistrict(District district);
	public MessageResponse deleteDistrict(Long idDistrict);
	public District findDistrictByLib(String lib);

}
