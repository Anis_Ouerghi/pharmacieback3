package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Presentation;

public interface PresentationService {

	public MessageResponse addpresentation(String label);
	public List<Presentation> getListPresentation();
	public MessageResponse updatePresentation(Long id, String label); 
	public MessageResponse deletePresentation(Long id);

}
