package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;

import net.sf.jasperreports.engine.JRDataSource;

public interface JasperService {


	File jasperFile(JRDataSource dataSource, String object) throws FileNotFoundException;

	File jasperFileWithDateParameter(JRDataSource dataSource, String object, Date start, Date end)
			throws FileNotFoundException;

	File jasperFileXlsx(JRDataSource dataSource, String object) throws FileNotFoundException;

	File jasperFile1(JRDataSource dataSource, String object) throws FileNotFoundException;

	File jasperFileExcelWithDateParameter(JRDataSource dataSource, String object, Date start, Date end)
			throws FileNotFoundException;

	
}
