package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Customer;
import com.transtu.persistence.entities.Presentation;

public interface CustomerService {

	
	public MessageResponse addCustomer(String label);
	public List<Customer> getListCustomer();
	public MessageResponse updateCustomer(Long id, String label); 
	public MessageResponse deleteCustomer(Long id);
}
