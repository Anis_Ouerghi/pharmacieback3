package com.transtu.service;

import java.io.IOException;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.transtu.persistence.entities.Agent;

public interface AgentService  {

	Agent getAgent(String matri) throws JsonParseException, JsonMappingException, IOException;


	Agent getDetailAgent(String matri) throws JsonParseException, JsonMappingException, IOException;


	


}
