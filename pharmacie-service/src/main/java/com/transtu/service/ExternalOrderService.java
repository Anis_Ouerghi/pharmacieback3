package com.transtu.service;

import com.transtu.core.json.request.ExternaOrderRequest;
import com.transtu.core.json.request.ExternalOrderFilter;
import com.transtu.core.json.request.ExternalOrderFilterResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateExternalOrderListRequest;
import com.transtu.core.json.request.UpdateExternalOrderRequest;

public interface ExternalOrderService {

	public MessageResponse addExternalOrder(ExternaOrderRequest externaOrderRequest);
	public MessageResponse CancelExternalOrder(Long externalOrderId );
	public MessageResponse UpdateExternalOrder(UpdateExternalOrderRequest updateExternalOrderRequest );
	public MessageResponse UpdateExternalOrderList(UpdateExternalOrderListRequest updateExternalOrderListRequest );
	public ExternalOrderFilterResponse filterExternalOrder(ExternalOrderFilter externalOrderFilter);
	public MessageResponse changeStateExternalOrder(Long externalOrderId, Long stateId);
	public MessageResponse deleteExternalOrderLine(Long externalOrderId);
	
}
