package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Depot;

public interface DepotService {
	
	public MessageResponse addDepot(String label);
	public MessageResponse deleteDepot(Long id);
	public Depot findById(Long id);
	public Depot findByLabel(String label);
	public List<Depot> findAllDepots();
	public MessageResponse updateDepot(Long id, String label);

}
