package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.AddRoleRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Role;


public interface RoleService {

	public MessageResponse addRole(AddRoleRequest roleRequest);

	public List<Role> getListRoles();

	public MessageResponse deleteRole(Long Id);

	public MessageResponse updateRole(AddRoleRequest roleRequest);

}
