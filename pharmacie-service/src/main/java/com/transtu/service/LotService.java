package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

import com.transtu.core.jasper.StockLotJasper;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateLotRequest;
import com.transtu.persistence.entities.Lot;


public interface LotService {

	public List<Lot> findAllLots();

	public MessageResponse updateLot(Long id, boolean state);



	File getPdfStockLotMvt(DateRequestPdf DateRequestPdf) throws FileNotFoundException;

	List<StockLotJasper> getStockLotFromPdf(DateRequestPdf dateRequestPdf);

	List<Lot> listLotBetwenDate(DateRequestPdf dateRequestPdf);

	List<StockLotJasper> getPerimeFromPdf();

	File getPdfPerime() throws FileNotFoundException;

	List<Lot> listLotInfDate();

	public MessageResponse updateDateLabelLot(UpdateLotRequest Oldlot);

	public List<Lot> ListActivatelLot();

	
}
