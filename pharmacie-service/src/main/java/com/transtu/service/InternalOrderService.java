package com.transtu.service;


import java.util.Date;
import java.util.List;
import java.util.Set;

import com.transtu.core.json.request.InternalOrderFilter;
import com.transtu.core.json.request.InternalOrderRequest;
import com.transtu.core.json.request.InternalordfilterResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateOrderRequest;
import com.transtu.persistence.entities.InternalOrder;

public interface InternalOrderService {

	public MessageResponse addInternalOrder(InternalOrderRequest internalOrderRequest);
	public MessageResponse addAssistInternalOrder(InternalOrderRequest internalOrderRequest);
	public List<InternalOrder> getlistOrder ();
	public MessageResponse cancelInternalOrder(Long internalOrderId);
	public MessageResponse updateOrder(UpdateOrderRequest updateOrderRequest);
	public InternalordfilterResponse filterInternalOrder(InternalOrderFilter internalOrderFilter);
	public List<InternalOrder> findByCurrentYear(Date date);
	public MessageResponse changeStateInternalOrder(Long internalOrderId, Long stateId);
	
	
}
