package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

import com.transtu.core.jasper.MissingProductJasper;
import com.transtu.core.json.request.DateRequestPdf;

public interface DistributionLineService {
	public File getPdfMissingProductsByDate(Date start , Date end) throws FileNotFoundException;
	public List<MissingProductJasper> getMissingProductFromPdf(Date start , Date end);
	File getExcelMissingProductsByDate(Date start, Date end) throws FileNotFoundException;
	File getExcelMissingProductsByDate(DateRequestPdf date) throws FileNotFoundException;
	File getExcelMissingProductsByDate1(DateRequestPdf date) throws FileNotFoundException;
	
}
