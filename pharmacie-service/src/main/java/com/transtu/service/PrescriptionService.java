package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.transtu.core.dto.ordonnanceNbre;
import com.transtu.core.jasper.PresecriptionResponseJasper;
import com.transtu.core.json.request.DistrubtionResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.PrescriptionFilterRequest;
import com.transtu.core.json.request.PrescriptionRequest;
import com.transtu.core.json.request.PresecriptionRequestSituationjasper;
import com.transtu.core.json.request.PresecriptionRequestjasper;
import com.transtu.core.json.request.updateAgentPresecription;
import com.transtu.core.presecriptionDisplay.FiltrePresecriptionResponse;
import com.transtu.persistence.entities.Prescription;
import com.transtu.persistence.entities.PrescriptionLine;

public interface PrescriptionService {
	

	public DistrubtionResponse addPrescription(PrescriptionRequest prescriptionRequest);
	
	public FiltrePresecriptionResponse prescriptionFilter(PrescriptionFilterRequest p) 
			throws JsonParseException, JsonMappingException, IOException;
	
	public List<PrescriptionLine> getAllPrescriptionLines();

	public  MessageResponse CancelDistrubtion(Long  id);

	public List<Prescription> getPresecriptionByAgent(String agentId, Long id);
	//public List<Prescription> getPresecriptionByAgent(String agentId);

	public Set<Prescription> presecriptionHistoryFilter(PrescriptionFilterRequest p);

	public void changeStatedistributed(Long id, Boolean distributed);

	public Set<Prescription> presecriptionHistoryFiltertest(PrescriptionFilterRequest p);

	public MessageResponse updateAgentPresecription(updateAgentPresecription updateAgentPresecription);

	MessageResponse deletePresecription(Long id);

	public List<PresecriptionResponseJasper> presecriptionDisease(PresecriptionRequestjasper presecriptionRequestjasper);

	File getPdfpresDisease(PresecriptionRequestjasper presecriptionRequestjasper) throws FileNotFoundException;

	ordonnanceNbre getNbreOrdonnance();

	List<PresecriptionResponseJasper> presecriptionSituation(
			PresecriptionRequestSituationjasper presecriptionRequestjasper);

	File getPdfpresSituation(PresecriptionRequestSituationjasper presecriptionRequestjasper)
			throws FileNotFoundException;

	

	

	
	
}
