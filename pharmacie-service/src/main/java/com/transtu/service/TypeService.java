package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Type;

public interface TypeService {

	
	public MessageResponse addtype(String label);
	public List<Type> getListType();
	public MessageResponse updateType(Long id, String label); 
	public MessageResponse deleteType(Long id);
}
