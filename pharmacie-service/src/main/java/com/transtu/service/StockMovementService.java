package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import javax.sql.rowset.serial.SerialException;

import com.transtu.core.jasper.MovementJasper;
import com.transtu.core.jasper.MovementsJaspersResponse;
import com.transtu.core.json.request.AddExceptionalMovement;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.StockMovementFilter;
import com.transtu.core.json.request.StockMvtfilter;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.StockMovement;

public interface StockMovementService {

	List<StockMovement> getListExceptionalStockMovement();
	
	StockMvtfilter filterExceptionalStockMovement(StockMovementFilter stockMovementFilter);

	MessageResponse addExceptionalStockMovement(AddExceptionalMovement addExceptionalMovement);

	Set<StockMovement> filterexceptionalMovemen(StockMovementFilter stockMovementFilter);
	
	File getMvtPdf(DateRequestPdf date) throws FileNotFoundException;
	
	Blob getMvtBlob(DateRequestPdf date) throws FileNotFoundException, SerialException, SQLException;
	
	List<MovementJasper> getMovementsFromPdf(DateRequestPdf date) ;

	File getPdfStockMvt(StockMovementFilter stockMovementFilter) throws FileNotFoundException;

	List<MovementsJaspersResponse> filterExceptionalStockMovements(StockMovementFilter stockMovementFilter);

	File getXlsxStockMvt(StockMovementFilter stockMovementFilter) throws FileNotFoundException;


	
	
	


}
