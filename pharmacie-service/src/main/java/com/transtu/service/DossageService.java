package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Dosage;


public interface DossageService {
	public MessageResponse addossage(String label);
	public List<Dosage> getListDossage();
	public MessageResponse updateDossage(Long id, String label); 
	public MessageResponse deleteDossage(Long id);

}
