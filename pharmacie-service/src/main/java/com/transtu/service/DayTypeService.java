package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.AddDayTypeRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.DayType;

public interface DayTypeService {

	public List<DayType> getAllDaysType();

	public MessageResponse addDayType(AddDayTypeRequest daytypeRequest);

	public MessageResponse deleteDayType(Long idDayType);

}
