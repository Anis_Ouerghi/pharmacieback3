package com.transtu.service.utils;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.transtu.rest.exception.RestExceptionCode;


public final class PlainJsonBuilder {

	public static void buildJsonError(HttpServletResponse response, RestExceptionCode exceptionCode, String error,
			String message, HttpStatus httpStatus) throws IOException {
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setStatus(httpStatus.value());
		response.getOutputStream().println(buildJsonError(exceptionCode, error, message, httpStatus));
	}

	public static String buildJsonError(RestExceptionCode exceptionCode, String error, String message,
			HttpStatus httpStatus) {
		StringBuilder sb = new StringBuilder();
		sb.append("{ \"code\": \"");
		sb.append(exceptionCode.name());
		sb.append("\", \"error\": \"");
		sb.append(error);
		sb.append("\", \"message\": \"");
		sb.append(message);
		sb.append("\", \"status\": \"");
		sb.append(httpStatus.value());
		sb.append("\" }");
		return sb.toString();
	}
}
