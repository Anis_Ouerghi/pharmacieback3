package com.transtu.service.utils;

import com.transtu.core.json.request.ConsultationRequest;
import com.transtu.persistence.entities.Reception;

public interface ConvertService {


	
	public ConsultationRequest calculateQte(Long ProductId, double ToDistributeQte, Long LocationId);
    public double calculateawaPrice(Long ProductId, Reception reception);

}
