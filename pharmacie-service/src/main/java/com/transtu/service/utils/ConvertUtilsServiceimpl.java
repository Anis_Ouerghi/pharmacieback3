package com.transtu.service.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transtu.core.json.request.ConsultationRequest;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.Reception;
import com.transtu.persistence.entities.ReceptionLine;
import com.transtu.persistence.entities.Stock;
import com.transtu.persistence.repositories.InternalOrderRepository;
import com.transtu.persistence.repositories.LocationRepository;
import com.transtu.persistence.repositories.ProductRepository;
import com.transtu.persistence.repositories.ReceptionLineRepository;
import com.transtu.persistence.repositories.StockRepository;


@Service
public class ConvertUtilsServiceimpl implements ConvertService{


	
	@Autowired
	InternalOrderRepository internalOrderRepository;
	@Autowired
	StockRepository  stockRepository;
	@Autowired
	ProductRepository productRepository;
	@Autowired
	LocationRepository locationRepository;
	Integer num;
	ConsultationRequest consultationRequest;
	@Autowired
	ReceptionLineRepository receptionLineRepository;
	
	@Override
	public ConsultationRequest calculateQte(Long ProductId, 
			double WantedQt, Long LocationId ) {
		
	Stock stkPharma = stockRepository.findByProductAndLocation(productRepository.findOne(ProductId),
			locationRepository.findOne(ProductId));
	
	 		if(WantedQt <= stkPharma.getQuantity() ){
	 			
			consultationRequest  = new ConsultationRequest();
			consultationRequest.setDeliveredQt(WantedQt);	
			consultationRequest.setMissingQt(0d);
		} else{
			
			consultationRequest.setDeliveredQt(stkPharma.getQuantity());
			consultationRequest.setMissingQt(WantedQt-stkPharma.getQuantity());
		}
		return consultationRequest;	
	}
	
	
	
	@Override
	public double calculateawaPrice(Long ProductId, Reception reception){
		double cmup;
		Product product = productRepository.findOne(ProductId);
		ReceptionLine receptionLine = receptionLineRepository.findByProductAndReception(product, reception);
		//Reception reception;
	cmup = ((stockRepository.getSumQteStock(ProductId)-receptionLine.getReceptionQte())*product.getWauCost()
		+(receptionLine.getReceptionQte()*receptionLine.getPrice()))/stockRepository.getSumQteStock(ProductId);
	
		return cmup;
	}
	

}
