package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Doctor;

public interface DoctorService  {

	public List<Doctor> getListDoctors();

	public MessageResponse addDoctor(Doctor doctor);

	public MessageResponse deleteDoctor(Long idDoctor);


	
	
	

}
