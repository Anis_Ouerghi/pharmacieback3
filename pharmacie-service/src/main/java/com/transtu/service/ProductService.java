package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.ActiveProductByLabelStartWith;
import com.transtu.core.json.request.ActiveProductByLabelStartWithAndDep;
import com.transtu.core.json.request.AddProductRequest;
import com.transtu.core.json.request.DuplcateEmptyResp;
import com.transtu.core.json.request.FilterProductAssistOrderResponse;
import com.transtu.core.json.request.FilterProductResponse;
import com.transtu.core.json.request.FilterProductResponseSpecific;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.ProductAutoCompleteResponse;
import com.transtu.core.json.request.ProductQuantityDep;
import com.transtu.core.json.request.ProductRequestFilter;
import com.transtu.core.json.request.ProductRequestFilterAssistOrder;
import com.transtu.core.json.request.ProductResponse;
import com.transtu.core.json.request.ProductResponseSearch;
import com.transtu.core.json.request.UpdateProductRequest;
import com.transtu.core.product.LightProduct;
import com.transtu.persistence.entities.Depot;
import com.transtu.persistence.entities.Product;

public interface ProductService {

	public MessageResponse addProduct(AddProductRequest productRequest);
	public List<ProductResponse> getListProducts();
	public List<Product> findByBarCode(String code);
	public List<Product> getLibStartingWith(String name);
	public List<Product> getListStockBreak();
	public FilterProductResponse filterProduct(ProductRequestFilter filterProduct);
	public ProductResponse findByBarCodes(String code);
	public String concatenationLabel(Long id);
	public List<ProductResponse> getLibsStartingWith(String name);
	public ProductResponse findByBarCodespec(String code);
	public MessageResponse updateProduct(UpdateProductRequest updateproductRequest);
    public MessageResponse changeProductState(Long id, boolean etat);
    public MessageResponse deleteProduct(Long id);
    public List<Product> findByDepot(Depot depot);
	public List<Product> findByDepotAndActived(Long id ,boolean boolean1);
	public List<LightProduct> getActivedProductLabelbStartingWith(ActiveProductByLabelStartWith activeProductByLabelStartWith);
    public List<ProductResponseSearch> getListProductsSearch();
	public Product getProductById(Long id);
	public List<ProductAutoCompleteResponse> getProductsForAutoComplete(Boolean status);
	public FilterProductResponseSpecific filterProductSpecific(ProductRequestFilter filterProduct);
	public DuplcateEmptyResp verifEmptyAndDuplicate();
	public FilterProductAssistOrderResponse filterProductAssitOrder(ProductRequestFilterAssistOrder filterProduct);
	public List<LightProduct> getActivedProductLabelbStartingWithByDep(
			ActiveProductByLabelStartWithAndDep activeProductByLabelStartWith);
	ProductQuantityDep existProductByDepot(Long depotId, boolean boolean1);
	int totalProductAssitOrder(ProductRequestFilterAssistOrder filterProduct);
	int totalPerime(ProductRequestFilterAssistOrder filterProduct);
	int totalPerime();

	
	

}
