package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Form;

public interface FormService {
	
	public MessageResponse addForm(String label);
	public MessageResponse deleteForm(Long id);
	public Form findById(Long id);
	public Form findByLabel(String label);
	public List<Form> findAllForms();
	public MessageResponse updateForm(Long id, String label);
	
}
