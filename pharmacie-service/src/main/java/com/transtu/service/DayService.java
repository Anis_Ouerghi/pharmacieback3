package com.transtu.service;

import java.text.ParseException;
import java.util.List;

import com.transtu.core.json.request.AddDayRequest;
import com.transtu.core.json.request.AddUserDayRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Day;

public interface DayService {
	public MessageResponse addDay(AddDayRequest dayRequest);
	public MessageResponse updateDate(AddDayRequest dayRequest);
	public MessageResponse deleteDate(Long idDay);
	public List<Day> getAllDays();
	public List<Day> filterDate();
	public List<Day> getDayByStateAndDayType(boolean state , Long dayTypeId);
	public void openDaytoFirstAuthentication();
	public MessageResponse addUserDay(AddUserDayRequest addUserDayRequest);
	public MessageResponse cancelUserDay(Long dayId, Long userId);
	public void openDayToAuthentication() throws ParseException;
	
}
