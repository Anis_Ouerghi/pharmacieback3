package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.AddFamilyRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Family;

public interface FamilyService {
	
	public List<Family> getListFamily();
	public MessageResponse addFamily(String label);
	public MessageResponse deleteFamily(Long id);
	MessageResponse updateFamily(Long id, String label);
}
