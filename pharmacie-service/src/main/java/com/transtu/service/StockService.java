package com.transtu.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import com.transtu.core.jasper.StockJasper;
import com.transtu.core.json.request.FilterStockReport;
import com.transtu.core.json.request.StockFilterRequest;
import com.transtu.core.json.request.StockResponse;
import com.transtu.persistence.entities.Product;

public interface StockService {

	public List<StockResponse> getListStockBreak();
	public List<StockResponse> getTotalListStockBreak();
	public Double getQteByProductAndLocation(Long idProduct , Long idLocation);
	File getPdfStock(FilterStockReport filterStockReport) throws FileNotFoundException;
	List<StockJasper> getStockFromPdf(FilterStockReport filterStockReport);
	List<Product> filterStock(StockFilterRequest stockFilterRequest);
	File getXlsxStock(FilterStockReport filterStockReport) throws FileNotFoundException;
	

}
