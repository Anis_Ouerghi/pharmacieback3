package com.transtu.service;

import java.util.List;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Motif;

public interface MotifService {
	public MessageResponse addMotif(String label);
	public List<Motif> getListMotif();
	public MessageResponse updateMotif(Long id, String label); 
	public MessageResponse deleteMotif(Long id);
}
