package com.transtu.rest.security;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;

import com.transtu.core.dto.AuthenticationResponseDTO;


@Service
public class AppUserStorage {
    
	private static final Logger logger = LoggerFactory.getLogger(AppUserStorage.class);

	public static ReentrantLock LOCK = new ReentrantLock();
	
    @Autowired
    private Cache appUserCache;
    
    public AuthenticationResponseDTO getAppUser(AuthenticationResponseDTO appUser) {
        if (appUser == null) return null;
        if (appUser.getUserName()== null) return null;
        return appUserCache.get(appUser.getUserName(), AuthenticationResponseDTO.class);
    }
    
    public List<AuthenticationResponseDTO> getAppUsers() {
    	
    	
    	return null;
    }
    
    public boolean isAppUserPresent(String username) {
        if (username == null) return false;
        return (appUserCache.get(username) != null);
    }
    
    public void removeAppUser(String username) {
        if (username == null) return;
        appUserCache.evict(username);
        
    }
    
    public void storeAppUser(AuthenticationResponseDTO appUser) {
    	logger.info("appUser =====>"+appUser.toString());
        if (appUser != null) {
            appUserCache.evict(appUser.getUserName());
            appUserCache.put(appUser.getUserName(), appUser);
        }
        logger.info("appUserCache =====>"+appUserCache.toString());
        logger.info("isAppUserPresent =====>"+isAppUserPresent(appUser.getUserName()));
    }
}

