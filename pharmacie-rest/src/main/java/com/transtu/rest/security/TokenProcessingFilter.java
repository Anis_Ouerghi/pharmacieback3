package com.transtu.rest.security;


import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.crypto.sign.InvalidSignatureException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.transtu.core.dto.AuthenticationResponseDTO;

/**
 * Checks for the X-Auth-Token HTTP header and if set automatically
 * authenticates the user using the data from the token.
 */
public class TokenProcessingFilter extends GenericFilterBean {

	private static final Logger logger = LoggerFactory.getLogger(TokenProcessingFilter.class);

	private final TokenService tokenService;
	private final AppUserStorage appUserStorage;
	private final ObjectMapper jsonMapper;

	public TokenProcessingFilter(TokenService tokenService, AppUserStorage appUserStorage) {
		this.tokenService = tokenService;
		this.appUserStorage = appUserStorage;
		this.jsonMapper = new ObjectMapper();
	}

	/**
	 * Extracts the token either from the X-Auth-Token HTTP header or from a
	 * request parameter called "token".
	 */
	private static String extractAuthTokenFromRequest(HttpServletRequest request) {

		// try getting the token from the header
		String token = request.getHeader("X-Auth-Token");
		logger.info("Request Header token: {}", token);

		// if no token was set in the header, try the request parameters
		if (token == null) {
			token = request.getParameter("token");
			logger.info("Request Parameter token: {}", token);
		}
		return token;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (!(request instanceof HttpServletRequest)) {
			chain.doFilter(request, response);
		}

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		httpResponse.setHeader("Access-Control-Allow-Origin", "*");
		httpResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT, HEAD");
		httpResponse.setHeader("Access-Control-Max-Age", "3600");
		httpResponse.setHeader("Access-Control-Allow-Headers",
				"X-Requested-With,Content-Type,Accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,X-Auth-Token,Authorization");
		httpResponse.setHeader("Access-Control-Expose-Headers", "X-Auth-Token");
		String token = extractAuthTokenFromRequest(httpRequest);
		logger.info("__________________doFilter_______________");

		if (token != null) {
			try {
				// extract JSON UserDetails from the token
				String jsonUserDetails = tokenService.extractJsonUserDetails(token);
				logger.info("Extracted JsonUserDetails from Token in Request: {}", jsonUserDetails);
				logger.info("isAppUserPresent"+appUserStorage.isAppUserPresent("test"));
				if (jsonUserDetails != null) {
					AppUserStorage.LOCK.lock();
					try {
						AuthenticationResponseDTO appUser = new AuthenticationResponseDTO();
						logger.info("get Queue Length: {}", AppUserStorage.LOCK.getQueueLength());
						logger.info("jsonMapper.readValue===>"+jsonMapper.readValue(jsonUserDetails, AuthenticationResponseDTO.class).toString());
						appUser = appUserStorage
								.getAppUser(jsonMapper.readValue(jsonUserDetails, AuthenticationResponseDTO.class));
						logger.info("AppUser: {}", appUser);

						if (appUser != null) {
							Date expirationDate = tokenService.extractExpirationDate(token);
							logger.info("Token expirationDate: {}", expirationDate);

							if (expirationDate != null && expirationDate.getTime() > System.currentTimeMillis()) {
								logger.info(
										"The token isn't expired, authenticate the user from the token for this request "
												+ "add an updated token to the response so the client-side session will be refreshed");

								// if the token isn't expired, authenticate the
								// user from the token for this request
								// and add an updated token to the response so
								// the client-side session will be refreshed
								UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
										appUser, null, appUser.getAuthorities());
								authentication
										.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
								SecurityContextHolder.getContext().setAuthentication(authentication);
								addUpdatedAuthTokenToResponse(jsonUserDetails, httpResponse);
								appUserStorage.removeAppUser(appUser.getUserName());// keeped One user for one session
								appUserStorage.storeAppUser(appUser);

								logger.info("Authenticated user: {} ", jsonUserDetails);
							} else {
								// We need to remove user from the cache because
								// his token is already
								// expired, So if want to conect again he will
								// get problem that this user is already
								// connected
								appUserStorage.removeAppUser(appUser.getUserName());
								logger.info("User athentication expired: {}", appUser.getFirstName());
								httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
							}
						} else {
							// We need to remove user from the cache because his
							// token is already
							// expired, So if want to conect again he will get
							// problem that this user is already connected
//							if (appUser != null)
//								appUserStorage.removeAppUser(appUser.getUserName());
							logger.info("User authentification expired on server side");
							httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

						}
					} finally {
						AppUserStorage.LOCK.unlock();
					}
				}
			} catch (InvalidSignatureException | IllegalArgumentException e) {
				logger.info("Invalid signature passed; continue filter chain");
			}
		}
		logger.info("httpRequest Method: {}", httpRequest.getMethod());
		if (!httpRequest.getMethod().equalsIgnoreCase("OPTIONS")) {
			chain.doFilter(request, response);
		}
	}

	/**
	 * Adds an updated token to the response's header.
	 */
	private void addUpdatedAuthTokenToResponse(String jsonUserDetails, HttpServletResponse response) {
		try {
			response.addHeader("X-Auth-Token", tokenService.createToken(jsonUserDetails));
		} catch (JsonProcessingException e) {
			logger.error("Failed to create auth token", e);
		}
	}
}

