package com.transtu.rest.security;

//------------

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.ServletListenerRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.service.utils.PlainJsonBuilder;

//-----------------
/**
 * NOTE: Global method security is not possible while using JPA! Just use
 * request security.
 */

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);

	@Autowired
	private DataSource datasource;

	@Value("${maxAuthMinutes?:10000}")
	private int maxAuthMinutes;
	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;

	@Autowired
	private AppUserStorage appUserStorage;
	@Autowired
	private MessageSource messages; 

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder builder) throws Exception {
		builder.authenticationProvider(customAuthenticationProvider);
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Autowired
	protected void registerAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("user").password("password").roles("USER");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable(); 
		http.addFilterBefore(new TokenProcessingFilter(tokenService(), appUserStorage),
				BasicAuthenticationFilter.class).sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().exceptionHandling()
				.accessDeniedHandler(new AccessDeniedHandler() {
					@Override
					public void handle(HttpServletRequest request, HttpServletResponse response,
							AccessDeniedException accessDeniedException) throws IOException, ServletException {

						logger.info("accessDeniedException>>>>>>>>>>>>>>>>>>>>>>>" + accessDeniedException.toString());

						PlainJsonBuilder.buildJsonError(response, RestExceptionCode.ACCESS_DENIED,
								messages.getMessage("message.access.denied", null, request.getLocale()), null,
								HttpStatus.FORBIDDEN);
					}
				}).authenticationEntryPoint(new AuthenticationEntryPoint() {
					@Override
					public void commence(HttpServletRequest request, HttpServletResponse response,
							AuthenticationException exception) throws IOException, ServletException {
						logger.info("request >>>>>>>>>>>>>>>>>>>>>>>>>>>>" + request.toString());
						logger.info("request header>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + request.getHeader("X-Auth-Token"));
						logger.info("exception >>>>>>>>>>>>>>>>>>>>>>>>>>>>" + exception.toString());
		 				PlainJsonBuilder.buildJsonError(response, RestExceptionCode.AUTHORIZATION_REQUIRED,
								messages.getMessage("message.authorization.required", null, request.getLocale()), null,
								HttpStatus.UNAUTHORIZED);
					}
				}) 

				.and().authorizeRequests().antMatchers("/authentication/**" , "/api/**","/jasper/**").permitAll();

				//.anyRequest().authenticated();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security",
				"/swagger-ui.html", "/webjars/**");
	}

	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
		db.setDataSource(datasource);
		return db;
	}

	@Bean
	public SavedRequestAwareAuthenticationSuccessHandler savedRequestAwareAuthenticationSuccessHandler() {
		SavedRequestAwareAuthenticationSuccessHandler auth = new SavedRequestAwareAuthenticationSuccessHandler();
		auth.setTargetUrlParameter("targetUrl");
		return auth;
	}

	@Bean
	public TokenService tokenService() {
		int[] keyInts = new int[] { 3, 35, 53, 75, 43, 15, 165, 188, 131, 126, 6, 101, 119, 123, 166, 143, 90, 179, 40,
				230, 240, 84, 201, 40, 169, 15, 132, 178, 210, 80, 46, 191, 211, 251, 90, 146, 210, 6, 71, 239, 150,
				138, 180, 195, 119, 98, 61, 34, 61, 46, 33, 114, 5, 46, 79, 8, 192, 205, 154, 245, 103, 208, 128, 163 };
		byte[] HMAC_KEY = new byte[keyInts.length];
		for (int i = 0; i < keyInts.length; i++) {
			HMAC_KEY[i] = (byte) keyInts[i];
		}
		MacSigner hmac = new MacSigner(HMAC_KEY);

		return new TokenService(hmac, hmac, maxAuthMinutes);
	}
	
	
	@Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
        return new ServletListenerRegistrationBean<HttpSessionEventPublisher>(new HttpSessionEventPublisher());
    }
}