package com.transtu.rest.security;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.transtu.core.dto.AuthenticationResponseDTO;
import com.transtu.persistence.entities.Role;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.persistence.repositories.UtilisateurRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;

@Service
public class CustomAuthenticationProvider implements AuthenticationProvider {

	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);

	@Autowired
	private AppUserStorage appUserStorage;

	@Autowired
	private UtilisateurRepository gUserRepository;

	@Autowired
	private PasswordEncoder gPasswordEncoder;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		AppUserStorage.LOCK.lock();
		try {

			String login = (String) authentication.getPrincipal();
			String password = (String) authentication.getCredentials();
			logger.info("Authentification for user: {}", login);
			logger.info("Authentification  password: {}", password);

			List<Utilisateur> vUsers = new ArrayList<>();
			List<Utilisateur> vUserEntity = new ArrayList<>();
			if (login.contains("@")) {
				vUsers = this.gUserRepository.findByEmail(login);
			} else { 
				vUsers = this.gUserRepository.findByUsername(login);

	
			}

			logger.info("vUsers size: " + vUsers.size());
			for (Utilisateur vUser : vUsers) {
				logger.info("vUser : " + vUser.toString());
				if (this.gPasswordEncoder.matches(password, vUser.getPassword())
						|| password.equals(vUser.getPassword())) {
					
					vUserEntity.add(vUser);
				}
			}

			if (vUserEntity.isEmpty()) {
				RestExceptionCode code = RestExceptionCode.USERNAME_PW_NOT_MATCH;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			}
			AuthenticationResponseDTO vAuthentificationResponseDTO = new AuthenticationResponseDTO();

			List<Role> vAutoritiesList = new ArrayList<Role>(vUserEntity.get(0).getRoles());
			Utilisateur vUser = null;
			vUser = vUserEntity.get(0);
			if (!vUser.isActived()) {
				throw new RestException(RestExceptionCode.USER_NOT_ACTIVATED);
			}
			logger.info("Auser: {}", vUser.toString());
				vAuthentificationResponseDTO = new AuthenticationResponseDTO();
				vAuthentificationResponseDTO.setUserName(vUser.getUsername());
				vAuthentificationResponseDTO.setUserId(vUser.getId());
				vAuthentificationResponseDTO.setMail(vUser.getEmail());
			for (Role authEntity : vAutoritiesList) {

				vAuthentificationResponseDTO.addRole(authEntity.getRole().trim());
				vAuthentificationResponseDTO.addAuthority(new SimpleGrantedAuthority(authEntity.getRole().trim()));
			}
			appUserStorage.storeAppUser(vAuthentificationResponseDTO);
			return new UsernamePasswordAuthenticationToken(vAuthentificationResponseDTO, null,
					vAuthentificationResponseDTO.getAuthorities());

		} finally {
			AppUserStorage.LOCK.unlock();
		}

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
