package com.transtu.rest.security;

public enum Claims {

	EXP("EXP"), JTI("JTI"), AUD("AUD"),SUB("SUB"),ISS("ISS"), IAT("IAT");

	final String claim;

	Claims(String claim) {
		this.claim = claim;
	}

	@Override
	public String toString() {
		return claim;
	}
}