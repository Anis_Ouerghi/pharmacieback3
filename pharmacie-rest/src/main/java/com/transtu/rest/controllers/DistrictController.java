package com.transtu.rest.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.District;
import com.transtu.service.DistrictService;
@CrossOrigin()
@RestController
@RequestMapping(value = DistrictController.PATH, produces = "application/json; charset=utf-8")
public class DistrictController  extends AbstractController {

	private static final Logger logger = LoggerFactory.getLogger(DistrictController.class);

	protected static final String PATH = "/districts";

	@Autowired(required = true)
	private DistrictService districtService;

	

	/**
	 * get list districts
	 * 
	 * List<District> <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/districts
	 * </h4>
	 */
	@CrossOrigin()
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<District> getListProducts() {

		return districtService.getListDistrict();
	}
	
	/**
	 * add district
	 * 
	 * message <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/districts
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addDistrict(@RequestBody District district) {

		return districtService.addDistrict(district);
	}
	
	/**
	 * update district
	 * 
	 * message <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/districts
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateDistrict(@RequestBody District district) {

		return districtService.updateDistrict(district);
	}
	
	
	/**
	 * delete district
	 * 
	 * message <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/districts/{id}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDistrict(@PathVariable Long id) {

		return districtService.deleteDistrict(id);
	}
	
	/**
	 * get district by lib 
	 * 
	 * district <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/districts/lib/:lib
	 * </h4>
	 */
	@RequestMapping(value = "/lib/{lib}", method = RequestMethod.GET)
	@ResponseBody
	public District getDistrictByLib(@PathVariable String lib) {

		return districtService.findDistrictByLib(lib);
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
