package com.transtu.rest.controllers;

import java.io.FileNotFoundException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.core.json.request.DeliveryRequest;
import com.transtu.core.json.request.DistributionRequestPdf;
import com.transtu.core.json.request.ExternaOrdRequest;
import com.transtu.core.json.request.FilterStockReport;
import com.transtu.core.json.request.InventoryPdfRequest;
import com.transtu.core.json.request.PresecriptionRequestSituationjasper;
import com.transtu.core.json.request.PresecriptionRequestjasper;
import com.transtu.core.json.request.ReceptionRequest;
import com.transtu.core.json.request.StockMovementFilter;
import com.transtu.service.DistributionLineService;
import com.transtu.service.DistributionService;
import com.transtu.service.InternalDeliveryService;
import com.transtu.service.InventoryService;
import com.transtu.service.LotService;
import com.transtu.service.PrescriptionService;
import com.transtu.service.ReceptionService;
import com.transtu.service.StockMovementService;
import com.transtu.service.StockService;

import io.swagger.models.Response;
import net.sf.jasperreports.engine.JRException;


@RestController 
@RequestMapping(value=JasperController.PATH)
//application/excel; application/pdf  , produces = " charset=utf-8";
public class JasperController {
	
	protected static final String PATH = "/jasper";

	
	@Autowired(required=true)
	StockMovementService stockMovementService;
	
	@Autowired(required=true)
	DistributionService distributionService ; 
	
	@Autowired(required=true)
	StockService stockService;
	
	@Autowired(required=true)
	DistributionLineService distributionLineService;
	
	@Autowired(required=true)
	InventoryService  inventoryService;
	
	
	@Autowired(required=true)
	LotService lotService;
	
	@Autowired(required=true)
	PrescriptionService prescriptionService;
	
	@Autowired(required=true)
	ReceptionService  receptionService;
	
	@Autowired(required=true)
	InternalDeliveryService internalDelvieryService;
	
	/**
	 * PDF movement
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/movement
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	@RequestMapping(value = "/movement", method = RequestMethod.POST)
	@ResponseBody
	public FileSystemResource getPdfMovement(@RequestBody DateRequestPdf date) throws FileNotFoundException {
		return new  FileSystemResource(this.stockMovementService.getMvtPdf(date));
	}
	
	/**
	 * PDF distributions
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/distribution
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	@RequestMapping(value = "/distributions", method = RequestMethod.POST)
	@ResponseBody
	public FileSystemResource getPdfDistributions(@RequestBody DistributionRequestPdf date) throws FileNotFoundException {
		return new  FileSystemResource(this.distributionService.getPdfDistributions(date));
	}
	
	/**
	 * PDF distribution by agent
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/distribution/agent
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	
	@RequestMapping(value = "/distributions/agent", method = RequestMethod.POST)
	@ResponseBody
	public FileSystemResource getPdfDistributionByAgent(@RequestBody DistributionRequestPdf date ) throws FileNotFoundException {
		return new  FileSystemResource(this.distributionService.getPdfDistributionByAgent(date));
	}
	
	/**
	 * PDF distribution by agent
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/distribution/agent
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	
	@RequestMapping(value = "/globaldistributions/agent", method = RequestMethod.POST)
	@ResponseBody
	public FileSystemResource getPdfglobaleDistributionByAgent(@RequestBody DistributionRequestPdf date ) throws FileNotFoundException {
		return new  FileSystemResource(this.distributionService.getPdfDistributionByAgents(date));
	}
	
	
	
	/**
	 * excel stock
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/stock/:idStock
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	@RequestMapping(value = "/stockByExcel", method = RequestMethod.POST, produces = " application/excel" )
	@ResponseBody
	public FileSystemResource getPdfStockByExcel(@RequestBody FilterStockReport filterStockReport) throws FileNotFoundException {
	
		return new  FileSystemResource(this.stockService.getXlsxStock(filterStockReport));
		
	}
	
	
	
	/**
	 * excel stock
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/stock/:idStock
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	@RequestMapping(value = "/stockByPdf", method = RequestMethod.POST, produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource getPdfStockByPdf(@RequestBody FilterStockReport filterStockReport) throws FileNotFoundException {
		
		return new  FileSystemResource(this.stockService.getPdfStock(filterStockReport));
	}
	
	
	/**
	 * Product-inventory
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/stock/:idlocal
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	@RequestMapping(value = "/inventory-product", method = RequestMethod.POST, produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource getProductInventory(@RequestBody InventoryPdfRequest inventoIryPdfRequest) throws FileNotFoundException {
	
		return new  FileSystemResource(this.inventoryService.getPdfStock(inventoIryPdfRequest));
	}
	
	
	/**
	 * PDF missing products
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/missing-product
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	@RequestMapping(value = "/missing-product", method = RequestMethod.POST , produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource getPdfMissingProduct(@RequestBody DateRequestPdf date) throws FileNotFoundException {
		
		return new  FileSystemResource(this.distributionLineService.getPdfMissingProductsByDate(date.getStart(), date.getEnd()));
	}
	
	@RequestMapping(value = "/missing-product-excel", method = RequestMethod.POST , produces = " application/excel" )
	@ResponseBody
	public FileSystemResource getExcelMissingProduct(@RequestBody DateRequestPdf date) throws FileNotFoundException {
		
		return new  FileSystemResource(this.distributionLineService.getExcelMissingProductsByDate1(date));
	}
	
	/**
	 * PDF stock
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/stock/:idStock
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	@RequestMapping(value = "/stockMvtByPdf", method = RequestMethod.POST, produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource getPdfStockMvt(@RequestBody StockMovementFilter stockMovementFilter) throws FileNotFoundException {
		
		return new  FileSystemResource(this.stockMovementService.getPdfStockMvt(stockMovementFilter));
	}
	
	/**
	 * PDF stock
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/stock/:idStock
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	@RequestMapping(value = "/stockMvtByExcel", method = RequestMethod.POST , produces = " application/excel")
	@ResponseBody
	public FileSystemResource getExcelStockMvt(@RequestBody StockMovementFilter stockMovementFilter) throws FileNotFoundException {
		
		return new  FileSystemResource(this.stockMovementService.getXlsxStockMvt(stockMovementFilter));
	}

	@RequestMapping(value = "/statestique-distribution", method = RequestMethod.POST, produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource getPdfPdfDistStatestique(@RequestParam Date startDate,@RequestParam Date endtDate) throws FileNotFoundException {
		
		return new  FileSystemResource(this.distributionService.getPdfPdfDistStatestique(startDate,endtDate));
	}
	
	
	/**
	 * PDF stock
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/jasper/stock/:idStock
	 * </h4>
	 * @throws FileNotFoundException 
	 * @throws JRException 
	 */
	
	@RequestMapping(value = "/inventory/", method = RequestMethod.POST, produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource getPdfPdfInventory(@RequestParam Long idPharma, @RequestParam Long idDep) throws FileNotFoundException {
		
		return new  FileSystemResource(this.inventoryService.getPdfinventory(idPharma,idDep));
	}
	
	
	@RequestMapping(value = "/ecart/", method = RequestMethod.POST, produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource getPdfPdfInventoryEcart(@RequestParam Long idPharma, @RequestParam Long idDep) throws FileNotFoundException {
		
		return new  FileSystemResource(this.inventoryService.getPdfinventoryEcart(idPharma,idDep));
	}
	
	
	
	@RequestMapping(value = "lot", method = RequestMethod.POST , produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource getkistLOt(@RequestBody DateRequestPdf dateRequestPdf) throws FileNotFoundException {

		return new  FileSystemResource( this.lotService.getPdfStockLotMvt(dateRequestPdf));
	}
	
	

	@RequestMapping(value = "perime", method = RequestMethod.POST , produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource Listperime() throws FileNotFoundException {

		return new  FileSystemResource( this.lotService.getPdfPerime());
		
	}
	
	
	@RequestMapping(value = "pres-stat", method = RequestMethod.POST , produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource statestiquePres(@RequestBody PresecriptionRequestjasper presecriptionRequestjasper) throws FileNotFoundException {

		return new  FileSystemResource(this.prescriptionService.getPdfpresDisease(presecriptionRequestjasper));
	}
	
	
	
	
	
	@RequestMapping(value = "pres-situation", method = RequestMethod.POST , produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource statestiquePresSituation(@RequestBody PresecriptionRequestSituationjasper presecriptionRequestjasper) throws FileNotFoundException {

		return new  FileSystemResource(this.prescriptionService.getPdfpresSituation(presecriptionRequestjasper));
	}
	
	
	
	@RequestMapping(value = "receptionList", method = RequestMethod.POST , produces = " application/pdf" )
	@ResponseBody
	public FileSystemResource getReceptionList(@RequestBody ExternaOrdRequest externaOrdRequest) throws FileNotFoundException {

		return new  FileSystemResource(this.receptionService.getPdfListReception(externaOrdRequest));
	}
	
	
	
	@RequestMapping(value = "/deliveryList", method = RequestMethod.POST,  produces = " application/pdf")
	@ResponseBody
	public FileSystemResource listDistributionByAgentFromPdf(@RequestBody DeliveryRequest deliveryRequest ) throws FileNotFoundException {
	    
		
	     return new  FileSystemResource(this.internalDelvieryService.get(deliveryRequest.getCustomerId(), deliveryRequest.getMaxDate(),
	    		 deliveryRequest.getMinDate()));
	}
	
	
}

