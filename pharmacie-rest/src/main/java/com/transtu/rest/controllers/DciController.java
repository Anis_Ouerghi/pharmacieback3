package com.transtu.rest.controllers;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Dci;
import com.transtu.service.DciService;

@RestController
@RequestMapping(value =DciController.PATH, produces = "application/json; charset=utf-8")
public class DciController  extends AbstractController {
	
	protected static final String PATH = "/DCIs";
	
	@Autowired(required=true)
	DciService dciService ;

	/**
	 * add dci
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/DCIs:{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{label}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addDci( @RequestBody @NotNull String label) {
	     return this.dciService.addDci(label);
	}
	
	/**
	 * update dci
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/DCIs:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "{id}/{label}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateDci( @PathVariable Long id, @PathVariable String label ) {
	     return this.dciService.updateDci(id, label);
	}
	
	/**
	 * find dci by id 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/DCIs/:id
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Dci findById( @PathVariable Long id ) {
	     return this.dciService.findById(id);
	}
	
	/**
	 * find dci by label 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/DCIs/label/:label
	 * </h4>
	 */
	@RequestMapping(value = "/label/{label}", method = RequestMethod.GET)
	@ResponseBody
	public Dci findByLabel( @PathVariable String label ) {
	     return this.dciService.findByLabel(label);
	}
	
	/**
	 * find all DCIs 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/DCIs
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<Dci> findDci() {
	     return this.dciService.findAllDci();
	}
	
	/**
	 * delete DCI 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/DCIs/:id
	 * </h4>
	 */
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDci(@PathVariable Long id) {
	     return this.dciService.deleteDci(id);
	}
	
}
