package com.transtu.rest.controllers;

import java.io.FileNotFoundException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import javax.sql.rowset.serial.SerialException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.jasper.MovementJasper;
import com.transtu.core.json.request.AddExceptionalMovement;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.StockMovementFilter;
import com.transtu.core.json.request.StockMvtfilter;
import com.transtu.persistence.entities.StockMovement;
import com.transtu.service.StockMovementService;

@RestController
@RequestMapping(value =StockMovementController.PATH, produces = "application/json; charset=utf-8")
public class StockMovementController extends AbstractController {
	
protected static final String PATH = "/exceptionalStockMovement";
	
	@Autowired(required=true)
	StockMovementService stockMovementService ;
	
	
	/**
	 * add exceptionalMovement
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/exceptionalMovement
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse AddExceptionalMovementService(@Valid @RequestBody AddExceptionalMovement addExceptionalMovement) {

		return stockMovementService.addExceptionalStockMovement(addExceptionalMovement);
		 
	}
	
	
	/**
	 * List exceptionalMovement
	 * 
	 * 
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/exceptionalMovement
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<StockMovement> getListExceptionalMovement() {
         
		return stockMovementService.getListExceptionalStockMovement();
	}
	
	/**
	 * filter exceptionalMovement
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/exceptionalMovement
	 * </h4>
	 */
	@RequestMapping(value = "/dynamic", method = RequestMethod.POST)
	@ResponseBody
	public Set<StockMovement> dynamicFilterStockMovement(@RequestBody StockMovementFilter stockMovementFilter) {
		return this.stockMovementService.filterexceptionalMovemen(stockMovementFilter);
	}
	
	/**
	 * get movements between from pdf
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/exceptionalMovement/jasper
	 * </h4>
	 * @return 
	 */
	@RequestMapping(value = "/jasper", method = RequestMethod.POST)
	@ResponseBody
	public List<MovementJasper> getMovementBetweenFromPdf(@RequestBody DateRequestPdf date) {
		return this.stockMovementService.getMovementsFromPdf(date);

	}
	
	/**
	 * get movements between from pdf version blob
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/exceptionalMovement/jasper/blob
	 * </h4>
	 * @return 
	 * @throws SQLException 
	 * @throws SerialException 
	 * @throws FileNotFoundException 
	 */
	@RequestMapping(value = "/jasper/blob", method = RequestMethod.POST)
	@ResponseBody
	public Blob getMovementBetweenFromPdfToBlob(@RequestBody DateRequestPdf date) throws FileNotFoundException, SerialException, SQLException {
		return this.stockMovementService.getMvtBlob(date);

	}
	
	/**
	 * filter stock movements
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/exceptionalMovement/filter
	 * </h4>
	 * @return 
	 */
	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	@ResponseBody
	public StockMvtfilter filterStockMovement(@RequestBody StockMovementFilter stockMovementFilter) {
		return this.stockMovementService.filterExceptionalStockMovement(stockMovementFilter);

	}
	
	



}
