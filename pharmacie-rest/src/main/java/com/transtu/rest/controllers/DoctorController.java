package com.transtu.rest.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Doctor;
import com.transtu.service.DoctorService;

@CrossOrigin()
@RestController
@RequestMapping(value = DoctorController.PATH, produces = "application/json; charset=utf-8")
 

public class DoctorController extends AbstractController {
	
	private static final Logger logger = LoggerFactory.getLogger(DoctorController.class);

	protected static final String PATH = "/doctors";


	@Autowired(required = true)
	private DoctorService doctorService;
	
	
	@CrossOrigin()
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Doctor> getListProducts() {

		return doctorService.getListDoctors();
	}

	
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addDossage(@Valid @RequestBody Doctor doctor) {

		return doctorService.addDoctor(doctor);
	}

	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDoctor(@PathVariable Long id) {

		return doctorService.deleteDoctor(id);
	}
	
	
	
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



}
