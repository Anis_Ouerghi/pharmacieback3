package com.transtu.rest.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.PharmaClass;
import com.transtu.service.MailService;
import com.transtu.service.PharmaService;


@RestController
@RequestMapping(value = PharmaClassController.PATH, produces = "application/json; charset=utf-8")
public class PharmaClassController {
	
	protected static final String PATH = "/pharmaclass";

	@Autowired(required = true)
	private PharmaService pharmaService;
	
	/**
	 * add pharmaclass
	 * 
	 * @param label
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/pharmaclass:{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{label}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addPharmaClass(@PathVariable String label) {

		return pharmaService.addpharmaClass(label);
	}
	
	
	/**
	 * Delete pharmaclass
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/pharmaclass:{id}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deletepharmaclass(@PathVariable Long id) {

		return pharmaService.deletepharmaClass(id);
	}

	/**
	 * Upadte pharmaclass
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/pharmaclass:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}/{label}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updatepharmaclass(@PathVariable Long id, @PathVariable String label) {

		return pharmaService.updatepharmaClass(id, label);
	}
	
	
	/**
	 * List pharmaclass
	 * 
	 * 
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/pharmaclass
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<PharmaClass> listpharmaclass() {
         
		return pharmaService.getListPharmaClass();
	}

	@RequestMapping(value="cc",method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getLogerUser(HttpServletRequest httpServletRequest){
		
		HttpSession httpSession = httpServletRequest.getSession();
	
		//httpSession.sessionManagement().maximumSessions(1).sessionRegistry(sessionRegistry());
	
		SecurityContext securityContext = (SecurityContext)
				httpSession.getAttribute("SPRING_SECURITY_CONTEXT");
		
		String username =securityContext.getAuthentication().getName();
	
		List<String> roles = new ArrayList<>();
		for(GrantedAuthority ga :securityContext.getAuthentication().getAuthorities()){
			roles.add(ga.getAuthority());
		
		}
		Map<String, Object> params = new HashMap<>() ;
		params.put("username", username);
		params.put("roles", roles);
		//List<String> roles = new ArrayList<>();
		return params;
		
	}
	

	
	
}
