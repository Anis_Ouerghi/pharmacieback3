package com.transtu.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.persistence.entities.Location;
import com.transtu.service.LocationService;

@RestController
@RequestMapping(value =LocationController.PATH, produces = "application/json; charset=utf-8")
public class LocationController extends AbstractController {
	

	protected static final String PATH = "/locations";
	
	@Autowired(required=true)
	LocationService locationService ;

	/**
	 * find all locations
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/locations
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<Location> findAll() {
	     return this.locationService.findAllLocations();
	}
}
