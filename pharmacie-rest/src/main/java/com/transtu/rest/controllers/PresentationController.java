package com.transtu.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.PharmaClass;
import com.transtu.persistence.entities.Presentation;
import com.transtu.service.PresentationService;

@RestController
@RequestMapping(value = PresentationController.PATH, produces = "application/json; charset=utf-8")
public class PresentationController {
	

	protected static final String PATH = "/presentation";

	@Autowired(required = true)
	private PresentationService presentationService;
	
	/**
	 * add presentation
	 * 
	 * @param label
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/presentation:{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{label}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addPresentation(@PathVariable String label) {

		return presentationService.addpresentation(label);
	}

	/**
	 * Delete presentation
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/presentation:{id}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deletePresentation(@PathVariable Long id) {

		return presentationService.deletePresentation(id);
	}
	
	
	/**
	 * List pharmaclass
	 * 
	 * 
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/pharmaclass
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Presentation> listPresentaion() {
         
		return presentationService.getListPresentation();
	}

	/**
	 * Upadte presentation
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/pharmaclass:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}/{label}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse deletepharmaclass(@PathVariable Long id, @PathVariable String label) {

		return presentationService.updatePresentation(id, label);
	}
}
