package com.transtu.rest.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.transtu.core.dto.ordonnanceNbre;
import com.transtu.core.jasper.PresecriptionResponseJasper;
import com.transtu.core.json.request.DistrubtionResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.PrescriptionFilterRequest;
import com.transtu.core.json.request.PrescriptionRequest;
import com.transtu.core.json.request.PresecriptionRequestjasper;
import com.transtu.core.json.request.updateAgentPresecription;
import com.transtu.core.json.request.updateDistrictDistribution;
import com.transtu.core.presecriptionDisplay.FiltrePresecriptionResponse;
import com.transtu.core.presecriptionDisplay.PrescriptionD;
import com.transtu.persistence.entities.Prescription;
import com.transtu.persistence.entities.PrescriptionLine;
import com.transtu.service.PrescriptionService;
import com.transtu.service.PresentationService;
@CrossOrigin()
@RestController
@RequestMapping(value = PrescriptionController.PATH, produces = "application/json; charset=utf-8")
public class PrescriptionController   extends AbstractController {
	
	protected static final String PATH = "/ordonnances";
	
	@Autowired(required = true)
	PrescriptionService prescriptionService;
	
	
	
	/**
	 * add  prescription
	 * 
	 * @param OrdonnanceRequest
	 * @return MessageResponse <br/>
	 *         <h4 style="color:green;">Path WS:
	 *         http://localhost:8080/api/ordonnances</h4>
	 */
	@Secured(value = { "admin" })
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public DistrubtionResponse addGlobalPrescription(@Valid @RequestBody PrescriptionRequest OrdonnanceRequest) {

		return prescriptionService.addPrescription(OrdonnanceRequest);
	}
	
	
	/**
	 * cancel Distribution
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/ordonnance/cancel/:id
	 * </h4>
	 */
	@RequestMapping(value = "/cancel/{id}", method = RequestMethod.GET)
	@ResponseBody
	public MessageResponse cancelInternalOrder(@PathVariable Long id) {
		return this.prescriptionService.CancelDistrubtion(id);
		
	}
	
	@RequestMapping(value = "/dynamic", method = RequestMethod.POST)
	@ResponseBody
	public FiltrePresecriptionResponse dynamicSearch(@RequestBody PrescriptionFilterRequest p)
			throws JsonParseException, JsonMappingException, IOException {
	
		return prescriptionService.prescriptionFilter(p);
	}
	
	@RequestMapping(value = "/lines", method = RequestMethod.GET)
	@ResponseBody
	public List<PrescriptionLine> getAllPrescriptionLines() {
		
		
		prescriptionService.getAllPrescriptionLines().stream().forEach(x->System.out.println( x.getProduct().getLib()));

		return prescriptionService.getAllPrescriptionLines();
	}
	
	

	/**
	 * getAllPresecriptionByAgent 
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/ordonnances/:agentId
	 * </h4>
	 */
	@RequestMapping(value = "/{agentId}", method = RequestMethod.GET)
	@ResponseBody
	public List<Prescription> getPresecriptionByAgent(@PathVariable String agentId, @RequestParam Long id ) {
		
		return prescriptionService.getPresecriptionByAgent(agentId, id);
	}
	
	
//	@RequestMapping(value = "/{agentId}", method = RequestMethod.GET)
//	@ResponseBody
//	public List<Prescription> getPresecriptionByAgent(@PathVariable String agentId ) {
//		
//	
//		return prescriptionService.getPresecriptionByAgent(agentId);
//	}
	
	
	
	@RequestMapping(value = "/nbreOrdonnance", method = RequestMethod.GET)
	@ResponseBody
	public ordonnanceNbre getNbreOrd() {
		
	
		return prescriptionService.getNbreOrdonnance();
	}

	
	@RequestMapping(value = "/dynamicHistory", method = RequestMethod.POST)
	@ResponseBody
	public Set<Prescription> presecriptionHistoryFilter(@RequestBody PrescriptionFilterRequest p)throws JsonParseException, JsonMappingException, IOException
			 {
		
		return prescriptionService.presecriptionHistoryFilter(p);
	}

	
	
	
	@RequestMapping(value = "/changeStateDistributed/{id}/{distributed}", method = RequestMethod.POST)
	@ResponseBody
	public void changeStateDistributed(@PathVariable Long id ,@PathVariable Boolean distributed) {
		
		 prescriptionService.changeStatedistributed(id, distributed);
	}
	
	
	
	@RequestMapping(value = "/statworkaccident", method = RequestMethod.POST)
	@ResponseBody
	public List<PresecriptionResponseJasper> statworkaccident(@RequestBody PresecriptionRequestjasper p)throws JsonParseException, JsonMappingException, IOException
			 {
		
		return prescriptionService.presecriptionDisease(p);
	}

	@RequestMapping(value = "/update-agent", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateAgentPresecription(@RequestBody updateAgentPresecription p)
			 {
		
		return prescriptionService.updateAgentPresecription(p);
	}
	
	@RequestMapping(value = "delete-preseription/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDistribution(@RequestParam Long id) {
		return prescriptionService.deletePresecription(id);
		 
	}
}
