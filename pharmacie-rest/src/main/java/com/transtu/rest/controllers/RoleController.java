package com.transtu.rest.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.AddProductRequest;
import com.transtu.core.json.request.AddRoleRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Product;
import com.transtu.persistence.entities.Role;
import com.transtu.service.ProductService;
import com.transtu.service.RoleService;

@CrossOrigin()
@RestController
@RequestMapping(value = RoleController.PATH, produces = "application/json; charset=utf-8")
public class RoleController  extends AbstractController {

	
	private static final Logger logger = LoggerFactory.getLogger(RoleController.class);

	protected static final String PATH = "/roles";

	@Autowired(required = true)
	private RoleService roleService;

	/**
	 * add role
	 * 
	 * @param AddRoleRequest
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8080/api/roles
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addRole(@RequestBody AddRoleRequest roleRequest) {

		return roleService.addRole(roleRequest);
		
	}
	/**
	 * get list roles
	 * 
	 * List<roles> <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/roles
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Role> getListRoles() {

		return roleService.getListRoles();
	}
}
