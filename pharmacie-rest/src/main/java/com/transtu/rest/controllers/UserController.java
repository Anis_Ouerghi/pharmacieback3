package com.transtu.rest.controllers;

import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdatePasswordRequest;
import com.transtu.core.json.request.UserDateRequest;
import com.transtu.core.json.request.UserRequest;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.service.UtilisateurService;


@CrossOrigin()
@RestController 
@RequestMapping(value=UserController.PATH, produces = "application/json; charset=utf-8")
public class UserController  extends AbstractController {
	
	protected static final String PATH = "/user";
	
	@Autowired(required=true)
	UtilisateurService UtilisateurService;
	
	/**
	 * add user
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/user
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addUser(@Valid @RequestBody UserRequest userRequest) {
		return this.UtilisateurService.addUser(userRequest);
	}
	
	/**
	 * get list users
	 * 
	 * List<Utilisateur> <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/Users
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Utilisateur> getListUsers() {

		return UtilisateurService.getListUsers();
	}
	
	
	
	/**
	 * get currentuser
	 * 
	 * List<Utilisateur> <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/User
	 * </h4>
	 */
	@RequestMapping(value = "currentuser", method = RequestMethod.GET)
	@ResponseBody
	public String getCurrentUser() {

		return UtilisateurService.getCurrentUserDetails();
	}
	
	
	/**
	 * activate user
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/user
	 * </h4>
	 */
	@CrossOrigin
	@RequestMapping(value = "/activate/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse activateUser(@PathVariable Long id){
		return this.UtilisateurService.activateUser(id);
	}
	
	/**
	 * desactivate user
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/user
	 * </h4>
	 */
	@CrossOrigin
	@RequestMapping(value = "/desactivate/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse desactivateUser(@PathVariable Long id){
		return this.UtilisateurService.desactivateUser(id);
	}
	
	
	/**
	 * desactivate user
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/user
	 * </h4>
	 * @throws ParseException 
	 */
	@CrossOrigin
	@RequestMapping(value = "/date", method = RequestMethod.POST)
	@ResponseBody
	public List<Utilisateur> getListUsersByIdAndDate( @RequestBody UserDateRequest userDateRequest ) throws ParseException{
		
//		(name = "date", required= false) @DateTimeFormat(pattern = "yyyy-MM-dd"), defaultValue = "2017-09-11"
     	//System.out.println("-----------------------date--AVANT PARSE "+date);
//		SimpleDateFormat parseFormat = 
//		    new SimpleDateFormat("yy-MM-dd");
//			Date date1 = parseFormat.parse("17-02-10");
////			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yy");
////			String result = format.format(date);
//			System.out.println("-----------------------date--apres  PARSE "+date1);
		
		return this.UtilisateurService.getListUsersByDay(userDateRequest.getUserId(),userDateRequest.getDate());
	}
	
	
	
	
	
	//(@RequestParam(name = "dateDay", defaultValue = "2017-09-11") @DateTimeFormat(pattern = "yyyy-MM-dd")
	
	
	
	
	/**
	 * delete user
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/user
	 * </h4>
	 */
	@CrossOrigin
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse deleteUser(@PathVariable Long id){
		return this.UtilisateurService.deleteUser(id);
	}
	
	/**
	 * update user
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/user
	 * </h4>
	 */
	@CrossOrigin
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateUser(@Valid @RequestBody UserRequest userRequest){
		return this.UtilisateurService.updateUser(userRequest);
	}
	
	/**
	 * update password
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/user
	 * </h4>
	 */
	@CrossOrigin
	@RequestMapping(value = "/updatePassword/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updatePassword(@Valid @RequestBody  UpdatePasswordRequest updatePasswordRequest){
		return this.UtilisateurService.updatePassword(updatePasswordRequest);
	}
	
	}
	


