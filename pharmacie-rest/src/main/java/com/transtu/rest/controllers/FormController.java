package com.transtu.rest.controllers;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Form;
import com.transtu.service.FormService;

@RestController
@RequestMapping(value =FormController.PATH, produces = "application/json; charset=utf-8")
public class FormController extends AbstractController {

	protected static final String PATH = "/forms";
	
	@Autowired(required=true)
	FormService formService ;
	
	/**
	 * add form
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/forms:{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{label}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addForm( @RequestBody @NotNull String label) {
	     return this.formService.addForm(label);
	}
	
	/**
	 * update form
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/forms:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "{id}/{label}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateForm(@PathVariable Long id, @PathVariable String label) {
	     return this.formService.updateForm(id, label);
	}
	
	/**
	 * find form by label 
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/forms/label/:label
	 * </h4>
	 */
	@RequestMapping(value = "/{label}", method = RequestMethod.GET)
	@ResponseBody
	public Form findByLabel(@PathVariable String label ) {
	     return this.formService.findByLabel(label);
	}
	
	/**
	 * find form by id 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/forms/:id
	 * </h4>
	 */
//	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
//	@ResponseBody
//	public Form findById( @PathVariable Long id ) {
//	     return this.formService.findById(id);
//	}
	
	/**
	 * find all forms 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/forms
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<Form> findForms() {
	     return this.formService.findAllForms();
	}
	
	/**
	 * delete form 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/forms/:id
	 * </h4>
	 */
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteForm(@PathVariable Long id) {
	     return this.formService.deleteForm(id);
	}
	
	
}
