package com.transtu.rest.controllers;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.AddDayRequest;
import com.transtu.core.json.request.AddUserDayRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Day;
import com.transtu.service.DayService;
@CrossOrigin()
@RestController
@RequestMapping(value =DayController.PATH, produces = "application/json; charset=utf-8")
public class DayController {

	protected static final String PATH = "/days";
	
	@Autowired(required=true)
	DayService dayService ; 
	
	
	/**
	 * add day
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/days
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addDay(@RequestBody AddDayRequest dayRequest) {
		return this.dayService.addDay(dayRequest);
	}
	
	/**
	 * get all days
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/days
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<Day> getAllDays() {
		return this.dayService.getAllDays();
	}
	
	/**
	 * update day
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/days
	 * </h4>
	 */
	@RequestMapping( method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateDay(@RequestBody AddDayRequest dayRequest) {
		return this.dayService.updateDate(dayRequest);
	}
	
	/**
	 * delete day
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/days/{id}
	 * </h4>
	 */
	@RequestMapping(value="/{id}",method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDay(@PathVariable Long id) {
		return this.dayService.deleteDate(id);
	}
	
	/**f
	 * get all days filtered by type and state
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/days/{state}/{id}
	 * </h4>
	 */
	@RequestMapping(value="/{state}/{id}",method = RequestMethod.GET)
	@ResponseBody
	public List<Day> getByStateAndDayType(@RequestParam boolean state ,@RequestParam Long dayTypeId) {
		return this.dayService.getDayByStateAndDayType(state, dayTypeId);
	}
	
	/**
	 * add day-users
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/days
	 * </h4>
	 */
	@RequestMapping(value = "addDayUser", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addDayUser(@RequestBody AddUserDayRequest addUserDayRequest) {
		return this.dayService.addUserDay(addUserDayRequest);
		  
	}
	
	@RequestMapping(value = "/{dayId}/{userId}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse cancelDayUser(@RequestParam Long userId,@RequestParam Long dayId) {
		return this.dayService.cancelUserDay(userId, dayId);
		  
	}
	
	@RequestMapping(value="/open/test",method = RequestMethod.GET)
	@ResponseBody
	public void openDay() throws ParseException {
		this.dayService.openDayToAuthentication();
	}
	
}
