package com.transtu.rest.controllers;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.DistributionUnit;
import com.transtu.service.DistributionUnitService;

@RestController
@RequestMapping(value =DistributionUnitController.PATH, produces = "application/json; charset=utf-8")
public class DistributionUnitController extends AbstractController {

	protected static final String PATH = "/distUnits";
	
	@Autowired(required=true)
	DistributionUnitService distributionUnitService ;
	
	/**
	 * add distribution unit
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/distUnits
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addDistributionUnit( @RequestBody @NotNull String label) {
	     return this.distributionUnitService.addDistributionUnit(label);
	}
	
	/**
	 * update distribution unit
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/distUnits
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateDistributionUnit( @RequestBody @NotNull DistributionUnit distributionUnit ) {
	     return this.distributionUnitService.updateDistributionUnit(distributionUnit);
	}
	
	/**
	 * find distribution unit by id 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/distUnits/:id
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public DistributionUnit findById( @PathVariable Long id ) {
	     return this.distributionUnitService.findById(id);
	}
	
	/**
	 * find distribution unit by label 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/distUnits/label/:label
	 * </h4>
	 */
	@RequestMapping(value = "/label/{label}", method = RequestMethod.GET)
	@ResponseBody
	public DistributionUnit findByLabel( @PathVariable String label ) {
	     return this.distributionUnitService.findByLabel(label);
	}
	
	/**
	 * find all distribution units 
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/distUnits
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<DistributionUnit> findAllDistributionUnits() {
	     return this.distributionUnitService.findAllDistributionUnits();
	}
	
	/**
	 * delete distribution unit
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/distUnits/:id
	 * </h4>
	 */
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDistributionUnit(@PathVariable Long id) {
	     return this.distributionUnitService.deleteDistributionUnit(id);
	}
}
