package com.transtu.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.service.DistributionLineService;

@RestController
@RequestMapping(value =DistributionLineController.PATH, produces = "application/json; charset=utf-8")
public class DistributionLineController {

	protected static final String PATH = "/distributions";
	
	@Autowired(required=true)
	DistributionLineService distributionLineService ;
}
