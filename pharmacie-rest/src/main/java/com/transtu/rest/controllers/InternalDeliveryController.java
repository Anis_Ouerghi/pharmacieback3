package com.transtu.rest.controllers;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.jasper.DistributionJasper;
import com.transtu.core.json.request.DeliveryRequest;
import com.transtu.core.json.request.DistributionRequestPdf;
import com.transtu.core.json.request.InterDeliveryFilterResponse;
import com.transtu.core.json.request.InternalDeliveryFilter;
import com.transtu.core.json.request.InternalDeliveryRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateDeliveryRequest;
import com.transtu.persistence.entities.InternalDelivery;
import com.transtu.persistence.entities.Reception;
import com.transtu.service.InternalDeliveryService;
@CrossOrigin()
@RestController 
@RequestMapping(value=InternalDeliveryController.PATH, produces = "application/json; charset=utf-8")
public class InternalDeliveryController  extends AbstractController {

protected static final String PATH = "/internalDelivery";
	
	@Autowired(required=true)
	InternalDeliveryService internalDelvieryService;
	
	/**
	 * add internal delivery
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/internalDelivery
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addInternalDelivery(@Valid @RequestBody InternalDeliveryRequest internalDeliveryRequest ) {
		return this.internalDelvieryService.addInternalDelivery(internalDeliveryRequest);
	}
	
	/**
	 * cancel internal delivery
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/internalDelivery/cancel/:id
	 * </h4>
	 */
	@RequestMapping(value = "/cancel/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteInternalDelivery(@PathVariable Long id ) {
		return this.internalDelvieryService.deleteInternalDelivery(id);
	}
	
	/**
	 * update internal delivery
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/internalDelivery
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateInternalDelivery(@Valid  @RequestBody UpdateDeliveryRequest updateDeliveryRequest ) {
		return this.internalDelvieryService.updateInternalDelviery(updateDeliveryRequest);
	}
	
	/**
	 * filter internal delivery
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/internalDelivery
	 * </h4>
	 */
	@RequestMapping(value = "/dynamic", method = RequestMethod.POST)
	@ResponseBody
	public InterDeliveryFilterResponse  filterInternalDelivery(@RequestBody InternalDeliveryFilter internalDeliveryFilter) {
		return this.internalDelvieryService.filterInternalDelivery(internalDeliveryFilter);
	}
	
	
	/**
	 * change state internal Delivery
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/livraison-interne/changeState/:{internalDeliveryId}/{stateId}
	 * </h4>
	 */
	@RequestMapping(value = "/changeState/{internalDeliveryId}/{stateId}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse changeStateInternalDlivery(@PathVariable Long internalDeliveryId, @PathVariable Long stateId ) {
		return this.internalDelvieryService.changeStateInternalDelivery(internalDeliveryId, stateId);
	}
	
	
	@RequestMapping(value = "{referenceId}/{deliverynum}", method = RequestMethod.GET)
	@ResponseBody
	public InternalDelivery findreceptionNum(@RequestParam Long referenceId,  @RequestParam String deliverynum) {

		return internalDelvieryService.findByInternalDeliveryNum(referenceId, deliverynum);
		 
	}
	
	

}
