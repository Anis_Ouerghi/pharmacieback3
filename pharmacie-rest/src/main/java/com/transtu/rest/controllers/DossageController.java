package com.transtu.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Dosage;
import com.transtu.persistence.entities.Presentation;
import com.transtu.service.DossageService;
import com.transtu.service.PresentationService;

@RestController
@RequestMapping(value = DossageController.PATH, produces = "application/json; charset=utf-8")
public class DossageController {


	protected static final String PATH = "/dossage";

	@Autowired(required = true)
	private DossageService dossageService;
	
	/**
	 * add dossage
	 * 
	 * @param label
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/dossage:{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{label}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addDossage(@PathVariable String label) {

		return dossageService.addossage(label);
	}

	/**
	 * Delete presentation
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/dossage:{id}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDossage(@PathVariable Long id) {

		return dossageService.deleteDossage(id);
	}
	
	
	/**
	 * List pharmaclass
	 * 
	 * 
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/dossage
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Dosage> listDossage() {
         
		return dossageService.getListDossage();
	}

	/**
	 * Upadte presentation
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/dossage:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}/{label}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateDossage(@PathVariable Long id, @PathVariable String label) {

		return dossageService.updateDossage(id, label);
	}
}
