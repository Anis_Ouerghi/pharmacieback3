package com.transtu.rest.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.service.MailService;

@RestController
@RequestMapping(value = MailController.PATH, produces = "application/json; charset=utf-8")
public class MailController {
	
	
	protected static final String PATH = "/mail";
	
	
	@Autowired
    private MailService mailService;


    @RequestMapping(value = "/send-mail",method = RequestMethod.POST)
    private String sendMail(HttpServletRequest request) {
         String covere = request.getParameter("covere");

        try {
            mailService.sendMail(covere);
            return "apply";
        } catch (MailException e) {
            e.printStackTrace();
        }
        return "apply";
    }

}
