package com.transtu.rest.controllers;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.AddFamilyRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Family;
import com.transtu.persistence.entities.Form;
import com.transtu.service.FamilyService;

//@CrossOrigin()
//@RestController 
//@RequestMapping("/famille")
@RestController
@RequestMapping(value =FamilyController.PATH, produces = "application/json; charset=utf-8")
public class FamilyController  extends AbstractController {
	
	protected static final String PATH = "/family";
	
	@Autowired(required = true)
	private FamilyService familleService;

	
	/**
	 * add family
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/family:{label}
	 * </h4>
	 */
	//@Secured(value = { "admin" })
	@RequestMapping(value = "/{label}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addFamille(@RequestBody @NotNull String label){
		return familleService.addFamily(label);
	}

	/**
	 * update family
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/family:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "{id}/{label}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateFamily(@PathVariable Long id, @PathVariable String label) {
	     return this.familleService.updateFamily(id, label);
	}
	
	/**
	 * find all forms 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/forms
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<Family> findFamily() {
	     return this.familleService.getListFamily();
	}
	
	/**
	 * delete form 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/forms/:id
	 * </h4>
	 */
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteFalily(@PathVariable Long id) {
	     return this.familleService.deleteFamily(id);
	}
	
}
