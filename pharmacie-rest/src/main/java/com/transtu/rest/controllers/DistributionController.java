package com.transtu.rest.controllers;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.transtu.core.jasper.DistributionJasper;
import com.transtu.core.jasper.UserDistStatestiqueJasper;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.core.json.request.DistributionRequest;
import com.transtu.core.json.request.DistributionRequestPdf;
import com.transtu.core.json.request.DistrubtionResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.updateAgentPresecription;
import com.transtu.core.json.request.updateDistrictDistribution;
import com.transtu.core.json.request.updateDoctorPrescription;
import com.transtu.persistence.entities.Distribution;
import com.transtu.service.DistributionService;
import com.transtu.service.StockMovementService;


@RestController
@RequestMapping(value =DistributionController.PATH, produces = "application/json; charset=utf-8")
public class DistributionController  extends AbstractController {
	
	protected static final String PATH = "/distributions";
	
	@Autowired(required=true)
	DistributionService distributionService;
	
	
	@Autowired(required=true)
	StockMovementService stockMovementService;

	/**
	 * add distribution
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/distributions
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public DistrubtionResponse addDistribution(@Valid @RequestBody DistributionRequest distributionRequest) {
	     return this.distributionService.addDistribution(distributionRequest);
	}


	/**
	 * list distribution
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/distributions/{AgentID}
	 * </h4>
	 */
	@RequestMapping(value = "{AgentID}", method = RequestMethod.GET)
	@ResponseBody
	public List<Distribution> listDistributionByAgent(@PathVariable String AgentID) {
	     return this.distributionService.listDistributionByAgent(AgentID);
	}
	
	/**
	 * list distribution from pdf 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/distributions/jasper
	 * </h4>
	 */
	@RequestMapping(value = "/jasper", method = RequestMethod.POST)
	@ResponseBody
	public List<DistributionJasper> listDistributionFromPdf(@RequestBody DistributionRequestPdf date) {
	     return this.distributionService.getDistributionsFromPdf(date);
	}
	
	/**
	 * list distribution by agent from pdf
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/distributions/jasper/{AgentID}
	 * </h4>
	 */
	@RequestMapping(value = "/jasper/{agentID}", method = RequestMethod.POST)
	@ResponseBody
	public List<DistributionJasper> listDistributionByAgentFromPdf(@RequestBody DistributionRequestPdf date ) {
	     return this.distributionService.getDistributionsByAgentFromPdf(date);
	}

	
	@RequestMapping(value = "{referenceId}/{distrubutionNum}", method = RequestMethod.GET)
	@ResponseBody
	public Distribution findByDistrubutionNum(@RequestParam Long referenceId,  @RequestParam String distrubutionNum) {

		return distributionService.findByDistrubutionNum(referenceId, distrubutionNum);
		 
	}
	

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	@RequestMapping(value = "/update-district", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateAgentPresecription(@RequestBody updateDistrictDistribution p)
			 {
		
		return distributionService.updateDistrictDistribution(p);
	}
	
	
	@RequestMapping(value = "/update-presecription-type", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateTypePresecription(@RequestBody updateDoctorPrescription   p)
			 {
		
		return distributionService.updateDoctorPresrcrption(p);
	}
	
	
	
	@RequestMapping(value = "distribution/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDistribution(@RequestParam Long id) {

		return distributionService.deleteDistribution(id);
		 
	}
	
	@RequestMapping(value = "distributionligne/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDistributionligne(@RequestParam Long id) {

		return distributionService.deleteDistributionLine(id);
		 
	}
	
	@RequestMapping(value = "stat-user-distribution/{startDate}/{endtDate}", method = RequestMethod.POST)
	@ResponseBody
	public List<UserDistStatestiqueJasper> deleteDistributionligne(@RequestParam Date startDate,@RequestParam Date endtDate) {

		return distributionService.findByDateDistAndUtilisateur(startDate, endtDate);
		 
	}
	
}
