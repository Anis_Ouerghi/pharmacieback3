//package com.transtu.rest.controllers;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.transtu.core.json.request.MessageResponse;
//import com.transtu.service.ExternalDeliveryService;
//
//@CrossOrigin()
//@RestController
//@RequestMapping(value=ExternalOrderController.PATH, produces = "application/json; charset=utf-8")
//public class ExternalDeliveryController  extends AbstractController {
//
//protected static final String PATH = "/livraison-externe";
//	
//	@Autowired(required=true)
//	ExternalDeliveryService externalDeliveryService;
//	
//	
//	
//	/** 
//	 * Fence External Delivery  
//	 * 
//	 *  <br/>
//	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/Livraison-externe/fermer-livaraison/:id
//	 * </h4>
//	 */
//	@RequestMapping(value = "/ferme-livaraison/{id}", method = RequestMethod.POST)
//	@ResponseBody
//	public MessageResponse fenceDelivery (@PathVariable Long id){
//		return externalDeliveryService.fenceDelivery(id);
//	}	
//	
//	/**
//	 * Open External Delivery  
//	 * 
//	 *  <br/>
//	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/Livraison-externe/ouvrir-livaraison/:id
//	 * </h4>
//	 */
//	@RequestMapping(value = "/ouvrir-livaraison/{id}", method = RequestMethod.POST)
//	@ResponseBody
//	public MessageResponse openDelivery (@PathVariable Long id){
//		return externalDeliveryService.openDelivery(id);
//	}	
//	
//	/**
//	 * cancel External Delivery  
//	 * 
//	 *  <br/>
//	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/Livraison-externe/annuler-livaraison/:id
//	 * </h4>
//	 */
//	@RequestMapping(value = "/annuler-livaraison/{id}", method = RequestMethod.POST)
//	@ResponseBody
//	public MessageResponse cancelDelivery (@PathVariable Long id){
//		return externalDeliveryService.cancelDelivery(id);
//	}	
//}
