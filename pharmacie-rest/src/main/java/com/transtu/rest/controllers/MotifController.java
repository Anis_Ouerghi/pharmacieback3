package com.transtu.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Dosage;
import com.transtu.persistence.entities.Motif;
import com.transtu.service.DossageService;
import com.transtu.service.MotifService;

@RestController
@RequestMapping(value = MotifController.PATH, produces = "application/json; charset=utf-8")
public class MotifController {
	
	protected static final String PATH = "/motif";

	@Autowired(required = true)
	private MotifService motifService;
	
	/**
	 * add motif
	 * 
	 * @param label
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/motif:{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{label}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addMotif(@PathVariable String label) {

		return motifService.addMotif(label);
	}
	
	/**
	 * Delete Motif
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/motif:{id}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteMotif(@PathVariable Long id) {

		return motifService.deleteMotif(id);
	}
	
	/**
	 * List Motifs
	 * 
	 * 
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/motif
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Motif> listMotif() {
         
		return motifService.getListMotif();
	}

	/**
	 * Upadte motif
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/motif:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}/{label}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateForm(@PathVariable Long id, @PathVariable String label) {

		return motifService.updateMotif(id, label);
	}

}
