package com.transtu.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Customer;
import com.transtu.service.CustomerService;

@RestController
@RequestMapping(value = CustomerController.PATH, produces = "application/json; charset=utf-8")
public class CustomerController {

	
	protected static final String PATH = "/client";

	@Autowired(required = true)
	private CustomerService customerService;
	
	/**
	 * add client
	 * 
	 * @param label
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/client:{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{label}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addCustomer(@PathVariable String label) {

		return customerService.addCustomer(label);
	}

	/**
	 * Delete client
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/client:{id}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteCustomer(@PathVariable Long id) {

		return customerService.deleteCustomer(id);
	}
	
	
	/**
	 * List client
	 * 
	 * 
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/client
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Customer> listCustomer() {
         
		return customerService.getListCustomer();
	}

	/**
	 * Upadte client
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/client:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}/{label}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse deleteCustomert(@PathVariable Long id, @PathVariable String label) {

		return customerService.updateCustomer(id, label);
	}
}
