package com.transtu.rest.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.transtu.core.dto.AuthenticationResponseDTO;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;

/**
 * 
 *         TODO: all the Controllers (except AuthenticationController) should
 *         extends AbstractController!!!
 */
public class AbstractController {

	private final static Logger logger = LoggerFactory.getLogger(AbstractController.class);

	String getAuthorizedUserName() {

		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && !(auth.getPrincipal() instanceof AuthenticationResponseDTO)) {
			logger.error("No authorized user found");
			RestExceptionCode code = RestExceptionCode.NO_USER_AUTHORIZED;
			RestException ex = new RestException(code.getError(), code);
			throw ex;

		}
		return ((AuthenticationResponseDTO) auth.getPrincipal()).getUserName();

	}
}
