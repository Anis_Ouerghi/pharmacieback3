package com.transtu.rest.controllers;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.InternalOrderFilter;
import com.transtu.core.json.request.InternalOrderRequest;
import com.transtu.core.json.request.InternalordfilterResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateOrderRequest;
import com.transtu.persistence.entities.InternalOrder;
import com.transtu.service.InternalOrderService;


@CrossOrigin()
@RestController 
@RequestMapping(value=InternalOrderController.PATH, produces = "application/json; charset=utf-8")
public class InternalOrderController  extends AbstractController {
	
	protected static final String PATH = "/commande-interne";
	
	@Autowired(required=true)
	InternalOrderService internalOrderService;
	
	/**
	 * add internal Order
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-interne
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addInternalOrder(@Valid @RequestBody InternalOrderRequest internalOrderRequest) {
		return this.internalOrderService.addInternalOrder(internalOrderRequest);
	}
	

	/**
	 * list internal Order
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-interne/list
	 * </h4>
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	public List<InternalOrder> ListInternalOrder() {
		return this.internalOrderService.getlistOrder();
		
	}
	
	/**
	 * list internal Order
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-interne/list/:date
	 * </h4>
	 */

	@RequestMapping(value = "/list/{date}", method = RequestMethod.GET)
	@ResponseBody
	public List<InternalOrder> listInternalOrder(@RequestParam(name = "date", defaultValue = "2017-01-01") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
		
		return this.internalOrderService.findByCurrentYear(date);
		
	}

	/**
	 * cancel internal Order
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-interne/cancel/:id
	 * </h4>
	 */
	@RequestMapping(value = "/cancel/{id}", method = RequestMethod.GET)
	@ResponseBody
	public MessageResponse cancelInternalOrder(@PathVariable Long id) {
		return this.internalOrderService.cancelInternalOrder(id);
		
	}
	
	/**
	 * change state internal Order
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-interne/changeState/:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "/changeState/{internalOrderId}/{stateId}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse changeStateInternalOrder(@PathVariable Long internalOrderId, @PathVariable Long stateId ) {
		return this.internalOrderService.changeStateInternalOrder(internalOrderId, stateId);
	}
	
	/**
	 * update internal Order
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-interne
	 * </h4>
	 */
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateOrder(@Valid  @RequestBody UpdateOrderRequest updateOrderRequest){
		return this.internalOrderService.updateOrder(updateOrderRequest);
	}
	
	/**
	 * filter internal order
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8081/api/commande-interne
	 * </h4>
	 */
	@RequestMapping(value = "/dynamic", method = RequestMethod.POST)
	@ResponseBody
	public InternalordfilterResponse filterInternalOrder(@RequestBody InternalOrderFilter internalOrderFilter) {
		return this.internalOrderService.filterInternalOrder(internalOrderFilter);
	}

}
