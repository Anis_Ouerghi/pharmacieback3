package com.transtu.rest.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.ExternaOrderRequest;
import com.transtu.core.json.request.ExternalOrderFilter;
import com.transtu.core.json.request.ExternalOrderFilterResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateExternalOrderListRequest;
import com.transtu.core.json.request.UpdateExternalOrderRequest;
import com.transtu.service.ExternalOrderService;

@CrossOrigin()
@RestController
@RequestMapping(value=ExternalOrderController.PATH, produces = "application/json; charset=utf-8")
public class ExternalOrderController  extends AbstractController {

	protected static final String PATH = "/commande-externe";
	
	@Autowired(required=true)
	ExternalOrderService externalOrderService;
	
	
	/**
	 * add External Order
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-externe
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addexternalOrder(@Valid @RequestBody ExternaOrderRequest externaOrderRequest) {
		return this.externalOrderService.addExternalOrder(externaOrderRequest);
	}
	
	/**
	 * Cancel External Order
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-externe/cancel/:id
	 * </h4>
	 */
	@RequestMapping(value = "/cancel/{id}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse cancelexternalOrder(@PathVariable Long id) {
		return externalOrderService.CancelExternalOrder(id);
	}
	
	/**
	 * update external Order
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-externe
	 * </h4>
	 */
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateExternalOrder(@Valid  @RequestBody UpdateExternalOrderRequest updateExternalOrderRequest){
		
		return this.externalOrderService.UpdateExternalOrder(updateExternalOrderRequest);
	}
	
	/**
	 * update external Order
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-externe
	 * </h4>
	 */
//	@RequestMapping(value = "/update", method = RequestMethod.POST)
//	@ResponseBody
//	public MessageResponse updateExternalOrderList(@Valid  @RequestBody UpdateExternalOrderListRequest updateExternalOrderListRequest){
//		
//		return this.externalOrderService.UpdateExternalOrderList(updateExternalOrderListRequest);
//	}
	
	/**
	 * filter external order
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8081/api/commande-externe
	 * </h4>
	 */
	@RequestMapping(value = "/dynamic", method = RequestMethod.POST)
	@ResponseBody
	public ExternalOrderFilterResponse filterInternalOrder(@RequestBody ExternalOrderFilter externalOrderFilter) {
		return this.externalOrderService.filterExternalOrder(externalOrderFilter);
	}

	/**
	 * change state external Order
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-externe/changeState/:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "/changeState/{externalOrderId}/{stateId}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse changeStateExternalOrder(@PathVariable Long externalOrderId, @PathVariable Long stateId ) {
		return this.externalOrderService.changeStateExternalOrder(externalOrderId, stateId);
	}
	
	
	
	/**
	 * change state external Order
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-externe/changeState/:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "/delete/{externalOrderId}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteExternalOrderLine(@PathVariable Long externalOrderId ) {
		return this.externalOrderService.deleteExternalOrderLine(externalOrderId);
	}
	
}
