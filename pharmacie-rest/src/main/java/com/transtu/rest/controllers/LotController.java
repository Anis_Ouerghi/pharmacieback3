package com.transtu.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.InterDeliveryFilterResponse;
import com.transtu.core.json.request.InternalDeliveryFilter;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateLotRequest;
import com.transtu.persistence.entities.Lot;
import com.transtu.persistence.entities.Motif;
import com.transtu.service.LotService;

@RestController
@RequestMapping(value = LotController.PATH, produces = "application/json; charset=utf-8")
public class LotController  extends AbstractController {
	
	
	protected static final String PATH = "/lots";

	@Autowired(required = true)
	private LotService lotService;

	/**
	 * add product
	 * 
	 * @param AddProductRequest
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8085/api/produits
	 * </h4>
	 */
	@RequestMapping(value = "/{id}/{state}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse changestateProduct(@PathVariable Long id, @PathVariable boolean state) {

		return lotService.updateLot(id, state);
	}
	
	
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Lot> listLot() {
         
		return lotService.ListActivatelLot();
	}
	
	
	
	
	@RequestMapping(value = "/UpdateLot", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse  filterInternalDelivery(@RequestBody UpdateLotRequest Oldlot) {
		return this.lotService.updateDateLabelLot(Oldlot);
	}
	
	
	

}
