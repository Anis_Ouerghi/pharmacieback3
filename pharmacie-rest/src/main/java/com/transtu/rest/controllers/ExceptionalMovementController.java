//package com.transtu.rest.controllers;
//
//import java.util.List;
//
//import javax.validation.Valid;
//import javax.validation.constraints.NotNull;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.transtu.core.json.request.AddExceptionalMovement;
//import com.transtu.core.json.request.DistrubtionResponse;
//import com.transtu.core.json.request.MessageResponse;
//import com.transtu.core.json.request.MovementResponse;
//import com.transtu.core.json.request.PrescriptionRequest;
//import com.transtu.persistence.entities.Dosage;
////import com.transtu.persistence.entities.ExceptionalMovement;
//import com.transtu.persistence.entities.Movement;
//import com.transtu.service.ExceptionalMovementService;
//
//
//@RestController
//@RequestMapping(value =ExceptionalMovementController.PATH, produces = "application/json; charset=utf-8")
//public class ExceptionalMovementController  extends AbstractController{
//	
//	
//protected static final String PATH = "/exceptionalMovement";
//	
//	@Autowired(required=true)
//	ExceptionalMovementService exceptionalMovementService   ;
//	
//	
//	/**
//	 * add exceptionalMovement
//	 * 
//	 *  <br/>
//	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/exceptionalMovement
//	 * </h4>
//	 */
//	@RequestMapping(value = "", method = RequestMethod.POST)
//	@ResponseBody
//	public MessageResponse AddexceptionalMovementService(@Valid @RequestBody AddExceptionalMovement addExceptionalMovement) {
//
//		 exceptionalMovementService.addExceptionalMovement(addExceptionalMovement);
//		 return null;
//	}
//	
//	
//	/**
//	 * List exceptionalMovement
//	 * 
//	 * 
//	 * @return boolean <br/>
//	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/exceptionalMovement
//	 * </h4>
//	 */
//	@RequestMapping(value = "", method = RequestMethod.GET)
//	@ResponseBody
//	public List<MovementResponse> getListExceptionalMovement() {
//         
//		return exceptionalMovementService.getListExceptionalMovement();
//	}
//	
//}
