package com.transtu.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Type;
import com.transtu.service.TypeService;

@RestController
@RequestMapping(value = TypeController.PATH, produces = "application/json; charset=utf-8")
public class TypeController {
	

	protected static final String PATH = "/type";

	@Autowired(required = true)
	private TypeService typeService;
	
	/**
	 * add type
	 * 
	 * @param label
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/presentation:{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{label}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addType(@PathVariable String label) {

		return typeService.addtype(label);
	}

	/**
	 * Delete type
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/type:{id}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deletePresentation(@PathVariable Long id) {

		return typeService.deleteType(id);
	}
	
	
	/**
	 * List pharmaclass
	 * 
	 * 
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/pharmaclass
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Type> listType() {
         
		return typeService.getListType();
	}

	/**
	 * Upadte Type
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/type:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}/{label}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updatetype(@PathVariable Long id, @PathVariable String label) {

		return typeService.updateType(id, label);
	}

}
