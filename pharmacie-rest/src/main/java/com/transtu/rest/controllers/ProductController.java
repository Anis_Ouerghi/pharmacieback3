package com.transtu.rest.controllers;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.ActiveProductByLabelStartWith;
import com.transtu.core.json.request.ActiveProductByLabelStartWithAndDep;
import com.transtu.core.json.request.AddProductRequest;
import com.transtu.core.json.request.DuplcateEmptyResp;
import com.transtu.core.json.request.FilterProductAssistOrderResponse;
import com.transtu.core.json.request.FilterProductResponse;
import com.transtu.core.json.request.FilterProductResponseSpecific;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.ProductAutoCompleteResponse;
import com.transtu.core.json.request.ProductQuantityDep;
import com.transtu.core.json.request.ProductRequestFilter;
import com.transtu.core.json.request.ProductRequestFilterAssistOrder;
import com.transtu.core.json.request.ProductResponse;
import com.transtu.core.json.request.ProductResponseSearch;
import com.transtu.core.json.request.UpdateProductRequest;
import com.transtu.core.product.LightProduct;
import com.transtu.persistence.entities.Depot;
import com.transtu.persistence.entities.Lot;
import com.transtu.persistence.entities.Product;
import com.transtu.service.LotService;
import com.transtu.service.ProductService;
//@CrossOrigin()
@RestController
@RequestMapping(value = ProductController.PATH, produces = "application/json; charset=utf-8")
public class ProductController extends AbstractController {

	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

	protected static final String PATH = "/produits";

	@Autowired(required = true)
	private ProductService produitService;
	
	@Autowired(required = true)
	private LotService lotService;

	/**
	 * add product
	 * 
	 * @param AddProductRequest
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8085/api/produits
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addProduct(@RequestBody AddProductRequest addProduit) {

		return produitService.addProduct(addProduit);
	}
	/**
	 * add product
	 * 
	 * @param AddProductRequest
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8085/api/produits
	 * </h4>
	 */
	@RequestMapping(value = "/auto-complete", method = RequestMethod.POST)
	@ResponseBody
	public List<LightProduct> getActivedProductLabelbStartingWith(@RequestBody ActiveProductByLabelStartWith activeProductByLabelStartWith) {
		
		return produitService.getActivedProductLabelbStartingWith(activeProductByLabelStartWith);
	}
	
	
	
	/**
	 * add product
	 * 
	 * @param AddProductRequest
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8085/api/produits
	 * </h4>
	 */
	@RequestMapping(value = "produit-depot/auto-complete", method = RequestMethod.POST)
	@ResponseBody
	public List<LightProduct> getActivedProductLabelbStartingWithByDep(@RequestBody ActiveProductByLabelStartWithAndDep activeProductByLabelStartWith) {
		
		return produitService.getActivedProductLabelbStartingWithByDep(activeProductByLabelStartWith);
	}

	/**
	 * get list products
	 * 
	 * List<Produit> <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/produits
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET, produces = { "application/json" })
	//@Secured("ROLE_ADM")
	public List<ProductResponse> getListProducts() {

		return produitService.getListProducts();
	}
	/**
	 * get  product by bar code
	 * 
	 * Produit <br/>
	 * <h4 style="color:green;">Path WS: http://localhost:8080/api/produits/codebar?code=
	 * </h4>
	 */
	@RequestMapping(value = "/codebar", method = RequestMethod.GET)
	@ResponseBody
	public List<Product> getProductByBarCode(@RequestParam String code) {
		
		return produitService.findByBarCode(code);
	}
	
	/**
	 * get  product by Libelle
	 * 
	 * list<Produit> <br/>
	 * <h4 style="color:green;">Path WS: http://localhost:8080/api/produits/libelle?val=
	 * </h4>
	 */
	@RequestMapping(value = "/libelle", method = RequestMethod.GET)
	@ResponseBody
	public List<Product> getProductByLib(@RequestParam String val) {
		
		return produitService.getLibStartingWith(val);
	}
	
	
	
	/**
	 * get  product by Libelle
	 * 
	 * list<Produit> <br/>
	 * <h4 style="color:green;">Path WS: http://localhost:8080/api/produits/libelle?val=
	 * </h4>
	 */
	@RequestMapping(value = "quantity-product-depot", method = RequestMethod.GET)
	@ResponseBody
	public ProductQuantityDep quantityProductByDepot(@RequestParam Long depotId, @RequestParam boolean etat) {
		
		return produitService.existProductByDepot(depotId, etat);
	}
	/**
	 * filter products
	 * 
	 * list<Produit> <br/>
	 * <h4 style="color:green;">Path WS: http://localhost:8080/api/produits/filter
	 * </h4>
	 */
	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	@ResponseBody
	public FilterProductResponse filterProducts(@RequestBody ProductRequestFilter productRequestFilter) {
		
		return produitService.filterProduct(productRequestFilter);
	}
	
	
	/**
	 * filter products
	 * 
	 * list<Produit> <br/>
	 * <h4 style="color:green;">Path WS: http://localhost:8080/api/produits/filter
	 * </h4>
	 */
	@RequestMapping(value = "/filterAssistOrder", method = RequestMethod.POST)
	@ResponseBody
	public FilterProductAssistOrderResponse filterAssistOrderProducts(@RequestBody ProductRequestFilterAssistOrder productRequestFilter) {
		
		return produitService.filterProductAssitOrder(productRequestFilter);
	}
	
	/**
	 * filter productsSpecific
	 * 
	 * list<Produit> <br/>
	 * <h4 style="color:green;">Path WS: http://localhost:8080/api/produits/filter
	 * </h4>
	 */
	@RequestMapping(value = "/filterSpecific", method = RequestMethod.POST)
	@ResponseBody
	public FilterProductResponseSpecific filterProductsSpecific(@RequestBody ProductRequestFilter productRequestFilter) {
		
		return produitService.filterProductSpecific(productRequestFilter);
	}

	/**
	 * changestateProduct
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/produits:{id}/{etat}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}/{etat}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse changestateProduct(@PathVariable Long id, @PathVariable boolean etat) {

		return produitService.changeProductState(id, etat);
	}
	
	
	
	/**
	 * changestateProduct
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/produits:{id}/{etat}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}/{etat}", method = RequestMethod.POST)
	@ResponseBody
	public List<Product> listProductByDepotAndState(@PathVariable Long id, @PathVariable boolean etat) {

		return produitService.findByDepotAndActived(id, etat);
	}
	
	
	
	/**
	 * update product
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/produits
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateProduct(@Valid  @RequestBody UpdateProductRequest updateProductRequest ) {
		return this.produitService.updateProduct(updateProductRequest);
	}
	
	
	
	/**
	 * Delete product
	 * 
	 * @param id
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/produits:{id}
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteProduit(@PathVariable Long id) {

		return produitService.deleteProduct(id);
	}
	
	/**
	 * get products for auto-complete
	 * 
	 * @param status
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/produits/local-search/status
	 * </h4>
	 */
	@RequestMapping(value = "local-search/{status}", method = RequestMethod.GET)
	@ResponseBody
	public List<ProductAutoCompleteResponse> getProductsForAutoComplete(@PathVariable Boolean status) {

		return produitService.getProductsForAutoComplete(status);
	}
	
	
	
	
	/**
	 * get products for auto-complete
	 * 
	 * @param status
	 * @return boolean <br/>
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/produits/local-search/status
	 * </h4>
	 */
	@RequestMapping(value = "verifEmptyAndDuplicate", method = RequestMethod.GET)
	@ResponseBody
	public DuplcateEmptyResp verifEmptyAndDuplicate() {

		return produitService.verifEmptyAndDuplicate();
	}
	
	
	
	
	
	@RequestMapping(value = "/totalPrdAssistOrd", method = RequestMethod.POST)
	@ResponseBody
	public int totalAssistProducts(@RequestBody ProductRequestFilterAssistOrder productRequestFilter) {
		
		return produitService.totalProductAssitOrder(productRequestFilter);
	}
	
	
	@RequestMapping(value = "/totalPerime", method = RequestMethod.GET)
	@ResponseBody
	public int totalPerimeProducts() {
		
		return produitService.totalPerime();
	}
	
	
//	/**
//	 * get  product by bar code
//	 * 
//	 * Produit <br/>
//	 * <h4 style="color:green;">Path WS: http://localhost:8080/api/produits/codebar?code=
//	 * </h4>
//	 */
//	@RequestMapping(value = "/barcodespec", method = RequestMethod.GET)
//	@ResponseBody
//	public ProductResponse getProductByBarCodes(@RequestParam String code) {
//		
//		return produitService.findByBarCodes(code);
//	}

	
	
//	/**
//	 * get  product by Libelle spec
//	 * 
//	 * list<Produit> <br/>
//	 * <h4 style="color:green;">Path WS: http://localhost:8080/api/produits/libelle?val=
//	 * </h4>
//	 */
//	@RequestMapping(value = "/libellespec", method = RequestMethod.GET)
//	@ResponseBody
//	public List<ProductResponse> getProductByLibspec(@RequestParam String val) {
//		
//		return produitService.getLibsStartingWith(val);
//	}
//	
	
	
	
//	/**
//	 * get list products
//	 * 
//	 * List<Produit> <br/>
//	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/produits/rupture-stock
//	 * </h4>
//	 */
//	@RequestMapping(value = "/rupture-stock", method = RequestMethod.GET)
//	@ResponseBody
//	public List<Product> getListStockBreak() {
//
//		return produitService.getListStockBreak();
//	}
	
	
	
//	/**
//	 * get  product by Libelle
//	 * 
//	 * list<Produit> <br/>
//	 * <h4 style="color:green;">Path WS: http://localhost:8080/api/produits/libelle?val=
//	 * </h4>
//	 */
//	@RequestMapping(value = "/libelle", method = RequestMethod.GET)
//	@ResponseBody
//	public List<Product> getProductByLib(@RequestParam String val) {
//		
//		return produitService.findByBarCode(code);
//	}
	
	@RequestMapping(value = "listProductSearch", method = RequestMethod.GET, produces = { "application/json" })
	//@Secured("ROLE_ADM")
	public List<ProductResponseSearch> getListProductsSearch() {

		return produitService.getListProductsSearch();
	}
	
	
	@RequestMapping(value = "/id", method = RequestMethod.GET)
	@ResponseBody
	public Product getProductById(@RequestParam Long id) {
		
		return produitService.getProductById(id);
	}
	
//	@RequestMapping(value = "lot/{datemin}/{datemax}", method = RequestMethod.GET)
//	@ResponseBody
//	public List<Lot> getkistLOt(@DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable Date datemin,
//			@DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable Date datemax) {
//
//		return lotService.listLotBetwenDate(datemin, datemax);
//	}
	
}
