package com.transtu.rest.controllers;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Dci;
import com.transtu.persistence.entities.Depot;
import com.transtu.service.DepotService;

@RestController
@RequestMapping(value =DepotController.PATH, produces = "application/json; charset=utf-8")
public class DepotController {

	protected static final String PATH = "/depots";
	
	@Autowired(required=true)
	DepotService depotService;
	
	/**
	 * add depot
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/depots:{label}
	 * </h4>
	 */
	@RequestMapping(value = "/{label}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addDepot( @RequestBody @NotNull String label) {
	     return this.depotService.addDepot(label);
	}
	
	/**
	 * update depot
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/depots:{id}/{label}
	 * </h4>
	 */
	@RequestMapping(value = "{id}/{label}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateDepot( @PathVariable Long id, @PathVariable String label ) {
	     return this.depotService.updateDepot(id, label);
	}
	
	/**
	 * find depot by id 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/depots/:id
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Depot findById( @PathVariable Long id ) {
	     return this.depotService.findById(id);
	}
	
	/**
	 * find depot by label 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/depots/label/:label
	 * </h4>
	 */
	@RequestMapping(value = "/label/{label}", method = RequestMethod.GET)
	@ResponseBody
	public Depot findByLabel( @PathVariable String label ) {
	     return this.depotService.findByLabel(label);
	}
	
	/**
	 * find all depots 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/depots
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<Depot> findDepot() {
	     return this.depotService.findAllDepots();
	}
	
	/**
	 * delete depot 
	 * 
	 *  <br/>
	 * <h4 style="color:g reen;">Path WS: https://localhost:8080/api/depots/:id
	 * </h4>
	 */
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDepot(@PathVariable Long id) {
	     return this.depotService.deleteDepot(id);
	}
	
}
