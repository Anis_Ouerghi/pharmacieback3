package com.transtu.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.jasper.MovementJasper;
import com.transtu.core.json.request.DateRequestPdf;
import com.transtu.service.MovementService;

@RestController
@RequestMapping(value =MovementController.PATH, produces = "application/json; charset=utf-8")
public class MovementController extends AbstractController{
	
	protected static final String PATH = "/movements";
	
	@Autowired(required=true)
	MovementService movementService;
	
	/**
	 * get movement between two dates 
	 * 
	 * @param date
	 *         <h4 style="color:green;">Path WS:https://localhost:8090/api/movements/jasper
	 * </h4>
	 */
	@RequestMapping(value="/jasper", method = RequestMethod.POST)
	@ResponseBody
	public List<MovementJasper> getMovementsBetween(@RequestBody DateRequestPdf date) {

		return this.movementService.getMovementsBetween(date);
	}
}

