package com.transtu.rest.controllers;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.ExternalOrderFilter;
import com.transtu.core.json.request.InternalDeliveryRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.ReceptionFilter;
import com.transtu.core.json.request.ReceptionRequest;
import com.transtu.core.json.request.UpdateDeliveryRequest;
import com.transtu.core.json.request.UpdateReceptionRequest;
import com.transtu.persistence.entities.Distribution;
import com.transtu.persistence.entities.ExternalOrder;
import com.transtu.persistence.entities.Reception;
import com.transtu.persistence.entities.ReceptionLine;
import com.transtu.service.ReceptionService;
@CrossOrigin()
@RestController 
@RequestMapping(value=ReceptionController.PATH, produces = "application/json; charset=utf-8")
public class ReceptionController  extends AbstractController {
	protected static final String PATH = "/reception";
	
	@Autowired(required=true)
	ReceptionService receptionService;
	

	
	/**
	 * add reception
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/reception
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addInternalDelivery(@Valid @RequestBody ReceptionRequest receptionRequest ) {
		return this.receptionService.AddReception(receptionRequest);
	}
	
	/**
	 * cancel reception
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/reception/cancel/:id
	 * </h4>
	 */
	@RequestMapping(value = "/cancel/{id}", method = RequestMethod.POST)
	public MessageResponse cancelReception(@PathVariable Long id) {
		return receptionService.cancelReception(id);
		}
	
	
	/**
	 * update reception
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/reception
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateReception(@Valid  @RequestBody UpdateReceptionRequest updateReceptionRequest ) {
		return this.receptionService.updateReception(updateReceptionRequest);
	}

	/**
	 * filter reception
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8081/api/reception
	 * </h4>
	 */
	@RequestMapping(value = "/dynamic", method = RequestMethod.POST)
	@ResponseBody
	public Set<Reception> filterReception(@RequestBody ReceptionFilter receptionFilter) {
		return this.receptionService.filterReception(receptionFilter);
	}
	
	/**
	 * change state reception
	 * 
	 * <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/reception/changeState/:{receptionId}/{stateId}
	 * </h4>
	 */
	@RequestMapping(value = "/changeState/{receptionId}/{stateId}", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse changeStateInternalDlivery(@PathVariable Long receptionId, @PathVariable Long stateId ) {
		return this.receptionService.changeStateReception(receptionId, stateId);
	}
	
	@RequestMapping(value = "{referenceId}/{receptionNum}", method = RequestMethod.GET)
	@ResponseBody
	public Reception findreceptionNum(@RequestParam Long referenceId,  @RequestParam String receptionNum) {

		return receptionService.findByReceptionnNum(referenceId, receptionNum);
		 
	}
	
//	@RequestMapping(value = "{referenceId}", method = RequestMethod.POST)
//	@ResponseBody
//	public MessageResponse findreceptionNum(@RequestParam Long referenceId) {
//
//		return receptionService.cancelRecep(referenceId);
//		 
//	}
	
	
	@RequestMapping(value = "/receptions", method = RequestMethod.GET)
	@ResponseBody
	public List<ReceptionLine> getReceptions(@RequestBody long id ) {

		return receptionService.getReceptionsJasper(id);
		 
	}
	
}


