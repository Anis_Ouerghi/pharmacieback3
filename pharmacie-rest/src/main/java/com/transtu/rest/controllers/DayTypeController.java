package com.transtu.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.AddDayRequest;
import com.transtu.core.json.request.AddDayTypeRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Day;
import com.transtu.persistence.entities.DayType;
import com.transtu.service.DayService;
import com.transtu.service.DayTypeService;

@CrossOrigin()
@RestController
@RequestMapping(value =DayTypeController.PATH, produces = "application/json; charset=utf-8")

public class DayTypeController {
	
	
	
protected static final String PATH = "/daysType";
	
	@Autowired(required=true)
	DayTypeService dayTypeService ; 
	
	

	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addDayType(@RequestBody AddDayTypeRequest dayTypeRequest) {
		return this.dayTypeService.addDayType(dayTypeRequest);
	}
	
	/**
	 * get all days
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/days
	 * </h4>
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<DayType> getAllDays() {
		return this.dayTypeService.getAllDaysType();
	}
	

	/**
	 * delete day
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/days/{id}
	 * </h4>
	 */
	@RequestMapping(value="/{id}",method = RequestMethod.DELETE)
	@ResponseBody
	public MessageResponse deleteDayType(@PathVariable Long id) {
		return this.dayTypeService.deleteDayType(id);
	}
	

}
