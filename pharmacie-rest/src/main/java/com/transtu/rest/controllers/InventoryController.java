package com.transtu.rest.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.transtu.core.json.request.AddInventoryRequest;
import com.transtu.core.json.request.FilterInventoryResponse;
import com.transtu.core.json.request.InventoryLineRequest;
import com.transtu.core.json.request.InventoryPagination;
import com.transtu.core.json.request.InventoryRequest;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.core.json.request.UpdateInventoryRequest;
import com.transtu.persistence.entities.Inventory;
import com.transtu.service.InventoryService;

@CrossOrigin()
@RestController 
@RequestMapping(value=InventoryController.PATH, produces = "application/json; charset=utf-8")
public class InventoryController  extends AbstractController {
	
	protected static final String PATH = "/inventory";
	
	@Autowired(required=true)
	InventoryService inventoryService;
	
	/**
	 * add iventor
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-interne
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse addInventory(@Valid @RequestBody AddInventoryRequest addInventoryRequest) {
		return this.inventoryService.addInventory(addInventoryRequest);
	} 
	
	
	
	/**
	 * get  iventor
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-interne
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.PUT)
	@ResponseBody
	public MessageResponse updateInventory(@Valid @RequestBody UpdateInventoryRequest updateInventoryRequest) {
		return this.inventoryService.updateInventory(updateInventoryRequest);
		
	} 
	
	
	/**
	 * get  iventors list
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-interne
	 * </h4>
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<Inventory> getInventory() {
		return this.inventoryService.listInventory();
		
	} 
	
	/**
	 * get  iventors list
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/inventory
	 * </h4>
	 */
	@RequestMapping(value = "inventoryLine", method = RequestMethod.POST)
	@ResponseBody
	public InventoryPagination getInventoryLine(@Valid @RequestBody InventoryLineRequest inventoryLineRequest) {
		return this.inventoryService.ListInventoryLine(inventoryLineRequest);
		
	} 
	
	
	
	/**
	 * get  iventor
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/inventory
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	@ResponseBody
	public MessageResponse stockRecovery(@PathVariable Long id) {
		return this.inventoryService.stockRecovery(id);
		
	} 
	
	
	/**
	 * get  iventor
	 * 
	 *  <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/commande-interne
	 * </h4>
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Inventory getInventory(@PathVariable Long id) {
		return this.inventoryService.getInventory(id);
		
	} 
	
	
	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	@ResponseBody
	public FilterInventoryResponse filterInventory(@RequestBody InventoryRequest inventoryRequest) {
		
		return inventoryService.filterInventory(inventoryRequest);
	}
	
	

}
