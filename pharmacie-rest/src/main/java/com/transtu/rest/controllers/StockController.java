package com.transtu.rest.controllers;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.transtu.core.jasper.StockJasper;
import com.transtu.core.json.request.FilterStockReport;
import com.transtu.core.json.request.StockFilterRequest;
import com.transtu.core.json.request.StockResponse;
import com.transtu.persistence.entities.Product;
import com.transtu.service.StockService;
 
@CrossOrigin()
@RestController
@RequestMapping(value = StockController.PATH, produces = "application/json; charset=utf-8")
public class StockController  extends AbstractController {
	
	private static final Logger logger = LoggerFactory.getLogger(StockController.class);

	protected static final String PATH = "/stocks";

	@Autowired(required = true)
	private StockService stockService;
//	@Autowired(required = true)
//	private AgentService agentService;
//	@Autowired(required = true)
//	ReceptionCommandeService receptionCommandeService;
	
	/**
	 * get list products
	 * 
	 * List<Stock> <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/stocks/rupture-pharmacie-stock
	 * </h4>
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@RequestMapping(value = "/rupture-pharmacie-stock", method = RequestMethod.GET)
	@ResponseBody
	public List<StockResponse> getListStockBreak(){
		
		return stockService.getListStockBreak();
	}
	/**
	 * get list products
	 * 
	 * List<Stock> <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/stocks/rupture-total-stock
	 * </h4>
	 */
	@RequestMapping(value = "/rupture-total-stock", method = RequestMethod.GET)
	@ResponseBody
	public List<StockResponse> getToatalStockBreak() {

		return stockService.getTotalListStockBreak();
	}
	
	/**
	 * get qte by location and product
	 * 
	 * List<Stock> <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/stocks/rupture-total-stock
	 * </h4>
	 */
	@RequestMapping(value = "/qte/{locationId}/{productId}", method = RequestMethod.GET)
	@ResponseBody
	public Double getQteByLocationAndProduct(@PathVariable Long locationId, @PathVariable Long productId) {

		return this.stockService.getQteByProductAndLocation(productId, locationId);
	}
	
	/**
	 * get stock state from pdf
	 * 
	 * List<Stock> <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/stocks/jasper/:idStock
	 * </h4>
	 * @return 
	 */
	@RequestMapping(value = "/jasper", method = RequestMethod.POST)
	@ResponseBody
	public  List<StockJasper> getStockFromPdf(@RequestBody FilterStockReport filterStockReport) {
		return this.stockService.getStockFromPdf(filterStockReport);
	}
	
	
	/**
	 * get stock state from pdf
	 * 
	 * List<Stock> <br/>
	 * <h4 style="color:green;">Path WS: https://localhost:8080/api/stocks/jasper/:idStock
	 * </h4>
	 * @return 
	 */
	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	@ResponseBody
	public  List<Product> filterStock(@RequestBody StockFilterRequest stokFilterRequest ) {

		return this.stockService.filterStock(stokFilterRequest);
	}
	
	
	/**
	 * get  product by bar code
	 * 
	 * Produit <br/>
	 * <h4 style="color:green;">Path WS: http://localhost:8080/api/stocks/id?code=
	 * </h4>
	 */
//	@RequestMapping(value = "/id", method = RequestMethod.GET)
//	@ResponseBody
//	public ExternalOrderResponse getProductByBarCode(@RequestParam Long code) {
//		
//		return receptionCommandeService.ListReceptionCommande(code);
//	}
}
