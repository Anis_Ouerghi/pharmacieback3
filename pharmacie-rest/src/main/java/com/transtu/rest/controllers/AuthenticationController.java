package com.transtu.rest.controllers;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.transtu.core.dto.AuthenticationRequestDTO;
import com.transtu.core.dto.AuthenticationResponseDTO;
import com.transtu.core.dto.TokenResponse;
import com.transtu.core.json.request.MessageResponse;
import com.transtu.persistence.entities.Utilisateur;
import com.transtu.persistence.repositories.RoleRepository;
import com.transtu.persistence.repositories.UtilisateurRepository;
import com.transtu.rest.exception.RestException;
import com.transtu.rest.exception.RestExceptionCode;
import com.transtu.rest.security.AppUserStorage;
import com.transtu.rest.security.TokenService;
import com.transtu.service.DayService;


@RestController
@RequestMapping(value = AuthenticationController.PATH, produces = "application/json; charset=utf-8")
public class AuthenticationController {

	private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

	protected static final String PATH = "/authentication";

	@Autowired
	private AuthenticationManager gAuthenticationManager;
	@Autowired
	private TokenService gTokenService;
	@Autowired
	private AppUserStorage appUserStorage;
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Autowired
	private
   RoleRepository roleRepository;
	
	
	@Autowired
	private DayService dayService;
	
	private final ObjectMapper gJsonMapper = new ObjectMapper();

	/**
	 * This function is used to login
	 * 
	 * @param pRequest
	 * @return AuthentificationResponseDTO
	 * @throws JsonProcessingException
	 *             <br/>
	 *             <h4 style="color:green;">Path WS:
	 *             https://localhost:8080/api/authentication/login</h4>
	 * @throws ParseException 
	 */

	@RequestMapping(value = { "/login" }, method = { RequestMethod.POST })
	@ResponseBody
	public TokenResponse authenticate(HttpServletRequest pRequest, HttpServletResponse pResponse)
			throws JsonProcessingException, ParseException {
		logger.debug("start authentication");
		String vHeader = pRequest.getHeader("Authorization");
		logger.debug("Request Header credentials: {}", vHeader);
		if ((vHeader == null) || (!vHeader.startsWith("Basic "))) {
			logger.debug("header not set correctly");
		} 
		AuthenticationRequestDTO vUser = new AuthenticationRequestDTO();
		AuthenticationResponseDTO vAuthentificationResponseDTO = new AuthenticationResponseDTO();
		TokenResponse vTokenResponse = null;
		try {
			String[] tokens = extractAndDecodeHeader(vHeader, pRequest);
			assert (tokens.length == 2);
			String login = tokens[0];
			String password = tokens[1];
			if ((StringUtils.isBlank(login)) || (StringUtils.isBlank(password))) {
				logger.info("no login parameters found");
			}
			logger.info("Basic Authentication Authorization header found for user '" + login + "'");
			if (login.contains("@")) {
				vUser.setMail(login);
			} else {
				vUser.setUsername(login);
				System.out.println("--authentifivation controllert 7----"+password);
			}
			
			vUser.setPassword(password);
			String vJsonUserDetails = null;

			UsernamePasswordAuthenticationToken vUsernamePasswordAuth = new UsernamePasswordAuthenticationToken(login,
					vUser.getPassword());
			
			Authentication vAuthentication = this.gAuthenticationManager.authenticate(vUsernamePasswordAuth);
			vAuthentificationResponseDTO = (AuthenticationResponseDTO) vAuthentication.getPrincipal();
		
			if ((vAuthentication.isAuthenticated()) && (vAuthentication.getPrincipal() != null)
					&& ((vAuthentication.getPrincipal() instanceof AuthenticationResponseDTO))) {
				vJsonUserDetails = this.gJsonMapper.writeValueAsString(vAuthentication.getPrincipal());
				
				//dayService.openDayToAuthentication();
				System.out.println("------------open day --first authentication ");
				logger.info("Authenticated user: {}", vJsonUserDetails);
				
			}
			
			
			
			vTokenResponse = new TokenResponse(this.gTokenService.createToken(vJsonUserDetails));
			
		    vTokenResponse.setUser(utilisateurRepository.findByUsername(login).get(0));
		//	vTokenResponse.setUser((Utilisateur) utilisateurRepository.findByUsername(login));
			System.out.println("------------open day --first authentication 2------------------ "+ vTokenResponse.getUser().getUsername());
			this.dayService.openDayToAuthentication();
	
		} catch (AuthenticationException | IOException e) {
			logger.info("FAIL_DECODE_TOKEN");
		}
		return vTokenResponse;
	}
	
	@RequestMapping(value = "/currentUser",method = RequestMethod.GET)
    public void currentUser(){
		
//    	System.out.println("-+-+-+-+-2 log out");
//    	appUserStorage.removeAppUser("user1");
    }
	
	
	/**
	 * used to logout
	 * 
	 * @param pUsername
	 *            user name of current user
	 * @param pRequest
	 * @param pResponse
	 * @return MessageResponseDTO<br/>
	 *         <h4 style="color:green;">Path WS:
	 *         https://sambi-edentom-backend2.herokuapp.com/sam-bi-edentom-rest/1.0/authentication/logout/:username</h4>
	 */
	@RequestMapping("/logout/{username}")
	public MessageResponse logout(@PathVariable("username") String pUsername, HttpServletRequest pRequest,
			HttpServletResponse pResponse) {

		String vAuthHeader = pRequest.getHeader("X-Auth-Token");
		if (vAuthHeader != null) {
			String vUserDetails = this.gTokenService.extractJsonUserDetails(vAuthHeader);
			try {
				JSONObject vJsonUserDetails = new JSONObject(vUserDetails);
				String vTokenUser = vJsonUserDetails.getString("userName");
				
				logger.info("vTokenUser=====>"+vTokenUser);
				if (vTokenUser.equals(pUsername)) {
					Authentication vAuth = SecurityContextHolder.getContext().getAuthentication();
					if (vAuth != null)
						new SecurityContextLogoutHandler().logout(pRequest, pResponse, vAuth);
					appUserStorage.removeAppUser(pUsername);
					return new MessageResponse(HttpStatus.OK.toString(), null,"Logout avec succÃ©s","");
				} else {
					RestExceptionCode code = RestExceptionCode.NO_CORRESPONDANCE_TOKEN_USERNAME;
					RestException ex = new RestException(code.getError(), code);
					throw ex;
				}

			} catch (JSONException e) {
				RestExceptionCode code = RestExceptionCode.NO_USER_DETAILS_TOKEN;
				RestException ex = new RestException(code.getError(), code);
				throw ex;
			} 
		} else {
			RestExceptionCode code = RestExceptionCode.NO_TOKEN;
			RestException ex = new RestException(code.getError(), code);
			throw ex;
		}
	}

	private String[] extractAndDecodeHeader(String header, HttpServletRequest request) throws IOException {
		byte[] base64Token = header.substring(6).getBytes("UTF-8");
		byte[] decoded = null;
		try {
			decoded = Base64.decode(base64Token);
		} catch (IllegalArgumentException e) {
			logger.info("FAIL_DECODE_TOKEN");
		}
		String token = new String(decoded, "UTF-8");
		int delim = token.indexOf(":");
		if (delim == -1) {
			logger.info("INVALID_BASIC_TOKEN");
		}
		return new String[] { token.substring(0, delim), token.substring(delim + 1) };
	}




}
