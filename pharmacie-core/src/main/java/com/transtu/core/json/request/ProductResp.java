
package com.transtu.core.json.request;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.transtu.persistence.entities.Dci;
import com.transtu.persistence.entities.Depot;
import com.transtu.persistence.entities.DistributionUnit;
import com.transtu.persistence.entities.Dosage;
import com.transtu.persistence.entities.Family;
import com.transtu.persistence.entities.Form;
import com.transtu.persistence.entities.Movement;
import com.transtu.persistence.entities.PharmaClass;
import com.transtu.persistence.entities.Presentation;
import com.transtu.persistence.entities.Stock;
import com.transtu.persistence.entities.Type;

//@Entity
//@Table(indexes = { @Index(name = "index_1", columnList = "barCode , lib, codePctProd") ,
//					@Index(name = "index_2", columnList = "actived")})
public class ProductResp  implements Serializable {

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String barCode;
	private String codePctProd;
	private String lib;
	private double qte;
	private boolean actived = false;
	private double vatRate;
	private double wauCost;
	private double safetyStock;
	private double minStock;
	private double pharmaStock;
	private double depStock;
	private double warningStock;
	private double  price;
	private double  colisage;
	private double averageConsum ;
	
	public double getAverageConsum() {
		return averageConsum;
	}


	public void setAverageConsum(double averageConsum) {
		this.averageConsum = averageConsum;
	}


	@ManyToOne
	@JoinColumn(name = "dossage_id")
	@Value("${maxuploadfilesize:'100'}")
	private Dosage dossage;
	
	@ManyToOne
	@JoinColumn(name = "type_id")
	
	private Type type;
	
	@ManyToOne
	@JoinColumn(name = "dci_id")
	
	private Dci dci;
	
	@ManyToOne
	@JoinColumn(name = "distribution_unit_id")
	
	private DistributionUnit distributionUnit;
	
	@ManyToOne
	@JoinColumn(name = "form_id")
	
	private Form form;
	
	@ManyToOne
	@JoinColumn(name = "pharma_class_id")
	
	private PharmaClass pharmaClass;
	
	@ManyToOne
	@JoinColumn(name = "depot_id")
	
	private Depot depot;
	
	@ManyToOne
	@JoinColumn(name = "presentation_id")

	private Presentation presentation;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "famille_id")
	
	private Family family;


	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "PRODUITS_GENERIQUE", joinColumns = { @JoinColumn(name = "PRODUIT_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "PRODUITGENERIQUE_ID") })
//	@JsonIgnore
//	private Set<Product> produitsGeneric = new HashSet<Product>();
//
//	@ManyToMany(mappedBy = "produitsGeneric")
//	@JsonIgnore
//	private Set<Product> produitParent = new HashSet<Product>();
	
	@OneToMany(mappedBy="product")
	//@JsonManagedReference
	//@JsonBackReference
	@JsonIgnore
	private List<Stock> Stocks;


	@OneToMany(mappedBy="product")
	@JsonBackReference
	private List<Movement> movements ;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getBarCode() {
		return barCode;
	}


	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}


	public String getCodePctProd() {
		return codePctProd;
	}


	public void setCodePctProd(String codePctProd) {
		this.codePctProd = codePctProd;
	}


	public String getLib() {
		return lib;
	}


	public void setLib(String lib) {
		this.lib = lib;
	}


	public boolean isActived() {
		return actived;
	}


	public void setActived(boolean actived) {
		this.actived = actived;
	}


	public double getVatRate() {
		return vatRate;
	}


	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}


	public double getWauCost() {
		return wauCost;
	}


	public void setWauCost(double wauCost) {
		this.wauCost = wauCost;
	}


	public double getSafetyStock() {
		return safetyStock;
	}


	public void setSafetyStock(double safetyStock) {
		this.safetyStock = safetyStock;
	}


	public double getMinStock() {
		return minStock;
	}


	public void setMinStock(double minStock) {
		this.minStock = minStock;
	}


	public double getWarningStock() {
		return warningStock;
	}


	public void setWarningStock(double warningStock) {
		this.warningStock = warningStock;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public double getColisage() {
		return colisage;
	}


	public void setColisage(double colisage) {
		this.colisage = colisage;
	}


	public Dosage getDossage() {
		return dossage;
	}


	public void setDossage(Dosage dossage) {
		this.dossage = dossage;
	}


	public Type getType() {
		return type;
	}


	public void setType(Type type) {
		this.type = type;
	}


	public Dci getDci() {
		return dci;
	}


	public void setDci(Dci dci) {
		this.dci = dci;
	}


	public DistributionUnit getDistributionUnit() {
		return distributionUnit;
	}


	public void setDistributionUnit(DistributionUnit distributionUnit) {
		this.distributionUnit = distributionUnit;
	}


	public Form getForm() {
		return form;
	}


	public void setForm(Form form) {
		this.form = form;
	}


	public PharmaClass getPharmaClass() {
		return pharmaClass;
	}


	public void setPharmaClass(PharmaClass pharmaClass) {
		this.pharmaClass = pharmaClass;
	}


	public Depot getDepot() {
		return depot;
	}


	public void setDepot(Depot depot) {
		this.depot = depot;
	}


	public Presentation getPresentation() {
		return presentation;
	}


	public void setPresentation(Presentation presentation) {
		this.presentation = presentation;
	}


	public Family getFamily() {
		return family;
	}


	public void setFamily(Family family) {
		this.family = family;
	}


	public List<Stock> getStocks() {
		return Stocks;
	}


	public void setStocks(List<Stock> stocks) {
		Stocks = stocks;
	}


	public List<Movement> getMovements() {
		return movements;
	}


	public void setMovements(List<Movement> movements) {
		this.movements = movements;
	}


	public ProductResp() {
		super();
		// TODO Auto-generated constructor stub
	}


	public double getPharmaStock() {
		return pharmaStock;
	}


	public void setPharmaStock(double pharmaStock) {
		this.pharmaStock = pharmaStock;
	}


	public double getDepStock() {
		return depStock;
	}


	public void setDepStock(double depStock) {
		this.depStock = depStock;
	}


	public double getQte() {
		return qte;
	}


	public void setQte(double qte) {
		this.qte = qte;
	}


	


	

	

	
}
