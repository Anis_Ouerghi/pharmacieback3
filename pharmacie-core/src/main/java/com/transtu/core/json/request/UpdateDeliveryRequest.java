package com.transtu.core.json.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.transtu.core.dto.InternalDeliveryLineDto;

public class UpdateDeliveryRequest {
	
	@NotNull
	private Long internalDeliveryId;
	private List<InternalDeliveryLineDto> internalDeliveryLinesDto;
	
	
	public Long getInternalDeliveryId() {
		return internalDeliveryId;
	}
	public void setInternalDeliveryId(Long internalDeliveryId) {
		this.internalDeliveryId = internalDeliveryId;
	}
	
	public List<InternalDeliveryLineDto> getInternalDeliveryLinesDto() {
		return internalDeliveryLinesDto;
	}
	public void setInternalDeliveryLinesDto(List<InternalDeliveryLineDto> internalDeliveryLinesDto) {
		this.internalDeliveryLinesDto = internalDeliveryLinesDto;
	}
	public UpdateDeliveryRequest() {
		super();
	}
	
	

}
