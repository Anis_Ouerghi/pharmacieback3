package com.transtu.core.json.request;

import java.util.Date;

public class DateRequestPdf {

	private Date start ; 
	private Date end ;
	//private String agentId;
	
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public DateRequestPdf() {
		super();
	}
	
	
	
	
}
