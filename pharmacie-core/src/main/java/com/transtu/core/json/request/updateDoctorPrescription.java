package com.transtu.core.json.request;

public class updateDoctorPrescription {
	
	
		private Long prescriptionId;
		private Long doctorId;
		public Long getPrescriptionId() {
			return prescriptionId;
		}
		public void setPrescriptionId(Long prescriptionId) {
			this.prescriptionId = prescriptionId;
		}
		public Long getDoctorId() {
			return doctorId;
		}
		public void setDoctorId(Long doctorId) {
			this.doctorId = doctorId;
		}
		
		
	
}
