package com.transtu.core.json.request;

public class ActiveProductByLabelStartWithAndDep {
	
	private String label;
	private Boolean active;
	private Long depID;
	private Long typeID;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Long getDepID() {
		return depID;
	}
	public void setDepID(Long depID) {
		this.depID = depID;
	}
	public ActiveProductByLabelStartWithAndDep(String label, Boolean active, Long depID) {
		super();
		this.label = label;
		this.active = active;
		this.depID = depID;
	}
	public ActiveProductByLabelStartWithAndDep() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getTypeID() {
		return typeID;
	}
	public void setTypeID(Long typeID) {
		this.typeID = typeID;
	}
	
	
	

}
