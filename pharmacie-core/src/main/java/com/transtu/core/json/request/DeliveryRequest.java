package com.transtu.core.json.request;

import java.util.Date;

public class DeliveryRequest {
	
public Long customerId;
public Date maxDate;
public Date minDate;
public Long getCustomerId() {
	return customerId;
}
public void setCustomerId(Long customerId) {
	this.customerId = customerId;
}
public Date getMaxDate() {
	return maxDate;
}
public void setMaxDate(Date maxDate) {
	this.maxDate = maxDate;
}
public Date getMinDate() {
	return minDate;
}
public void setMinDate(Date minDate) {
	this.minDate = minDate;
}





}
