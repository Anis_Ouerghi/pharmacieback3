package com.transtu.core.json.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.transtu.core.dto.UserRoleDto;

public class UserRequest {
	
	
	private Long id;
	@NotNull
	private String username;
	private String password;
	private String lastname;
	private String firstname;
	private boolean actived;
	private String email;
	private List <UserRoleDto> UserRoleDtos;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isActived() {
		return actived;
	}
	public void setActived(boolean actived) {
		this.actived = actived;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<UserRoleDto> getUserRoleDtos() {
		return UserRoleDtos;
	}
	public void setUserRoleDtos(List<UserRoleDto> userRoleDtos) {
		UserRoleDtos = userRoleDtos;
	}
	public UserRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public UserRequest(Long id, String username, String password, boolean actived, String email,
			List<UserRoleDto> userRoleDtos) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.actived = actived;
		this.email = email;
		UserRoleDtos = userRoleDtos;
	}

	
	
	
	
}
