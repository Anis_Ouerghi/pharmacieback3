package com.transtu.core.json.request;

import java.io.Serializable;

import javax.persistence.*;

//import com.entities.Column;

import java.util.Date;


/**
 * The persistent class for the agent database table.
 * 
 */

//@NamedQuery(name="Agent.findAll", query="SELECT a FROM Agent a")
public class AgentSTT implements Serializable {
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String adress1;
	private Integer cdcessa;
	private Integer cddeces;
	private String cdfonc;
	private Integer cdgrade;
	private String cdnivau;
	private Integer cdpost;
	private Integer cdposte;
	private String cdquali;
	private Integer cdstatu;
	private String cdville;
	private Integer chefdfa;
	private Integer conjointstt;
	private Date dateRetraite;
	private String datecin;
	private Date daterec;
	private Date datnais;
	private String dedatef;
	private Integer fH48;
	private String identite;
	private String lieuDn;
	private String lieucin;
	private String mutualiste;
	private String nCin;
	private String nation;
	private Integer nbrEnf;
	private Integer nenfant;
	private String nom;
	private String nomdjf;
	private String numsecu;
	private String numtel;
	private String prenom;
	private String prenomMere;
	private String prenomPr;
	private String regime;
	private String sexe;
	private String sitfam;
	private String situation;
	private String socialOrg;
	private Integer matric;
	private String classification;
	private String societe;

	public AgentSTT() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAdress1() {
		return this.adress1;
	}

	public void setAdress1(String adress1) {
		this.adress1 = adress1;
	}

	public Integer getCdcessa() {
		return this.cdcessa;
	}

	public void setCdcessa(Integer cdcessa) {
		this.cdcessa = cdcessa;
	}

	public Integer getCddeces() {
		return this.cddeces;
	}

	public void setCddeces(Integer cddeces) {
		this.cddeces = cddeces;
	}

	public String getCdfonc() {
		return this.cdfonc;
	}

	public void setCdfonc(String cdfonc) {
		this.cdfonc = cdfonc;
	}

	public Integer getCdgrade() {
		return this.cdgrade;
	}

	public void setCdgrade(Integer cdgrade) {
		this.cdgrade = cdgrade;
	}

	public String getCdnivau() {
		return this.cdnivau;
	}

	public void setCdnivau(String cdnivau) {
		this.cdnivau = cdnivau;
	}

	public Integer getCdpost() {
		return this.cdpost;
	}

	public void setCdpost(Integer cdpost) {
		this.cdpost = cdpost;
	}

	public Integer getCdposte() {
		return this.cdposte;
	}

	public void setCdposte(Integer cdposte) {
		this.cdposte = cdposte;
	}

	public String getCdquali() {
		return this.cdquali;
	}

	public void setCdquali(String cdquali) {
		this.cdquali = cdquali;
	}

	public Integer getCdstatu() {
		return this.cdstatu;
	}

	public void setCdstatu(Integer cdstatu) {
		this.cdstatu = cdstatu;
	}

	public String getCdville() {
		return this.cdville;
	}

	public void setCdville(String cdville) {
		this.cdville = cdville;
	}

	public Integer getChefdfa() {
		return this.chefdfa;
	}

	public void setChefdfa(Integer chefdfa) {
		this.chefdfa = chefdfa;
	}

	public String getClassification() {
		return this.classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public Integer getConjointstt() {
		return this.conjointstt;
	}

	public void setConjointstt(Integer conjointstt) {
		this.conjointstt = conjointstt;
	}

	public Date getDateRetraite() {
		return this.dateRetraite;
	}

	public void setDateRetraite(Date dateRetraite) {
		this.dateRetraite = dateRetraite;
	}

	public String getDatecin() {
		return this.datecin;
	}

	public void setDatecin(String datecin) {
		this.datecin = datecin;
	}

	public Date getDaterec() {
		return this.daterec;
	}

	public void setDaterec(Date daterec) {
		this.daterec = daterec;
	}

	public Date getDatnais() {
		return this.datnais;
	}

	public void setDatnais(Date datnais) {
		this.datnais = datnais;
	}

	public String getDedatef() {
		return this.dedatef;
	}

	public void setDedatef(String dedatef) {
		this.dedatef = dedatef;
	}

	public Integer getFH48() {
		return this.fH48;
	}

	public void setFH48(Integer fH48) {
		this.fH48 = fH48;
	}

	public String getIdentite() {
		return this.identite;
	}

	public void setIdentite(String identite) {
		this.identite = identite;
	}

	public String getLieuDn() {
		return this.lieuDn;
	}

	public void setLieuDn(String lieuDn) {
		this.lieuDn = lieuDn;
	}

	public String getLieucin() {
		return this.lieucin;
	}

	public void setLieucin(String lieucin) {
		this.lieucin = lieucin;
	}

	public Integer getMatric() {
		return this.matric;
	}

	public void setMatric(Integer matric) {
		this.matric = matric;
	}

	public String getMutualiste() {
		return this.mutualiste;
	}

	public void setMutualiste(String mutualiste) {
		this.mutualiste = mutualiste;
	}

	public String getNCin() {
		return this.nCin;
	}

	public void setNCin(String nCin) {
		this.nCin = nCin;
	}

	public String getNation() {
		return this.nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public Integer getNbrEnf() {
		return this.nbrEnf;
	}

	public void setNbrEnf(Integer nbrEnf) {
		this.nbrEnf = nbrEnf;
	}

	public Integer getNenfant() {
		return this.nenfant;
	}

	public void setNenfant(Integer nenfant) {
		this.nenfant = nenfant;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNomdjf() {
		return this.nomdjf;
	}

	public void setNomdjf(String nomdjf) {
		this.nomdjf = nomdjf;
	}

	public String getNumsecu() {
		return this.numsecu;
	}

	public void setNumsecu(String numsecu) {
		this.numsecu = numsecu;
	}

	public String getNumtel() {
		return this.numtel;
	}

	public void setNumtel(String numtel) {
		this.numtel = numtel;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPrenomMere() {
		return this.prenomMere;
	}

	public void setPrenomMere(String prenomMere) {
		this.prenomMere = prenomMere;
	}

	public String getPrenomPr() {
		return this.prenomPr;
	}

	public void setPrenomPr(String prenomPr) {
		this.prenomPr = prenomPr;
	}

	public String getRegime() {
		return this.regime;
	}

	public void setRegime(String regime) {
		this.regime = regime;
	}

	public String getSexe() {
		return this.sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getSitfam() {
		return this.sitfam;
	}

	public void setSitfam(String sitfam) {
		this.sitfam = sitfam;
	}

	public String getSituation() {
		return this.situation;
	}

	public void setSituation(String situation) {
		this.situation = situation;
	}

	public String getSocialOrg() {
		return this.socialOrg;
	}

	public void setSocialOrg(String socialOrg) {
		this.socialOrg = socialOrg;
	}

	public String getSociete() {
		return this.societe;
	}

	public void setSociete(String societe) {
		this.societe = societe;
	}

}