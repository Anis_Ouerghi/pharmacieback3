package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

public class PrescriptionFilterRequest {

	private Long userId ;
	private Long page ;
	private Long linesNbr ; 
	private Date dateMin ; 
	private Date dateMax ;
	private String agentId ;
	private List<Long> produits ;
	
	public Date getDateMin() {
		return dateMin;
	}
	public void setDateMin(Date dateMin) {
		this.dateMin = dateMin;
	}
	public Date getDateMax() {
		return dateMax;
	}
	public void setDateMax(Date dateMax) {
		this.dateMax = dateMax;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public List<Long> getProduits() {
		return produits;
	}
	public void setProduits(List<Long> produits) {
		this.produits = produits;
	}
	
	
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}

	
	public PrescriptionFilterRequest(Date dateMin, Date dateMax, String agentId, List<Long> produits) {
		super();
		this.dateMin = dateMin;
		this.dateMax = dateMax;
		this.agentId = agentId;
		this.produits = produits;
	}
	public PrescriptionFilterRequest() {
	}
	public Long getLinesNbr() {
		return linesNbr;
	}
	public void setLinesNbr(Long linesNbr) {
		this.linesNbr = linesNbr;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	
	
}
