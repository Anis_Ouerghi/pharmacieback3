package com.transtu.core.json.request;

import java.util.List;

import com.transtu.persistence.entities.InventoryLine;

public class InventoryPagination {

	private Long nbrPages; 
	private Long page;
	private Long totalProducts;
	private List<InventoryLine> inventoryLines ;
	
	public Long getNbrPages() {
		return nbrPages;
	}
	public void setNbrPages(Long nbrPages) {
		this.nbrPages = nbrPages;
	}
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getTotalProducts() {
		return totalProducts;
	}
	public void setTotalProducts(Long totalProducts) {
		this.totalProducts = totalProducts;
	}
	public List<InventoryLine> getInventoryLines() {
		return inventoryLines;
	}
	public void setInventoryLines(List<InventoryLine> inventoryLines) {
		this.inventoryLines = inventoryLines;
	}

	


	
}
