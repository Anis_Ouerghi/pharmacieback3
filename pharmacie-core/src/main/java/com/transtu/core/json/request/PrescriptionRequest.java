package com.transtu.core.json.request;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.transtu.core.dto.DistributionLineDto;

public class PrescriptionRequest implements Serializable{
	
	private static final long serialVersionUID = 3911963462722462710L;
	
	@NotNull
	private String agentId ;
	@NotNull
	private Long locationId ; 
	@NotNull
	private Long dayId ;
	private Long districtId ; 
	private String type;
	private String agentSituation;
	private Long doctorId ;
	
	private List<DistributionLineDto> distributionLinesDto ;
	
	
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String matriculeAgent) {
		this.agentId = matriculeAgent;
	}
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long emplacementId) {
		this.locationId = emplacementId;
	}
	public Long getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}
	public List<DistributionLineDto> getDistributionLinesDto() {
		return distributionLinesDto;
	}
	public void setDistributionLinesDto(List<DistributionLineDto> distributionLinesDto) {
		this.distributionLinesDto = distributionLinesDto;
	}
	
	
	public String getAgentSituation() {
		return agentSituation;
	}
	public void setAgentSituation(String agentSituation) {
		this.agentSituation = agentSituation;
	}
	public Long getDoctorId() {
		return doctorId;
	}
	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "PrescriptionRequest [agentId=" + agentId + ", locationId=" + locationId + ", districtId=" + districtId
				+ ", type=" + type + ", distributionLinesDto=" + distributionLinesDto + "]";
	}
	public Long getDayId() {
		return dayId;
	}
	public void setDayId(Long dayId) {
		this.dayId = dayId;
	}
	
	
	
	
	

}
