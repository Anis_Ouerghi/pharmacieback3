package com.transtu.core.json.request;

public class ProductAutoCompleteResponse {
	
	private Long id ; 
	private String labSearch ;
	private double vatRate;
	private double  price;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLabSearch() {
		return labSearch;
	}
	public void setLabSearch(String labSearch) {
		this.labSearch = labSearch;
	}
	
	public ProductAutoCompleteResponse() {
		super();
	}
	public ProductAutoCompleteResponse(Long id, String labSearch) {
		super();
		this.id = id;
		this.labSearch = labSearch;
	}
	public ProductAutoCompleteResponse(Long id, String labSearch, double vatRate, double price) {
		super();
		this.id = id;
		this.labSearch = labSearch;
		this.vatRate = vatRate;
		this.price = price;
	}
	public double getVatRate() {
		return vatRate;
	}
	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public ProductAutoCompleteResponse(String labSearch, double vatRate, double price) {
		super();
		this.labSearch = labSearch;
		this.vatRate = vatRate;
		this.price = price;
	}
	
	
	
	

}
