package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

import com.transtu.core.dto.InventoryLineDto;

public class UpdateInventoryRequest {
	
	
//	private Date inventoryDate ;
	private Long inventoryId ;
	
	private List<InventoryLineDto> inventoryLineDtos ;

	

	public Long getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}

	public List<InventoryLineDto> getInventoryLineDtos() {
		return inventoryLineDtos;
	}

	public void setInventoryLineDtos(List<InventoryLineDto> inventoryLineDtos) {
		this.inventoryLineDtos = inventoryLineDtos;
	}




	
	
	

}
