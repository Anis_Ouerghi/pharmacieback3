package com.transtu.core.json.request;

import javax.validation.constraints.NotNull;

public class ConsultationRequest {
	
	@NotNull
	private Long ProductId;
	private double DeliveredQt;
	private double MissingQt;
	public Long getProductId() {
		return ProductId;
	}
	public void setProductId(Long productId) {
		ProductId = productId;
	}
	public Double getDeliveredQt() {
		return DeliveredQt;
	}
	public void setDeliveredQt(Double deliveredQt) {
		DeliveredQt = deliveredQt;
	}
	public Double getMissingQt() {
		return MissingQt;
	}
	public void setMissingQt(Double missingQt) {
		MissingQt = missingQt;
	}
	
	
	
	

}
