package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.transtu.core.dto.ReceptionLineDto;

public class UpdateReceptionRequest {
	@NotNull
	private Long receptionId;
	private Date dateInvoice;
	private Date dateLiv;
	private String numInvoice;
	private String  numExternalDelivery;
	private List<ReceptionLineDto> receptionLinesDto;
	
	public Long getReceptionId() {
		return receptionId;
	}
	public void setReceptionId(Long receptionId) {
		this.receptionId = receptionId;
	}
	public List<ReceptionLineDto> getReceptionLinesDto() {
		return receptionLinesDto;
	}
	public void setReceptionLinesDto(List<ReceptionLineDto> receptionLinesDto) {
		this.receptionLinesDto = receptionLinesDto;
	}
	public UpdateReceptionRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Date getDateInvoice() {
		return dateInvoice;
	}
	public void setDateInvoice(Date dateInvoice) {
		this.dateInvoice = dateInvoice;
	}
	public Date getDateLiv() {
		return dateLiv;
	}
	public void setDateLiv(Date dateLiv) {
		this.dateLiv = dateLiv;
	}
	public String getNumInvoice() {
		return numInvoice;
	}
	public void setNumInvoice(String numInvoice) {
		this.numInvoice = numInvoice;
	}
	public String getNumExternalDelivery() {
		return numExternalDelivery;
	}
	public void setNumExternalDelivery(String numExternalDelivery) {
		this.numExternalDelivery = numExternalDelivery;
	}
	
	
}
