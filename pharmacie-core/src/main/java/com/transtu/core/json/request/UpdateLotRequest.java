package com.transtu.core.json.request;

import java.util.Date;



public class UpdateLotRequest {
	
	
	private Long id ; 
	private Date dateRefusal ;
	
	private String label;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateRefusal() {
		return dateRefusal;
	}

	public void setDateRefusal(Date dateRefusal) {
		this.dateRefusal = dateRefusal;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	
	

}
