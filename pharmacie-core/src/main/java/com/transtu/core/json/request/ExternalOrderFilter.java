package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

public class ExternalOrderFilter {
	
	private Long page ;
	private Long linesNbr ; 
	private Date minDate ; 
	private Date maxDate;
	private List<Long> productId;
	private String numExternalOrder ;
	private Long externalOrderId;
	private Long stateId;
	public Date getMinDate() {
		return minDate;
	}
	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}
	public Date getMaxDate() {
		return maxDate;
	}
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	public List<Long> getProductId() {
		return productId;
	}
	public void setProductId(List<Long> productId) {
		this.productId = productId;
	}
	public String getNumExternalOrder() {
		return numExternalOrder;
	}
	public void setNumExternalOrder(String numExternalOrder) {
		this.numExternalOrder = numExternalOrder;
	}
	public Long getExternalOrderId() {
		return externalOrderId;
	}
	public void setExternalOrderId(Long externalOrderId) {
		this.externalOrderId = externalOrderId;
	}
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getLinesNbr() {
		return linesNbr;
	}
	public void setLinesNbr(Long linesNbr) {
		this.linesNbr = linesNbr;
	}
	public Long getStateId() {
		return stateId;
	}
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	
	

}
