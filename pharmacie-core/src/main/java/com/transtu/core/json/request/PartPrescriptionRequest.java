package com.transtu.core.json.request;

import java.io.Serializable;
import java.util.List;

import com.transtu.core.dto.DistributionLineDto;
import com.transtu.core.dto.PrescriptionLineDto;

public class PartPrescriptionRequest implements Serializable {
	

	private static final long serialVersionUID = 3911963462722462710L;
	
	private String agentId ; 
	private Long districtId ; 
	private Long locationId; 
	private List<PrescriptionLineDto> prescriptionLinesDto ; 
	private List<DistributionLineDto> distributionLinesDto ;
	
	public String getAgentId() {
		return agentId;
	}
	
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	
	public Long getDistrictId() {
		return districtId;
	}
	
	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}
	
	public Long getLocationId() {
		return locationId;
	}
	
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	
	public List<PrescriptionLineDto> getPrescriptionLinesDto() {
		return prescriptionLinesDto;
	}
	
	public void setPrescriptionLinesDto(List<PrescriptionLineDto> prescriptionLinesDto) {
		this.prescriptionLinesDto = prescriptionLinesDto;
	}
	
	public List<DistributionLineDto> getDistributionLinesDto() {
		return distributionLinesDto;
	}
	
	public void setDistributionLinesDto(List<DistributionLineDto> distributionLines) {
		this.distributionLinesDto = distributionLines;
	}
	
//	public PartPrescriptionRequest(String agentId, Long districtId, Long locationId,
//			List<PrescriptionLineDto> prescriptionLinesDto, List<DistributionLineDto> distributionLines) {
//		super();
//		this.agentId = agentId;
//		this.districtId = districtId;
//		this.locationId = locationId;
//		this.prescriptionLinesDto = prescriptionLinesDto;
//		this.distributionLinesDto = distributionLines;
//	}
//	
	

	@Override
	public String toString() {
		return "PartPrescriptionRequest [agentId=" + agentId + ", districtId=" + districtId + ", locationId="
				+ locationId + "]";
	}
}
