package com.transtu.core.json.request;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class InternalOrderResponse {

	
	
	private Long id;
	private Date DateOrder;
	private String numOrder;
	private Long emplacementId;;
	private Long statId;
	private Long nbrprod;
	
	
	public InternalOrderResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDateOrder() {
		return DateOrder;
	}
	public void setDateOrder(Date dateOrder) {
		DateOrder = dateOrder;
	}
	public String getNumOrder() {
		return numOrder;
	}
	public void setNumOrder(String numOrder) {
		this.numOrder = numOrder;
	}
	public Long getEmplacementId() {
		return emplacementId;
	}
	public void setEmplacementId(Long emplacementId) {
		this.emplacementId = emplacementId;
	}
	public Long getStatId() {
		return statId;
	}
	public void setStatId(Long statId) {
		this.statId = statId;
	}
	public Long getNbrprod() {
		return nbrprod;
	}
	public void setNbrprod(Long nbrprod) {
		this.nbrprod = nbrprod;
	}
	
	
}  
