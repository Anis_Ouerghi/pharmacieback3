package com.transtu.core.json.request;

import java.util.List;

import javax.validation.constraints.NotNull;
import com.transtu.core.dto.InternalOrderLineDto;

public class UpdateOrderRequest {
	
	@NotNull
	private Long internalOrderId;
	private List<InternalOrderLineDto> internalOrderLinesDto;
	public Long getInternalOrderId() {
		return internalOrderId;
	}
	public void setInternalOrderId(Long internalOrderId) {
		this.internalOrderId = internalOrderId;
	}
	public List<InternalOrderLineDto> getInternalOrderLinesDto() {
		return internalOrderLinesDto;
	}
	public void setInternalOrderLinesDto(List<InternalOrderLineDto> internalOrderLinesDto) {
		this.internalOrderLinesDto = internalOrderLinesDto;
	}
	
	
	
	

}
