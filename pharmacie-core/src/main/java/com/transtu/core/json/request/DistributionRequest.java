package com.transtu.core.json.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.transtu.core.dto.PrescriptionLineDto;

public class DistributionRequest {
	
	@NotNull
	private Long locationId;
	private Long districtId;
	//private Long doctorId;
	@NotNull
	private Long prescriptionId;
	@NotNull
	private Long dayId;
	
	private List<PrescriptionLineDto> distributionLines ;
	
	
	
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	public Long getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	
	public Long getDayId() {
		return dayId;
	}
	public void setDayId(Long dayId) {
		this.dayId = dayId;
	}
	public Long getPrescriptionId() {
		return prescriptionId;
	}
	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}
	public DistributionRequest() {
		super();
	}
	public List<PrescriptionLineDto> getDistributionLines() {
		return distributionLines;
	}
	public void setDistributionLines(List<PrescriptionLineDto> distributionLines) {
		this.distributionLines = distributionLines;
	}
//	public Long getDoctorId() {
//		return doctorId;
//	}
//	public void setDoctorId(Long doctorId) {
//		this.doctorId = doctorId;
//	}
	
	
	
}
