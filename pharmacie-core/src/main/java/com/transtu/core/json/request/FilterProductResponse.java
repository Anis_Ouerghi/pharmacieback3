package com.transtu.core.json.request;

import java.util.List;

import com.transtu.persistence.entities.Product;

public class FilterProductResponse {

	private Long page ; 
	private Long nbrPages ;
	private Long totalProducts ; 
	List<Product> products ;
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getNbrPages() {
		return nbrPages;
	}
	public void setNbrPages(Long nbrPages) {
		this.nbrPages = nbrPages;
	}
	public Long getTotalProducts() {
		return totalProducts;
	}
	public void setTotalProducts(Long totalProducts) {
		this.totalProducts = totalProducts;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	
	
	
}
