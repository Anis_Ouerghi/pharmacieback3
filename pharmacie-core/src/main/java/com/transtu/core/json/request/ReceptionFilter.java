package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

public class ReceptionFilter {

	
	private Date minDate ; 
	private Date maxDate;
	private List<Long> productId;
	private String numReception ;
	private Long externalDeliveryId;
	private Long stateId;
	public Date getMinDate() {
		return minDate;
	}
	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}
	public Date getMaxDate() {
		return maxDate;
	}
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	public List<Long> getProductId() {
		return productId;
	}
	public void setProductId(List<Long> productId) {
		this.productId = productId;
	}
	public String getNumReception() {
		return numReception;
	}
	public void setNumReception(String numReception) {
		this.numReception = numReception;
	}
	public Long getExternalDeliveryId() {
		return externalDeliveryId;
	}
	public void setExternalDeliveryId(Long externalDeliveryId) {
		this.externalDeliveryId = externalDeliveryId;
	}
	public Long getStateId() {
		return stateId;
	}
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	
	
	
}
