package com.transtu.core.json.request;

public class DuplcateEmptyResp {
	
	private Long numberDup ;
	private Long numberEmpty ;
    private Boolean state;
	public Long getNumberDup() {
		return numberDup;
	}
	public void setNumberDup(Long numberDup) {
		this.numberDup = numberDup;
	}
	public Long getNumberEmpty() {
		return numberEmpty;
	}
	public void setNumberEmpty(Long numberEmpty) {
		this.numberEmpty = numberEmpty;
	}
	public Boolean getState() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
    
	
    

}
