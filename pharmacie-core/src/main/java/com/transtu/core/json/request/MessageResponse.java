/**
 * 
 */
package com.transtu.core.json.request;

public final class MessageResponse extends AbstractResponse {

	private final String code;
    private final String error;
	private final String message;
	private final String num;
	


	
	

	public String getCode() {
		return code;
	}

	public MessageResponse(String code, String error, String message, String num) {
		super();
		this.code = code;
		this.error = error;
		this.message = message;
		this.num = num;
	}

	public String getNum() {
		return num;
	}

	public String getError() {
		return error;
	}

	public String getMessage() {
		return message;
	}

}
