package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.boot.autoconfigure.security.SecurityProperties.User;

import com.transtu.core.dto.UsersDto;



public class AddUserDayRequest {
	
	
	@NotNull
	private Long dayId;
    private Date dateDay;
	private Long dayTypeId;
	private List <UsersDto> usersDto;
	
	public Long getDayId() {
		return dayId;
	}
	public void setDayId(Long dayId) {
		this.dayId = dayId;
	}
	public List<UsersDto> getUsersDto() {
		return usersDto;
	}
	public void setUsersDto(List<UsersDto> usersDto) {
		this.usersDto = usersDto;
	}
	
	public Long getDayTypeId() {
		return dayTypeId;
	}
	public void setDayTypeId(Long dayTypeId) {
		this.dayTypeId = dayTypeId;
	}
	public Date getDateDay() {
		return dateDay;
	}
	public void setDateDay(Date dateDay) {
		this.dateDay = dateDay;
	}
	

}
