package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

public class Usersfilter {
	
	private Date dateday ; 
    private Long dayTypeId;
    
	public Date getDateday() {
		return dateday;
	}
	public void setDateday(Date dateday) {
		this.dateday = dateday;
	}
	public Long getDayTypeId() {
		return dayTypeId;
	}
	public void setDayTypeId(Long dayTypeId) {
		this.dayTypeId = dayTypeId;
	}
    
    

}
