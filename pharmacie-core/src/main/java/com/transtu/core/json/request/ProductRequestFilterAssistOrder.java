package com.transtu.core.json.request;

import java.util.Date;

public class ProductRequestFilterAssistOrder {
	
	private Long page ;
	private Long linesNbr ; 
	private Long familyId ;
	private Long pharmaClassId;
	private Long presentationId;
	private Long dciId;
	private Long formId;
	private Long dossageId;
	private Long typeId;
	private Date satart;
	private Date end;
	private Boolean alerte;
	private Boolean security; 
	
	
	private Long depotId;
	private Boolean actived;
	private Long productId;
	private Long distributionUnitId;
	private String barCode;
	private String codePctProd;
	private String lib;
	
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getLinesNbr() {
		return linesNbr;
	}
	public void setLinesNbr(Long linesNbr) {
		this.linesNbr = linesNbr;
	}
	public Long getFamilyId() {
		return familyId;
	}
	public void setFamilyId(Long familyId) {
		this.familyId = familyId;
	}
	public Long getPharmaClassId() {
		return pharmaClassId;
	}
	public void setPharmaClassId(Long pharmaClassId) {
		this.pharmaClassId = pharmaClassId;
	}
	public Long getPresentationId() {
		return presentationId;
	}
	public void setPresentationId(Long presentationId) {
		this.presentationId = presentationId;
	}
	public Long getDciId() {
		return dciId;
	}
	public void setDciId(Long dciId) {
		this.dciId = dciId;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Long getDossageId() {
		return dossageId;
	}
	public void setDossageId(Long dossageId) {
		this.dossageId = dossageId;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public Date getSatart() {
		return satart;
	}
	public void setSatart(Date satart) {
		this.satart = satart;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public Boolean getAlerte() {
		return alerte;
	}
	public void setAlerte(Boolean alerte) {
		this.alerte = alerte;
	}
	public Long getDepotId() {
		return depotId;
	}
	public void setDepotId(Long depotId) {
		this.depotId = depotId;
	}
	public Boolean getActived() {
		return actived;
	}
	public void setActived(Boolean actived) {
		this.actived = actived;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getDistributionUnitId() {
		return distributionUnitId;
	}
	public void setDistributionUnitId(Long distributionUnitId) {
		this.distributionUnitId = distributionUnitId;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getCodePctProd() {
		return codePctProd;
	}
	public void setCodePctProd(String codePctProd) {
		this.codePctProd = codePctProd;
	}
	public String getLib() {
		return lib;
	}
	public void setLib(String lib) {
		this.lib = lib;
	}
	public Boolean getSecurity() {
		return security;
	}
	public void setSecurity(Boolean security) {
		this.security = security;
	}
	

	
	
	
	
	
	

 
}
