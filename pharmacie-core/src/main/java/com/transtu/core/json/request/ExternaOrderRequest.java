package com.transtu.core.json.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.transtu.core.dto.ExternalOrderLineDto;

public class ExternaOrderRequest {

	@NotNull
	private Long locationId;
	private Long statId;
	private Long dayId;
	private List <ExternalOrderLineDto> externalOrderLinesDto;
	
	public Long getLocationId() {
		return locationId;
	}
	public Long getStatId() {
		return statId;
	}
	public void setStatId(Long statId) {
		this.statId = statId;
	}
	public List<ExternalOrderLineDto> getExternalOrderLinesDto() {
		return externalOrderLinesDto;
	}
	public void setExternalOrderLinesDto(List<ExternalOrderLineDto> externalOrderLinesDto) {
		this.externalOrderLinesDto = externalOrderLinesDto;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	
	public Long getDayId() {
		return dayId;
	}
	public void setDayId(Long dayId) {
		this.dayId = dayId;
	}
	public ExternaOrderRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
