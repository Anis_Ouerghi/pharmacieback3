package com.transtu.core.json.request;

import java.util.List;

import com.transtu.persistence.entities.ExternalOrder;

public class ExternalOrderFilterResponse {

	private Long page ; 
	private Long nbrPages ;
	private Long totalExternalOrders ;
	private List<ExternalOrder> externalOrders ;
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getNbrPages() {
		return nbrPages;
	}
	public void setNbrPages(Long nbrPages) {
		this.nbrPages = nbrPages;
	}
	public Long getTotalExternalOrders() {
		return totalExternalOrders;
	}
	public void setTotalExternalOrders(Long totalExternalOrders) {
		this.totalExternalOrders = totalExternalOrders;
	}
	public List<ExternalOrder> getExternalOrders() {
		return externalOrders;
	}
	public void setExternalOrders(List<ExternalOrder> externalOrders) {
		this.externalOrders = externalOrders;
	}
	public ExternalOrderFilterResponse() {
		super();
	}
	public ExternalOrderFilterResponse(Long page, Long nbrPages, Long totalExternalOrders,
			List<ExternalOrder> externalOrders) {
		super();
		this.page = page;
		this.nbrPages = nbrPages;
		this.totalExternalOrders = totalExternalOrders;
		this.externalOrders = externalOrders;
	}
	
	
}
