package com.transtu.core.json.request;

public class ProductResponse {
	private Long id;
	private String barCode;
	private String codePctProd;
	private String lib;
	//***
	private String labelSearch;
	private String labelSearchStkPha;
	private double vatRate;
	private double wauCost;
	private double safetyStock;
	private double minStock;
	private double warningStock;
	private boolean actived;
	private double  price;
	private double  colisage;
		//**

		private String dossage;
		private String type;
		private String dci;
		private String distributionUnit;
		private String form;
		private String pharmaClass;
		private String depot;
		private String presentation;
		private String family;
		private double StockPharma;
		private double StockDep;
		
		
		
		
		
		public Long getId() {
			return id;
		}
		
		
		public double getColisage() {
			return colisage;
		}


		public void setColisage(double colisage) {
			this.colisage = colisage;
		}


		public void setId(Long id) {
			this.id = id;
		}
		public String getBarCode() {
			return barCode;
		}
		public void setBarCode(String barCode) {
			this.barCode = barCode;
		}
		public String getCodePctProd() {
			return codePctProd;
		}
		public void setCodePctProd(String codePctProd) {
			this.codePctProd = codePctProd;
		}
		public String getLib() {
			return lib;
		}
		public void setLib(String lib) {
			this.lib = lib;
		}
		public String getLabelSearch() {
			return labelSearch;
		}
		public void setLabelSearch(String labelSearch) {
			this.labelSearch = labelSearch;
		}
		public String getLabelSearchStkPha() {
			return labelSearchStkPha;
		}
		public void setLabelSearchStkPha(String labelSearchStkPha) {
			this.labelSearchStkPha = labelSearchStkPha;
		}
		public double getVatRate() {
			return vatRate;
		}
		public void setVatRate(double vatRate) {
			this.vatRate = vatRate;
		}
		public double getWauCost() {
			return wauCost;
		}
		public void setWauCost(double wauCost) {
			this.wauCost = wauCost;
		}
		public double getSafetyStock() {
			return safetyStock;
		}
		public void setSafetyStock(double safetyStock) {
			this.safetyStock = safetyStock;
		}
		public double getMinStock() {
			return minStock;
		}
		public void setMinStock(double minStock) {
			this.minStock = minStock;
		}
		public double getWarningStock() {
			return warningStock;
		}
		public void setWarningStock(double warningStock) {
			this.warningStock = warningStock;
		}
		public String getDossage() {
			return dossage;
		}
		public void setDossage(String dossage) {
			this.dossage = dossage;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getDci() {
			return dci;
		}
		public void setDci(String dci) {
			this.dci = dci;
		}
		public String getDistributionUnit() {
			return distributionUnit;
		}
		public void setDistributionUnit(String distributionUnit) {
			this.distributionUnit = distributionUnit;
		}
		public String getForm() {
			return form;
		}
		public void setForm(String form) {
			this.form = form;
		}
		public String getPharmaClass() {
			return pharmaClass;
		}
		public void setPharmaClass(String pharmaClass) {
			this.pharmaClass = pharmaClass;
		}
		public String getDepot() {
			return depot;
		}
		public void setDepot(String depot) {
			this.depot = depot;
		}
		public String getPresentation() {
			return presentation;
		}
		public void setPresentation(String presentation) {
			this.presentation = presentation;
		}
		public String getFamily() {
			return family;
		}
		public void setFamily(String family) {
			this.family = family;
		}
		
		public double getStockDep() {
			return StockDep;
		}
		public void setStockDep(double stockDep) {
			StockDep = stockDep;
		}
		public ProductResponse() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		
		public boolean isActived() {
			return actived;
		}
		public void setActived(boolean actived) {
			this.actived = actived;
		}
		
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		public ProductResponse(Long id, String barCode, String codePctProd, String lib, String labelSearch,
				String labelSearchStkPha, double vatRate, double wauCost, double safetyStock, double minStock,
				double warningStock, String dossage, String type, String dci, String distributionUnit, String form,
				String pharmaClass, String depot, String presentation, String family, double stockPharma,
				double stockDep) {
			super();
			this.id = id;
			this.barCode = barCode;
			this.codePctProd = codePctProd;
			this.lib = lib;
			this.labelSearch = labelSearch;
			this.labelSearchStkPha = labelSearchStkPha;
			this.vatRate = vatRate;
			this.wauCost = wauCost;
			this.safetyStock = safetyStock;
			this.minStock = minStock;
			this.warningStock = warningStock;
			this.dossage = dossage;
			this.type = type;
			this.dci = dci;
			this.distributionUnit = distributionUnit;
			this.form = form;
			this.pharmaClass = pharmaClass;
			this.depot = depot;
			this.presentation = presentation;
			this.family = family;
			StockPharma = stockPharma;
			StockDep = stockDep;
		}
		public double getStockPharma() {
			return StockPharma;
		}
		public void setStockPharma(double stockPharma) {
			StockPharma = stockPharma;
		}
		public ProductResponse(Long id, String barCode, String codePctProd, String lib) {
			super();
			this.id = id;
			this.barCode = barCode;
			this.codePctProd = codePctProd;
			this.lib = lib;
		}
		
		

}
