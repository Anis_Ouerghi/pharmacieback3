package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

public class InternalDeliveryFilter {
	
	private Date minDate ; 
	private Date maxDate;
	private List<Long> productId;
	private String numDelivery = "";
    private Long internalDeliveryId;
    private Long internalOrderyId;
    private Long stateId;
    private Long page;
    public Long getInternalOrderyId() {
		return internalOrderyId;
	}

	public void setInternalOrderyId(Long internalOrderyId) {
		this.internalOrderyId = internalOrderyId;
	}

	private Long customerId;
    private String numInternalOrder; 
	
	public InternalDeliveryFilter() {
		super();
	}
	
	public Date getMinDate() {
		return minDate;
	}
	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}
	public Date getMaxDate() {
		return maxDate;
	}
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	public List<Long> getProductId() {
		return productId;
	}
	public void setProductId(List<Long> productId) {
		this.productId = productId;
	}
	public String getNumDelivery() {
		return numDelivery;
	}
	public void setNumDelivery(String numDelivery) {
		this.numDelivery = numDelivery;
	}

	public Long getInternalDeliveryId() {
		return internalDeliveryId;
	}

	public void setInternalDeliveryId(Long internalDeliveryId) {
		this.internalDeliveryId = internalDeliveryId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getNumInternalOrder() {
		return numInternalOrder;
	}

	public void setNumInternalOrder(String numInternalOrder) {
		this.numInternalOrder = numInternalOrder;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	

	
	
	
}
