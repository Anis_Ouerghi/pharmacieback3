package com.transtu.core.json.request;

import com.transtu.core.dto.InternalOrderLineDto;


import java.util.List;
import javax.validation.constraints.NotNull;



public class InternalOrderRequest {
	
	@NotNull
	private Long locationId;
	private Long statId;
	private Long dayId;
	private Long typeId;
	private List <InternalOrderLineDto> internalOrderLines;
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	public Long getStatId() {
		return statId;
	}
	public void setStatId(Long statId) {
		this.statId = statId;
	}
	public List<InternalOrderLineDto> getInternalOrderLines() {
		return internalOrderLines;
	}
	public void setInternalOrderLines(List<InternalOrderLineDto> internalOrderLines) {
		this.internalOrderLines = internalOrderLines;
	}
	public InternalOrderRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public InternalOrderRequest(Long locationId, Long statId, List<InternalOrderLineDto> internalOrderLines) {
		super();
		this.locationId = locationId;
		this.statId = statId;
		this.internalOrderLines = internalOrderLines;
	}
	public Long getDayId() {
		return dayId;
	}
	public void setDayId(Long dayId) {
		this.dayId = dayId;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	

}
