package com.transtu.core.json.request;

import java.sql.Date;
import java.util.List;

import com.transtu.core.dto.ReceptionLineDto;

public class ReceptionRequest {
	
	private Long extrernalOrderId;
	private Long extrernalDeliveryId;
	private String extrernalDeliveryNum;
	private Date dateLiv;
	private String numInvoice;
	private Date dateInvoice;
	
	private Long dayId;
	private Long locationId;
	private Long stateId;
	private Long typeId; //Additif=2 or Reception Ordinaire=1 
	private List<ReceptionLineDto> ReceptionLinesDto;
	
	public Long getExtrernalOrderId() {
		return extrernalOrderId;
	}
	public void setExtrernalOrderId(Long extrernalOrderId) {
		this.extrernalOrderId = extrernalOrderId;
	}
	public Long getExtrernalDeliveryId() {
		return extrernalDeliveryId;
	}
	public void setExtrernalDeliveryId(Long extrernalDeliveryId) {
		this.extrernalDeliveryId = extrernalDeliveryId;
	}
	public String getExtrernalDeliveryNum() {
		return extrernalDeliveryNum;
	}
	public void setExtrernalDeliveryNum(String extrernalDeliveryNum) {
		this.extrernalDeliveryNum = extrernalDeliveryNum;
	}
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	public Long getStateId() {
		return stateId;
	}
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	public List<ReceptionLineDto> getReceptionLinesDto() {
		return ReceptionLinesDto;
	}
	public void setReceptionLinesDto(List<ReceptionLineDto> receptionLinesDto) {
		ReceptionLinesDto = receptionLinesDto;
	}
	
	public Long getDayId() {
		return dayId;
	}
	public void setDayId(Long dayId) {
		this.dayId = dayId;
	}
	public ReceptionRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Date getDateLiv() {
		return dateLiv;
	}
	public void setDateLiv(Date dateLiv) {
		this.dateLiv = dateLiv;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public String getNumInvoice() {
		return numInvoice;
	}
	public void setNumInvoice(String numInvoice) {
		this.numInvoice = numInvoice;
	}
	public Date getDateInvoice() {
		return dateInvoice;
	}
	public void setDateInvoice(Date dateInvoice) {
		this.dateInvoice = dateInvoice;
	}
	
	
	

}
