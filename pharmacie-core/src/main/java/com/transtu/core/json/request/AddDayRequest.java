

package com.transtu.core.json.request;

import java.util.Date;

public class AddDayRequest {
	
	private Long id;
	private Date date ;
	private long dayTypeId ;
	private boolean state ;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public long getDayTypeId() {
		return dayTypeId;
	}
	public void setDayTypeId(long dayTypeId) {
		this.dayTypeId = dayTypeId;
	}
	public boolean isState() {
		return state;
	}
	public void setState(boolean state) {
		this.state = state;
	} 


	

}
