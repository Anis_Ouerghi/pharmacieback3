package com.transtu.core.json.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.transtu.core.dto.ExternalOrderLineDto;

public class UpdateExternalOrderRequest {
	@NotNull
	private Long ExternalOrderId;
	private List<ExternalOrderLineDto> externalOrderLinesDto;
	public Long getExternalOrderId() {
		return ExternalOrderId;
	}
	public void setExternalOrderId(Long externalOrderId) {
		ExternalOrderId = externalOrderId;
	}
	public List<ExternalOrderLineDto> getExternalOrderLinesDto() {
		return externalOrderLinesDto;
	}
	public void setExternalOrderLinesDto(List<ExternalOrderLineDto> externalOrderLinesDto) {
		this.externalOrderLinesDto = externalOrderLinesDto;
	}
	public UpdateExternalOrderRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
