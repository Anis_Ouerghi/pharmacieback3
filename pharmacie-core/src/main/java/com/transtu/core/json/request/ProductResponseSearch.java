package com.transtu.core.json.request;

public class ProductResponseSearch {
	
	
	private Long id;
	
	private String barCode;
	
	private String labelSearchStkPha;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getLabelSearchStkPha() {
		return labelSearchStkPha;
	}

	public void setLabelSearchStkPha(String labelSearchStkPha) {
		this.labelSearchStkPha = labelSearchStkPha;
	}

	
	
		
		


}
