package com.transtu.core.json.request;

import java.util.List;

public class StockFilterRequest {

	private Long locationId ; 
	private List<Long> productsId;
	
	
	
	public Long getLocationId() {
		return locationId;
	}



	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}







	public List<Long> getProductsId() {
		return productsId;
	}



	public void setProductsId(List<Long> productsId) {
		this.productsId = productsId;
	}



	public StockFilterRequest() {
		super();
	}
	
	
}
