package com.transtu.core.json.request;


public class ActiveProductByLabelStartWith  {
	
	private String label;
	private Boolean active;

    
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

	}
	
