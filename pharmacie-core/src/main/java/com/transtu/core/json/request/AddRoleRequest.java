package com.transtu.core.json.request;

import java.io.Serializable;

public class AddRoleRequest implements Serializable {

	private Long id;
	private String role;
	private String discription;
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public AddRoleRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AddRoleRequest(String role, String discription) {
		super();
		this.role = role;
		this.discription = discription;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public AddRoleRequest(Long id, String role, String discription) {
		super();
		this.id = id;
		this.role = role;
		this.discription = discription;
	}
	
	
}
