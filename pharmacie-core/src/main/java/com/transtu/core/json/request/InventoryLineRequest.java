package com.transtu.core.json.request;

public class InventoryLineRequest {
	
	private Long page ;
	private Long linesNbr ; 
	private Long inventoryId ;
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getLinesNbr() {
		return linesNbr;
	}
	public void setLinesNbr(Long linesNbr) {
		this.linesNbr = linesNbr;
	}
	public Long getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}

	
	
	
}
