package com.transtu.core.json.request;

import java.util.List;

import com.transtu.persistence.entities.Inventory;
;

public class FilterInventoryResponse {
	
	

		private Long page ; 
		private Long nbrPages ;
		private Long totalInventorys ; 
		List<Inventory> inventorys ;
		public Long getPage() {
			return page;
		}
		public void setPage(Long page) {
			this.page = page;
		}
		public Long getNbrPages() {
			return nbrPages;
		}
		public void setNbrPages(Long nbrPages) {
			this.nbrPages = nbrPages;
		}
	
		public Long getTotalInventorys() {
			return totalInventorys;
		}
		public void setTotalInventorys(Long totalInventorys) {
			this.totalInventorys = totalInventorys;
		}
		public List<Inventory> getInventorys() {
			return inventorys;
		}
		public void setInventorys(List<Inventory> inventorys) {
			this.inventorys = inventorys;
		}
	

}
