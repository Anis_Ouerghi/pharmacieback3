package com.transtu.core.json.request;

import java.util.List;

import com.transtu.persistence.entities.InternalOrder;


public class InternalordfilterResponse {
	
	private List<InternalOrder> internalOrders;
    private Long totalItemes;
    
    
	public List<InternalOrder> getInternalOrders() {
		return internalOrders;
	}
	public void setInternalOrders(List<InternalOrder> internalOrders) {
		this.internalOrders = internalOrders;
	}
	public Long getTotalItemes() {
		return totalItemes;
	}
	public void setTotalItemes(Long totalItemes) {
		this.totalItemes = totalItemes;
	}
    
    

}
