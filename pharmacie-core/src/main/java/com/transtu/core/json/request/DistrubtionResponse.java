package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

import com.transtu.core.dto.DistributionLineDto;


public class DistrubtionResponse {
	
	private String agentId;
	private String distributionNumber ; 
	private Date dateDist ;
	private String Locationlib;
	private List<DistributionLineDto> distributionLineDtos;
	
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getDistributionNumber() {
		return distributionNumber;
	}
	public void setDistributionNumber(String distributionNumber) {
		this.distributionNumber = distributionNumber;
	}
	public Date getDateDist() {
		return dateDist;
	}
	public void setDateDist(Date dateDist) {
		this.dateDist = dateDist;
	}
	public String getLocationlib() {
		return Locationlib;
	}
	public void setLocationlib(String locationlib) {
		Locationlib = locationlib;
	}
	public List<DistributionLineDto> getDistributionLineDtos() {
		return distributionLineDtos;
	}
	public void setDistributionLineDtos(List<DistributionLineDto> distributionLineDtos) {
		this.distributionLineDtos = distributionLineDtos;
	}
	
	public DistrubtionResponse(String distributionNumber, Date dateDist, String locationlib,
			List<DistributionLineDto> distributionLineDtos) {
		super();
		this.distributionNumber = distributionNumber;
		this.dateDist = dateDist;
		Locationlib = locationlib;
		this.distributionLineDtos = distributionLineDtos;
	}
	public DistrubtionResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	

}
