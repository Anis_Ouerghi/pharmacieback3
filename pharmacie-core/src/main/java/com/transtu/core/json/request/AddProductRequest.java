package com.transtu.core.json.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

public class AddProductRequest  {
	
	
    
	private String codeaBarre;
	private String codePctProd;
	private String libelle;
	private double price;
	private double stockMin;
	private double warningStock;
    private double safetyStock;
	private double wauCost;
	private double VatRate;
	private Double colisage;
	private Long dciId;
	private Long distributionUnitId;
	private Long dosageId ;
	private Long formId ;
	private Long presentationId ;
	private Long depotId ;
	private Long typeId;
	private Long pharmaClassId ;

	
	
	
	
	public String getCodeaBarre() {
		return codeaBarre;
	}
	public void setCodeaBarre(String codeaBarre) {
		this.codeaBarre = codeaBarre;
	}
	public String getCodePctProd() {
		return codePctProd;
	}
	public void setCodePctProd(String codePctProd) {
		this.codePctProd = codePctProd;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public double getVatRate() {
		return VatRate;
	}
	public void setVatRate(double vatRate) {
		VatRate = vatRate;
	}
	public double getSafetyStock() {
		return safetyStock;
	}
	public void setSafetyStock(double safetyStock) {
		this.safetyStock = safetyStock;
	}
	public double getStockMin() {
		return stockMin;
	}
	public void setStockMin(double stockMin) {
		this.stockMin = stockMin;
	}
	public double getWarningStock() {
		return warningStock;
	}
	public void setWarningStock(double warningStock) {
		this.warningStock = warningStock;
	}
	public Double getColisage() {
		return colisage;
	}
	public void setColisage(Double colisage) {
		this.colisage = colisage;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public Long getDciId() {
		return dciId;
	}
	public void setDciId(Long dciId) {
		this.dciId = dciId;
	}
	public Long getDistributionUnitId() {
		return distributionUnitId;
	}
	public void setDistributionUnitId(Long distributionUnitId) {
		this.distributionUnitId = distributionUnitId;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Long getPresentationId() {
		return presentationId;
	}
	public void setPresentationId(Long presentationId) {
		this.presentationId = presentationId;
	}
	public Long getDepotId() {
		return depotId;
	}
	public void setDepotId(Long depotId) {
		this.depotId = depotId;
	}
	public Long getDosageId() {
		return dosageId;
	}
	public void setDosageId(Long dosageId) {
		this.dosageId = dosageId;
	}
	public Long getPharmaClassId() {
		return pharmaClassId;
	}
	public void setPharmaClassId(Long pharmaClassId) {
		this.pharmaClassId = pharmaClassId;
	}
	public double getWauCost() {
		return wauCost;
	}
	public void setWauCost(double wauCost) {
		this.wauCost = wauCost;
	}
	public AddProductRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
