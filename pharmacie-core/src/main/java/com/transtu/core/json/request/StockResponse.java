package com.transtu.core.json.request;

public class StockResponse {
	
	private ProductResponse productResponse;
	//private Long productid ;
	private double quantity;
	private double minStock;
	private double safetyStock;
	private double sumStock;
	
	public double getSumStock() {
		return sumStock;
	}
	public void setSumStock(double sumStock) {
		this.sumStock = sumStock;
	}
	public ProductResponse getProductResponse() {
		return productResponse;
	}
	public void setProductResponse(ProductResponse productResponse) {
		this.productResponse = productResponse;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	public double getMinStock() {
		return minStock;
	}
	
	public double getSafetyStock() {
		return safetyStock;
	}
	public void setSafetyStock(double safetyStock) {
		this.safetyStock = safetyStock;
	}
	public void setMinStock(double minStock) {
		this.minStock = minStock;
	}
	public StockResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
