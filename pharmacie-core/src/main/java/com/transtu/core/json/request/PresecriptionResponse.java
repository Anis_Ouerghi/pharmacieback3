package com.transtu.core.json.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.transtu.core.dto.DistributionLineDto;

public class PresecriptionResponse {
	
	@NotNull
	private String agentId ;
	@NotNull
	private Long locationId ; 
	@NotNull
	private Long dayId ;
	private Long districtId ; 
	private String type;
	private List<DistributionLineDto> distributionLinesDto ;
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	public Long getDayId() {
		return dayId;
	}
	public void setDayId(Long dayId) {
		this.dayId = dayId;
	}
	public Long getDistrictId() {
		return districtId;
	}
	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<DistributionLineDto> getDistributionLinesDto() {
		return distributionLinesDto;
	}
	public void setDistributionLinesDto(List<DistributionLineDto> distributionLinesDto) {
		this.distributionLinesDto = distributionLinesDto;
	}
	public PresecriptionResponse(String agentId, Long locationId, Long dayId, Long districtId, String type,
			List<DistributionLineDto> distributionLinesDto) {
		super();
		this.agentId = agentId;
		this.locationId = locationId;
		this.dayId = dayId;
		this.districtId = districtId;
		this.type = type;
		this.distributionLinesDto = distributionLinesDto;
	}
	public PresecriptionResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
