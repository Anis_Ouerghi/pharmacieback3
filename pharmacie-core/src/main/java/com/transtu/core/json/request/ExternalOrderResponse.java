package com.transtu.core.json.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.transtu.core.dto.ExternalOrderLineDto;
import com.transtu.core.dto.ReceptionLineDto;

public class ExternalOrderResponse {


	public ExternalOrderResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	@NotNull
	private Long locationId;
	private Long statId;
	private List <ExternalOrderLineDto> externalOrderLinesDto;
	private List<ReceptionLineDto>receptionLineDtos;
	
	public Long getLocationId() {
		return locationId;
	}
	public Long getStatId() {
		return statId;
	}
	public void setStatId(Long statId) {
		this.statId = statId;
	}
	public List<ExternalOrderLineDto> getExternalOrderLinesDto() {
		return externalOrderLinesDto;
	}
	public void setExternalOrderLinesDto(List<ExternalOrderLineDto> externalOrderLinesDto) {
		this.externalOrderLinesDto = externalOrderLinesDto;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	public List<ReceptionLineDto> getReceptionLineDtos() {
		return receptionLineDtos;
	}
	public void setReceptionLineDtos(List<ReceptionLineDto> receptionLineDtos) {
		this.receptionLineDtos = receptionLineDtos;
	}
	
}

