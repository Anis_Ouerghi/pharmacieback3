package com.transtu.core.json.request;

import java.util.List;

import com.transtu.core.dto.InternalDeliveryLineDto;

public class InternalDeliveryRequest {
	
	private Long internalOrderId;
	private Long locationId;
	private Long customerId;
	private Long dayId;
	private List<InternalDeliveryLineDto> internalDeliveryLinesDto;
	
	
	
	public InternalDeliveryRequest() {
		super();
	}
	
	public Long getInternalOrderId() {
		return internalOrderId;
	}
	public void setInternalOrderId(Long internalOrderId) {
		this.internalOrderId = internalOrderId;
	}
	
	public List<InternalDeliveryLineDto> getInternalDeliveryLinesDto() {
		return internalDeliveryLinesDto;
	}
	public void setInternalDeliveryLinesDto(List<InternalDeliveryLineDto> internalDeliveryLinesDto) {
		this.internalDeliveryLinesDto = internalDeliveryLinesDto;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	

	public Long getDayId() {
		return dayId;
	}

	public void setDayId(Long dayId) {
		this.dayId = dayId;
	}
	
	
	
}
