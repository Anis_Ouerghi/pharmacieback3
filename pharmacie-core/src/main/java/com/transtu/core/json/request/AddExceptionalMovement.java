package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

import com.transtu.core.dto.StockMovementLineDto;

public class AddExceptionalMovement {

	private Date dateMovement;
	private Long dayId;
	private Long locationId;
	
	
	private String numMovement;
	private String typeMvt;
	private String descriminator;
	
	private Long referenceTypeId;
	private Long distributionId;
	private Long internalDeliveryId;
	private Long receptionId;
	List<StockMovementLineDto> stockMovementLineDtos;
	public Date getDateMovement() {
		return dateMovement;
	}
	public void setDateMovement(Date dateMovement) {
		this.dateMovement = dateMovement;
	}
	public Long getDayId() {
		return dayId;
	}
	public void setDayId(Long dayId) {
		this.dayId = dayId;
	}
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	
	public String getNumMovement() {
		return numMovement;
	}
	public void setNumMovement(String numMovement) {
		this.numMovement = numMovement;
	}
	public String getTypeMvt() {
		return typeMvt;
	}
	public void setTypeMvt(String typeMvt) {
		this.typeMvt = typeMvt;
	}
	public String getDescriminator() {
		return descriminator;
	}
	public void setDescriminator(String descriminator) {
		this.descriminator = descriminator;
	}
	public List<StockMovementLineDto> getStockMovementLineDtos() {
		return stockMovementLineDtos;
	}
	public void setStockMovementLineDtos(List<StockMovementLineDto> stockMovementLineDtos) {
		this.stockMovementLineDtos = stockMovementLineDtos;
	}
	public Long getReferenceTypeId() {
		return referenceTypeId;
	}
	public void setReferenceTypeId(Long referenceTypeId) {
		this.referenceTypeId = referenceTypeId;
	}
	public Long getDistributionId() {
		return distributionId;
	}
	public void setDistributionId(Long distributionId) {
		this.distributionId = distributionId;
	}
	public Long getInternalDeliveryId() {
		return internalDeliveryId;
	}
	public void setInternalDeliveryId(Long internalDeliveryId) {
		this.internalDeliveryId = internalDeliveryId;
	}
	public Long getReceptionId() {
		return receptionId;
	}
	public void setReceptionId(Long receptionId) {
		this.receptionId = receptionId;
	}

	
	
	
	
	
}
