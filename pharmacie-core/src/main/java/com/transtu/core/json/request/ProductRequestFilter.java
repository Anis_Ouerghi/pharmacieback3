package com.transtu.core.json.request;

public class ProductRequestFilter {
	
	private Long page ;
	private Long linesNbr ; 
	private Long familyId ;
	private Long pharmaClassId;
	private Long presentationId;
	private Long dciId;
	private Long formId;
	private Long dossageId;
	private Long typeId;
	

	private Long depotId;
	private Boolean actived;
	private Boolean state;
	private Long productId;
	private Long distributionUnitId;
	private String barCode;
	private String codePctProd;
	private String lib;
	

	
	
	
	public Boolean getActived() {
		return actived;
	}
	public void setActived(Boolean actived) {
		this.actived = actived;
	}
	public ProductRequestFilter() {
		super();
	}
	public Long getFamilyId() {
		return familyId;
	}
	public void setFamilyId(Long familyId) {
		this.familyId = familyId;
	}
	public Long getPharmaClassId() {
		return pharmaClassId;
	}
	public void setPharmaClassId(Long pharmaClassId) {
		this.pharmaClassId = pharmaClassId;
	}
	public Long getPresentationId() {
		return presentationId;
	}
	public void setPresentationId(Long presentationId) {
		this.presentationId = presentationId;
	}
	public Long getDciId() {
		return dciId;
	}
	public void setDciId(Long dciId) {
		this.dciId = dciId;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Long getDepotId() {
		return depotId;
	}
	public void setDepotId(Long depotId) {
		this.depotId = depotId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getCodePctProd() {
		return codePctProd;
	}
	public void setCodePctProd(String codePctProd) {
		this.codePctProd = codePctProd;
	}
	public String getLib() {
		return lib;
	}
	public void setLib(String lib) {
		this.lib = lib;
	}
	public Long getDistributionUnitId() {
		return distributionUnitId;
	}
	public void setDistributionUnitId(Long distributionUnitId) {
		this.distributionUnitId = distributionUnitId;
	}
	public Long getDossageId() {
		return dossageId;
	}
	public void setDossageId(Long dossageId) {
		this.dossageId = dossageId;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getLinesNbr() {
		return linesNbr;
	}
	public void setLinesNbr(Long linesNbr) {
		this.linesNbr = linesNbr;
	}
	public Boolean getState() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
	
	
	
	
	

 
}
