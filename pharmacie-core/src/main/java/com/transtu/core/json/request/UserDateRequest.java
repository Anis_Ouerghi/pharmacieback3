package com.transtu.core.json.request;

import java.util.Date;

public class UserDateRequest {
	
	private Date date ; 
	private Long userId;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public UserDateRequest() {
		super();
	}
	
	

}
