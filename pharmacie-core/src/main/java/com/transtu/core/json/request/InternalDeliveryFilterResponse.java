package com.transtu.core.json.request;

import java.util.ArrayList;
import java.util.List;

public class InternalDeliveryFilterResponse {
	
	private String deliveryNumber;
	private int productNumber;
	private String orderNumber;
	private List<Long> ProductsId= new ArrayList<>();
	public String getDeliveryNumber() {
		return deliveryNumber;
	}
	public void setDeliveryNumber(String deliveryNumber) {
		this.deliveryNumber = deliveryNumber;
	}
	public int getProductNumber() {
		return productNumber;
	}
	public void setProductNumber(int productNumber) {
		this.productNumber = productNumber;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public List<Long> getProductsId() {
		return ProductsId;
	}
	public void setProductsId(List<Long> productsId) {
		ProductsId = productsId;
	}
	public InternalDeliveryFilterResponse() {
		super();
	}

	
}
