package com.transtu.core.json.request;

import java.util.Date;
import java.util.List;

public class StockMovementFilter {
	
	private Long page ; 
	//Mvt
	private Date minDate ; 
	private Date maxDate;
	private String numMovement ;
	private String typeMovement ;
	private String descriminator ; 
    private Long movementId;
    private Long userId;
   // private Long locationId;
	//Mvt-Ligne
	private List<Long> productId;
    private Long internalDeliveryId;
    private Long distributionId;
    private Long receptionId;
    private Long referenceTypeId;
    private Long dayId;
    private Long localeId;
    
    private Boolean express;
    
  
	public Long getMovementId() {
		return movementId;
	}
	public void setMovementId(Long movementId) {
		this.movementId = movementId;
	}
    
	public Date getMinDate() {
		return minDate;
	}
	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}
	public Date getMaxDate() {
		return maxDate;
	}
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	public String getNumMovement() {
		return numMovement;
	}
	public void setNumMovement(String numMovement) {
		this.numMovement = numMovement;
	}
	public String getTypeMovement() {
		return typeMovement;
	}
	public void setTypeMovement(String typeMovement) {
		this.typeMovement = typeMovement;
	}
	public String getDescriminator() {
		return descriminator;
	}
	public void setDescriminator(String descriminator) {
		this.descriminator = descriminator;
	}
	public List<Long> getProductId() {
		return productId;
	}
	public void setProductId(List<Long> productId) {
		this.productId = productId;
	}
	public Long getInternalDeliveryId() {
		return internalDeliveryId;
	}
	public void setInternalDeliveryId(Long internalDeliveryId) {
		this.internalDeliveryId = internalDeliveryId;
	}
	public Long getDistributionId() {
		return distributionId;
	}
	public void setDistributionId(Long distributionId) {
		this.distributionId = distributionId;
	}
	public Long getReceptionId() {
		return receptionId;
	}
	public void setReceptionId(Long receptionId) {
		this.receptionId = receptionId;
	}
	
	public Long getReferenceTypeId() {
		return referenceTypeId;
	}
	public void setReferenceTypeId(Long referenceTypeId) {
		this.referenceTypeId = referenceTypeId;
	}
	public Long getDayId() {
		return dayId;
	}
	public void setDayId(Long dayId) {
		this.dayId = dayId;
	}
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
//	public Long getLocationId() {
//		return locationId;
//	}
//	public void setLocationId(Long locationId) {
//		this.locationId = locationId;
//     }
	public Long getLocaleId() {
		return localeId;
	}
	public void setLocaleId(Long localeId) {
		this.localeId = localeId;
	}
	public Boolean getExpress() {
		return express;
	}
	public void setExpress(Boolean express) {
		this.express = express;
	}
	

	
  

}
