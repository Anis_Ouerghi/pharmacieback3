package com.transtu.core.json.request;

import java.util.List;

public class FilterProductAssistOrderResponse {

	private Long page ; 
	private Long nbrPages ;
	private Long totalProducts ; 
	List<ProductResp> products ;
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getNbrPages() {
		return nbrPages;
	}
	public void setNbrPages(Long nbrPages) {
		this.nbrPages = nbrPages;
	}
	public Long getTotalProducts() {
		return totalProducts;
	}
	public void setTotalProducts(Long totalProducts) {
		this.totalProducts = totalProducts;
	}
	public List<ProductResp> getProducts() {
		return products;
	}
	public void setProducts(List<ProductResp> products) {
		this.products = products;
	}
	

	
	
	
}
