package com.transtu.core.product;

import java.util.List;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.transtu.persistence.entities.Presentation;
import com.transtu.persistence.entities.Stock;


	public class LightProduct {

		public LightProduct() {
			super();
			// TODO Auto-generated constructor stub
		}

	
		private Long id;
		private String barCode;
		private String codePctProd;
		private String lib;
		
		private double vatRate;
		private double  price;
		
		private boolean actived = false;
		
		
		public LightProduct(
				Long id,
				String barCode,
				String codePctProd,
				String lib,
				Boolean actived,
				Presentation presentation,
				List<Stock> stocks
				) {
	
		this.id = id;
		this.barCode = barCode;
		this.codePctProd = codePctProd;
		this.lib = lib;
		this.actived = actived;
		this.setPresentation(presentation);
		this.setStocks(stocks);
	}
		
//		private double vatRate;
//		private double wauCost;
//		private double safetyStock;
//		private double minStock;
//		private double warningStock;
//		private double  price;
//		private double  colisage;
		
//		@ManyToOne
//		@JoinColumn(name = "dossage_id")
//		@Value("${maxuploadfilesize:'100'}")
//		private Dosage dossage;
//		
//		@ManyToOne
//		@JoinColumn(name = "type_id")
//		
//		private Type type;
//		
//		@ManyToOne
//		@JoinColumn(name = "dci_id")
//		
//		private Dci dci;
		
//		@ManyToOne
//		@JoinColumn(name = "distribution_unit_id")
//		
//		private DistributionUnit distributionUnit;
//		
//		@ManyToOne
//		@JoinColumn(name = "form_id")
//		
//		private Form form;
//		
//		@ManyToOne
//		@JoinColumn(name = "pharma_class_id")
//		
//		private PharmaClass pharmaClass;
		
//		@ManyToOne
//		@JoinColumn(name = "depot_id")
//		
//		private Depot depot;
//		
		@ManyToOne
		@JoinColumn(name = "presentation_id")

		private Presentation presentation;
		
		public Presentation getPresentation() {
			return presentation;
		}

		public void setPresentation(Presentation presentation) {
			this.presentation = presentation;
		}

		
		@OneToMany(mappedBy="product")
		//@JsonManagedReference
		//@JsonBackReference
		@JsonIgnore
		private List<Stock> Stocks;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getBarCode() {
			return barCode;
		}

		public void setBarCode(String barCode) {
			this.barCode = barCode;
		}

		public String getCodePctProd() {
			return codePctProd;
		}

		public void setCodePctProd(String codePctProd) {
			this.codePctProd = codePctProd;
		}

		public String getLib() {
			return lib;
		}

		public void setLib(String lib) {
			this.lib = lib;
		}



		public List<Stock> getStocks() {
			return Stocks;
		}

		public void setStocks(List<Stock> stocks) {
			Stocks = stocks;
		}




		public boolean isActived() {
			return actived;
		}

		public void setActived(boolean actived) {
			this.actived = actived;
		}

		public double getVatRate() {
			return vatRate;
		}

		public void setVatRate(double vatRate) {
			this.vatRate = vatRate;
		}

		public double getPrice() {
			return price;
		}

		public void setPrice(double price) {
			this.price = price;
		}

		public LightProduct(Long id, String barCode, String codePctProd, String lib, double vatRate, double price,
				boolean actived, Presentation presentation, List<Stock> stocks) {
			super();
			this.id = id;
			this.barCode = barCode;
			this.codePctProd = codePctProd;
			this.lib = lib;
			this.vatRate = vatRate;
			this.price = price;
			this.actived = actived;
			this.presentation = presentation;
			Stocks = stocks;
		}






		

		
	}

