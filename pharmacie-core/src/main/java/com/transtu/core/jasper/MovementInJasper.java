package com.transtu.core.jasper;

import java.util.Date;

public class MovementInJasper {
	private Long year ; 
	private String number ; 
	private String referenceBl ;
	private Date receptionDate ; 
	private Double qtReceived ;
	public Long getYear() {
		return year;
	}
	public void setYear(Long year) {
		this.year = year;
	}

	
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getReferenceBl() {
		return referenceBl;
	}
	public void setReferenceBl(String referenceBl) {
		this.referenceBl = referenceBl;
	}
	public Date getReceptionDate() {
		return receptionDate;
	}
	public void setReceptionDate(Date receptionDate) {
		this.receptionDate = receptionDate;
	}

	
	
	public Double getQtReceived() {
		return qtReceived;
	}
	public void setQtReceived(Double qtReceived) {
		this.qtReceived = qtReceived;
	}
	public MovementInJasper() {
		super();
	} 
	
	
	
}
