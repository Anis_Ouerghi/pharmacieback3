package com.transtu.core.jasper;

import java.util.Date;

public class StockLotJasper {
	
	
	private double qtStockDep ; 
	private String location;
	//****
	private double qtStock ; 
	private String codePct ;  
	private String designation ; 
	private String Lot; 
	private Date dateInv ;
	private Date datemin ;
	private Date datemax;
	public double getQtStockDep() {
		return qtStockDep;
	}
	public void setQtStockDep(double qtStockDep) {
		this.qtStockDep = qtStockDep;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public double getQtStock() {
		return qtStock;
	}
	public void setQtStock(double qtStock) {
		this.qtStock = qtStock;
	}
	public String getCodePct() {
		return codePct;
	}
	public void setCodePct(String codePct) {
		this.codePct = codePct;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getLot() {
		return Lot;
	}
	public void setLot(String lot) {
		Lot = lot;
	}
	public Date getDateInv() {
		return dateInv;
	}
	public void setDateInv(Date dateInv) {
		this.dateInv = dateInv;
	}
	public Date getDatemin() {
		return datemin;
	}
	public void setDatemin(Date datemin) {
		this.datemin = datemin;
	}
	public Date getDatemax() {
		return datemax;
	}
	public void setDatemax(Date datemax) {
		this.datemax = datemax;
	}
	
	

}
