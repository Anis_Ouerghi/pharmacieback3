package com.transtu.core.jasper;

public class MissingProductJasper {
	private String barCode ;
	private String codePct ;
	private String designation ; 
	private Double qt =(double) 0; 
	private Double price ; 
	private Double vatRate ; 
	private Double total ;
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getCodePct() {
		return codePct;
	}
	public void setCodePct(String codePct) {
		this.codePct = codePct;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Double getQt() {
		return qt;
	}
	public void setQt(Double qt) {
		this.qt = qt;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getVatRate() {
		return vatRate;
	}
	public void setVatRate(Double vatRate) {
		this.vatRate = vatRate;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}

	
	
	

	
	
}
