package com.transtu.core.jasper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DistributionJasper {

	
	private String distNum ;
	
	private Date date ; 
	private double montant; 
	private Integer numberDist;
	
	
	public Integer getNumberDist() {
		return numberDist;
	}
	public void setNumberDist(Integer numberDist) {
		this.numberDist = numberDist;
	}
	private AgentJasper agent ; 
	private List<DistributionLineJasper> distributionLines = new ArrayList<DistributionLineJasper>() ;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public AgentJasper getAgent() {
		return agent;
	}
	public void setAgent(AgentJasper agent) {
		this.agent = agent;
	}
	public List<DistributionLineJasper> getDistributionLines() {
		return distributionLines;
	}
	public void setDistributionLines(List<DistributionLineJasper> distributionLines) {
		this.distributionLines = distributionLines;
	}
	public DistributionJasper() {
		super();
	}
	public String getDistNum() {
		return distNum;
	}
	public void setDistNum(String distNum) {
		this.distNum = distNum;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}


	
	
	
	
}
