package com.transtu.core.jasper;

import java.util.Date;

public class StockJasperEcart {

	
	
	private Date dateInv ;
	private String codePct ;
	private String locale ;
	private String designation ;
	private String designationlot ;
	private double qtStockphyDep ; 
	private double qtStockphypharma ; 
	private double qtStockphy ;

	private double qtStockDepThe ; 
	private double qtStockpharmaThe; 
	private double qtStockThe; 
	private double ecartStockpha; 
	private double ecartStockdepo; 
	private double ecartStock;
	private double barCode;
	private double qtStock;
	private double price;
	private double qtStockpharma;
	
	
	public Date getDateInv() {
		return dateInv;
	}
	public void setDateInv(Date dateInv) {
		this.dateInv = dateInv;
	}
	public String getCodePct() {
		return codePct;
	}
	public void setCodePct(String codePct) {
		this.codePct = codePct;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public double getQtStockphyDep() {
		return qtStockphyDep;
	}
	public void setQtStockphyDep(double qtStockphyDep) {
		this.qtStockphyDep = qtStockphyDep;
	}
	public double getQtStockphypharma() {
		return qtStockphypharma;
	}
	public void setQtStockphypharma(double qtStockphypharma) {
		this.qtStockphypharma = qtStockphypharma;
	}
	public double getQtStockphy() {
		return qtStockphy;
	}
	public void setQtStockphy(double qtStockphy) {
		this.qtStockphy = qtStockphy;
	}
	public double getQtStockDepThe() {
		return qtStockDepThe;
	}
	public void setQtStockDepThe(double qtStockDepThe) {
		this.qtStockDepThe = qtStockDepThe;
	}
	public double getQtStockpharmaThe() {
		return qtStockpharmaThe;
	}
	public void setQtStockpharmaThe(double qtStockpharmaThe) {
		this.qtStockpharmaThe = qtStockpharmaThe;
	}
	public double getQtStockThe() {
		return qtStockThe;
	}
	public void setQtStockThe(double qtStockThe) {
		this.qtStockThe = qtStockThe;
	}
	public double getEcartStockpha() {
		return ecartStockpha;
	}
	public void setEcartStockpha(double ecartStockpha) {
		this.ecartStockpha = ecartStockpha;
	}
	public double getEcartStockdepo() {
		return ecartStockdepo;
	}
	public void setEcartStockdepo(double ecartStockdepo) {
		this.ecartStockdepo = ecartStockdepo;
	}
	public double getEcartStock() {
		return ecartStock;
	}
	public void setEcartStock(double ecartStock) {
		this.ecartStock = ecartStock;
	}
	public double getBarCode() {
		return barCode;
	}
	public void setBarCode(double barCode) {
		this.barCode = barCode;
	}
	public double getQtStock() {
		return qtStock;
	}
	public void setQtStock(double qtStock) {
		this.qtStock = qtStock;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getQtStockpharma() {
		return qtStockpharma;
	}
	public void setQtStockpharma(double qtStockpharma) {
		this.qtStockpharma = qtStockpharma;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getDesignationlot() {
		return designationlot;
	}
	public void setDesignationlot(String designationlot) {
		this.designationlot = designationlot;
	} 
	
	
	
	
	
	
	
}
