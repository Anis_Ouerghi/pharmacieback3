package com.transtu.core.jasper;

import java.util.Date;

public class DeliveryJasper {
	

	private Date dateDelivery;
	private Date dateStart;
	private Date dateEnd;
	private String numDelivery ;
	private String productCode ;
	private String designation  ;
	private double qte ; 
	private double missingqte;
	private String customerName;
	
	
	public Date getDateDelivery() {
		return dateDelivery;
	}
	public void setDateDelivery(Date dateDelivery) {
		this.dateDelivery = dateDelivery;
	}
	public String getNumDelivery() {
		return numDelivery;
	}
	public void setNumDelivery(String numDelivery) {
		this.numDelivery = numDelivery;
	}

	
	
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public double getQte() {
		return qte;
	}
	public void setQte(double qte) {
		this.qte = qte;
	}
	public double getMissingqte() {
		return missingqte;
	}
	public void setMissingqte(double missingqte) {
		this.missingqte = missingqte;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public DeliveryJasper() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public Date getDateStart() {
		return dateStart;
	}
	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
	public Date getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	

}
