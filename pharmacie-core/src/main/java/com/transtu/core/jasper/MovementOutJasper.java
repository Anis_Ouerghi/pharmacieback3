package com.transtu.core.jasper;

import java.util.Date;

public class MovementOutJasper {
	
	private Date date ; 
	private String agent ; 
	private Double deliveredQt ;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public Double getDeliveredQt() {
		return deliveredQt;
	}
	public void setDeliveredQt(Double deliveredQt) {
		this.deliveredQt = deliveredQt;
	}
	public MovementOutJasper() {
		super();
	} 
	
	
}
