package com.transtu.core.jasper;

import java.util.ArrayList;
import java.util.List;

public class MovementJasper {

	private String barCode ; 
	private String codePct ; 
	private String designation ; 
	private Double initialStock = 0.00 ; 
	private List<MovementInJasper> movementsIn = new ArrayList<>() ; 
	private List<MovementOutJasper> movementsOut = new ArrayList<>();
	private Double totalOut = 0.00;
	private Double totalIn = 0.00 ;
	
	public Double getTotalOut() {
		return totalOut;
	}
	public void setTotalOut(Double totalOut) {
		this.totalOut = totalOut;
	}
	public Double getTotalIn() {
		return totalIn;
	}
	public void setTotalIn(Double totalIn) {
		this.totalIn = totalIn;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getCodePct() {
		return codePct;
	}
	public void setCodePct(String codePct) {
		this.codePct = codePct;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Double getInitialStock() {
		return initialStock;
	}
	public void setInitialStock(Double initialStock) {
		this.initialStock = initialStock;
	}
	public List<MovementInJasper> getMovementsIn() {
		return movementsIn;
	}
	public void setMovementsIn(List<MovementInJasper> movementsIn) {
		this.movementsIn = movementsIn;
	}
	public List<MovementOutJasper> getMovementsOut() {
		return movementsOut;
	}
	public void setMovementsOut(List<MovementOutJasper> movementsOut) {
		this.movementsOut = movementsOut;
	}
	public MovementJasper() {
		super();
	} 
	
	
		
	
}
