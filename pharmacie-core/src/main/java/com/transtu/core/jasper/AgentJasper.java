package com.transtu.core.jasper;

public class AgentJasper {
	
	private String name ; 
	private Integer agentId ;
	
	public AgentJasper() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAgentId() {
		return agentId;
	}
	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	
	
	
}
