package com.transtu.core.jasper;

import java.util.Date;


public class ReceptionResponseJasper {
	
	
	private Date dateCde;
	private String numCde;
	
	
	private int num; 
	private String codePctProd;
	private String lib;
	private double receptionQte;
	private double price = 0;
	private double vat;
	private String lotNumber;
	private Date expirationDate;
	
	
	
	public Date getDateCde() {
		return dateCde;
	}
	public void setDateCde(Date dateCde) {
		this.dateCde = dateCde;
	}
	public String getNumCde() {
		return numCde;
	}
	public void setNumCde(String numCde) {
		this.numCde = numCde;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getCodePctProd() {
		return codePctProd;
	}
	public void setCodePctProd(String codePctProd) {
		this.codePctProd = codePctProd;
	}
	public String getLib() {
		return lib;
	}
	public void setLib(String lib) {
		this.lib = lib;
	}
	public double getReceptionQte() {
		return receptionQte;
	}
	public void setReceptionQte(double receptionQte) {
		this.receptionQte = receptionQte;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getVat() {
		return vat;
	}
	public void setVat(double vat) {
		this.vat = vat;
	}
	public String getLotNumber() {
		return lotNumber;
	}
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	

	
	
	
	

}
