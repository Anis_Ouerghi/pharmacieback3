package com.transtu.core.jasper;

import java.util.Date;

public class StockJasper {
	
	
	
	private double qtStockDep ; 
	private double qtStockpharma ; 
	private double vatRate;
	private double totalTtc ; 
	private double pricePh;
	private double priceDep;
	private String location;
	//****
	
	private double qtStock ; 
	private String codePct ; 
	private String barCode ; 
	private String designation ; 
	private String designationlot;
	private Double price ;
	private double total ; 
	private Date dateInv ;
	public double getQtStockDep() {
		return qtStockDep;
	}
	public void setQtStockDep(double qtStockDep) {
		this.qtStockDep = qtStockDep;
	}
	public double getQtStockpharma() {
		return qtStockpharma;
	}
	public void setQtStockpharma(double qtStockpharma) {
		this.qtStockpharma = qtStockpharma;
	}
	public double getVatRate() {
		return vatRate;
	}
	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}
	public double getTotalTtc() {
		return totalTtc;
	}
	public void setTotalTtc(double totalTtc) {
		this.totalTtc = totalTtc;
	}
	public double getQtStock() {
		return qtStock;
	}
	public void setQtStock(double qtStock) {
		this.qtStock = qtStock;
	}
	public String getCodePct() {
		return codePct;
	}
	public void setCodePct(String codePct) {
		this.codePct = codePct;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}

	public double getPricePh() {
		return pricePh;
	}
	public void setPricePh(double pricePh) {
		this.pricePh = pricePh;
	}
	public double getPriceDep() {
		return priceDep;
	}
	public void setPriceDep(double priceDep) {
		this.priceDep = priceDep;
	}
	public Date getDateInv() {
		return dateInv;
	}
	public void setDateInv(Date dateInv) {
		this.dateInv = dateInv;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDesignationlot() {
		return designationlot;
	}
	public void setDesignationlot(String designationlot) {
		this.designationlot = designationlot;
	}
	


}
