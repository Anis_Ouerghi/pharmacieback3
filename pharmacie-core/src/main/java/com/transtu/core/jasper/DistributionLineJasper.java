package com.transtu.core.jasper;

import java.util.Date;

public class DistributionLineJasper {

	

	private String barCode ; 
	private String designation ; 
	private Double wantedQt ; 
	private Double livredQt ; 
	private Double missingQt ;
	private Double distributed ;
	private Date date ; 
	private Double price;
	private Double pricettc;
	private Double vatRate;
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Double getWantedQt() {
		return wantedQt;
	}
	public void setWantedQt(Double wantedQt) {
		this.wantedQt = wantedQt;
	}
	public Double getMissingQt() {
		return missingQt;
	}
	public void setMissingQt(Double missingQt) {
		this.missingQt = missingQt;
	}
	public Double getDistributed() {
		return distributed;
	}
	public void setDistributed(Double distributed) {
		this.distributed = distributed;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getVatRate() {
		return vatRate;
	}
	public void setVatRate(Double vatRate) {
		this.vatRate = vatRate;
	}
	public Double getLivredQt() {
		return livredQt;
	}
	public void setLivredQt(Double livredQt) {
		this.livredQt = livredQt;
	}
	public Double getPricettc() {
		return pricettc;
	}
	public void setPricettc(Double pricettc) {
		this.pricettc = pricettc;
	}
	
	
	
}
