package com.transtu.core.jasper;

import java.util.Date;

public class InventoryProductJasper {
	
	
	
	
	private String codePct ; 
	private String barCode ; 
	private String designationlot;
	private Integer numOrd ; 
	private String designation ;  
	private Date inventorydate ;
	private double  price;
	private String locationLbel ;
	public String getCodePct() {
		return codePct;
	}
	public void setCodePct(String codePct) {
		this.codePct = codePct;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public Integer getNumOrd() {
		return numOrd;
	}
	public void setNumOrd(Integer numOrd) {
		this.numOrd = numOrd;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Date getInventorydate() {
		return inventorydate;
	}
	public void setInventorydate(Date inventorydate) {
		this.inventorydate = inventorydate;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getLocationLbel() {
		return locationLbel;
	}
	public void setLocationLbel(String locationLbel) {
		this.locationLbel = locationLbel;
	}
	public InventoryProductJasper() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getDesignationlot() {
		return designationlot;
	}
	public void setDesignationlot(String designationlot) {
		this.designationlot = designationlot;
	}
	



}
