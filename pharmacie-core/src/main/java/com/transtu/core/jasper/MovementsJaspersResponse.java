package com.transtu.core.jasper;

import java.util.Date;

public class MovementsJaspersResponse {
	 
	private String codePct ; 
	private String designation ; 
	private String user ; 
	private String refference ; 
	private String presentation ;
	private Double initialStock = 100000d ; 
	private Double actuallStock = 100000d ;
	private Date DateMvt ;
	private Double outQte ; 
	private Double enterQte ;
	private Double stkth;
	private Double totaloutQte  ; 
	private Double totalentertQte;
	public String getCodePct() {
		return codePct;
	}
	public void setCodePct(String codePct) {
		this.codePct = codePct;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Double getInitialStock() {
		return initialStock;
	}
	public void setInitialStock(Double initialStock) {
		this.initialStock = initialStock;
	}
	public Double getActuallStock() {
		return actuallStock;
	}
	public void setActuallStock(Double actuallStock) {
		this.actuallStock = actuallStock;
	}
	public Date getDateMvt() {
		return DateMvt;
	}
	public void setDateMvt(Date dateMvt) {
		DateMvt = dateMvt;
	}
	public Double getOutQte() {
		return outQte;
	}
	public void setOutQte(Double outQte) {
		this.outQte = outQte;
	}
	public Double getEnterQte() {
		return enterQte;
	}
	public void setEnterQte(Double enterQte) {
		this.enterQte = enterQte;
	}
	public Double getTotaloutQte() {
		return totaloutQte;
	}
	public void setTotaloutQte(Double totaloutQte) {
		this.totaloutQte = totaloutQte;
	}
	public Double getTotalentertQte() {
		return totalentertQte;
	}
	public void setTotalentertQte(Double totalentertQte) {
		this.totalentertQte = totalentertQte;
	}
	public String getPresentation() {
		return presentation;
	}
	public void setPresentation(String presentation) {
		this.presentation = presentation;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Double getStkth() {
		return stkth;
	}
	public void setStkth(Double stkth) {
		this.stkth = stkth;
	}
	public String getRefference() {
		return refference;
	}
	public void setRefference(String refference) {
		this.refference = refference;
	}  
 
	

}
