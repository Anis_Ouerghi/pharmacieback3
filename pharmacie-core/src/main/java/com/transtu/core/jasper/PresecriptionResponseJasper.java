package com.transtu.core.jasper;

import java.util.Date;

public class PresecriptionResponseJasper {

    private String distNum ;
	private Date datestart ; 
	private Date dateend ; 
	private Date dateord ;
	private double montantOrd; 
	private double totalmontant; 
	private Integer totalnbreOrd;
	private String matricule ;
	private String maladie ;
	
	
	
	public PresecriptionResponseJasper() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getDistNum() {
		return distNum;
	}
	public void setDistNum(String distNum) {
		this.distNum = distNum;
	}
	public Date getDatestart() {
		return datestart;
	}
	public void setDatestart(Date datestart) {
		this.datestart = datestart;
	}
	public Date getDateend() {
		return dateend;
	}
	public void setDateend(Date dateend) {
		this.dateend = dateend;
	}
	public Date getDateord() {
		return dateord;
	}
	public void setDateord(Date dateord) {
		this.dateord = dateord;
	}
	public double getMontantOrd() {
		return montantOrd;
	}
	public void setMontantOrd(double montantOrd) {
		this.montantOrd = montantOrd;
	}
	
	public Integer getTotalnbreOrd() {
		return totalnbreOrd;
	}
	public void setTotalnbreOrd(Integer totalnbreOrd) {
		this.totalnbreOrd = totalnbreOrd;
	}
	public String getMatricule() {
		return matricule;
	}
	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}
	public double getTotalmontant() {
		return totalmontant;
	}
	public void setTotalmontant(double totalmontant) {
		this.totalmontant = totalmontant;
	}
	public String getMaladie() {
		return maladie;
	}
	public void setMaladie(String maladie) {
		this.maladie = maladie;
	} 
	
	
	
	
	

}
