package com.transtu.core.jasper;

import java.util.Date;

public class UserDistStatestiqueJasper {
	
	public String name;
	public Double nbreDist;
	public Double totalnbreDist;
	public Date start;
	public Date end;
	public Double totalDist;
	public Double totalMissing;
	public Double total;
	public Double nbredelivred;
	public Double nbreMissing;
	public Double percentnbreMissing;
	public Double percentMontMissing;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getNbreDist() {
		return nbreDist;
	}
	public void setNbreDist(Double nbreDist) {
		this.nbreDist = nbreDist;
	}
	public Double getTotalnbreDist() {
		return totalnbreDist;
	}
	public void setTotalnbreDist(Double totalnbreDist) {
		this.totalnbreDist = totalnbreDist;
	}
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public Double getTotalDist() {
		return totalDist;
	}
	public void setTotalDist(Double totalDist) {
		this.totalDist = totalDist;
	}
	public Double getTotalMissing() {
		return totalMissing;
	}
	public void setTotalMissing(Double totalMissing) {
		this.totalMissing = totalMissing;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getNbredelivred() {
		return nbredelivred;
	}
	public void setNbredelivred(Double nbredelivred) {
		this.nbredelivred = nbredelivred;
	}
	public Double getNbreMissing() {
		return nbreMissing;
	}
	public void setNbreMissing(Double nbreMissing) {
		this.nbreMissing = nbreMissing;
	}
	public Double getPercentnbreMissing() {
		return percentnbreMissing;
	}
	public void setPercentnbreMissing(Double percentnbreMissing) {
		this.percentnbreMissing = percentnbreMissing;
	}
	public Double getPercentMontMissing() {
		return percentMontMissing;
	}
	public void setPercentMontMissing(Double percentMontMissing) {
		this.percentMontMissing = percentMontMissing;
	}
	
	
	

}
