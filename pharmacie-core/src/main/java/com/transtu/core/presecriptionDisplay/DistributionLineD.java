package com.transtu.core.presecriptionDisplay;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;




public class DistributionLineD implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id; 
	public Date getNextDistributionDate() {
		return nextDistributionDate;
	}

	public void setNextDistributionDate(Date nextDistributionDate) {
		this.nextDistributionDate = nextDistributionDate;
	}

	private Date nextDistributionDate;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "product_id")
	private ProductD product;
	
	@ManyToOne
	@JoinColumn(name = "distribution_id")
	@JsonBackReference
	private DistributionD distribution;
	private double deliveredQt = 0;
	private double missingQt = 0;
    private  double toDistribute;
    private LocationD location;
	 
	 
	public LocationD getLocation() {
		return location;
	}

	public void setLocation(LocationD location) {
		this.location = location;
	}

	public DistributionLineD() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductD getProduct() {
		return product;
	}

	public void setProduct(ProductD product) {
		this.product = product;
	}

	public DistributionD getDistribution() {
		return distribution;
	}

	public void setDistribution(DistributionD distribution) {
		this.distribution = distribution;
	}

	public double getDeliveredQt() {
		return deliveredQt;
	}

	public void setDeliveredQt(double deliveredQt) {
		this.deliveredQt = deliveredQt;
	}

	public double getMissingQt() {
		return missingQt;
	}

	public void setMissingQt(double missingQt) {
		this.missingQt = missingQt;
	}

	public double getToDistribute() {
		return toDistribute;
	}

	public void setToDistribute(double toDistribute) {
		this.toDistribute = toDistribute;
	}

	public DistributionLineD(Long id, Date nextDistributionDate, ProductD product, DistributionD distribution,
			double deliveredQt, double missingQt, double toDistribute) {
		super();
		this.id = id;
		this.nextDistributionDate = nextDistributionDate;
		this.product = product;
		this.distribution = distribution;
		this.deliveredQt = deliveredQt;
		this.missingQt = missingQt;
		this.toDistribute = toDistribute;
	}

	public DistributionLineD(Long id, ProductD product, DistributionD distribution, double deliveredQt, double missingQt,
			double toDistribute) {
		super();
		this.id = id;
		this.product = product;
		this.distribution = distribution;
		this.deliveredQt = deliveredQt;
		this.missingQt = missingQt;
		this.toDistribute = toDistribute;
	}

	
}