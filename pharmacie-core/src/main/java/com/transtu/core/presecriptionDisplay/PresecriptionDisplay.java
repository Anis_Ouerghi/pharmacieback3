//package com.transtu.core.presecriptionDisplay;
//
//import java.util.Date;
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.FetchType;
//import javax.persistence.OneToMany;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonManagedReference;
//
//
//public class PresecriptionDisplay {
//
//	
//	private Long id;
//	//new
//	private String prescriptionNumber;
//	private AgentDisplay agentdDisplay;
//	private String type ;
//	//new
//	private Date prescriptionDate;
//	
//	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @Column(nullable = true)
//    @JsonManagedReference
//	private List<PrescriptionLineDisplay> prescriptionLineDisplays;
//	
//	
//	private List<DistributionDisplay> distributiondisplay;
//
//
//	public Long getId() {
//		return id;
//	}
//
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//
//	public String getPrescriptionNumber() {
//		return prescriptionNumber;
//	}
//
//
//	public void setPrescriptionNumber(String prescriptionNumber) {
//		this.prescriptionNumber = prescriptionNumber;
//	}
//
//
//	public AgentDisplay getAgentdDisplay() {
//		return agentdDisplay;
//	}
//
//
//	public void setAgentdDisplay(AgentDisplay agentdDisplay) {
//		this.agentdDisplay = agentdDisplay;
//	}
//
//
//	public String getType() {
//		return type;
//	}
//
//
//	public void setType(String type) {
//		this.type = type;
//	}
//
//
//	public Date getPrescriptionDate() {
//		return prescriptionDate;
//	}
//
//
//	public void setPrescriptionDate(Date prescriptionDate) {
//		this.prescriptionDate = prescriptionDate;
//	}
//
//
//	public List<PrescriptionLineDisplay> getPrescriptionLineDisplays() {
//		return prescriptionLineDisplays;
//	}
//
//
//	public void setPrescriptionLineDisplays(List<PrescriptionLineDisplay> prescriptionLineDisplays) {
//		this.prescriptionLineDisplays = prescriptionLineDisplays;
//	}
//
//
//	public List<DistributionDisplay> getDistributiondisplay() {
//		return distributiondisplay;
//	}
//
//
//	public PresecriptionDisplay(Long id, String prescriptionNumber, AgentDisplay agentdDisplay, String type,
//			Date prescriptionDate, List<PrescriptionLineDisplay> prescriptionLineDisplays,
//			List<DistributionDisplay> distributiondisplay) {
//		super();
//		this.id = id;
//		this.prescriptionNumber = prescriptionNumber;
//		this.agentdDisplay = agentdDisplay;
//		this.type = type;
//		this.prescriptionDate = prescriptionDate;
//		this.prescriptionLineDisplays = prescriptionLineDisplays;
//		this.distributiondisplay = distributiondisplay;
//	}
//
//
//	public PresecriptionDisplay() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//
//
//	public void setDistributiondisplay(List<DistributionDisplay> distributiondisplay) {
//		this.distributiondisplay = distributiondisplay;
//	}
//	
//	
//}
