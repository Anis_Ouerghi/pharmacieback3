package com.transtu.core.presecriptionDisplay;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.transtu.persistence.entities.Distribution;


public class PrescriptionD implements Serializable {

	private static final long serialVersionUID = 5696028901605256384L;

	
	private Long id;
	private String prescriptionNumber;
	private AgentD agent;
	private String type ;
	private Boolean  distributed ;
	private Date prescriptionDate;
	@OneToMany(mappedBy="prescription")
    @JsonManagedReference
	private List<PrescriptionLineD> prescriptionLine = new ArrayList<>();
	@OneToMany(mappedBy="prescription")
	@JsonManagedReference
	private List<Distribution> distribution = new ArrayList<>();
	
	
	
	
	public PrescriptionD() {
		super();
	}

	



	public AgentD getAgent() {
		return agent;
	}





	public Boolean getDistributed() {
		return distributed;
	}





	public void setDistributed(Boolean distributed) {
		this.distributed = distributed;
	}





	public PrescriptionD(Long id, String prescriptionNumber, AgentD agent, String type, Date prescriptionDate,
			List<PrescriptionLineD> prescriptionLine, List<Distribution> distribution) {
		super();
		this.id = id;
		this.prescriptionNumber = prescriptionNumber;
		this.agent = agent;
		this.type = type;
		this.prescriptionDate = prescriptionDate;
		this.prescriptionLine = prescriptionLine;
		this.distribution = distribution;
	}





	public void setAgent(AgentD agent) {
		this.agent = agent;
	}





	public String getPrescriptionNumber() {
		return prescriptionNumber;
	}

	public void setPrescriptionNumber(String prescriptionNumber) {
		this.prescriptionNumber = prescriptionNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public String getType() {
		return type;
	}

	public void setType(String typeOrd) {
		this.type = typeOrd;
	}

	public List<PrescriptionLineD> getPrescriptionLine() {
		return prescriptionLine;
	}

	public void setPrescriptionLine(List<PrescriptionLineD> prescriptionLine) {
		this.prescriptionLine = prescriptionLine;
	}

	public List<Distribution> getDistribution() {
		return distribution;
	}

	public void setDistribution(List<Distribution> distribution) {
		this.distribution = distribution;
	}

	public Date getPrescriptionDate() {
		return prescriptionDate;
	}

	public void setPrescriptionDate(Date prescriptionDate) {
		this.prescriptionDate = prescriptionDate;
	}
	


}