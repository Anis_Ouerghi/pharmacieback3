package com.transtu.core.presecriptionDisplay;

public class ProductDisplay {
	
	private Long id;
	private String barCode;
	private String codePctProd;
	private String lib;
	private double vatRate;
	private double wauCost;
	private double safetyStock;
	private double minStock;
	private double warningStock;
	private String dci;
	private String distributionType;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getCodePctProd() {
		return codePctProd;
	}
	public void setCodePctProd(String codePctProd) {
		this.codePctProd = codePctProd;
	}
	public String getLib() {
		return lib;
	}
	public void setLib(String lib) {
		this.lib = lib;
	}
	public double getVatRate() {
		return vatRate;
	}
	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}
	public double getWauCost() {
		return wauCost;
	}
	public void setWauCost(double wauCost) {
		this.wauCost = wauCost;
	}
	public double getSafetyStock() {
		return safetyStock;
	}
	public void setSafetyStock(double safetyStock) {
		this.safetyStock = safetyStock;
	}
	public double getMinStock() {
		return minStock;
	}
	public void setMinStock(double minStock) {
		this.minStock = minStock;
	}
	public double getWarningStock() {
		return warningStock;
	}
	public void setWarningStock(double warningStock) {
		this.warningStock = warningStock;
	}
	public String getDci() {
		return dci;
	}
	public void setDci(String dci) {
		this.dci = dci;
	}
	public String getDistributionType() {
		return distributionType;
	}
	public void setDistributionType(String distributionType) {
		this.distributionType = distributionType;
	}
	public ProductDisplay() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ProductDisplay(Long id, String barCode, String codePctProd, String lib, double vatRate, double wauCost,
			double safetyStock, double minStock, double warningStock, String dci, String distributionType) {
		super();
		this.id = id;
		this.barCode = barCode;
		this.codePctProd = codePctProd;
		this.lib = lib;
		this.vatRate = vatRate;
		this.wauCost = wauCost;
		this.safetyStock = safetyStock;
		this.minStock = minStock;
		this.warningStock = warningStock;
		this.dci = dci;
		this.distributionType = distributionType;
	}
	
	

}
