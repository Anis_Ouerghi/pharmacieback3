package com.transtu.core.presecriptionDisplay;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreType;


public class FamilyD  implements Serializable{

	
	private Long id;
	private String libelle;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public FamilyD() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FamilyD(String libelle) {
		super();
		this.libelle = libelle;
	}



}