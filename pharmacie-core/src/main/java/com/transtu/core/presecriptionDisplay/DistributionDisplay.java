//package com.transtu.core.presecriptionDisplay;
//
//import java.util.Date;
//import java.util.List;
//
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToMany;
//
//import com.fasterxml.jackson.annotation.JsonBackReference;
//import com.fasterxml.jackson.annotation.JsonManagedReference;
//
//
//
//public class DistributionDisplay {
//
//	
//	private Long Id;
//	private String distributionNumber ; 
//	private Date dateDist;
//    private String location;
//	private String prescriptionNumber ;
//	
//	@ManyToOne
//	@JoinColumn(name = "ordonnance_id")
//	@JsonBackReference
//	private PresecriptionDisplay prescriptionDisplay ;
//	
//	@OneToMany(mappedBy="distribution")
//	@JsonManagedReference
//	private List<DistributionLineDisplay> distributionLinesDisplay;
//	
//	public DistributionDisplay(Long id, String distributionNumber, Date dateDist, String location,
//			String prescriptionNumber, PresecriptionDisplay prescriptionDisplay) {
//		super();
//		Id = id;
//		this.distributionNumber = distributionNumber;
//		this.dateDist = dateDist;
//		this.location = location;
//		this.prescriptionNumber = prescriptionNumber;
//		this.prescriptionDisplay = prescriptionDisplay;
//	}
//	public PresecriptionDisplay getPrescriptionDisplay() {
//		return prescriptionDisplay;
//	}
//	public void setPrescriptionDisplay(PresecriptionDisplay prescriptionDisplay) {
//		this.prescriptionDisplay = prescriptionDisplay;
//	}
//	public Long getId() {
//		return Id;
//	}
//	public void setId(Long id) {
//		Id = id;
//	}
//	public String getDistributionNumber() {
//		return distributionNumber;
//	}
//	public void setDistributionNumber(String distributionNumber) {
//		this.distributionNumber = distributionNumber;
//	}
//	public Date getDateDist() {
//		return dateDist;
//	}
//	public void setDateDist(Date dateDist) {
//		this.dateDist = dateDist;
//	}
//	public String getLocation() {
//		return location;
//	}
//	public void setLocation(String location) {
//		this.location = location;
//	}
//	public String getPrescriptionNumber() {
//		return prescriptionNumber;
//	}
//	public void setPrescriptionNumber(String prescriptionNumber) {
//		this.prescriptionNumber = prescriptionNumber;
//	}
//	public DistributionDisplay() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//	public DistributionDisplay(Long id, String distributionNumber, Date dateDist, String location,
//			String prescriptionNumber) {
//		super();
//		Id = id;
//		this.distributionNumber = distributionNumber;
//		this.dateDist = dateDist;
//		this.location = location;
//		this.prescriptionNumber = prescriptionNumber;
//	}
//	public List<DistributionLineDisplay> getDistributionLinesDisplay() {
//		return distributionLinesDisplay;
//	}
//	public void setDistributionLinesDisplay(List<DistributionLineDisplay> distributionLinesDisplay) {
//		this.distributionLinesDisplay = distributionLinesDisplay;
//	}
//	
//	
//}
