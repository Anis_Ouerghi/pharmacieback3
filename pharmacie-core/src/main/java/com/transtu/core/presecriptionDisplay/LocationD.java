package com.transtu.core.presecriptionDisplay;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
//import com.transtu.domain.base.DomainBase;



public class LocationD  implements Serializable {

	public LocationD(Long id, String lib) {
		super();
		this.id = id;
		this.lib = lib;
	}





	private static final long serialVersionUID = 1250461945246642698L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String lib;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLib() {
		return lib;
	}

	public void setLib(String lib) {
		this.lib = lib;
	}





	public LocationD() {
		super();
		// TODO Auto-generated constructor stub
	}



}
