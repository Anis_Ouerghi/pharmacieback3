package com.transtu.core.presecriptionDisplay;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class AgentD implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Integer matricule;
	private String nom;
	private String prenom;
	
	public AgentD() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AgentD( Integer matricule, String nom, String prenom) {
		super();
		
		this.matricule = matricule;
		this.nom = nom;
		this.prenom = prenom;
	}

	

	public Integer getMatricule() {
		return matricule;
	}

	public void setMatricule(Integer matricule) {
		this.matricule = matricule;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	

}
