package com.transtu.core.presecriptionDisplay;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreType;


public class DistrictD  implements Serializable {

	private static final long serialVersionUID = 1L;

	
	private Long id;
	private String lib;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLib() {
		return lib;
	}

	public void setLib(String libelle) {
		this.lib = libelle;
	}

	public DistrictD(Long id, String libelle) {
		super();
		this.id = id;
		this.lib = libelle;
	}

	

	public DistrictD() {
		super();
	}

	public DistrictD(String libelle) {
		super();
		this.lib = libelle;
	}

}