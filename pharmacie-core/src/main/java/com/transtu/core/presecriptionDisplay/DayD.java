package com.transtu.core.presecriptionDisplay;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;


public class DayD implements Serializable {


	
	private Long id ; 
	@NotNull
	private boolean state ; 
	@NotNull
	private Date date ;
	
	@ManyToOne
	@JoinColumn(name = "type_id")
	private DayTypeD dayType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public DayTypeD getDayType() {
		return dayType;
	}

	public void setDayType(DayTypeD dayType) {
		this.dayType = dayType;
	}
	


}
