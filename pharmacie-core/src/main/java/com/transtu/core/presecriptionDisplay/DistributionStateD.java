package com.transtu.core.presecriptionDisplay;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;


public class DistributionStateD {

	
	private static final long serialVersionUID = 1L;

	private Long id;
	
//	@ManyToOne
//	@JsonBackReference
	
	private State state;
	
	@ManyToOne
	@JsonBackReference
	
	private DistributionD distribution;
	
	private Date dateState;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public DistributionStateD(Long id, State state, DistributionD distribution, Date dateState) {
		super();
		this.id = id;
		this.state = state;
		this.distribution = distribution;
		this.dateState = dateState;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public DistributionD getDistribution() {
		return distribution;
	}
	public void setDistribution(DistributionD distribution) {
		this.distribution = distribution;
	}
	public Date getDateState() {
		return dateState;
	}
	public void setDateState(Date dateState) {
		this.dateState = dateState;
	}
	public DistributionStateD() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	
}
