package com.transtu.core.presecriptionDisplay;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


public class DistributionD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3327240259140302531L;
	
	private Long Id;
	private String distributionNumber ; 
	private Date dateDist ;
	
	@ManyToOne
	//@JoinColumn(name = "emplacement_id")
	private LocationD location;
	
	@ManyToOne
	@JoinColumn(name = "day_id")
	private DayD day ; 
	
	@ManyToOne
	@JoinColumn(name = "ordonnance_id")
	@JsonBackReference
	private PrescriptionD prescription ;

	@ManyToOne
	@JoinColumn(name = "district_id")
	private DistrictD district;
	
	@OneToMany(mappedBy="distribution")
	@JsonManagedReference
	private List<DistributionLineD> distributionLines;
	
	@OneToMany(mappedBy="distribution")
	@JsonManagedReference
	private List<DistributionStateD> distributionStates;
	
	public List<DistributionStateD> getDistributionStates() {
		return distributionStates;
	}
	public void setDistributionStates(List<DistributionStateD> distributionStates) {
		this.distributionStates = distributionStates;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	
	public LocationD getLocation() {
		return location;
	}
	public void setLocation(LocationD emplacement) {
		this.location = emplacement;
	}
	public PrescriptionD getPrescription() {
		return prescription;
	}
	public void setPrescription(PrescriptionD ordonnance) {
		this.prescription = ordonnance;
	}
	public DistrictD getDistrict() {
		return district;
	}
	public void setDistrict(DistrictD district) {
		this.district = district;
	}
	public String getDistributionNumber() {
		return distributionNumber;
	}
	public void setDistributionNumber(String distributionNumber) {
		this.distributionNumber = distributionNumber;
	}
	public Date getDateDist() {
		return dateDist;
	}
	public void setDateDist(Date dateDist) {
		this.dateDist = dateDist;
	}
	public List<DistributionLineD> getDistributionLines() {
		return distributionLines;
	}
	public void setDistributionLines(List<DistributionLineD> distributionLines) {
		this.distributionLines = distributionLines;
	}
	public DayD getDay() {
		return day;
	}
	public void setDay(DayD day) {
		this.day = day;
	}
	public DistributionD(Long id, String distributionNumber, Date dateDist, LocationD location, DayD day,
			PrescriptionD prescription, DistrictD district, List<DistributionLineD> distributionLines,
			List<DistributionStateD> distributionStates) {
		super();
		Id = id;
		this.distributionNumber = distributionNumber;
		this.dateDist = dateDist;
		this.location = location;
		this.day = day;
		this.prescription = prescription;
		this.district = district;
		this.distributionLines = distributionLines;
		this.distributionStates = distributionStates;
	}
	public DistributionD() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	

}
