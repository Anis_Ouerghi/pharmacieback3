//package com.transtu.core.presecriptionDisplay;
//
//import javax.persistence.CascadeType;
//import javax.persistence.FetchType;
//import javax.persistence.ManyToOne;
//
//import com.fasterxml.jackson.annotation.JsonBackReference;
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//public class PrescriptionLineDisplay {
//
//	
//	private long id;
//	private double totalQt ;
//	private Boolean isPeriodic;
//	private String periodicity;
//	private Integer days;
//	private Integer distNumber;
//	private ProductDisplay productDisplay ;
//	   @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//	  
//	    @JsonBackReference
//	private PresecriptionDisplay prescriptionDisplay ;
//	public long getId() {
//		return id;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
//	public double getTotalQt() {
//		return totalQt;
//	}
//	public void setTotalQt(double totalQt) {
//		this.totalQt = totalQt;
//	}
//	public Boolean getIsPeriodic() {
//		return isPeriodic;
//	}
//	public void setIsPeriodic(Boolean isPeriodic) {
//		this.isPeriodic = isPeriodic;
//	}
//	public String getPeriodicity() {
//		return periodicity;
//	}
//	public void setPeriodicity(String periodicity) {
//		this.periodicity = periodicity;
//	}
//	public Integer getDays() {
//		return days;
//	}
//	public void setDays(Integer days) {
//		this.days = days;
//	}
//	public Integer getDistNumber() {
//		return distNumber;
//	}
//	public void setDistNumber(Integer distNumber) {
//		this.distNumber = distNumber;
//	}
//	public ProductDisplay getProductDisplay() {
//		return productDisplay;
//	}
//	public void setProductDisplay(ProductDisplay productDisplay) {
//		this.productDisplay = productDisplay;
//	}
//	public PresecriptionDisplay getPrescriptionDisplay() {
//		return prescriptionDisplay;
//	}
//	public void setPrescriptionDisplay(PresecriptionDisplay prescriptionDisplay) {
//		this.prescriptionDisplay = prescriptionDisplay;
//	}
//	public PrescriptionLineDisplay(long id, double totalQt, Boolean isPeriodic, String periodicity, Integer days,
//			Integer distNumber, ProductDisplay productDisplay, PresecriptionDisplay prescriptionDisplay) {
//		super();
//		this.id = id;
//		this.totalQt = totalQt;
//		this.isPeriodic = isPeriodic;
//		this.periodicity = periodicity;
//		this.days = days;
//		this.distNumber = distNumber;
//		this.productDisplay = productDisplay;
//		this.prescriptionDisplay = prescriptionDisplay;
//	}
//	public PrescriptionLineDisplay() {
//		super();
//		// TODO Auto-generated constructor stub
//	}
//	
//	
//}
