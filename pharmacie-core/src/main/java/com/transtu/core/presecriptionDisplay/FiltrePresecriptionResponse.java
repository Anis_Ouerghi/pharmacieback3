package com.transtu.core.presecriptionDisplay;

import java.util.List;

public class FiltrePresecriptionResponse {
	


		private Long page ; 
		private Long totalPages ;
		private Long totalElements;
		List<PrescriptionD> prescriptionDs ;
		public Long getPage() {
			return page;
		}
		public void setPage(Long page) {
			this.page = page;
		}
		public Long getTotalPages() {
			return totalPages;
		}
		public void setTotalPages(Long totalPages) {
			this.totalPages = totalPages;
		}
		
		public List<PrescriptionD> getPrescriptionDs() {
			return prescriptionDs;
		}
		public void setPrescriptionDs(List<PrescriptionD> prescriptionDs) {
			this.prescriptionDs = prescriptionDs;
		}
		public Long getTotalElements() {
			return totalElements;
		}
		public void setTotalElements(Long totalElements) {
			this.totalElements = totalElements;
		}
		
		

}
