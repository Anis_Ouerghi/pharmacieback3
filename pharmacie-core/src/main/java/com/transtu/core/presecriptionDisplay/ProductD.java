
package com.transtu.core.presecriptionDisplay;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.transtu.persistence.entities.Dci;
import com.transtu.persistence.entities.Depot;
import com.transtu.persistence.entities.DistributionUnit;
import com.transtu.persistence.entities.Form;
import com.transtu.persistence.entities.PharmaClass;
import com.transtu.persistence.entities.Presentation;


public class ProductD  implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8852825660241906000L;
	private Long id;
	private String barCode;
	private String codePctProd;
	private String lib;
	private double vatRate;
	private double wauCost;
	private double safetyStock;
	private double minStock;
	private double warningStock;
	private Dci dci;
	private DistributionUnit distributionUnit;
	private Form form ;
	private Presentation presentation ;
	private Depot depot ;
	private PharmaClass pharmaClass ;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "famille_id")
	private FamilyD family;


	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "PRODUITS_GENERIQUE", joinColumns = { @JoinColumn(name = "PRODUIT_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "PRODUITGENERIQUE_ID") })
	@JsonIgnore
	private Set<ProductD> produitsGeneric = new HashSet<ProductD>();

	@ManyToMany(mappedBy = "produitsGeneric")
	@JsonIgnore
	private Set<ProductD> produitParent = new HashSet<ProductD>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getCodePctProd() {
		return codePctProd;
	}

	public void setCodePctProd(String codePctProd) {
		this.codePctProd = codePctProd;
	}

	public String getLib() {
		return lib;
	}

	public void setLib(String lib) {
		this.lib = lib;
	}

	public double getVatRate() {
		return vatRate;
	}

	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}

	public double getWauCost() {
		return wauCost;
	}

	public void setWauCost(double wauCost) {
		this.wauCost = wauCost;
	}

	public double getSafetyStock() {
		return safetyStock;
	}

	public void setSafetyStock(double safetyStock) {
		this.safetyStock = safetyStock;
	}

	public double getMinStock() {
		return minStock;
	}

	public void setMinStock(double minStock) {
		this.minStock = minStock;
	}

	public double getWarningStock() {
		return warningStock;
	}

	public void setWarningStock(double warningStock) {
		this.warningStock = warningStock;
	}

	public FamilyD getFamily() {
		return family;
	}

	public void setFamily(FamilyD family) {
		this.family = family;
	}

	public Set<ProductD> getProduitsGeneric() {
		return produitsGeneric;
	}

	public void setProduitsGeneric(Set<ProductD> produitsGeneric) {
		this.produitsGeneric = produitsGeneric;
	}

	public Set<ProductD> getProduitParent() {
		return produitParent;
	}

	public void setProduitParent(Set<ProductD> produitParent) {
		this.produitParent = produitParent;
	}

	public ProductD() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductD(Long id, String barCode, String codePctProd, String lib, double vatRate, double wauCost,
			double safetyStock, double minStock, double warningStock, 
			FamilyD family, Set<ProductD> produitsGeneric, Set<ProductD> produitParent) {
		super();
		this.id = id;
		this.barCode = barCode;
		this.codePctProd = codePctProd;
		this.lib = lib;
		this.vatRate = vatRate;
		this.wauCost = wauCost;
		this.safetyStock = safetyStock;
		this.minStock = minStock;
		this.warningStock = warningStock;
		this.family = family;
		this.produitsGeneric = produitsGeneric;
		this.produitParent = produitParent;
	}

	public ProductD(Long id, String barCode, String codePctProd, String lib) {
		super();
		this.id = id;
		this.barCode = barCode;
		this.codePctProd = codePctProd;
		this.lib = lib;
	}

	public Dci getDci() {
		return dci;
	}

	public void setDci(Dci dci) {
		this.dci = dci;
	}

	public DistributionUnit getDistributionUnit() {
		return distributionUnit;
	}

	public void setDistributionUnit(DistributionUnit distributionUnit) {
		this.distributionUnit = distributionUnit;
	}

	public Form getForm() {
		return form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	public Presentation getPresentation() {
		return presentation;
	}

	public void setPresentation(Presentation presentation) {
		this.presentation = presentation;
	}

	public Depot getDepot() {
		return depot;
	}

	public void setDepot(Depot depot) {
		this.depot = depot;
	}

	public PharmaClass getPharmaClass() {
		return pharmaClass;
	}

	public void setPharmaClass(PharmaClass pharmaClass) {
		this.pharmaClass = pharmaClass;
	}
	
	
	
//	@OneToMany(mappedBy="product")
//	@JsonBackReference
//	private List<PrescriptionLine> prescriptionLines ;


	

//
//	public List<PrescriptionLine> getPrescriptionLines() {
//		return prescriptionLines;
//	}
//
//
//
//
//	public void setPrescriptionLines(List<PrescriptionLine> prescriptionLines) {
//		this.prescriptionLines = prescriptionLines;
//	}
//	
	


	
}
