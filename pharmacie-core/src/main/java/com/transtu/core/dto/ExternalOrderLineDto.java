package com.transtu.core.dto;

import javax.validation.constraints.NotNull;

public class ExternalOrderLineDto {

	
	@NotNull
	private Long productId ;
	@NotNull
	private double orderQte = 0;
	private double  price;
	private double vatRate;
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public double getOrderQte() {
		return orderQte;
	}
	public void setOrderQte(double orderQte) {
		this.orderQte = orderQte;
	}
	public ExternalOrderLineDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getVatRate() {
		return vatRate;
	}
	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}
	
	
	
}
