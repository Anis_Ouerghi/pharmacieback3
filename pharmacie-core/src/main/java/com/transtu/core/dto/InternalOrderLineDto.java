package com.transtu.core.dto;


import javax.validation.constraints.NotNull;

public class InternalOrderLineDto {
	
	@NotNull
	private Long productId ;
	@NotNull
	private double orderQte = 0;
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public double getOrderQte() {
		return orderQte;
	}
	public void setOrderQte(double orderQte) {
		this.orderQte = orderQte;
	}
	public InternalOrderLineDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
}
