package com.transtu.core.dto;

import javax.validation.constraints.NotNull;

public class InventoryLineDto {
	
	
	private Long id ;
	@NotNull
	private Long productId ;
	@NotNull
	private double qte = 0;
	private double discard = 0;
	private Long lotId ;
	private String note;
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public double getQte() {
		return qte;
	}
	public void setQte(double qte) {
		this.qte = qte;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public double getDiscard() {
		return discard;
	}
	public void setDiscard(double discard) {
		this.discard = discard;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLotId() {
		return lotId;
	}
	public void setLotId(Long lotId) {
		this.lotId = lotId;
	}
	

}
