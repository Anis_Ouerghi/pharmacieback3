package com.transtu.core.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DistributionLineDto {
	@NotNull
	private double wantedQt ;
	@NotNull
	private double deliveredQt;
	private double missingQt;
	@NotNull
	private Long productId ;
	@NotNull
	private String posologie;
	@NotNull
	private Boolean periodic ; 
	private Date nextDistributionDate;
	private double toDistribute ;
	private Integer distNumber ;
	private double consumesQte;

	
	public DistributionLineDto() {
		super();
		// TODO Auto-generated constructor stub
	}



	public String getPosologie() {
		return posologie;
	}



	public void setPosologie(String posologie) {
		this.posologie = posologie;
	}



	public Boolean getPeriodic() {
		return periodic;
	}



	public void setPeriodic(Boolean periodic) {
		this.periodic = periodic;
	}



	public double getWantedQt() {
		return wantedQt;
	}



	public void setWantedQt(double wantedQt) {
		this.wantedQt = wantedQt;
	}



	public double getDeliveredQt() {
		return deliveredQt;
	}



	public void setDeliveredQt(double deliveredQt) {
		this.deliveredQt = deliveredQt;
	}



	public double getMissingQt() {
		return missingQt;
	}



	public void setMissingQt(double missingQt) {
		this.missingQt = missingQt;
	}



	public Long getProductId() {
		return productId;
	}



	public void setProductId(Long productId) {
		this.productId = productId;
	}



	public boolean isPeriodic() {
		return periodic;
	}



	public void setPeriodic(boolean periodic) {
		this.periodic = periodic;
	}



	public double getToDistribute() {
		return toDistribute;
	}



	public void setToDistribute(double toDistribute) {
		this.toDistribute = toDistribute;
	}



	public Integer getDistNumber() {
		return distNumber;
	}



	public void setDistNumber(Integer distNumber) {
		this.distNumber = distNumber;
	}



	public Date getNextDistributionDate() {
		return nextDistributionDate;
	}



	public void setNextDistributionDate(Date nextDistributionDate) {
		this.nextDistributionDate = nextDistributionDate;
	}



	public DistributionLineDto(double wantedQt, double deliveredQt, double missingQt, Long productId, boolean periodic,
			Date nextDistributionDate, double toDistribute, Integer distNumber) {
		super();
		this.wantedQt = wantedQt;
		this.deliveredQt = deliveredQt;
		this.missingQt = missingQt;
		this.productId = productId;
		this.periodic = periodic;
		this.nextDistributionDate = nextDistributionDate;
		this.toDistribute = toDistribute;
		this.distNumber = distNumber;
	}



	public DistributionLineDto(double wantedQt, double deliveredQt, double missingQt, Long productId, boolean periodic,
			double toDistribute, Integer distNumber) {
		super();
		this.wantedQt = wantedQt;
		this.deliveredQt = deliveredQt;
		this.missingQt = missingQt;
		this.productId = productId;
		this.periodic = periodic;
		this.toDistribute = toDistribute;
		this.distNumber = distNumber;
	}



	public double getConsumesQte() {
		return consumesQte;
	}



	public void setConsumesQte(double consumesQte) {
		this.consumesQte = consumesQte;
	}
	
	
	

}
