package com.transtu.core.dto;

import com.transtu.persistence.entities.Lot;

public class InternalDeliveryLineDto {
	
	private double qte ; 
	private Long productId;
	private Lot lot;
	
	
	public InternalDeliveryLineDto() {
		super();
	}
	public double getQte() {
		return qte;
	}
	public void setQte(double qte) {
		this.qte = qte;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Lot getLot() {
		return lot;
	}
	public void setLot(Lot lot) {
		this.lot = lot;
	}

	
}
