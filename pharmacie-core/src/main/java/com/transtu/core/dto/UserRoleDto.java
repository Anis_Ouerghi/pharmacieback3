package com.transtu.core.dto;

import javax.validation.constraints.NotNull;

public class UserRoleDto {
	
	@NotNull
	private Long roleId ;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public UserRoleDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserRoleDto(Long roleId) {
		super();
		this.roleId = roleId;
	}
	

}
