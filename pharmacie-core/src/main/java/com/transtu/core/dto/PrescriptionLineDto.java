package com.transtu.core.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class PrescriptionLineDto {

	@NotNull
	private Long productId ;
	private String posologie ;
	@NotNull
	private double wantedQt ;
	private Date nextDistributionDate;
	
	
	public String getPosologie() {
		return posologie;
	}

	public void setPosologie(String posologie) {
		this.posologie = posologie;
	}

	public PrescriptionLineDto() {
		super();
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public double getWantedQt() {
		return wantedQt;
	}

	public void setWantedQt(double wantedQt) {
		this.wantedQt = wantedQt;
	}

	public Date getNextDistributionDate() {
		return nextDistributionDate;
	}

	public void setNextDistributionDate(Date nextDistributionDate) {
		this.nextDistributionDate = nextDistributionDate;
	}

	public PrescriptionLineDto(Long productId, double wantedQt, Date nextDistributionDate) {
		super();
		this.productId = productId;
		this.wantedQt = wantedQt;
		this.nextDistributionDate = nextDistributionDate;
	}

}
