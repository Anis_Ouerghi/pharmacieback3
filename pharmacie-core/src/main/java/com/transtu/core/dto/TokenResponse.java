package com.transtu.core.dto;

import com.transtu.persistence.entities.Utilisateur;

public class TokenResponse {

	private final String token;
	public Utilisateur user;
	public TokenResponse(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public Utilisateur getUser() {
		return user;
	}

	public void setUser(Utilisateur user) {
		this.user = user;
	}
	
	

}
