package com.transtu.core.dto;

import com.transtu.persistence.entities.InternalDelivery;

public class StockMovementLineDto {
	
	private Long productId;
	private double movmentQte = 0;
	//private Long stockMovementId;
	private String description;
	private Long motifId;
	private Long lotId = 0l;
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public double getMovmentQte() {
		return movmentQte;
	}
	public void setMovmentQte(double movmentQte) {
		this.movmentQte = movmentQte;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getMotifId() {
		return motifId;
	}
	public void setMotifId(Long motifId) {
		this.motifId = motifId;
	}
	public Long getLotId() {
		return lotId;
	}
	public void setLotId(Long lotId) {
		this.lotId = lotId;
	}

	
	
	
}
