package com.transtu.core.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AuthenticationResponseDTO {

	private static final long serialVersionUID = -5707428629925404312L;
	private String userName;
	private String firstName;
	private String lastName;
	private String mail;
	private long userId;
	private List<String> roles = new ArrayList<>(1);
	@JsonIgnore
	private List<GrantedAuthority> authorities;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public List<String> getRoles() {
		return roles;
	}
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	public List<GrantedAuthority> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
	public void addRole(String role) {
		this.roles.add(role);
	}
	public AuthenticationResponseDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "AuthenticationResponseDTO [userName=" + userName + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", mail=" + mail + ", userId=" + userId + ", roles=" + roles + ", authorities=" + authorities + "]";
	}
	public void addAuthority(GrantedAuthority authority) {
		if (this.authorities == null) {
			this.authorities = new ArrayList();
		}
		this.authorities.add(authority);
	}


}
