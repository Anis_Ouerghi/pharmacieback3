package com.transtu.core.dto;


public class AuthenticationRequestDTO  {

	private static final long serialVersionUID = -2856446877033977821L;
	private String username;
	private String password;
	private String mail;

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public String toString() {
		return "AuthenticationRequestDTO [username=" + username + ", password=" + password + ", mail=" + mail + "]";
	}

}
