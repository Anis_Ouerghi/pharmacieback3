package com.transtu.core.dto;

import java.sql.Date;

import javax.validation.constraints.NotNull;

public class ReceptionLineDto {
	
	
	@NotNull
	private Long productId ;
	@NotNull
	private double receptionQte = 0;
	private double invoiceQte = 0;
	private double price = 0;
	private double vat = 0;
	private String lotNumber;
	private Date expirationDate;
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public double getReceptionQte() {
		return receptionQte;
	}
	public void setReceptionQte(double receptionQte) {
		this.receptionQte = receptionQte;
	}
	public double getInvoiceQte() {
		return invoiceQte;
	}
	public void setInvoiceQte(double invoiceQte) {
		this.invoiceQte = invoiceQte;
	}
	
	public double getVat() {
		return vat;
	}
	public void setVat(double vat) {
		this.vat = vat;
	}
	public ReceptionLineDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getLotNumber() {
		return lotNumber;
	}
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	

	
	
}
